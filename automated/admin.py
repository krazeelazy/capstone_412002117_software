from django.contrib import admin
from .models import (
    AutomatedAnalysis, 
    Image, 
    IaiVehicleDetection,
    IaiVehiclePrediction,
    IaiColourDetection,
    PrVehicleDetection,
    PrVehiclePrediction,
    PrColourDetection,
    LicensePlate,
    Timing
    )

admin.site.register(AutomatedAnalysis)
admin.site.register(Image)
admin.site.register(IaiVehicleDetection)
admin.site.register(IaiVehiclePrediction)
admin.site.register(IaiColourDetection)
admin.site.register(PrVehicleDetection)
admin.site.register(PrVehiclePrediction)
admin.site.register(PrColourDetection)
admin.site.register(LicensePlate)
admin.site.register(Timing)