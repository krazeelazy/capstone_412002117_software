from django.urls import path, include
from .views import EntryListView, EntryDetailView, DayDetailView

urlpatterns = [
    path('',EntryListView.as_view(), name='automated-home'),
    path('day/<int:pk>/',DayDetailView.as_view(), name='automated-day-detail'),
    path('entry/<int:pk>/',EntryDetailView.as_view(), name='automated-entry-detail'),

]