"""Has classes that have functions to gather all the data for the templates.
"""

from django.shortcuts import render, get_object_or_404
from django.views.generic import DetailView, ListView

from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin, RequestConfig

from psa.tables import AutomatedAnalysisTable, DayAutomatedAnalysisTable, EntryTimesTable
from analysis.models import Day, Entry

from psa.filters import AutomatedEntryFilter, DayAutomatedEntryFilter

class EntryListView(SingleTableMixin, FilterView):
    """Gets the data needed by the automated/home.html template. Uses :class:`django_tables2.views.SingleTableMixin` and 
    :class:`django_filters.views.FilterView`. 
    """

    model = Entry
    """(Entry) the model the view uses to get data from the database"""

    table_class = AutomatedAnalysisTable
    """(AutomatedAnalysisTable) the class of the table in :mod:`psa.tables`"""

    template_name = 'automated/home.html'
    """(str) the name of the template the view is passing the data to (automated/home.html)"""

    filterset_class = AutomatedEntryFilter
    """(AutomatedEntryFilter) the class of the filterset in :mod:`psa.filters`"""


    def get_queryset(self):
        """Returns the list of items for this view.

        :return: a list of the entries in the database with automated analyses (sorted in descending order by arrival time) 
                 (should be all of them, this is just a safe guard)  
        :rtype: list  
        """

        return Entry.objects.exclude(automatedanalysis=None).order_by('-arrival_time')

    def get_context_data(self, **kwargs):
        """Adds the page's title to the context data

        :return: context data with the new title info added  
        :rtype: dict  
        """

        context = super(EntryListView, self).get_context_data(**kwargs)
        context['title'] = 'Automated Analysis Home'
        return context

class EntryDetailView(DetailView):
    """Gets the data needed by the automated/entry_detail.html template. Uses :class:`django.views.generic.DetailView`  
    """
    
    model = Entry
    """(Entry) the model the view uses to get data from the database"""

    template_name = 'automated/entry_detail.html'
    """(str) the name of the template the view is passing the data to (automated/entry_detail.html)"""


    def get_context_data(self, **kwargs):
        """Adds the page's title to the context data and performs queries to get the entry data for the template 

        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """

        context = super(EntryDetailView, self).get_context_data(**kwargs)

        entry = context['entry']

        context['title'] = f'{entry.day.date.strftime("%b %d, %Y")}, {entry} Automated Analysis'
        
        iai_auto_colour_names = []
        iai_auto_colour_percentages = {}
        iai_auto_colour_detected_r = {}
        iai_auto_colour_detected_g = {}
        iai_auto_colour_detected_b = {}
        iai_auto_colour_detected_hex = {}
        iai_auto_colour_chart_colours = {}
        for colour in entry.automatedanalysis.iaicolourdetections.order_by('-percentage'):
            iai_auto_colour_names.append(colour.colour.name)
            iai_auto_colour_percentages[colour.colour.name] = float(colour.percentage)
            iai_auto_colour_detected_r[colour.colour.name] = colour.detected_r
            iai_auto_colour_detected_g[colour.colour.name] = colour.detected_g
            iai_auto_colour_detected_b[colour.colour.name] = colour.detected_b
            iai_auto_colour_detected_hex[colour.colour.name] = colour.detected_hex
            iai_auto_colour_chart_colours[colour.colour.name] = f'rgb({colour.colour.r}, {colour.colour.g}, {colour.colour.b})'

        pr_auto_colour_names = []
        pr_auto_colour_percentages = {}
        pr_auto_colour_detected_r = {}
        pr_auto_colour_detected_g = {}
        pr_auto_colour_detected_b = {}
        pr_auto_colour_detected_hex = {}
        pr_auto_colour_chart_colours = {}
        for colour in entry.automatedanalysis.prcolourdetections.order_by('-percentage'):
            pr_auto_colour_names.append(colour.colour.name)
            pr_auto_colour_percentages[colour.colour.name] = float(colour.percentage)
            pr_auto_colour_detected_r[colour.colour.name] = colour.detected_r
            pr_auto_colour_detected_g[colour.colour.name] = colour.detected_g
            pr_auto_colour_detected_b[colour.colour.name] = colour.detected_b
            pr_auto_colour_detected_hex[colour.colour.name] = colour.detected_hex
            pr_auto_colour_chart_colours[colour.colour.name] = f'rgb({colour.colour.r}, {colour.colour.g}, {colour.colour.b})'


        context['iai_auto_colour_names'] = iai_auto_colour_names
        context['iai_auto_colour_percentages'] = iai_auto_colour_percentages
        context['iai_auto_colour_detected_r'] = iai_auto_colour_detected_r
        context['iai_auto_colour_detected_g'] = iai_auto_colour_detected_g
        context['iai_auto_colour_detected_b'] = iai_auto_colour_detected_b
        context['iai_auto_colour_detected_hex'] = iai_auto_colour_detected_hex
        context['iai_auto_colour_chart_colours'] = iai_auto_colour_chart_colours

        context['pr_auto_colour_names'] = pr_auto_colour_names
        context['pr_auto_colour_percentages'] = pr_auto_colour_percentages
        context['pr_auto_colour_detected_r'] = pr_auto_colour_detected_r
        context['pr_auto_colour_detected_g'] = pr_auto_colour_detected_g
        context['pr_auto_colour_detected_b'] = pr_auto_colour_detected_b
        context['pr_auto_colour_detected_hex'] = pr_auto_colour_detected_hex
        context['pr_auto_colour_chart_colours'] = pr_auto_colour_chart_colours

        return context

class DayDetailView(FilterView, ListView):
    """Gets the data needed by the automated/day_detail.html template. Uses :class:`django_filters.views.FilterView` and 
    :class:`django.views.generic.ListView`. 
    """

    model = Entry  
    """(Entry) the model the view uses to get data from the database"""

    template_name = 'automated/day_detail.html'
    """(str) the name of the template the view is passing the data to (automated/day_detail.html)"""

    filterset_class = DayAutomatedEntryFilter
    """(DayAutomatedEntryFilter) the class of the filterset in :mod:`psa.filters`"""


    def get_queryset(self):
        """Returns the list of items for this view.

        :return: a list of the entries for a particular day (sorted in descending order by arrival time)  
        :rtype: list  
        """

        day = get_object_or_404(Day, id=self.kwargs.get('pk'))
        return Entry.objects.filter(day=day).exclude(automatedanalysis=None).order_by('-arrival_time')

    def get_context_data(self, **kwargs):
        """Adds the day object and the page's title to the context data and builds the tables needed by the template using 
        :class:`psa.tables.DayAutomatedAnalysisTable` and :class:`psa.tables.EntryTimesTable`

        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """

        context = super(DayDetailView, self).get_context_data(**kwargs)
        context['day'] = get_object_or_404(Day, id=self.kwargs.get('pk'))
        
        context['title'] = f'{context["day"].date.strftime("%b %d, %Y")} Automated Analysis'

        tables = [
            DayAutomatedAnalysisTable(context['object_list'], prefix="1-"),
            EntryTimesTable(context['object_list'], prefix="2-")
        ]

        #execute requestConfig for each table
        for table in tables:
            RequestConfig(self.request).configure(table)

        context['tables'] = list(tables)
        
        return context
