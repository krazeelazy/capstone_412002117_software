"""Has the models for the automated app.
"""

from django.db import models
from analysis.models import Entry

class AutomatedAnalysis(models.Model):
    """Class representing an automated analysis (has the basic info for an automated analysis). Subclass of :class:`django.db.models.Model`.  
    """

    
    entry = models.OneToOneField(Entry,on_delete=models.CASCADE, related_name='automatedanalysis')
    """(OneToOneField) the automated analysis' entry \(:class:`analysis.models.Entry`\)"""

    duration = models.DurationField()
    """(DurationField) the automatically calculated duration"""

    successful_feed_in = models.BooleanField(blank=True, null=True,help_text='Was iai extracted image successfully fed into pr analysis (image with imageai_pr in name produced)?')
    """(BooleanField) whether or not the image extrcaten during Imageai analysis was successfully fed into the Plate Recognizer analysis"""


    class Meta:
        """Adds metadata about the AutomatedAnalysis model class (used in admin section) 
        """

        verbose_name = 'automated analysis'
        """(str) the name of the model class"""
        verbose_name_plural = 'automated analyses'
        """(str) the plural version of the name of the model class"""

    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (arrival time of the entry) 
                 (format = '%I:%M:%S %p' = 
                 Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        :rtype: str
        """

        return self.entry.arrival_time.strftime('%I:%M:%S %p')


class Image(models.Model):
    """Class representing an image. Subclass of :class:`django.db.models.Model`.
    """

    image = models.ImageField(default='default.jpg', max_length=250)
    """(ImageField) the image, defaults to 'default.jpg'"""

    type = models.ForeignKey('psa.ImageType',on_delete=models.CASCADE)
    """(ForeignKey) the image's type \(:class:`psa.models.ImageType`\)"""

    entry = models.ForeignKey(Entry,on_delete=models.CASCADE, related_name='images')
    """(ForeignKey) the entry associated with the image \(:class:`analysis.models.Entry`\)"""


    class Meta:
        """Adds metadata about the Image model class (used in admin section) 
        """        
        verbose_name = 'image'
        """(str) the name of the model class"""

        verbose_name_plural = 'images'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (image's url)  
        :rtype: str
        """

        return self.image.url


class IaiVehicleDetection(models.Model):
    """Class representing an Imageai Vehicle Detection. Subclass of :class:`django.db.models.Model`.  
    """

    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='iaivehicledetection')
    """(OneToOneField) the automated analysis associated with the Imageai Vehicle Detection \(:class:`automated.models.AutomatedAnalysis`\)"""

    type = models.ForeignKey('psa.IaiDetectionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Imageai Vehicle Detection class detected \(:class:`psa.models.IaiDetectionClass`\)"""

    probability = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    """(DecimalField) the probability that the correct Imageai Vehicle Detection class was detected, defaults to 0.00"""


    class Meta:
        """Adds metadata about the IaiVehicleDetection model class (used in admin section)
        """ 

        verbose_name = 'imageai vehicle detection'
        """(str) the name of the model class"""

        verbose_name_plural = 'imageai vehicle detections'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (Imageai Detection class' name)  
        :rtype: str
        """

        return self.type.name

class IaiVehiclePrediction(models.Model):
    """Class representing an Imageai Vehicle Prediction. Subclass of :class:`django.db.models.Model`.  
    """
        
    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='iaivehicleprediction')
    """(OneToOneField) the automated analysis associated with the Imageai Vehicle Prediction \(:class:`automated.models.AutomatedAnalysis`\)"""

    type = models.ForeignKey('psa.PredictionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Prediction class identified during the Imageai analysis \(:class:`psa.models.PredictionClass`\)"""

    probability = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    """(DecimalField) the probability that the correct Prediction class was identified during the Imageai analysis, defaults to 0.00"""


    class Meta:
        """Adds metadata about the IaiVehiclePrediction model class (used in admin section)
        """ 

        verbose_name = 'imageai vehicle prediction'
        """(str) the name of the model class"""

        verbose_name_plural = 'imageai vehicle predictions'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (name of prediction class identified during the Imageai analysis)  
        :rtype: str
        """

        return self.type.name

class IaiColourDetection(models.Model):
    """Class representing an Imageai Colour Detection. Subclass of :class:`django.db.models.Model`.  
    """

    automated_analysis = models.ForeignKey(AutomatedAnalysis, on_delete=models.CASCADE, related_name='iaicolourdetections')
    """(ForeignKey) the automated analysis associated with the Imageai Colour Detection \(:class:`automated.models.AutomatedAnalysis`\)"""

    colour = models.ForeignKey('psa.Colour', on_delete=models.PROTECT)
    """(ForeignKey) the colour in the database that's closest to the detected colour \(:class:`psa.models.Colour`\)"""

    percentage = models.DecimalField(max_digits=29, decimal_places=26, default=100)
    """(DecimalField) the percentage of the image the colour takes up, defaults to 100"""

    detected_r = models.IntegerField()
    """(IntegerField) the detected red value of the colour"""

    detected_g = models.IntegerField()
    """(IntegerField) the detected green value of the colour"""

    detected_b = models.IntegerField()
    """(IntegerField) the detected blue value of the colour"""

    detected_hex = models.CharField(max_length=7)
    """(CharField) the detected hex value of the colour"""


    class Meta:
        """Adds metadata about the IaiColourDetection model class (used in admin section)
        """ 
        
        verbose_name = 'imageai colour detection'
        """(str) the name of the model class"""

        verbose_name_plural = 'imageai colour detections'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (colour's name)  
        :rtype: str
        """

        return self.colour.name


class PrVehicleDetection(models.Model):
    """Class representing a Pr Vehicle Detection. Subclass of :class:`django.db.models.Model`.   
    """

    
    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='prvehicledetection')
    """(OneToOneField) the automated analysis associated with the Pr Vehicle Detection \(:class:`automated.models.AutomatedAnalysis`\)"""

    type = models.ForeignKey('psa.PrDetectionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Pr Vehicle Detection class detected \(:class:`psa.models.PrDetectionClass`\)"""

    probability = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    """(DecimalField) the probability that the correct Pr Vehicle Detection class was detected, defaults to 0.00"""


    class Meta:
        """Adds metadata about the PRVehicleDetection model class (used in admin section)
        """ 

        
        verbose_name = 'plate recognizer vehicle detection'
        """(str) the name of the model class"""

        verbose_name_plural = 'plate recognizer vehicle detections'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (PR Detection class' name)  
        :rtype: str
        """
        
        return self.type.name

class PrVehiclePrediction(models.Model):
    """Class representing a PR Vehicle Prediction. Subclass of :class:`django.db.models.Model`.  
    """
        
    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='prvehicleprediction')
    """(OneToOneField) the automated analysis associated with the PR Vehicle Prediction \(:class:`automated.models.AutomatedAnalysis`\)"""

    type = models.ForeignKey('psa.PredictionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Prediction class identified during the PR analysis \(:class:`psa.models.PredictionClass`\)"""

    probability = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    """(DecimalField) the probability that the correct Prediction class was identified during the PR analysis, defaults to 0.00"""


    class Meta:
        """Adds metadata about the PRVehiclePrediction model class (used in admin section)
        """ 

        
        verbose_name = 'plate recognizer vehicle prediction'
        """(str) the name of the model class"""

        verbose_name_plural = 'plate recognizer vehicle predictions'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (name of prediction class identified during the PR analysis)  
        :rtype: str
        """

        return self.type.name

class PrColourDetection(models.Model):
    """Class representing a PR Colour Detection. Subclass of :class:`django.db.models.Model`. 
    """

    automated_analysis = models.ForeignKey(AutomatedAnalysis, on_delete=models.CASCADE, related_name='prcolourdetections')
    """(ForeignKey) the automated analysis associated with the PR Colour Detection \(:class:`automated.models.AutomatedAnalysis`\)"""

    colour = models.ForeignKey('psa.Colour', on_delete=models.PROTECT)
    """(ForeignKey) the colour in the database that's closest to the detected colour \(:class:`psa.models.Colour`\)"""

    percentage = models.DecimalField(max_digits=29, decimal_places=26, default=100)
    """(DecimalField) the percentage of the image the colour takes up, defaults to 100"""

    detected_r = models.IntegerField()
    """(IntegerField) the detected red value of the colour"""

    detected_g = models.IntegerField()
    """(IntegerField) the detected green value of the colour"""

    detected_b = models.IntegerField()
    """(IntegerField) the detected blue value of the colour"""

    detected_hex = models.CharField(max_length=7)
    """(CharField) the detected hex value of the colour"""


    class Meta:
        """Adds metadata about the PrColourDetection model class (used in admin section)
        """ 
        
        verbose_name = 'plate recognizer colour detection'
        """(str) the name of the model class"""

        verbose_name_plural = 'plate recognizer colour detections'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (colour's name)  
        :rtype: str
        """

        return self.colour.name

class LicensePlate(models.Model):
    """Class representing a License PLate. Subclass of :class:`django.db.models.Model`.  
    """

    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='licenseplate')
    """(OneToOneField) the automated analysis associated with the License PLate \(:class:`automated.models.AutomatedAnalysis`\)"""

    number = models.CharField(max_length=8)
    """(CharField) the license plate number"""

    probability = models.DecimalField(max_digits=5, decimal_places=2, default=0.00)
    """(DecimalField) the probability that the correct license plate number was detected, defaults to 0.00"""


    class Meta:
        """Adds metadata about the LicensePlate model class (used in admin section)
        """ 

        verbose_name = 'license plate'
        """(str) the name of the model class"""

        verbose_name_plural = 'license plates'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (license plate's number)  
        :rtype: str
        """
        
        return self.number


class Timing(models.Model):
    """Class representing an automated analysis' entry's timing. Subclass of :class:`django.db.models.Model`.  
    """

    
    automated_analysis = models.OneToOneField(AutomatedAnalysis, on_delete=models.CASCADE, related_name='timing')
    """(OneToOneField) the automated analysis associated with the Timing \(:class:`automated.models.AutomatedAnalysis`\)"""

    get_duration_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.get_duration` function"""

    imageai_detection_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.imageai_detect` function"""

    imageai_prediction_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the Imageai extracted image"""

    imageai_colour_detection_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.detect_colours` function  when called with the Imageai extracted image"""

    total_imageai_analysis_time = models.DurationField()
    """(DurationField) time taken for Imageai analysis"""

    pr_detection_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.plate_recognizer_detect` function"""

    pr_prediction_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the  plate recognizer extracted image"""

    pr_colour_detection_time = models.DurationField()
    """(DurationField) time taken to run the :func:`spot_analysis.detect_colours` function  when called with the  plate recognizer extracted image"""

    total_pr_analysis_time = models.DurationField()
    """(DurationField) time taken for Plate Recognizer analysis"""

    total_image_analysis_time = models.DurationField()
    """(DurationField) total time to analyze the image"""

    entry_total_time = models.DurationField()
    """(DurationField) total time to analyze the entry"""


    class Meta:
        """Adds metadata about the Timing model class (used in admin section)
        """ 

        verbose_name = 'timing'
        """(str) the name of the model class"""

        verbose_name_plural = 'timings'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (entry's total time)  
        :rtype: str
        """
        
        return str(self.entry_total_time)