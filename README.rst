capstone\_412002117\_software
=============================

capstone\_412002117\_software is the software component of the capstone project by Mellissa Marie (412002117).

Background
----------

Purpose
~~~~~~~
This is the software component of my capstone project (Parking Spot Analysis - PSA).  

According to the “Green Economy Scoping Study Barbados” published by the United Nations Environment Programme, congestion within Bridgetown is caused 
by on-street parking due to limited off-street parking facilities. They suggest that a system of on-street parking charges and regularization of 
length-of-stay in authorized parking areas should be considered as measures to address the problem. This project focuses on the 
length-of-stay and authorized parking areas part of this.  

At a meeting with personnel at the Barbados Ministry of Transport and Works to try to narrow down the scope of this project from the broad topic 
of general parking to something that they have a need for and would be useful to them, several things were brought up. 
First, there isn’t a system to centralize the control or regulation (of prices) of the current off-street parking structures and facilities. 
Second, they need research to be done to analyse the usage of delivery spots (parking spots designated for use by delivery trucks). 
More specifically, what vehicles are parking in the spots (delivery trucks or private vehicles) and how long the vehicles are parking in the spots.

Currently, if they want to conduct this type of research, it has to be done manually. Where a person either goes to the area and manually monitors the
metrics they are trying to measure or a person sits and watches recorded video to get the data.  

The goal of this project is to automate the data capture and make it easier to get useful information from the data captured. This part of the project, 
software component, deals with the analysis and presentation of the data collected by the hardware component that does the automated monitoring of a 
parking spot.


Overview
--------

Analysis
~~~~~~~~

This is done by the spot_analysis.py script. It takes the arrival and departure times captured by the hardware section to determine the duration 
of stay. The image captured is processed to determine things such as vehicle type, colour and license plate number. The time it takes to process the data 
is also noted. All the information produced is stored in a JSON file that is fed into the final part of the solution.


Presentation
~~~~~~~~~~~~

This section is a Django web application. It serves multiple purposes/ has multiple subsections:

1) provides a user-friendly interface to interact with the spot_analysis.py script to produce automated analyses
2) displays the raw data produced by the automated analyses
3) makes the raw automated data searchable (e.g. you can search for partial/full plate numbers, vehicle types, etc.)
4) allows you to create manual analyses (that are used to test the accuracy of the automated ones)
5) allows you to view and search the manual analyses
6) shows relevant statistics. This section shows raw counts of number of entries, how many of the different vehicle types, colours, etc were detected by 
   the automated and manual analyses. It also compares the automated and manual analyses to show the correct counts and accuracies of the automated detections. 
   These statistics are broken down by day but are also summaries to show overall correct counts and accuracies.  


Development Environment
-----------------------

| OS: Ubuntu 18.04.4 LTS running in VirtualBox 6.0.18 on Windows 10  
| RAM: 11410 MB  
| CPU: Intel® Core™ i7-6700HQ CPU @ 2.60GHz × 4   

(RAM and CPU affect the analysis times)



Requirements
------------

Python version
~~~~~~~~~~~~~~

Python 3.7

Python packages used
~~~~~~~~~~~~~~~~~~~~

| datetime
| Django
| Django-crispy-forms
| Django-extensions
| Django-filter
| Django-tables2
| imageai
| imutils
| json
| keras
| logging
| math
| numpy
| opencv-contrib-python (cv2)
| os
| os.path
| pandas
| Pillow
| random
| requests
| scikit_learn
| shutil
| tensorflow
| time

(sphinx was used for documentation)

Additional libraries used (via cdn)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

| Bootstrap (4.4.1)
| Chart.js (2.9.3)
| Font Awesome (5.13.0)
| jQuery (3.4.1)
| Popper (1.16.0)
| Select2 (4.0.13)


Usage
-----


Installation and Setup
~~~~~~~~~~~~~~~~~~~~~~

.. note:: Steps 2 - 4 are optional. They only need to be done if you're going to be running analyses. If you're going to use the analyses that're already in the database only steps 1 and 5 are needed.

1) Download the project from `Bitbucket <https://bitbucket.org/krazeelazy/capstone_412002117_software/src/master/>`_

.. warning:: Some of the file names for the images used in the documentation are long, so you may get an error if you try to download the project on a Windows system.

2) `Sign up <https://app.platerecognizer.com/accounts/signup/>`_ for a plate recognizer account
3) Go to your `account <https://app.platerecognizer.com/accounts/plan/>`_ to get your Cloud API Plan Token
4) Paste your token as the value for the CLOUD_API_TOKEN constant in spot_analysis.py (Line 56) 
5) Use the requirements.txt file to install the correct versions of the required packages

Admin User (to access admin section)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1) Create a new user from the terminal:
    a) open a terminal
    b) navigate to the capstone_412002117_software directory
    c) run: 
        
        ::

            python3 manage.py createsuperuser

OR

2) You can use the admin superuser:
    | username: admin
    | password: admin

Admin section accessed via: http://localhost:8000/admin/


Running the server
~~~~~~~~~~~~~~~~~~

1) Either:
    a) right click on project folder and click open in terminal or      
    b) open a terminal and navigate to the capstone_412002117_software directory   

2) type in terminal:

    ::

	    python3 manage.py runserver

3) open browser and go to:
	a) http://127.0.0.1:8000/ or	 
	b) http://localhost:8000/ 


Stopping the server
~~~~~~~~~~~~~~~~~~~

1) Open the terminal for the running server   

2) Press control + C



Starting from scratch
~~~~~~~~~~~~~~~~~~~~~

These are the steps if you want to remove the data that's currently in the database and start over (after doing the steps in `Installation and Setup`_ (you must follow all steps)) :

1) Clear data:
    a) Follow the steps in `Running the server`_ to run the server
    b) Go to the admin section of the site (http://localhost:8000/admin/) in a browser
    c) Go to the Days section
    d) Select all the Days
    e) Click the dropdown at the top and select delete selection, to delete all the data for the days, including the files, images and data stored in the database
    f) Delete the db.sqlite3 file from the main directory

2) Run the migrations using the following in the terminal (while in the capstone_412002117_software directory):
    
    ::

        python3 manage.py migrate

3) make a superuser so you can use it in the admin section:
    
    ::

        python3 manage.py createsuperuser

4) Repopulate the psa tables:
    a) Start the server using:

        ::

	    python3 manage.py runserver
    b) Go back to the main site in a browser (http://localhost:8000/)
    c) Click on PSA -> Populate Tables in the main navigation or go to (http://localhost:8000/populate_tables)
    d) Either go through one by one and click the action button next to each table name to populate the tables or click the button at the top to populate all tables

5) Put your data that you want to analyze in data/new subdir (you can produce your own data using `this template <features_analysis.html#get-data-from-the-json-file>`_  or use the 
   `input data provided here <https://drive.google.com/drive/folders/1si5IDD8KKxpMnVctYS86tf9mY9ZARro6?usp=sharing>`_)

6) Go back to the homepage in the browser

7) Run the automated analyses (`help using the home page <features_presentation.html#home-page>`_) 
   (completed `automated anlyses can be found here <https://drive.google.com/drive/folders/1BkNC0n-fR2qPGVeGN4fJix7CG8j8ZfyQ?usp=sharing>`_ 
   and placed in data/analyzed dir instead)

.. warning:: If using the provided data:
    
    1) March 18, 19, 24-30 use regular automated analyses (no feed in)
    2) March 19 needs to use the rotated analysis
    3) April 1-9 use feed in analysis
    4) April 2 needs to use the rotated feed in analyse


8) Make manual analyses for the entries (`help with the manual analyses <features_presentation.html#manual>`_)

9) Once the manual analyses are done you can view the statistics in the statistics section


Useful Links
------------

1) Hardware component:

    a) Code on `Bitbucket <https://bitbucket.org/krazeelazy/capstone_412002117_hardware/src/master/>`_
    b) Docs on `Read the Docs <https://capstone-412002117-hardware.readthedocs.io/en/latest/index.html>`_

2) Software component:

    a) Code on `Bitbucket <https://bitbucket.org/krazeelazy/capstone_412002117_software/src/master/>`_
    b) Docs on `Read the Docs <https://capstone-412002117-software.readthedocs.io/en/latest/index.html>`_