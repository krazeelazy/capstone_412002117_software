"""Has the models for the analysis app.
"""

from django.db import models
from django.urls import reverse

class Day(models.Model):
    """Class representing a day. Subclass of :class:`django.db.models.Model`. 
    """

    date = models.DateField()
    """(DateField) the day's date"""

    tried_feed_in = models.BooleanField(default=False, help_text='Tried to feed iai extracted image into pr analysis?')
    """(BooleanField) whether or not an attempt was made to feed the images extracted during the Imageai analysis into the Plate Recognizer analysis, defaults to False"""

    json_file = models.FileField( max_length=250, blank=True, null=True)
    """(FileField) the JSON file associated with the day"""
    
    log_file = models.FileField(max_length=250, blank=True, null=True)
    """(FileField) the log file associated with the day"""

    class Meta:
        """Adds metadata about the Day model class (used in admin section)
        """

        verbose_name = 'day'
        """(str) the name of the model class"""

        verbose_name_plural = 'days'
        """(str) the plural version of the name of the model class"""
        

    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object 
                 (format = '%b %d, %Y' = abbreviated month: zero padded decimal day of the month: year with century as a decimal number)  
        :rtype: str
        """

        return self.date.strftime('%b %d, %Y')

    def get_absolute_url(self):
        """ Tells Django how to calculate the canonical URL for an object

        :return: the url to the analysis day detail for the particular day  
        :rtype: str  
        """

        return reverse('analysis-day-detail', kwargs={'pk': self.pk})

class Entry(models.Model):
    """Class representing a day's entry. Subclass of :class:`django.db.models.Model`.  
    """

    arrival_time = models.DateTimeField()
    """(DateField) the entry's arrival time"""

    departure_time = models.DateTimeField()
    """(DateTimeField) the entry's departure time"""

    status = models.CharField(max_length=30, default='needs automated analysis')
    """(CharField) the entry's status, defaults to 'needs automated analysis', updated by signals"""

    day = models.ForeignKey(Day,on_delete=models.CASCADE, related_name='entries')
    """(ForeignKey) the entry's day \(:class:`analysis.models.Day`\)"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object  (arrival time of the entry) 
                 (format = '%I:%M:%S %p' = 
                 Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        :rtype: str
        """

        return self.arrival_time.strftime('%I:%M:%S %p')

    def get_absolute_url(self):
        """ Tells Django how to calculate the canonical URL for an object

        :return: the url to the analysis entry detail for the particular entry  
        :rtype: str  
        """

        return reverse('analysis-entry-detail', kwargs={'pk': self.pk})

class DayTiming(models.Model):
    """Class representing a day's timing. Subclass of :class:`django.db.models.Model`.  
    """

    day = models.OneToOneField(Day,on_delete=models.CASCADE, related_name='daytiming')
    """(OneToOneField) the day associated with the timing data  \(:class:`analysis.models.Day`\)"""

    total_get_duration_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.get_duration` function"""

    total_imageai_detection_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.imageai_detect` function"""

    total_imageai_prediction_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the Imageai extracted images"""

    total_imageai_colour_detection_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.detect_colours` function  when called with the Imageai extracted images"""

    total_imageai_analysis_time = models.DurationField()
    """(DurationField) total time taken for Imageai analysis"""

    total_pr_detection_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.plate_recognizer_detect` function"""

    total_pr_prediction_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the  plate recognizer extracted images"""

    total_pr_colour_detection_time = models.DurationField()
    """(DurationField) total time taken to run the :func:`spot_analysis.detect_colours` function  when called with the  plate recognizer extracted image"""

    total_pr_analysis_time = models.DurationField()
    """(DurationField) total time taken for Plate Recognizer analysis"""

    total_image_analysis_time = models.DurationField()
    """(DurationField) total time to analyze all the images"""

    total_entry_time = models.DurationField()
    """(DurationField) total time to analyze all the entries"""

    total_day_time = models.DurationField()
    """(DurationField) total time to process the entire day (analyze entries, setup and move files, etc.)"""


    class Meta:
        """Adds metadata about the DayTiming model class (used in admin section)
        """

        verbose_name = 'day timing'
        """(str) the name of the model class"""

        verbose_name_plural = 'day timings'
        """(str) the plural version of the name of the model class"""


    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (the total day time value)  
        :rtype: str  
        """

        return str(self.total_day_time)