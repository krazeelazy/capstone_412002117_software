from django.urls import path, include
from .views import EntryListView, DayDetailView, EntryDetailView

urlpatterns = [
    path('',EntryListView.as_view(), name='analysis-home'),
    path('day/<int:pk>/',DayDetailView.as_view(), name='analysis-day-detail'),
    path('entry/<int:pk>/',EntryDetailView.as_view(), name='analysis-entry-detail'),
    path('automated/', include('automated.urls')),
    path('manual/', include('manual.urls'))
]