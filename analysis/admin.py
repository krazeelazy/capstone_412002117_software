from django.contrib import admin
from .models import Day, Entry, DayTiming

admin.site.register(Day)
admin.site.register(Entry)
admin.site.register(DayTiming)