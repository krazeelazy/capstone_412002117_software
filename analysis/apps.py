"""Has the configuration for the analysis app (sets the name and imports the signals from the psa app)
"""

from django.apps import AppConfig


class AnalysisConfig(AppConfig):
    """Class representing the analysis application and its configuration. Subclass of :class:`django.apps.AppConfig`.
    """
    name = 'analysis'
    """ (str) The name of the application
    """

    def ready(self):
        """Imports the signals file from the psa app when Django starts
        """

        import psa.signals
