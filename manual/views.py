"""Has classes that have functions to gather all the data for the templates and to handle incoming data from the creation and update forms.
"""

from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView, CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin

from django_filters.views import FilterView
from django_tables2.views import SingleTableMixin, RequestConfig

from analysis.models import Day, Entry
from manual.models import ManualAnalysis, GetManualContext

from psa.tables import ManualAnalysisTable, DayManualAnalysisTable, EntryTimesTable
from psa.filters import ManualEntryFilter, DayManualEntryFilter

class EntryListView(SingleTableMixin, FilterView):
    """Gets the data needed by the 'manual/home.html template. Uses :class:`django_tables2.views.SingleTableMixin` and 
    :class:`django_filters.views.FilterView`. 
    """

    model = Entry
    """(Entry) the model the view uses to get data from the database"""

    table_class = ManualAnalysisTable
    """(ManualAnalysisTable) the class of the table in :mod:`psa.tables`"""

    template_name = 'manual/home.html'
    """(str) the name of the template the view is passing the data to ('manual/home.html)"""

    filterset_class = ManualEntryFilter
    """(ManualEntryFilter) the class of the filterset in :mod:`psa.filters`"""


    def get_queryset(self):
        """Returns the list of items for this view.

        :return: a list of the entries in the database with manual analyses (sorted in descending order by arrival time)  
        :rtype: list  
        """

        return Entry.objects.exclude(manualanalysis=None).order_by('-arrival_time')

    def get_context_data(self, **kwargs):
        """Adds the page's title to the context data

        :return: context data with the new title info added  
        :rtype: dict  
        """

        context = super(EntryListView, self).get_context_data(**kwargs)
        context['title'] = 'Manual Analysis Home'
        return context

class EntryDetailView(DetailView):
    """Gets the data needed by the manual/entry_detail.html template. Uses :class:`django.views.generic.DetailView`
    """
    
    model = Entry
    """(Entry) the model the view uses to get data from the database"""

    template_name = 'manual/entry_detail.html'
    """(str) the name of the template the view is passing the data to (manual/entry_detail.html)"""


    def get_context_data(self, **kwargs):
        """Adds the page's title to the context data

        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """

        context = super(EntryDetailView, self).get_context_data(**kwargs)

        context['title'] = f'{context["entry"].day.date.strftime("%b %d, %Y")}, {context["entry"]} Manual Analysis'

        return context

class DayDetailView(FilterView, ListView):
    """Gets the data needed by the manual/day_detail.html template. Uses :class:`django_filters.views.FilterView` and 
    :class:`django.views.generic.ListView`.  
    """

    model = Entry  
    """(Entry) the model the view uses to get data from the database"""

    template_name = 'manual/day_detail.html'
    """(str) the name of the template the view is passing the data to (manual/day_detail.html)"""

    filterset_class = DayManualEntryFilter
    """(DayManualEntryFilter) the class of the filterset in :mod:`psa.filters`"""


    def get_queryset(self):
        """Returns the list of items for this view.

        :return: a list of the entries with manual analyses for a particular day (sorted in descending order by arrival time)  
        :rtype: list  
        """

        day = get_object_or_404(Day, id=self.kwargs.get('pk'))
        return Entry.objects.filter(day=day).exclude(manualanalysis=None).order_by('-arrival_time')

    def get_context_data(self, **kwargs):
        """Adds the day object and the page's title to the context data and builds the tables needed by the template using 
        :class:`psa.tables.DayManualAnalysisTable` and :class:`psa.tables.EntryTimesTable`

        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """

        context = super(DayDetailView, self).get_context_data(**kwargs)
        context['day'] = get_object_or_404(Day, id=self.kwargs.get('pk'))

        context['title'] = f'{context["day"].date.strftime("%b %d, %Y")} Manual Analysis'

        tables = [
            DayManualAnalysisTable(context['object_list'], prefix="1-"),
            EntryTimesTable(context['object_list'], prefix="2-")
        ]

        #execute requestConfig for each table
        for table in tables:
            RequestConfig(self.request).configure(table)

        context['tables'] = list(tables)

        return context

class ManualAnalysisCreateView(SuccessMessageMixin, CreateView):
    """Gets the data needed by the manualanalysis_form.html template and handles the incoming data to make a new manual analysis. 
    Uses :class:`django.contrib.messages.views.SuccessMessageMixin` and :class:`django.views.generic.CreateView`.   
    """

    model = ManualAnalysis
    """(ManualAnalysis) the model the view uses to interact with the database"""

    fields = ['duration', 'iai_detection_class', 'pr_detection_class', 'prediction_class', 'license_plate_number',
            'dominant_colour', 'dominant_colour_detected_by_iai', 'dominant_colour_identified_as_dominant_by_iai', 
            'dominant_colour_detected_by_pr', 'dominant_colour_identified_as_dominant_by_pr', 'image_angle', 
            'iai_extract_correct', 'iai_extract_entire', 'pr_extract_correct', 'pr_extract_entire', 'plate_extract_correct', 'plate_extract_entire']
    """(list) a list of fields in the template form"""


    success_message = "Manual Analysis successfully created!"
    """(str) the message displayed if the manual analysis is successfully created"""



    def get_context_data(self, **kwargs): 
        """Calls the :meth:`manual.models.GetManualContext.get_manual_context` method to get all the data needed by the template and adds it to 
        the context dictionary

        :return: a dictionary containing all the context data for the template
        :rtype: dict
        """
        
        context = super(ManualAnalysisCreateView, self).get_context_data(**kwargs)
        entry = get_object_or_404(Entry, id=self.kwargs.get('pk'))

        for (key, value) in GetManualContext.get_manual_context(entry).items():
            context[key] = value

        return context

    def form_valid(self, form):
        """Convert the license plate number to upper case, make sure the entry is a valid one and call the parent's :meth:`form_valid` method to validate 
        the rest of the form and save the associated model if it's valid, then redirect to the entry detail page with the success message

        :param form: the form being processed  
        :type form: tuple
        """

        form.instance.license_plate_number = form.instance.license_plate_number.upper()
        form.instance.entry = get_object_or_404(Entry, id=self.kwargs.get('pk'))
        return super().form_valid(form)

class ManualAnalysisUpdateView(SuccessMessageMixin, UpdateView):
    """Gets the data needed by the manualanalysis_form.html template and handles the incoming data toupdate an existing manual analysis. 
    Uses :class:`django.contrib.messages.views.SuccessMessageMixin` and :class:`django.views.generic.UpdateView`.   
    """

    model = ManualAnalysis
    """(ManualAnalysis) the model the view uses to interact with the database"""

    fields = ['duration', 'iai_detection_class', 'pr_detection_class', 'prediction_class', 'license_plate_number',
            'dominant_colour', 'dominant_colour_detected_by_iai', 'dominant_colour_identified_as_dominant_by_iai', 
            'dominant_colour_detected_by_pr', 'dominant_colour_identified_as_dominant_by_pr', 'image_angle', 
            'iai_extract_correct', 'iai_extract_entire', 'pr_extract_correct', 'pr_extract_entire', 'plate_extract_correct', 'plate_extract_entire']
    """(list) a list of fields in the template form"""


    success_message = "Manual Analysis successfully updated!"
    """(str) the message displayed if the manual analysis is successfully updated"""



    def get_context_data(self, **kwargs):
        """Calls the :meth:`manual.models.GetManualContext.get_manual_context` method to get all the data needed by the template and adds it to 
        the context dictionary

        :return: a dictionary containing all the context data for the template
        :rtype: dict
        """
        
        context = super(ManualAnalysisUpdateView, self).get_context_data(**kwargs)
        entry = get_object_or_404(Entry, id=self.kwargs.get('pk'))

        for (key, value) in GetManualContext.get_manual_context(entry).items():
            context[key] = value

        return context

    def form_valid(self, form):
        """Convert the license plate number to upper case, make sure the entry is a valid one and call the parent's :meth:`form_valid` method to validate 
        the rest of the form and save the associated model if it's valid, then redirect to the entry detail page with the success message

        :param form: the form being processed  
        :type form: tuple
        """
        
        form.instance.license_plate_number = form.instance.license_plate_number.upper()
        form.instance.entry = get_object_or_404(Entry, id=self.kwargs.get('pk'))
        return super().form_valid(form)
