from django.urls import path, include
from .views import EntryListView, EntryDetailView, DayDetailView, ManualAnalysisCreateView, ManualAnalysisUpdateView

urlpatterns = [
    path('',EntryListView.as_view(), name='manual-home'),
    path('day/<int:pk>/',DayDetailView.as_view(), name='manual-day-detail'),
    path('entry/<int:pk>/',EntryDetailView.as_view(), name='manual-entry-detail'),
    path('new/<int:pk>/', ManualAnalysisCreateView.as_view(), name='manual-create'),
    path('entry/<int:pk>/update/', ManualAnalysisUpdateView.as_view(), name='manual-update'),
]