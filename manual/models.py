"""Has the model for the manual app (ManualAnalysis) and a class that has a function to get the data the view needs to pass to the form in the 
manualanalysis_form.html template.
"""

from django.db import models
from django.urls import reverse
from analysis.models import Entry

class ManualAnalysis(models.Model):
    """Class representing a manual analysis (has all the info for a manual analysis). Subclass of :class:`django.db.models.Model`.
    """

    entry = models.OneToOneField(Entry,on_delete=models.CASCADE, related_name='manualanalysis')
    """(OneToOneField) the manual analysis' entry \(:class:`analysis.models.Entry`\)"""
    
    duration = models.DurationField()
    """(DurationField) the manually calculated duration"""
    
    iai_detection_class = models.ForeignKey('psa.IaiDetectionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Imageai Vehicle Detection class identified \(:class:`psa.models.IaiDetectionClass`\)"""
    
    pr_detection_class = models.ForeignKey('psa.PrDetectionClass', on_delete=models.PROTECT)
    """(ForeignKey) the PR Vehicle Detection class identified \(:class:`psa.models.PrDetectionClass`\)"""
    
    prediction_class = models.ForeignKey('psa.PredictionClass', on_delete=models.PROTECT)
    """(ForeignKey) the Prediction class identified \(:class:`psa.models.PredictionClass`\)"""
    
    license_plate_number = models.CharField(max_length=8)
    """(CharField) The license plate number"""
    
    dominant_colour = models.ForeignKey('psa.Colour', on_delete=models.PROTECT)
    """(ForeignKey) the dominant colour of the vehicle \(:class:`psa.models.Colour`\)"""
    
    dominant_colour_detected_by_iai = models.BooleanField(default=False, help_text='Was the dominant colour detected by Imageai analysis?')
    """(BooleanField) whether or not the dominant colour was detected during Imageai analysis, deafults to False"""
    
    dominant_colour_identified_as_dominant_by_iai = models.BooleanField(blank=True, null=True,help_text='Was the dominant colour identified as the dominant colour by Imageai analysis?')
    """(BooleanField) whether or not the dominant colour was identified as dominant during Imageai analysis"""
    
    dominant_colour_detected_by_pr = models.BooleanField(default=False, help_text='Was the dominant colour detected by Plate Recognizer analysis?')
    """(BooleanField) whether or not the dominant colour was detected during PR analysis, deafults to False"""
    
    dominant_colour_identified_as_dominant_by_pr = models.BooleanField(blank=True, null=True,help_text='Was the dominant colour identified as the dominant colour by Plate Recognizer analysis?')
    """(BooleanField) whether or not the dominant colour was identified as dominant during PR analysis"""
    
    image_angle = models.ForeignKey('psa.ImageAngle',on_delete=models.CASCADE)
    """(ForeignKey) the angle of the object in the image \(:class:`psa.models.ImageAngle`\)"""
    
    iai_extract_correct = models.BooleanField(blank=True, null=True,help_text='Did IAI extract the correct object?')
    """(BooleanField) whether or not the correct object was extracted during Imageai analysis"""
    
    iai_extract_entire = models.BooleanField(blank=True, null=True,help_text='Did IAI extract the entire object?')
    """(BooleanField) whether or not the entire object was extracted during Imageai analysis"""
    
    pr_extract_correct = models.BooleanField(blank=True, null=True,help_text='Did PR extract the correct object?')
    """(BooleanField) whether or not the correct object was extracted during PR analysis"""
    
    pr_extract_entire = models.BooleanField(blank=True, null=True,help_text='Did PR extract the entire object?')
    """(BooleanField) whether or not the entire object was extracted during PR analysis"""
    
    plate_extract_correct = models.BooleanField(blank=True, null=True,help_text='Did PR extract the correct plate?')
    """(BooleanField) whether or not the correct plate was extracted during PR analysis"""
    
    plate_extract_entire = models.BooleanField(blank=True, null=True,help_text='Did PR extract the entire plate?')
    """(BooleanField) whether or not the entire plate was extracted during PR analysis"""
    


    class Meta:
        """Adds metadata about the PrColourDetection model class (used in admin section)
        """ 
        
        verbose_name = 'manual analysis'
        """(str) the name of the model class"""
        
        verbose_name_plural = 'manual analyses'
        """(str) the plural version of the name of the model class"""
        

    def __str__(self):
        """Returns the string representation of the object

        :return: the string representation of the object (manual analysis' entry's arrival time) 
                 (format = '%I:%M:%S %p' = 
                 Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        :rtype: str
        """

        return self.entry.arrival_time.strftime('%I:%M:%S %p')

    def get_absolute_url(self):
        return reverse('manual-entry-detail', kwargs={'pk' : self.entry.pk})


class GetManualContext():
    """Gets the context data the form views need
    """
    def get_manual_context(entry):
        """Gets the title of the page and performs queries to get the data needed for the dropdown select inputs

        :param entry: The entry that the manual analysis is associated with \(:class:`analysis.models.Entry`\)  
        :type entry: Entry  
        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """
        response = {}

        if entry.status == 'complete':
            response['title'] = f'Update {entry.day.date.strftime("%b %d, %Y")}, {entry} Manual Analysis'
        else:
            response['title'] = f'Make {entry.day.date.strftime("%b %d, %Y")}, {entry} Manual Analysis'
        
        response['entry'] = entry

        iai_auto_colour_names = []
        iai_auto_colour_percentages = {}
        iai_auto_colour_detected_r = {}
        iai_auto_colour_detected_g = {}
        iai_auto_colour_detected_b = {}
        iai_auto_colour_detected_hex = {}
        iai_auto_colour_chart_colours = {}
        for colour in entry.automatedanalysis.iaicolourdetections.order_by('-percentage'):
            iai_auto_colour_names.append(colour.colour.name)
            iai_auto_colour_percentages[colour.colour.name] = float(colour.percentage)
            iai_auto_colour_detected_r[colour.colour.name] = colour.detected_r
            iai_auto_colour_detected_g[colour.colour.name] = colour.detected_g
            iai_auto_colour_detected_b[colour.colour.name] = colour.detected_b
            iai_auto_colour_detected_hex[colour.colour.name] = colour.detected_hex
            iai_auto_colour_chart_colours[colour.colour.name] = f'rgb({colour.colour.r}, {colour.colour.g}, {colour.colour.b})'

        pr_auto_colour_names = []
        pr_auto_colour_percentages = {}
        pr_auto_colour_detected_r = {}
        pr_auto_colour_detected_g = {}
        pr_auto_colour_detected_b = {}
        pr_auto_colour_detected_hex = {}
        pr_auto_colour_chart_colours = {}
        for colour in entry.automatedanalysis.prcolourdetections.order_by('-percentage'):
            pr_auto_colour_names.append(colour.colour.name)
            pr_auto_colour_percentages[colour.colour.name] = float(colour.percentage)
            pr_auto_colour_detected_r[colour.colour.name] = colour.detected_r
            pr_auto_colour_detected_g[colour.colour.name] = colour.detected_g
            pr_auto_colour_detected_b[colour.colour.name] = colour.detected_b
            pr_auto_colour_detected_hex[colour.colour.name] = colour.detected_hex
            pr_auto_colour_chart_colours[colour.colour.name] = f'rgb({colour.colour.r}, {colour.colour.g}, {colour.colour.b})'


        response['iai_auto_colour_names'] = iai_auto_colour_names
        response['iai_auto_colour_percentages'] = iai_auto_colour_percentages
        response['iai_auto_colour_detected_r'] = iai_auto_colour_detected_r
        response['iai_auto_colour_detected_g'] = iai_auto_colour_detected_g
        response['iai_auto_colour_detected_b'] = iai_auto_colour_detected_b
        response['iai_auto_colour_detected_hex'] = iai_auto_colour_detected_hex
        response['iai_auto_colour_chart_colours'] = iai_auto_colour_chart_colours

        response['pr_auto_colour_names'] = pr_auto_colour_names
        response['pr_auto_colour_percentages'] = pr_auto_colour_percentages
        response['pr_auto_colour_detected_r'] = pr_auto_colour_detected_r
        response['pr_auto_colour_detected_g'] = pr_auto_colour_detected_g
        response['pr_auto_colour_detected_b'] = pr_auto_colour_detected_b
        response['pr_auto_colour_detected_hex'] = pr_auto_colour_detected_hex
        response['pr_auto_colour_chart_colours'] = pr_auto_colour_chart_colours

        return response