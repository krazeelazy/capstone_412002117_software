"""Has the configuration for the manual app (sets the name)
"""

from django.apps import AppConfig


class ManualConfig(AppConfig):
    """Class representing the manual application and its configuration. Subclass of :class:`django.apps.AppConfig`.
    """
    
    name = 'manual'
    """ (str) The name of the application
    """
