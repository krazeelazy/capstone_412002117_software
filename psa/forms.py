"""Handles custom forms.
"""

from django import forms 

#: iterable contating all possible statuses
STATUS_CHOICES =( 
    ("all", "All"),
    ("need_automated_analysis", "Need Auto Analysis"),
    ("not_in_database", "Not in database"),
    ("need_manual_analysis", "Need Manual Analysis"),
    ("complete", "Complete")
) 

# creating a form  
class StatusForm(forms.Form): 
    """Makes a form containing a dropdown select input to filter the data in the psa/home.html template based on status
    """

    status = forms.ChoiceField(choices = STATUS_CHOICES) 
