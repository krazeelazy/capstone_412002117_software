"""Handles the display and processing of all filters throughout the web app.
"""

import django_filters
from django import forms

from psa.models import IaiDetectionClass, PrDetectionClass, PredictionClass, Colour, ImageAngle, PredictionClass
from analysis.models import Entry, Day

class DayAutomatedEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the automated/day_detail.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.  
    """

    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)"""

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""


    STATUS_CHOICES = (
        ('needs manual analysis', ('needs manual analysis')),
        ('complete', ('complete')),
    )
    """(tuple) choices for a choice filter ('needs manual analysis' and 'complete')"""
    status = django_filters.ChoiceFilter(field_name='status', choices=STATUS_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter)  a choice filter populated by STATUS_CHOICES"""

    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times"""
    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times"""
    
    duration__gte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all durations must be greater than or equal to"""
    duration = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration')
    """(DurationFilter) a duration value that all durations must be equal to"""
    duration__lte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all durations must be less than or equal to"""

    iaivehicledetection= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicledetection__type__name', label='Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(iaivehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have automated analyses associated with them  """
    iaivehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicleprediction__type__name', label='Iai Prediction Class', queryset=PredictionClass.objects.exclude(iaivehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the Imageai analysis  """
    prvehicledetection = django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicledetection__type__name', label='Pr Detection Class', queryset=PrDetectionClass.objects.exclude(prvehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have automated analyses associated with them  """
    prvehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicleprediction__type__name', label='Pr Prediction Class', queryset=PredictionClass.objects.exclude(prvehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the PR analysis  """

    licenseplate__number = django_filters.CharFilter(field_name='automatedanalysis__licenseplate__number', label='Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates containing the input string"""
    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'successful_feed_in': ['successful_feed_in'],
            'status' : ['status'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'duration' : ['duration__gte', 'duration', 'duration__lte'],
            'iaivehicledetection': ['iaivehicledetection'],
            'iaivehicleprediction': ['iaivehicleprediction'],
            'prvehicledetection': ['prvehicledetection'],            
            'prvehicleprediction': ['prvehicleprediction'],            
            'licenseplate__number' : ['licenseplate__number']
        }
        """(dict) the filter  fields in order"""

class AutomatedEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the automated/home.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.  
    """

    
    day = django_filters.DateFromToRangeFilter(field_name='day__date', label='Day', widget=django_filters.widgets.DateRangeWidget(attrs={'class': 'datepicker form-control form-control-sm', 'type':"date"}))
    """(DateFromToRangeFilter) a date range filter for the days"""


    NON_NULLABLE_CHOICES = (
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a non-nullable choice filter (True/False)  """

    tried_feed_in = django_filters.ChoiceFilter(field_name='day__tried_feed_in', label='Tried Feed In', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`  """


    
    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)  """

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """


    STATUS_CHOICES = (
        ('needs manual analysis', ('needs manual analysis')),
        ('complete', ('complete')),
    )
    """(tuple) choices for a choice filter ('needs manual analysis' and 'complete')  """

    status = django_filters.ChoiceFilter(field_name='status', choices=STATUS_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a choice filter populated by STATUS_CHOICES  """

    
    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times  """

    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times  """

    
    duration__gte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all durations must be greater than or equal to  """

    duration = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration')
    """(DurationFilter)  a duration value that all durations must be equal to  """

    duration__lte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all durations must be less than or equal to  """


    iaivehicledetection= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicledetection__type__name', label='Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(iaivehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have automated analyses associated with them  """

    iaivehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicleprediction__type__name', label='Iai Prediction Class', queryset=PredictionClass.objects.exclude(iaivehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the Imageai analysis  """

    prvehicledetection = django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicledetection__type__name', label='Pr Detection Class', queryset=PrDetectionClass.objects.exclude(prvehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have automated analyses associated with them  """

    prvehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicleprediction__type__name', label='Pr Prediction Class', queryset=PredictionClass.objects.exclude(prvehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the PR analysis  """


    licenseplate__number = django_filters.CharFilter(field_name='automatedanalysis__licenseplate__number', label='Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates containing the input string  """

        
    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'day' : ['day'],
            'tried_feed_in': ['tried_feed_in'],
            'successful_feed_in': ['successful_feed_in'],
            'status' : ['status'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'duration' : ['duration__gte', 'duration', 'duration__lte'],
            'iaivehicledetection': ['iaivehicledetection'],
            'iaivehicleprediction': ['iaivehicleprediction'],
            'prvehicledetection': ['prvehicledetection'],            
            'prvehicleprediction': ['prvehicleprediction'],            
            'licenseplate__number' : ['licenseplate__number']
        }
        """(dict) the filter  fields in order"""


class DayManualEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the manual/day_detail.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.   
    """

    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)"""

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES` """


    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times  """

    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times  """

    
    duration__gte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all durations must be greater than or equal to """

    duration = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration')
    """(DurationFilter) a duration value that all durations must be equal to  """

    duration__lte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all durations must be less than or equal to"""


    iaivehicledetection= django_filters.ModelChoiceFilter(field_name='manualanalysis__iai_detection_class__name', label='Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have manual analyses associated with them  """

    prvehicledetection = django_filters.ModelChoiceFilter(field_name='manualanalysis__pr_detection_class__name', label='Pr Detection Class', queryset=PrDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have manual analyses associated with them  """

    prediction = django_filters.ModelChoiceFilter(field_name='manualanalysis__prediction_class__name', label='Prediction Class', queryset=PredictionClass.objects.exclude(manualanalysis=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have manual analyses associated with them  """

    licenseplate__number = django_filters.CharFilter(field_name='manualanalysis__license_plate_number', label='Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates containing the input string  """


    NON_NULLABLE_CHOICES = (
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a non-nullable choice filter (True/False)  """

    dominant_colour = django_filters.ModelChoiceFilter(field_name='manualanalysis__dominant_colour', label='Dominant Colour', queryset=Colour.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.Colour` class that have manual analyses associated with them  """

    dominant_colour_detected_by_iai = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_detected_by_iai', label='Dominant Colour detected by IAI', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`  """

    dominant_colour_identified_as_dominant_by_iai = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_identified_as_dominant_by_iai', label='Dominant Colour identified as dominant by IAI', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """

    dominant_colour_detected_by_pr = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_detected_by_pr', label='Dominant Colour detected by PR', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`  """

    dominant_colour_identified_as_dominant_by_pr = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_identified_as_dominant_by_pr', label='Dominant Colour identified as dominant by PR', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """


    image_angle = django_filters.ModelChoiceFilter(field_name='manualanalysis__image_angle__name', label='Image Angle', queryset=ImageAngle.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.ImageAngle` class that have manual analyses associated with them  """

    iai_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__iai_extract_correct', label='IAI extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """

    pr_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__pr_extract_correct', label='PR extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES` """

    plate_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__plate_extract_correct', label='Plate extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """


    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'successful_feed_in' : ['successful_feed_in'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'duration' : ['duration__gte', 'duration', 'duration__lte'],
            'iaivehicledetection': ['iaivehicledetection'],
            'prvehicledetection': ['prvehicledetection'],            
            'prediction': ['prediction'],            
            'licenseplate__number' : ['licenseplate__number'],
            'dominant_colour': ['dominant_colour'],            
            'dominant_colour_detected_by_iai' : ['dominant_colour_detected_by_iai'],
            'dominant_colour_identified_as_dominant_by_iai' : ['dominant_colour_identified_as_dominant_by_iai'],
            'dominant_colour_detected_by_pr' : ['dominant_colour_detected_by_pr'],
            'dominant_colour_identified_as_dominant_by_pr' : ['dominant_colour_identified_as_dominant_by_pr'],
            'image_angle' : ['image_angle'],
            'iai_extract_correct' : ['iai_extract_correct'],
            'pr_extract_correct' : ['pr_extract_correct'],
            'plate_extract_correct' : ['plate_extract_correct']
        }
        """(dict) the filter  fields in order"""

class ManualEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the manual/home.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.  
    """

    day = django_filters.DateFromToRangeFilter(field_name='day__date', label='Day', widget=django_filters.widgets.DateRangeWidget(attrs={'class': 'datepicker form-control form-control-sm', 'type':"date"}))
    """(DateFromToRangeFilter) a date range filter for the days"""


    NON_NULLABLE_CHOICES = (
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a non-nullable choice filter (True/False)"""

    tried_feed_in = django_filters.ChoiceFilter(field_name='day__tried_feed_in', label='Tried Feed In', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`"""

    
    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)"""

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """


    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times"""

    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times"""

    
    duration__gte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all durations must be greater than or equal to"""

    duration = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration')
    """(DurationFilter) a duration value that all durations must be equal to"""

    duration__lte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all durations must be less than or equal to"""


    iaivehicledetection= django_filters.ModelChoiceFilter(field_name='manualanalysis__iai_detection_class__name', label='Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have manual analyses associated with them  """

    prvehicledetection = django_filters.ModelChoiceFilter(field_name='manualanalysis__pr_detection_class__name', label='Pr Detection Class', queryset=PrDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have manual analyses associated with them  """

    prediction = django_filters.ModelChoiceFilter(field_name='manualanalysis__prediction_class__name', label='Manual Prediction Class', queryset=PredictionClass.objects.exclude(manualanalysis=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have manual analyses associated with them  """

    licenseplate__number = django_filters.CharFilter(field_name='manualanalysis__license_plate_number', label='Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates containing the input string """


    NON_NULLABLE_CHOICES = (
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a non-nullable choice filter (True/False) """

    dominant_colour = django_filters.ModelChoiceFilter(field_name='manualanalysis__dominant_colour', label='Dominant Colour', queryset=Colour.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.Colour` class that have manual analyses associated with them  """

    dominant_colour_detected_by_iai = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_detected_by_iai', label='Dominant Colour detected by IAI', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`"""

    dominant_colour_identified_as_dominant_by_iai = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_identified_as_dominant_by_iai', label='Dominant Colour identified as dominant by IAI', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""

    dominant_colour_detected_by_pr = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_detected_by_pr', label='Dominant Colour detected by PR', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES` """

    dominant_colour_identified_as_dominant_by_pr = django_filters.ChoiceFilter(field_name='manualanalysis__dominant_colour_identified_as_dominant_by_pr', label='Dominant Colour identified as dominant by PR', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""


    image_angle = django_filters.ModelChoiceFilter(field_name='manualanalysis__image_angle__name', label='Image Angle', queryset=ImageAngle.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.ImageAngle` class that have manual analyses associated with them  """

    iai_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__iai_extract_correct', label='IAI extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""

    pr_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__pr_extract_correct', label='PR extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""

    plate_extract_correct = django_filters.ChoiceFilter(field_name='manualanalysis__plate_extract_correct', label='Plate extract correct', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`"""


    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'day' : ['day'],
            'tried_feed_in': ['tried_feed_in'],
            'successful_feed_in': ['successful_feed_in'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'duration' : ['duration__gte', 'duration', 'duration__lte'],
            'iaivehicledetection': ['iaivehicledetection'],
            'prvehicledetection': ['prvehicledetection'],            
            'prediction': ['prediction'],            
            'licenseplate__number' : ['licenseplate__number'],
            'dominant_colour': ['dominant_colour'],            
            'dominant_colour_detected_by_iai' : ['dominant_colour_detected_by_iai'],
            'dominant_colour_identified_as_dominant_by_iai' : ['dominant_colour_identified_as_dominant_by_iai'],
            'dominant_colour_detected_by_pr' : ['dominant_colour_detected_by_pr'],
            'dominant_colour_identified_as_dominant_by_pr' : ['dominant_colour_identified_as_dominant_by_pr'],
            'image_angle' : ['image_angle'],
            'iai_extract_correct' : ['iai_extract_correct'],
            'pr_extract_correct' : ['pr_extract_correct'],
            'plate_extract_correct' : ['plate_extract_correct']
        }
        """(dict) the filter  fields in order"""


class DayAnalysisEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the analysis/day_detail.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.
    """
    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)"""

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES` """


    STATUS_CHOICES = (
        ('needs manual analysis', ('needs manual analysis')),
        ('complete', ('complete')),
    )
    """(tuple) choices for a choice filter ('needs manual analysis' and 'complete')"""

    status = django_filters.ChoiceFilter(field_name='status', choices=STATUS_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a choice filter populated by STATUS_CHOICES"""


    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times  """

    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times  """



    automated_duration__gte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all automated durations must be greater than or equal to  """

    automated_duration = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration')
    """(DurationFilter) a duration value that all automated durations must be equal to  """

    automated_duration__lte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all automated durations must be less than or equal to  """

    automated_iaivehicledetection= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicledetection__type__name', label='Automated Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(iaivehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have automated analyses associated with them  """

    automated_iaivehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicleprediction__type__name', label='Automated Iai Prediction Class', queryset=PredictionClass.objects.exclude(iaivehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the Imageai analysis  """

    automated_prvehicledetection = django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicledetection__type__name', label='Automated Pr Detection Class', queryset=PrDetectionClass.objects.exclude(prvehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have automated analyses associated with them  """

    automated_prvehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicleprediction__type__name', label='Automated Pr Prediction Class', queryset=PredictionClass.objects.exclude(prvehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the PR analysis  """

    automated_licenseplate__number = django_filters.CharFilter(field_name='automatedanalysis__licenseplate__number', label='Automated Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates (associated with automated analyses) containing the input string  """


    manual_duration__gte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all manual durations must be greater than or equal to  """

    manual_duration = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration')
    """(DurationFilter) a duration value that all manual durations must be equal to  """

    manual_duration__lte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all manual durations must be less than or equal to  """

    manual_iaivehicledetection= django_filters.ModelChoiceFilter(field_name='manualanalysis__iai_detection_class__name', label='Manual Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have manual analyses associated with them  """

    manual_prvehicledetection = django_filters.ModelChoiceFilter(field_name='manualanalysis__pr_detection_class__name', label='Manual Pr Detection Class', queryset=PrDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have manual analyses associated with them  """

    manual_prediction = django_filters.ModelChoiceFilter(field_name='manualanalysis__prediction_class__name', label='Manual Prediction Class', queryset=PredictionClass.objects.exclude(manualanalysis=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have manual analyses associated with them  """

    manual_licenseplate__number = django_filters.CharFilter(field_name='manualanalysis__license_plate_number', label='Manual Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates (associated with manual analyses) containing the input string  """


    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'successful_feed_in' : ['successful_feed_in'],
            'status' : ['status'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'automated_duration': ['automated_duration__gte', 'automated_duration', 'automated_duration__lte'],
            'automated_iaivehicledetection': ['automated_iaivehicledetection'],
            'automated_iaivehicleprediction': ['automated_iaivehicleprediction'],
            'automated_prvehicledetection': ['automated_prvehicledetection'],
            'automated_prvehicleprediction': ['automated_prvehicleprediction'],
            'automated_licenseplate__number': ['automated_licenseplate__number'],
            'manual_duration': ['manual_duration__gte', 'manual_duration', 'manual_duration__lte'],
            'manual_iaivehicledetection': ['manual_iaivehicledetection'],
            'manual_prvehicledetection': ['manual_prvehicledetection'],
            'manual_prediction': ['manual_prediction'],
            'manual_licenseplate__number' : ['manual_licenseplate__number']
        }
        """(dict) the filter  fields in order"""

class AnalysisEntryFilter(django_filters.FilterSet):
    """Builds the filters needed by the analysis/home.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`.     
    """

    day = django_filters.DateFromToRangeFilter(field_name='day__date', label='Day', widget=django_filters.widgets.DateRangeWidget(attrs={'class': 'datepicker form-control form-control-sm', 'type':"date"}))
    """(DateFromToRangeFilter) a date range filter for the days """

    
    NON_NULLABLE_CHOICES = (
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a non-nullable choice filter (True/False)  """

    tried_feed_in = django_filters.ChoiceFilter(field_name='day__tried_feed_in', label='Tried Feed In', choices=NON_NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a non-nullable choice filter populated by :attr:`NON_NULLABLE_CHOICES`  """

    
    NULLABLE_CHOICES = (
        ('null', ('None')),
        (True, ('True')),
        (False, ('False')),
    )
    """(tuple) choices for a nullable choice filter (null/True/False)  """

    successful_feed_in = django_filters.ChoiceFilter(field_name='automatedanalysis__successful_feed_in', label='Successful Feed In', choices=NULLABLE_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a nullable choice filter populated by :attr:`NULLABLE_CHOICES`  """

    
    STATUS_CHOICES = (
        ('needs manual analysis', ('needs manual analysis')),
        ('complete', ('complete')),
    )
    """(tuple) choices for a choice filter ('needs manual analysis' and 'complete')"""

    status = django_filters.ChoiceFilter(field_name='status', choices=STATUS_CHOICES, widget=forms.Select(attrs={'class': 'form-control form-control-sm'}), lookup_expr='exact')
    """(ChoiceFilter) a choice filter populated by STATUS_CHOICES"""

    
    arrival_time = django_filters.TimeRangeFilter(field_name='arrival_time__time', label='Arrival Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the arrival times  """

    departure_time = django_filters.TimeRangeFilter(field_name='departure_time__time', label='Departure Time (24hr) ', widget=django_filters.widgets.RangeWidget(attrs={'class': 'timepicker form-control form-control-sm', 'type':"time"}))
    """(TimeRangeFilter) a time range filter for the departure times  """


    automated_duration__gte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all automated durations must be greater than or equal to  """

    automated_duration = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration')
    """(DurationFilter) a duration value that all automated durations must be equal to  """

    automated_duration__lte = django_filters.DurationFilter(field_name='automatedanalysis__duration', label='Automated Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all automated durations must be less than or equal to  """

    automated_iaivehicledetection= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicledetection__type__name', label='Automated Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(iaivehicledetection=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have automated analyses associated with them  """

    automated_iaivehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__iaivehicleprediction__type__name', label='Automated Iai Prediction Class', queryset=PredictionClass.objects.exclude(iaivehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the Imageai analysis  """

    automated_prvehicledetection = django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicledetection__type__name', label='Automated Pr Detection Class', queryset=PrDetectionClass.objects.exclude(prvehicledetection=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have automated analyses associated with them  """

    automated_prvehicleprediction= django_filters.ModelChoiceFilter(field_name='automatedanalysis__prvehicleprediction__type__name', label='Automated Pr Prediction Class', queryset=PredictionClass.objects.exclude(prvehicleprediction=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have automated analyses associated with them because of the PR analysis  """

    automated_licenseplate__number = django_filters.CharFilter(field_name='automatedanalysis__licenseplate__number', label='Automated Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates (associated with automated analyses) containing the input string  """


    manual_duration__gte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration greater than or equal', lookup_expr='gte')
    """(DurationFilter) a duration value that all manual durations must be greater than or equal to  """

    manual_duration = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration')
    """(DurationFilter) a duration value that all manual durations must be equal to"""

    manual_duration__lte = django_filters.DurationFilter(field_name='manualanalysis__duration', label='Manual Duration less than or equal to', lookup_expr='lte')
    """(DurationFilter) a duration value that all manual durations must be less than or equal to  """

    manual_iaivehicledetection= django_filters.ModelChoiceFilter(field_name='manualanalysis__iai_detection_class__name', label='Manual Iai Detection Class', queryset=IaiDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.IaiDetectionClass` class that have manual analyses associated with them  """

    manual_prvehicledetection = django_filters.ModelChoiceFilter(field_name='manualanalysis__pr_detection_class__name', label='Manual Pr Detection Class', queryset=PrDetectionClass.objects.exclude(manualanalysis=None))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PrDetectionClass` class that have manual analyses associated with them  """

    manual_prediction = django_filters.ModelChoiceFilter(field_name='manualanalysis__prediction_class__name', label='Manual Prediction Class', queryset=PredictionClass.objects.exclude(manualanalysis=None).order_by('name'))
    """(ModelChoiceFilter) a choice filter populated with values from the :class:`psa.models.PredictionClass` class that have manual analyses associated with them  """

    manual_licenseplate__number = django_filters.CharFilter(field_name='manualanalysis__license_plate_number', label='Manual Plate number contains',lookup_expr='icontains')
    """(CharFilter) a character filter that will find plates (associated with manual analyses) containing the input string  """


    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'day' : ['day'],
            'tried_feed_in': ['tried_feed_in'],
            'successful_feed_in': ['successful_feed_in'],
            'arrival_time' : ['arrival_time'],
            'departure_time' : ['departure_time'],
            'automated_duration': ['automated_duration__gte', 'automated_duration', 'automated_duration__lte'],
            'automated_iaivehicledetection': ['automated_iaivehicledetection'],
            'automated_iaivehicleprediction': ['automated_iaivehicleprediction'],
            'automated_prvehicledetection': ['automated_prvehicledetection'],
            'automated_prvehicleprediction': ['automated_prvehicleprediction'],
            'automated_licenseplate__number': ['automated_licenseplate__number'],
            'manual_duration': ['manual_duration__gte', 'manual_duration', 'manual_duration__lte'],
            'manual_iaivehicledetection': ['manual_iaivehicledetection'],
            'manual_prvehicledetection': ['manual_prvehicledetection'],
            'manual_prediction': ['manual_prediction'],
            'manual_licenseplate__number' : ['manual_licenseplate__number']
        }
        """(dict) the filter  fields in order"""


class StatisticsFilter(django_filters.FilterSet):
    """Builds the filters needed by the analysis_statistics/home.html template and processes the incoming info to properly filter the data. Subclass of 
    :class:`django_filters.FilterSet`. 
    """

    def filter_dates(self, queryset, name, value):
        """Get the data for specific days (stored in value param), if the days have manual analyses. Days without manual analyses are excluded.
        """
        if value: #if they are trying to filter using specific dates
            dates = []
            for day in value:
                if Entry.objects.filter(day=day).exclude(manualanalysis = None):
                    dates.append(day.date)
            return queryset.filter(date__in = dates)
        else:
            return queryset

    date = django_filters.DateFromToRangeFilter(field_name='date', label='Date Range', widget=django_filters.widgets.DateRangeWidget(attrs={'class': 'datepicker form-control form-control-sm', 'type':"date"}))
    """(DateFromToRangeFilter) a date range filter for the days"""
    dates = django_filters.ModelMultipleChoiceFilter(label='Specific Date(s)', queryset=Day.objects.all(), method='filter_dates')
    """(ModelMultipleChoiceFilter) a multiple choice filter populated with the days that have atleast one manual analysis"""
    
    class Meta:
        """Sets the order in which the filter fields appear in the form.
        """

        fields = {
            'date' : ['date'],
            'dates' : ['dates']
        }
        """(dict) the filter  fields in order"""