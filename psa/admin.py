from django.contrib import admin
from .models import Colour, ImageType, ImageAngle, IaiDetectionClass, PrDetectionClass, PredictionClass


admin.site.register(Colour)
admin.site.register(ImageType)
admin.site.register(ImageAngle)
admin.site.register(IaiDetectionClass)
admin.site.register(PrDetectionClass)
admin.site.register(PredictionClass)