"""Has classes that have functions to gather all the data for the templates and to handle incoming data. 
Makes calls to the spot_analysis.py script to do the analyses.
"""

from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import CheckDates, MakeDay, CheckPsaTables, PopulatePsaTables
from django.contrib import messages
from django_tables2 import RequestConfig
from .tables import DatesTable, PsaTable
from .forms import StatusForm

from spot_analysis import analyze_day, main

def home(request):
    """Gets the data needed by the psa/home.html template.

    :param request: the incoming request  
    :type request: tuple  
    :return: renders the template with all its data  
    :rtype: HttpResponse  
    """

    config = RequestConfig(request)

    need_automated_analysis = CheckDates.check_new()
    not_in_database = CheckDates.check_analyzed()
    need_manual_analysis = CheckDates.check_need_manual_analysis()
    complete =  CheckDates.check_complete()

    all_dates = need_automated_analysis + not_in_database + complete + need_manual_analysis

    def my_sort(val):
        """A function to specify the sorting criteria for the all_dates list (the date)

        :param val: the all_dates list  
        :type val: list  
        :return: the day's date  
        :rtype: str  
        """
        return val['date']

    all_dates.sort(key=my_sort)

    status = request.GET.get('status', None)

    if all_dates != [] and status == None or status == 'all':
        dates_table = DatesTable(data=all_dates)
    else:
        if need_automated_analysis != [] and status == 'need_automated_analysis':
            dates_table = DatesTable(data=need_automated_analysis) 

        elif not_in_database != [] and status == 'not_in_database':
            dates_table = DatesTable(data=not_in_database)

        elif need_manual_analysis != [] and status == 'need_manual_analysis':
            dates_table = DatesTable(data=need_manual_analysis)

        elif complete != [] and status == 'complete':
            dates_table = DatesTable(data=complete) 
        else:
            dates_table = None

    if dates_table != None:        
        dates_table.paginate(page=request.GET.get("page", 1), per_page=10) 
        config.configure(dates_table)    

    form = StatusForm()
    form.fields['status'].initial = status

    return render(request, 'psa/home.html', context={'form' : form, 'dates_table' : dates_table, 'title': 'Home'})

def add_day(request, sub_dir):
    """Adds a day to the database

    :param request: the incoming request  
    :type request: tuple  
    :param sub_dir: the path the the day's subdir in the data/analyzed dir (data/analyzed/{date})  
    :type sub_dir: str  
    :return: redirect to the psa-home page with the success message  
    :rtype: HttpResponseRedirect  
    """

    path = sub_dir
    day = MakeDay(path)
    date = day.make_day()
    messages.success(request,f'{date} successfully added!')

    return redirect('psa-home')

def add_days(request):
    """Adds all days in the data/analyzed dir to the database.

    :param request: the incoming request  
    :type request: tuple  
    :return: redirect to the psa-home page with the success message, or a warning message if no dates are available  
    :rtype: HttpResponseRedirect  
    """
    not_in_database = CheckDates.check_analyzed()
    
    if(not_in_database):
        dates = ''
    
        for day_obj in not_in_database:
            path = day_obj['path']
            day = MakeDay(path)
            dates += f'{day.make_day()}, '
            
        dates = dates.rstrip(', ')
        dates = ' and'.join(dates.rsplit(',', 1)) 
        messages.success(request,f'{dates} successfully added!')
    else:
        messages.warning(request,f'No new dates detected!')

    return redirect('psa-home')

def open_file(request, filepath):
    """Opens a file. Used to open the JSON and log files.

    :param request: the incoming request  
    :type request: tuple  
    :param filepath: the path to the file  
    :type filepath: str  
    :return: a url so the file can be opened  
    :rtype: HttpResponseRedirect  
    """
    return redirect(filepath)

def populate_tables(request):
    """Gets the data needed by the psa/populate_psa_tables.html template.

    :param request: the incoming request  
    :type request: tuple  
    :return: renders the template with all its data  
    :rtype: HttpResponse  
    """

    config = RequestConfig(request)

    psa_tables = CheckPsaTables.check_psa_tables()

    have_unpop = False

    for table in psa_tables:
        if table['status'] == 'needs to be populated':
            have_unpop = True
            break

    pop_table = request.GET.get('pop_table', None)

    if have_unpop == True and pop_table != None:
        if pop_table == 'all':
            count = PopulatePsaTables.populate_all()
            messages.success(request,f'{count["table_count"]} tables successfully populated with {count["value_count"]} values!')            
            
        elif pop_table == 'Colour':
            count = PopulatePsaTables.populate_colour()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')

        elif pop_table == 'ImageAngle':
            count = PopulatePsaTables.populate_image_angle()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')

        elif pop_table == 'ImageType':
            count = PopulatePsaTables.populate_image_type()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')

        elif pop_table == 'IaiDetectionClass':
            count = PopulatePsaTables.populate_iai_detection_class()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')

        elif pop_table == 'PrDetectionClass':
            count = PopulatePsaTables.populate_pr_detection_class()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')

        elif pop_table == 'PredictionClass':
            count = PopulatePsaTables.populate_prediction_class()
            messages.success(request,f'{pop_table} successfully populated with {count} values!')


        psa_tables = CheckPsaTables.check_psa_tables()
    
    elif have_unpop == False and pop_table != None:
        messages.warning(request, 'No unpopulated tables present!')

    table = PsaTable(data=psa_tables)

    config.configure(table)    

    return render(request, 'psa/populate_psa_tables.html', context={'table' : table, 'title' : 'Populate Tables', 'have_unpop': have_unpop})

def auto_analyze_day(request):
    """Analyzes a day by calling the :func:`spot_analysis.analyze_day` function.

    :param request: the incoming request  
    :type request: tuple  
    :return: redirect to the psa-home page with the success message, or warning message if no date was selected  
    :rtype: HttpResponseRedirect  
    """
    
    date = request.GET.get('date', None)
    if(date):
        rot = request.GET.get('rotate', 'False')
        feed = request.GET.get('feed_in', 'False')

        if rot == 'True':
            rotate = True
            rot_msg = ' (rotated)'
        elif rot == 'False':
            rotate = False
            rot_msg = ''

        if feed == 'True':
            feed_in = True
            feed_msg = ' (fed in)'
        elif feed == 'False':
            feed_in = False
            feed_msg = ''

        if rot == 'True' and feed == 'True':
            rot_msg = ' (rotated and'
            feed_msg = ' fed in)'
        
        output_date = analyze_day(date=date, rotate=rotate, feed_in=feed_in)

        messages.success(request,f'Auto Analysis{rot_msg}{feed_msg} successfully made for {output_date}!')
    else:
        messages.warning(request, 'No date selected!')

    return redirect('psa-home')

def auto_analyze_days(request):
    """Analyzes all days with the status 'needs automated analysis' by calling the :func:`spot_analysis.main` function.

    :param request: the incoming request  
    :type request: tuple  
    :return: redirect to the psa-home page with the success message, or warning message if no new dates are detected (data/new is empty)  
    :rtype: HttpResponseRedirect  
    """
    
    need_automated_analysis = CheckDates.check_new()

    if(need_automated_analysis):
        rot = request.GET.get('rotate', 'False')
        feed = request.GET.get('feed_in', 'False')

        if rot == 'True':
            rotate = True
            rot_msg = ' (rotated)'
        elif rot == 'False':
            rotate = False
            rot_msg = ''

        if feed == 'True':
            feed_in = True
            feed_msg = ' (fed in)'
        elif feed == 'False':
            feed_in = False
            feed_msg = ''

        if rot == 'True' and feed == 'True':
            rot_msg = ' (rotated and'
            feed_msg = ' fed in)'

        dates = main(rotate=rotate, feed_in=feed_in)

        dates = ', '.join(dates) 
        dates = ' and'.join(dates.rsplit(',', 1)) 
        messages.success(request,f'Auto Analysis{rot_msg}{feed_msg} successfully made for {dates}!')
    else:
        messages.warning(request,f'No new dates detected!')

    return redirect('psa-home')
