"""Has functions used in the templates to generate and display information dynamically.
"""

from django import template
register = template.Library()

@register.filter
def index(indexable, i):
    """Gets an element from an indexable container

    :param indexable: the indexable containiner  
    :param i: the index  
    :return: the item with the index (i) in the indexable container  
    """
    return indexable[i]

@register.filter
def day_date_index(indexable, date):
    """Gets an element from an indexable container where the indices are dates

    :param indexable: the indexable containiner  
    :param date: the date index  
    :return: the item with the index (date) in the indexable container  
    """
    i = date.strftime('%b %d, %Y')
    return indexable[i]

@register.filter
def subtract(value, arg):
    """Subtracts two values

    :param value: the minuend (number being subtracted from)  
    :param arg: the subtrahend (amount being subtracted)  
    :return: the difference  
    """
    return value - arg

@register.filter
def format_title(value):
    """Replaces underscores with spaces and converts a string to title case (capitalize the first letter of each word)

    :param value: the original string  
    :type value: str  
    :return: the sting with underscores replaced by spaces and converted to title case  
    :rtype: str  
    """
    return value.replace("_"," ").title()