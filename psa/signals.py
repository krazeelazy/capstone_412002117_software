"""Has functions that helps allow decoupled applications get notified when actions occur elsewhere in the framework. Signals allow certain senders to 
notify a set of receivers that some action has taken place.
"""

from django.db.models.signals import post_delete, post_save, pre_delete
from django.dispatch import receiver
from analysis.models import Day
from automated.models import AutomatedAnalysis, Image
from manual.models import ManualAnalysis

@receiver(post_delete, sender=Day)
def submission_delete(sender, instance, **kwargs):
    """Deletes the JSON and log files associated with a day if the day is deleted from the database. Uses the :func:`django.dispatch.receiver` decorator
    (@receiver(post_delete, sender=Day)). 
    """

    instance.json_file.delete(False) 
    instance.log_file.delete(False) 

@receiver(post_save, sender=AutomatedAnalysis)
@receiver(post_save, sender=ManualAnalysis)
def save_entry(sender, instance, **kwargs):
    """Updates the status of an entry as it's worked on. When an automated analysis is made for the entry the status is updated to 'needs manual analysis'. 
    When a manual analysis is made for the entry the status is updated to 'complete'. Uses the :func:`django.dispatch.receiver` decorator
    (@receiver(post_save, sender=AutomatedAnalysis) and @receiver(post_save, sender=ManualAnalysis)). 
    """    
    
    if sender.__name__ == 'AutomatedAnalysis':
        status = 'needs manual analysis'
    elif sender.__name__ == 'ManualAnalysis':
        status = 'complete'
        
    instance.entry.status = status
    instance.entry.save()

@receiver(pre_delete, sender=Image)
def image_delete(sender, instance, **kwargs):
    """Deletes the image file associated with an image object if the image object is deleted from the database and is not set to the default image. 
    Uses the :func:`django.dispatch.receiver` decorator (@receiver(pre_delete, sender=Image)). 
    """
    
    if instance.image.name != 'default.jpg':
        instance.image.delete(False)