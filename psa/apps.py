"""Has the configuration for the psa app (sets the name)
"""

from django.apps import AppConfig


class PsaConfig(AppConfig):
    """Class representing the psa application and its configuration. Subclass of :class:`django.apps.AppConfig`. 
    """
    
    name = 'psa'
    """ (str) The name of the application
    """
