"""Has the models for the psa app and classes that have functions to get the data the views need to pass to the templates, and to process the input data. 
The models for this app are referenced by the models of the other apps.
"""

from django.db import models
import os
import os.path
import json
from datetime import datetime, timedelta
from django.core.files import File
from django.core.files.images import ImageFile
from django.core.files.base import ContentFile
from PIL import Image as PImage
import shutil

from capstone_412002117_software.settings import MEDIA_ROOT, BASE_DIR
from analysis.models import Day, Entry, DayTiming
from automated.models import (
	AutomatedAnalysis, 
	Image, 
	IaiVehicleDetection,
	IaiVehiclePrediction,
	IaiColourDetection,
	PrVehicleDetection,
	PrVehiclePrediction,
	PrColourDetection,
	LicensePlate,
	Timing
	)

class Colour(models.Model):
	"""Class representing a colour. Subclass of :class:`django.db.models.Model`. 
	"""
	
	name = models.CharField(max_length=250,unique=True)
	"""(CharField) the name of the colour"""
	r = models.IntegerField()
	"""(IntegerField) the red value of the colour"""
	g = models.IntegerField()
	"""(IntegerField) the green value of the colour"""
	b = models.IntegerField()
	"""(IntegerField) the blue value of the colour"""
	hex = models.CharField(max_length=7)
	"""(CharField) the hex value of the colour"""

	class Meta:
		"""Adds metadata about the Colour model class (used in admin section) 
		"""

		verbose_name = 'colour'
		"""(str) the name of the model class"""

		verbose_name_plural = 'colours'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the colour)  
		:rtype: str
		"""

		return self.name

class ImageType(models.Model):
	"""Class representing an image type. Subclass of :class:`django.db.models.Model`.
	"""
	
	name = models.CharField(max_length=250,unique=True)
	"""(CharField) the name of the image type"""

	class Meta:
		"""Adds metadata about the ImageType model class (used in admin section) 
		"""

		verbose_name = 'image type'
		"""(str) the name of the model class"""

		verbose_name_plural = 'image types'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the image type)  
		:rtype: str
		"""

		return self.name

class ImageAngle(models.Model):
	"""Class representing an image angle. Subclass of :class:`django.db.models.Model`.  
	"""
	
	name = models.CharField(max_length=250,unique=True)
	"""(CharField) the name of the image angle"""

	class Meta:
		"""Adds metadata about the ImageAngle model class (used in admin section)  
		"""

		verbose_name = 'image angle'
		"""(str) the name of the model class"""

		verbose_name_plural = 'image angles'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the image angle)  
		:rtype: str
		"""

		return self.name

class IaiDetectionClass(models.Model):
	"""Class representing an Imageai Detection Class. Subclass of :class:`django.db.models.Model`.  
	"""
	
	name = models.CharField(max_length=250,unique=True)
	"""(CharField) the name of the Imageai Detection Class"""

	class Meta:
		"""Adds metadata about the IaiDetectionClass model class (used in admin section) 
		"""

		verbose_name = 'imageai detection class'
		"""(str) the name of the model class"""

		verbose_name_plural = 'imageai detection classes'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the Imageai Detection Class)  
		:rtype: str
		"""

		return self.name

class PrDetectionClass(models.Model):
	"""Class representing an Plate Recognizer Detection Class. Subclass of :class:`django.db.models.Model`. 
	"""
	
	name = models.CharField(max_length=250,unique=True)
	"""(CharField) the name of the Plate Recognizer Detection Class"""

	class Meta:
		"""Adds metadata about the PrDetectionClass model class (used in admin section) 
		"""

		verbose_name = 'plate recognizer detection class'
		"""(str) the name of the model class"""

		verbose_name_plural = 'plate recognizer detection classes'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the Plate Recognizer Detection Class)  
		:rtype: str
		"""

		return self.name

class PredictionClass(models.Model):
	"""Class representing a Prediction Class. Subclass of :class:`django.db.models.Model`.
	"""
	
	name = models.CharField(max_length=250)
	"""(CharField) the name of the Prediction Class"""

	class Meta:
		"""Adds metadata about the PredictionClass model class (used in admin section)
		"""

		verbose_name = 'prediction class'
		"""(str) the name of the model class"""

		verbose_name_plural = 'prediction classes'
		"""(str) the plural version of the name of the model class"""


	def __str__(self):
		"""Returns the string representation of the object

		:return: the string representation of the object (the name of the Prediction Class)  
		:rtype: str
		"""

		return self.name


class CheckDates():  
	"""Collects the data for all the days.  
	Data includes:  
	path to the day's dir if it's in data/new or data/analyzed (status = 'needs automated analysis' or 'not in database')  
	day's date  
	day's id if it has one  
	day's status  
	tried_feed_in (whether or not an attempt was made to feed the IAI extracted image into the PR analysis)   
	actions that can be performed with the day's data   
	paths to the day's JSON and log files if they exist (status = 'needs manual analysis' or 'complete')
	"""

	def check_new(): #check the 'new' sub dir for new data to be analyzed
		"""Checks the data/new dir for new data to be analyzed. Adds the data for the days in the dir to a list.

		:return: a list of dictionaries each containing the path to a day's sub dir (data/new/{date}), the day's date, status = 'needs automated analysis', 
		         tried_feed_in = None, and actions = ['make_auto_analysis']  
		:rtype: list  
		"""

		new_dates = []
		for f in os.scandir('data/new'):
			if f.is_dir():
				path = None
				date = os.path.split(f.path)[1]
				status = 'needs automated analysis' 
				tried_feed_in = None
				actions = ['make_auto_analysis']
				data = {'date': date, 'path' : path, 'status' : status, 'tried_feed_in' : tried_feed_in, 'actions' : actions}
				new_dates.append(data)
		
		if new_dates != []:
			new_dates.sort(key = lambda x:x['date']) 
			
		return new_dates

	def check_analyzed(): #check the 'analyzed' sub dir for analyzed data to be added to the database
		"""Checks the data/analyzed dir for  data to be added to the database. Adds the data for the days in the dir to a list.

		:return: a list of dictionaries each containing the path to a day's sub dir (data/analyzed/{date}), the day's date, id = None, 
		         status = 'not in database', tried_feed_in = None, and actions = ['add_to_db']  
		:rtype: list  
		"""

		analyzed_dates = []
		for f in os.scandir('data/analyzed'):
			if f.is_dir():
				path = f.path
				date = os.path.split(path)[1]
				id = None
				status = 'not in database' 
				tried_feed_in = None
				actions = ['add_to_db']
				data = {'date': date, 'path' : path, 'status' : status, 'tried_feed_in' : tried_feed_in, 'actions' : actions, 'id' : id}
				analyzed_dates.append(data)

		if analyzed_dates != []:
			analyzed_dates.sort(key = lambda x:x['date']) 

		return analyzed_dates

	def check_need_manual_analysis():
		"""Checks the database for days that have entries that need manual analyses. Adds the data for the days in the dir to a list.

		:return: a list of dictionaries each containing path = None, the day's date, the day's id, path to the day's JSON file, path to the day's log file, 
		         status = 'needs manual analysis', the day's tried_feed_in value, and 
		         actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_stats', 'view_json', 'view_log'] if any entries have manual analyses, else 
		         actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_json', 'view_log'] if none of the day's entries have manual analyses  
		:rtype: list  
		"""

		need_manual_analysis_dates = []

		days = Day.objects.exclude(entries=None) #days that have entries
		for day in days:
			entries = Entry.objects.filter(day=day,manualanalysis=None).exclude(automatedanalysis=None)
			if entries:
				path = None
				date = day.date.strftime('%Y-%m-%d')
				id = day.id
				json_file = day.json_file
				log_file = day.log_file
				status = 'needs manual analysis' 
				tried_feed_in = day.tried_feed_in

				if Entry.objects.filter(day=day).exclude(automatedanalysis=None).exclude(manualanalysis=None): #if any entries have a manual analysis
					actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_stats', 'view_json', 'view_log']
				else:
					actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_json', 'view_log']

				data = {'date': date, 'path' : path, 'status' : status, 'tried_feed_in' : tried_feed_in, 'actions' : actions, 'id' : id, 'json_file':json_file, 'log_file':log_file }
				need_manual_analysis_dates.append(data)
		return need_manual_analysis_dates

	def check_complete():
		"""Checks the database for days with all entries complete. Adds the data for the days in the dir to a list.

		:return: a list of dictionaries each containing path = None, the day's date, the day's id, path to the day's JSON file, path to the day's log file, 
		         status = 'complete', the day's tried_feed_in value, and 
		         actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_manual_analysis_list', 'view_stats', 'view_json', 'view_log']  
		:rtype: list  
		"""

		complete_dates = []

		days = Day.objects.exclude(entries=None) #days that have entries
		for day in days:
			entries = Entry.objects.filter(day=day).exclude(automatedanalysis=None).exclude(manualanalysis=None)
			entries_with_auto = Entry.objects.filter(day=day).exclude(automatedanalysis=None)
			entries_with_manual = Entry.objects.filter(day=day).exclude(manualanalysis=None)
			if entries and  not(entries_with_manual.count() < entries_with_auto.count()):
				path = None
				date = day.date.strftime('%Y-%m-%d')
				id = day.id
				json_file = day.json_file
				log_file = day.log_file
				status = 'complete' 
				tried_feed_in = day.tried_feed_in
				actions = ['view_analysis_summary_list', 'view_auto_analysis_list', 'view_manual_analysis_list', 'view_stats', 'view_json', 'view_log']
				data = {'date': date, 'path' : path, 'status' : status, 'tried_feed_in' : tried_feed_in, 'actions' : actions, 'id' : id, 'json_file':json_file, 'log_file':log_file }
				complete_dates.append(data)
		return complete_dates

class MakeDay():
	"""Processes an input JSON file in data/analyzed/{date}/{date}.json to add the information about a day to the database.
		
		:param path_to_date_dir: the path to the day's directory (data/analyzed/{date})  
		:type path_to_date_dir: str   
	"""

	files_dir = 'files'
	"""(str) the path to the files subdir inside the media dir"""

	log_dir = os.path.join(files_dir, 'log')
	"""(str) the path to the log subdir inside the files dir"""

	json_dir = os.path.join(files_dir, 'json')
	"""(str) the path to the JSON subdir inside the files dir"""


	images_dir = 'images'
	"""(str) the path to the images subdir inside the media dir"""


	default_image = 'default.jpg'
	"""(str) the path to the default.jpg file inside the media dir"""



	def __init__(self, path_to_date_dir):
		"""Sets up the initial values needed to process the day. Sets the path to the day's directory (self.path_to_date_dir = data/analyzed/{date}), the string 
		representation of the day's date (self.date_str) and the path to the input JSON file (self.input_json_file_path = data/analyzed/{date}/{date}.json).

		:param path_to_date_dir: the path to the day's directory (data/analyzed/{date})  
		:type path_to_date_dir: str  
		"""
		self.path_to_date_dir = path_to_date_dir
		self.date_str = os.path.split(path_to_date_dir)[1]
		self.input_json_file_path = os.path.join(path_to_date_dir, f'{self.date_str}.json')

	def make_day(self):
		"""Uses the data in the input JSON file (data/analyzed/{date}/{date}.json) to make a new :class:`analysis.models.Day` object, along with all its new 
		:class:`analysis.models.Entry` objects. All the files and images are moved to the correct subdirectories in the media directory. A new output JSON file 
		(media/files/json/{date}.json) containing all the day's data (images now use the path to the image file in the media dir).

		:return: the date string for the day  
		:rtype: str  
		"""
		with open(self.input_json_file_path, 'r') as json_file:
			self.input_json_data = json.load(json_file)

		log_file_path = os.path.join(self.path_to_date_dir, self.input_json_data['log_file'])
		date_obj = datetime.strptime(self.date_str, "%Y-%m-%d")

		log_file_name = os.path.split(log_file_path)[1]
		new_log_file_path = os.path.join(MakeDay.log_dir, log_file_name)

		with open(log_file_path, 'r') as f: #make the day object with the log file
			new_file = File(file=f, name=new_log_file_path) 
			tried_feed_in = self.input_json_data['tried_feed_in']
			day = Day(date=date_obj, log_file=new_file, tried_feed_in = tried_feed_in)  
			day.save()

		analysis = []
		total_times = []

		input_analysis = self.input_json_data['analysis']
		input_total_times = self.input_json_data['total_times']

		for entry_info in input_analysis:
			entry_analysis = entry_info['analysis']
			entry_timing = entry_info['timing']

			entry_arrival_string = entry_analysis['arrival_time']
			arrival_time = datetime.strptime(entry_arrival_string, "%Y-%m-%d-%H:%M:%S")
			entry_departure_string = entry_analysis['departure_time']
			departure_time = datetime.strptime(entry_departure_string, "%Y-%m-%d-%H:%M:%S")

			#make the entry
			entry = Entry(arrival_time= arrival_time, departure_time=departure_time, day=day)
			entry.save()

			#make the automated analysis
			entry_successful_feed_in = entry_analysis['successful_feed_in']
			automated_analysis = AutomatedAnalysis(entry = entry, duration = timedelta(seconds=entry_analysis['duration']['in_seconds']), 
													successful_feed_in = entry_successful_feed_in)
			automated_analysis.save()

			#make the images
			for image_type, orig_image_path in entry_analysis['images'].items(): 
				if orig_image_path == None:
					if image_type == 'original':
						entry_original_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_original_image.save()
					elif image_type == 'extracted_imageai':
						entry_extracted_imageai_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_extracted_imageai_image.save()
					elif image_type == 'extracted_pr':
						entry_extracted_pr_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_extracted_pr_image.save()
					elif image_type == 'extracted_plate':
						entry_extracted_plate_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_extracted_plate_image.save()
					elif image_type == 'analyzed_comparison':
						entry_analyzed_comparison_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_analyzed_comparison_image.save()
					elif image_type == 'analyzed_imageai':
						entry_analyzed_imageai_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_analyzed_imageai_image.save()
					elif image_type == 'analyzed_pr':
						entry_analyzed_pr_image = Image(image=MakeDay.default_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
						entry_analyzed_pr_image.save()
				else:
					if image_type == 'original':
						sub_dir = 'original'
					else:
						split = image_type.split('_')
						sub_dir = os.path.join(split[0], split[1])

					image_sub_dir = os.path.join(MakeDay.images_dir, sub_dir)
					orig_image_path = os.path.join(self.path_to_date_dir, orig_image_path)
					image_name = os.path.split(orig_image_path)[1]
					new_image_path = os.path.join(image_sub_dir, image_name)

					with open(orig_image_path, 'rb') as f: #make the image

						new_image = ImageFile(file=f, name=new_image_path) #make the image object

						if image_type == 'original':
							entry_original_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_original_image.save()
						elif image_type == 'extracted_imageai':
							entry_extracted_imageai_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_extracted_imageai_image.save()
						elif image_type == 'extracted_pr':
							entry_extracted_pr_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_extracted_pr_image.save(new_image_path)
						elif image_type == 'extracted_plate':
							entry_extracted_plate_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_extracted_plate_image.save(new_image_path)
						elif image_type == 'analyzed_comparison':
							entry_analyzed_comparison_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_analyzed_comparison_image.save()
						elif image_type == 'analyzed_imageai':
							entry_analyzed_imageai_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_analyzed_imageai_image.save()
						elif image_type == 'analyzed_pr':
							entry_analyzed_pr_image = Image(image= new_image, type=ImageType.objects.filter(name__iexact=image_type).first(), entry=entry)
							entry_analyzed_pr_image.save(new_image_path)

			
			#imageai analysis
			entry_imageai_analysis = entry_analysis['imageai_analysis']

			entry_imageai_analysis['detection']['class'] = entry_imageai_analysis['detection']['class'] if entry_imageai_analysis['detection']['class'] != None else 'unknown'
			entry_imageai_analysis['detection']['probability'] = entry_imageai_analysis['detection']['probability'] if entry_imageai_analysis['detection']['probability'] != None else 0.00

			iai_vehicle_detection = IaiVehicleDetection(
				automated_analysis=automated_analysis,
				type=IaiDetectionClass.objects.filter(name__iexact=entry_imageai_analysis['detection']['class']).first(),
				probability = entry_imageai_analysis['detection']['probability']
				)
			iai_vehicle_detection.save()

			entry_imageai_analysis['prediction']['class'] = entry_imageai_analysis['prediction']['class'] if entry_imageai_analysis['prediction']['class'] != None else 'unknown'
			entry_imageai_analysis['prediction']['probability'] = entry_imageai_analysis['prediction']['probability'] if entry_imageai_analysis['prediction']['probability'] != None else 0.00
			entry_imageai_analysis['prediction']['class'] = entry_imageai_analysis['prediction']['class'].replace('_', ' ')
			iai_vehicle_prediction = IaiVehiclePrediction(
				automated_analysis=automated_analysis,
				type=PredictionClass.objects.filter(name__iexact=entry_imageai_analysis['prediction']['class']).first(),
				probability = entry_imageai_analysis['prediction']['probability']
				)
			iai_vehicle_prediction.save()

			if entry_imageai_analysis['colours'] != None:
				for key, colour in entry_imageai_analysis['colours'].items():
					iai_colour_detection = IaiColourDetection(
						automated_analysis=automated_analysis,
						colour = Colour.objects.filter(name__iexact=colour['name']).first(),
						percentage = colour['percent'],
						detected_r = colour['r'],
						detected_g = colour['g'],
						detected_b = colour['b'],
						detected_hex = colour['hex']
					)
					iai_colour_detection.save()

			elif entry_imageai_analysis['colours'] == None:
				colour_info = Colour.objects.filter(name__iexact='unknown').first()
				iai_colour_detection = IaiColourDetection(
						automated_analysis=automated_analysis,
						colour = colour_info,
						percentage = 100,
						detected_r = colour_info.r,
						detected_g = colour_info.g,
						detected_b = colour_info.b,
						detected_hex = colour_info.hex
					)
				iai_colour_detection.save()

			#plate recognizer analysis
			entry_pr_analysis = entry_analysis['pr_analysis']

			entry_pr_analysis['detection']['class'] = entry_pr_analysis['detection']['class'] if entry_pr_analysis['detection']['class'] != None else 'unknown'
			entry_pr_analysis['detection']['probability'] = entry_pr_analysis['detection']['probability'] if entry_pr_analysis['detection']['probability'] != None else 0.00

			pr_vehicle_detection = PrVehicleDetection(
				automated_analysis=automated_analysis,
				type=PrDetectionClass.objects.filter(name__iexact=entry_pr_analysis['detection']['class']).first(),
				probability = entry_pr_analysis['detection']['probability']
			)
			pr_vehicle_detection.save()

			entry_pr_analysis['prediction']['class'] = entry_pr_analysis['prediction']['class'] if entry_pr_analysis['prediction']['class'] != None else 'unknown'
			entry_pr_analysis['prediction']['probability'] = entry_pr_analysis['prediction']['probability'] if entry_pr_analysis['prediction']['probability'] != None else 0.00
			entry_pr_analysis['prediction']['class'] = entry_pr_analysis['prediction']['class'].replace('_', ' ')
			pr_vehicle_prediction = PrVehiclePrediction(
				automated_analysis=automated_analysis,
				type=PredictionClass.objects.filter(name__iexact=entry_pr_analysis['prediction']['class']).first(),
				probability = entry_pr_analysis['prediction']['probability']
			)
			pr_vehicle_prediction.save()

			if entry_pr_analysis['colours'] != None:
				for key, colour in entry_pr_analysis['colours'].items():
					pr_colour_detection = PrColourDetection(
						automated_analysis=automated_analysis,
						colour = Colour.objects.filter(name__iexact=colour['name']).first(),
						percentage = colour['percent'],
						detected_r = colour['r'],
						detected_g = colour['g'],
						detected_b = colour['b'],
						detected_hex = colour['hex']
					)
					pr_colour_detection.save()

			elif entry_pr_analysis['colours'] == None:
				colour_info = Colour.objects.filter(name__iexact='unknown').first()
				pr_colour_detection = PrColourDetection(
						automated_analysis=automated_analysis,
						colour = colour_info,
						percentage = 100,
						detected_r = colour_info.r,
						detected_g = colour_info.g,
						detected_b = colour_info.b,
						detected_hex = colour_info.hex
					)
				pr_colour_detection.save()

			#license plate
			entry_pr_analysis['license_plate']['plate_number'] = entry_pr_analysis['license_plate']['plate_number'].upper() if entry_pr_analysis['license_plate']['plate_number'] != None else 'UNKNOWN'
			entry_pr_analysis['license_plate']['plate_probability'] = entry_pr_analysis['license_plate']['plate_probability'] if entry_pr_analysis['license_plate']['plate_probability'] != None else 0.00
			license_plate = LicensePlate(
				automated_analysis=automated_analysis,
				number = entry_pr_analysis['license_plate']['plate_number'],
				probability = entry_pr_analysis['license_plate']['plate_probability']
			)
			license_plate.save()
			
			
			entry_times = Timing(
				automated_analysis = automated_analysis,
				get_duration_time = timedelta(seconds=entry_timing['get_duration_time']),
				imageai_detection_time = timedelta(seconds=entry_timing['imageai_detection_time']),
				imageai_prediction_time = timedelta(seconds=entry_timing['imageai_prediction_time']),
				imageai_colour_detection_time = timedelta(seconds=entry_timing['imageai_colour_detection_time']),
				total_imageai_analysis_time = timedelta(seconds=entry_timing['total_imageai_analysis_time']),
				pr_detection_time = timedelta(seconds=entry_timing['pr_detection_time']),
				pr_prediction_time = timedelta(seconds=entry_timing['pr_prediction_time']),
				pr_colour_detection_time = timedelta(seconds=entry_timing['pr_colour_detection_time']),
				total_pr_analysis_time = timedelta(seconds=entry_timing['total_pr_analysis_time']),
				total_image_analysis_time = timedelta(seconds=entry_timing['total_image_analysis_time']),
				entry_total_time = timedelta(seconds=entry_timing['entry_total_time'])
			)
			entry_times.save()

			
			#Data for JSON File
			entry_data_for_json = {
						"analysis":{
									"arrival_time": entry_arrival_string,
									"departure_time": entry_departure_string,
									"duration": {
													'in_seconds': entry_analysis['duration']['in_seconds'],
													'human_readable': entry_analysis['duration']['human_readable']
												},
									"images": {
												'original' : entry_original_image.image.url,
												'extracted_imageai' : entry_extracted_imageai_image.image.url,
												'extracted_pr' : entry_extracted_pr_image.image.url,
												'extracted_plate' : entry_extracted_plate_image.image.url,
												'analyzed_comparison' : entry_analyzed_comparison_image.image.url,
												'analyzed_imageai' : entry_analyzed_imageai_image.image.url,
												'analyzed_pr' : entry_analyzed_pr_image.image.url
									},
									"successful_feed_in" : entry_successful_feed_in,
									"imageai_analysis": entry_imageai_analysis,
									"pr_analysis": entry_pr_analysis
						},
						"timing": entry_timing
					}

			analysis.append(entry_data_for_json)

		#save the total times to the database
		day_timing = DayTiming(
			day = day,
			total_get_duration_time = timedelta(seconds=input_total_times['total_get_duration_time']),
			total_imageai_detection_time = timedelta(seconds=input_total_times['total_imageai_detection_time']),
			total_imageai_prediction_time = timedelta(seconds=input_total_times['total_imageai_prediction_time']),
			total_imageai_colour_detection_time = timedelta(seconds=input_total_times['total_imageai_colour_detection_time']),
			total_imageai_analysis_time = timedelta(seconds=input_total_times['total_imageai_analysis_time']),
			total_pr_detection_time = timedelta(seconds=input_total_times['total_pr_detection_time']),
			total_pr_prediction_time = timedelta(seconds=input_total_times['total_pr_prediction_time']),
			total_pr_colour_detection_time = timedelta(seconds=input_total_times['total_pr_colour_detection_time']),
			total_pr_analysis_time = timedelta(seconds=input_total_times['total_pr_analysis_time']),
			total_image_analysis_time = timedelta(seconds=input_total_times['total_image_analysis_time']),
			total_entry_time = timedelta(seconds=input_total_times['total_entry_time']),
			total_day_time = timedelta(seconds=input_total_times['total_day_time'])
		)
		day_timing.save()

		json_data = {
			"tried_feed_in": tried_feed_in,
			"analysis" : analysis,
			"total_times" : input_total_times,
			"log_file": day.log_file.name
		}

		#make the JSON file
		json_relative_path = os.path.join(MakeDay.json_dir, f'{self.date_str}.json')
		file = ContentFile(json.dumps(json_data, indent=4))
		new_json_file = File(file=file, name=json_relative_path) 
		day.json_file = new_json_file 
		day.save() 

		#remove the old sub dir
		shutil.rmtree(self.path_to_date_dir, ignore_errors=True)

		return self.date_str


class CheckPsaTables():  
	"""Collects the data for all the psa tables.  
	Data includes:  
	name (table name)  
	status ('needs to be populated' or 'populated')  
	actions (['populate_table'] if the status is 'needs to be populated')
	"""

	def check_psa_tables():
		"""Collects the data for all the psa tables.  
		"""
		psa_tables = []
		"""(list) a list of dictionaries each containing the table's name, status and actions."""

		if not Colour.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'Colour', 'status' : status, 'actions' : actions}
		psa_tables.append(data)

		if not ImageAngle.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'ImageAngle', 'status' : status, 'actions' : actions}
		psa_tables.append(data)

		if not ImageType.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'ImageType', 'status' : status, 'actions' : actions}
		psa_tables.append(data)

		if not IaiDetectionClass.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'IaiDetectionClass', 'status' : status, 'actions' : actions}
		psa_tables.append(data)

		if not PrDetectionClass.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'PrDetectionClass', 'status' : status, 'actions' : actions}
		psa_tables.append(data)

		if not PredictionClass.objects.all():
			status = 'needs to be populated'
			actions = ['populate_table']
		else:
			status = 'populated'
			actions = []
		data = {'name' : 'PredictionClass', 'status' : status, 'actions' : actions}
		psa_tables.append(data)
		
		return psa_tables

class PopulatePsaTables(): 
	"""Uses the files in the json_for_psa_models directory to populate the psa tables.   
	"""

	json_for_psa_models_dir = 'json_for_psa_models'
	"""(str) the path to the json_for_psa_models directory from the project's parent dir (json_for_psa_models)"""

	def populate_colour():
		"""Populates the psa.Colour table using the json_for_psa_models/colours.json file

		:return: the number of :class:`psa.models.Colour` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'colours.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = Colour(name=obj_data['name'], r=obj_data['R'], g=obj_data['G'], b=obj_data['B'], hex=obj_data['hex'])
			new_obj.save()

		return len(json_data)

	def populate_image_angle():
		"""Populates the psa.ImageAngle table using the json_for_psa_models/image_angles.json file

		:return: the number of :class:`psa.models.ImageAngle` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'image_angles.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = ImageAngle(name=obj_data['name'])
			new_obj.save()

		return len(json_data)

	def populate_image_type():
		"""Populates the psa.ImageType table using the json_for_psa_models/image_types.json file

		:return: the number of :class:`psa.models.ImageType` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'image_types.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = ImageType(name=obj_data['name'])
			new_obj.save()

		return len(json_data)

	def populate_iai_detection_class():
		"""Populates the psa.IaiDetectionClass table using the json_for_psa_models/imageai_detection_classes.json file

		:return: the number of :class:`psa.models.IaiDetectionClass` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'imageai_detection_classes.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = IaiDetectionClass(name=obj_data['name'])
			new_obj.save()

		return len(json_data)

	def populate_pr_detection_class():
		"""Populates the psa.PrDetectionClass table using the json_for_psa_models/pr_detection_classes.json file

		:return: the number of :class:`psa.models.PrDetectionClass` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'pr_detection_classes.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = PrDetectionClass(name=obj_data['name'])
			new_obj.save()

		return len(json_data)

	def populate_prediction_class():
		"""Populates the psa.PredictionClass table using the json_for_psa_models/prediction_classes.json file

		:return: the number of :class:`psa.models.PredictionClass` objects created and added to the table  
		:rtype: int 
		"""

		json_file_path = os.path.join(PopulatePsaTables.json_for_psa_models_dir, 'prediction_classes.json')

		with open(json_file_path, 'r') as json_file:
			json_data = json.load(json_file)

		for obj_data in json_data:
			new_obj = PredictionClass(name=obj_data['name'])
			new_obj.save()

		return len(json_data)

	def populate_all():
		"""Populates all unpopulated tables using the files in the json_for_psa_models directory. Does this by checking each table to see if it is populated, 
		then if it is unpopulated the correct method from the :class:`psa.PopulatePsaTables` class is called.

		:return: a dictionary containing the number of tables that were populated and the number of values added to the tables  
		:rtype: dict 
		"""

		table_count = 0
		value_count = 0

		if not Colour.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_colour()

		if not ImageAngle.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_image_angle()

		if not ImageType.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_image_type()

		if not IaiDetectionClass.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_iai_detection_class()

		if not PrDetectionClass.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_pr_detection_class()

		if not PredictionClass.objects.all():
			table_count += 1
			value_count += PopulatePsaTables.populate_prediction_class()
			
		return {'table_count' : table_count, 'value_count' : value_count}