import django_tables2 as tables
from django.db.models.functions import Length
from django_tables2.utils import A

from analysis.models import Entry

class DatesTable(tables.Table):
    """Builds the table needed by the analysis/home.html template. Subclass of :class:`django_tables2.tables.Table`. 
    """

    date = tables.columns.TemplateColumn(template_code=u"""{{ record.date }}""", orderable=True, verbose_name='Date')
    """(TemplateColumn) the day's date (orderable)"""

    status = tables.columns.TemplateColumn(template_code=u"""{{ record.status }}""", orderable=True, verbose_name='Status')
    """(TemplateColumn) the day's status (orderable)"""

    actions = tables.columns.TemplateColumn(template_name='psa/partials/actions_column.html', orderable=False, verbose_name='Actions')
    """(TemplateColumn) the actions that can be performed with the day's data"""

    
    def render_date(self, record):
        """Checks the day's status to determine what class the table data element (\<td\>...\</td\>) should have. It also appends '(tried feed in)' to the date, 
        if the status is 'complete' or 'needs manual analysis' and an attempt was made to feed the image extracted during Imageai analysis into the Plate Recognizer 
        analysis.
        """

        if record['status'] == 'complete':
            self.attrs={"td":{ "class": "table-success"}}
            if record['tried_feed_in'] == True:
                record['date'] = f"{record['date']} (tried feed in)"
        elif record['status'] == 'needs manual analysis':
            self.attrs={"td":{ "class": "table-warning bg-warning"}}
            if record['tried_feed_in'] == True:
                record['date'] = f"{record['date']} (tried feed in)"
        elif record['status'] == 'not in database':
            self.attrs={"td":{ "class": "table-danger bg-danger"}}
        elif record['status'] == 'needs automated analysis':
            self.attrs={"td":{ "class": "table-dark"}}
        return record['date']


    class Meta:
        """Adds metadata about the table.  
        """

        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('date', 'status', 'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-date', )
        """(tuple) the default order of the data (descending dates)"""

        per_page = 10
        """(int) the number of entries per page (10)"""


class PsaTable(tables.Table):
    """Builds the table needed by the psa/populate_psa_tables.html template. Subclass of :class:`django_tables2.tables.Table`. 
    """
    name = tables.columns.TemplateColumn(template_code=u"""{{ record.name }}""", orderable=True, verbose_name='Name')
    """(TemplateColumn) the name of the psa table (orderable)"""

    status = tables.columns.TemplateColumn(template_code=u"""{{ record.status }}""", orderable=False, verbose_name='Status')
    """(TemplateColumn) the psa table's status (needs to be populated' or 'populated') (orderable)"""

    actions = tables.columns.TemplateColumn(template_name='psa/partials/actions_column.html', orderable=False, verbose_name='Actions')
    """(TemplateColumn) the actions that can be performed with the psa table's data"""

    
    def render_status(self, record):
        """Checks the psa table's status to determine what class the table data element (\<td\>...\</td\>) should have. 
        """
        
        if record['status'] == 'needs to be populated':
            self.attrs={"td":{ "class": "table-danger bg-danger"}}
        elif record['status'] == 'populated':
            self.attrs={"td":{ "class": "table-success"}}
        return record['status']

    class Meta:
        """Adds metadata about the table. 
        """

        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""



class DayAutomatedAnalysisTable(tables.Table):
    """Builds the table with entry data needed by the automated/day_detail.html template. Subclass of :class:`django_tables2.tables.Table`.
    """
    
    successful_feed_in = tables.columns.BooleanColumn(null=True, accessor='automatedanalysis.successful_feed_in', orderable=True, verbose_name='Successful Feed In')
    """(BooleanColumn) a nullable boolean column"""

    status = tables.columns.TemplateColumn(template_code=u"""{{ record.status }}""", orderable=True, verbose_name='Status')
    """(TemplateColumn) the entry's status (orderable)"""

    arrival_time = tables.columns.TemplateColumn(template_code=u"""{{ record.arrival_time }}""", orderable=True, verbose_name='Arrival Time')
    """(TemplateColumn) the entry's arrival time (orderable)"""

    departure_time = tables.columns.TemplateColumn(template_code=u"""{{ record.departure_time }}""", orderable=True, verbose_name='Departure Time')
    """(TemplateColumn) the entry's departure time (orderable)"""

    duration = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.duration }}""", accessor='automatedanalysis.duration', orderable=True, verbose_name='Duration (H:M:S)')
    """(TemplateColumn) the entry's duration (automated) (orderable)"""

    iai_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.iaivehicledetection.type }}""", accessor='automatedanalysis.iaivehicledetection.type', orderable=True, verbose_name='Iai Detection Class')
    """(TemplateColumn) the entry's IAI detection class (automated) (orderable)"""

    iai_prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.iaivehicleprediction.type }}""", accessor='automatedanalysis.iaivehicleprediction.type', orderable=True, verbose_name='Iai Prediction Class')
    """(TemplateColumn) the entry's IAI prediction class (automated) (orderable)"""

    pr_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.prvehicledetection.type }}""", accessor='automatedanalysis.prvehicledetection.type', orderable=True, verbose_name='Pr Detection Class')
    """(TemplateColumn) the entry's PR detection class (automated) (orderable)"""

    pr_prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.prvehicleprediction.type }}""", accessor='automatedanalysis.prvehicleprediction.type', orderable=True, verbose_name='Pr Prediction Class')
    """(TemplateColumn) the entry's PR prediction class (automated) (orderable)"""

    plate = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.licenseplate.number }}""", accessor='automatedanalysis.licenseplate.number', orderable=True, verbose_name='Plate')
    """(TemplateColumn) the entry's license plate number (automated) (orderable)"""

    actions = tables.columns.TemplateColumn(template_name='psa/partials/actions_column.html', orderable=False, verbose_name='Actions')
    """(TemplateColumn) the actions that can be performed with the entry's data"""

    
    def render_arrival_time(self, record):
        """Renders the arrival time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.arrival_time.strftime('%I:%M:%S %p')
    
    def render_departure_time(self, record):
        """Renders the departure time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.departure_time.strftime('%I:%M:%S %p')

    def render_status(self, record):
        """Checks the entry's status to determine what class the table data element (\<td\>...\</td\>) should have. 
        """
        
        if record.status == 'complete':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'view_manual_analysis']
            self.attrs={"td":{ "class": "table-success"}}
        elif record.status == 'needs manual analysis':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'make_manual_analysis']
            self.attrs={"td":{ "class": "table-warning bg-warning"}}
        return record.status

    
    def order_iai_prediction_class(self, queryset, is_descending):
        """Handles the reordering of the IAI Prediction Class column
        """
        queryset = queryset.order_by(("-" if is_descending else "") + "automatedanalysis__iaivehicleprediction__type__name")
        return (queryset, True)

    def order_pr_prediction_class(self, queryset, is_descending):
        """Handles the reordering of the PR Prediction Class column
        """
        queryset = queryset.order_by(("-" if is_descending else "") + "automatedanalysis__prvehicleprediction__type__name")
        return (queryset, True)

    class Meta:
        """Adds metadata about the table.
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('successful_feed_in', 'status', 'arrival_time', 'departure_time', 'duration', 'iai_detection_class', 'iai_prediction_class',
                    'pr_detection_class', 'pr_prediction_class','plate', 'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-arrival_time')
        """(tuple) the default order of the data (descending arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""


class AutomatedAnalysisTable(DayAutomatedAnalysisTable):
    """Builds the table with entry data needed by the automated/home.html template. Subclass of :class:`DayAutomatedAnalysisTable`.
    """

    day = tables.columns.LinkColumn('automated-day-detail', kwargs={'pk': A('day.id')}, orderable=True, verbose_name='Day')
    """(LinkColumn) the day associated with the entry (clickable link to the 'automated-day-detail page) (orderable)"""

    tried_feed_in = tables.columns.BooleanColumn(null=True, accessor='day.tried_feed_in', orderable=True, verbose_name='Tried Feed In')
    """(BooleanColumn) a nullable boolean column (orderable)"""


    class Meta:
        """Adds metadata about the table.  
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('day', 'tried_feed_in', 'successful_feed_in', 'status', 'arrival_time', 'departure_time', 'duration', 'iai_detection_class', 'iai_prediction_class', 
                    'pr_detection_class', 'pr_prediction_class', 'plate', 'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-day', '-arrival_time')
        """(tuple) the default order of the data (descending dates and arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""



class DayManualAnalysisTable(tables.Table):
    """Builds the table with entry data needed by the manual/day_detail.html template. Subclass of :class:`django_tables2.tables.Table`.
    """
    
    successful_feed_in = tables.columns.BooleanColumn(null=True, accessor='automatedanalysis.successful_feed_in', orderable=True, verbose_name='Successful Feed In')
    """(BooleanColumn) a nullable boolean column"""
    
    status = tables.columns.TemplateColumn(template_code=u"""{{ record.status }}""", orderable=True, verbose_name='Status')
    """(TemplateColumn) the entry's status (orderable)"""
    
    arrival_time = tables.columns.TemplateColumn(template_code=u"""{{ record.arrival_time }}""", orderable=True, verbose_name='Arrival Time')
    """(TemplateColumn) the entry's arrival time (orderable)"""
    
    departure_time = tables.columns.TemplateColumn(template_code=u"""{{ record.departure_time }}""", orderable=True, verbose_name='Departure Time')
    """(TemplateColumn) the entry's departure time (orderable)"""
    
    duration = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.duration }}""", accessor='manualanalysis.duration', orderable=True, verbose_name='Duration (H:M:S)')
    """(TemplateColumn) the entry's duration (manual) (orderable)"""
    
    iai_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.iai_detection_class }}""", accessor='manualanalysis.iai_detection_class', orderable=True, verbose_name='Iai Detection Class')
    """(TemplateColumn) the entry's IAI detection class (manual) (orderable)"""
    
    pr_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.pr_detection_class }}""", accessor='manualanalysis.pr_detection_class', orderable=True, verbose_name='Pr Detection Class')
    """(TemplateColumn) the entry's PR detection class (manual) (orderable)"""
    
    prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.prediction_class }}""", accessor='manualanalysis.prediction_class', orderable=True, verbose_name='Prediction Class')
    """(TemplateColumn) the entry's Prediction class (manual) (orderable)"""
    
    license_plate_number = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.license_plate_number }}""", accessor='manualanalysis.license_plate_number', orderable=True, verbose_name='Plate')
    """(TemplateColumn) the entry's license plate number (manual) (orderable)"""
    
    dominant_colour = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.dominant_colour }}""", accessor='manualanalysis.dominant_colour', orderable=True, verbose_name='Dominant Colour')
    """(TemplateColumn) the name of the dominant colour (orderable)"""
    
    dominant_colour_detected_by_iai = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.dominant_colour_detected_by_iai', orderable=True, verbose_name='Dominant Colour detected by IAI')
    """(BooleanColumn) True/False value if the dominant colour was deteted in the Imageai analysis (orderable)"""
    
    dominant_colour_identified_as_dominant_by_iai = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.dominant_colour_identified_as_dominant_by_iai', orderable=True, verbose_name='Dominant Colour identified as dominant by IAI')
    """(BooleanColumn) True/False value if the dominant colour was identified as dominant in the Imageai analysis; None if no colours were identified (orderable)  """
    
    dominant_colour_detected_by_pr = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.dominant_colour_detected_by_pr', orderable=True, verbose_name='Dominant Colour detected by PR')
    """(BooleanColumn) True/False value if the dominant colour was deteted in the PR analysis (orderable)"""
    
    dominant_colour_identified_as_dominant_by_pr = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.dominant_colour_identified_as_dominant_by_pr', orderable=True, verbose_name='Dominant Colour identified as dominant by PR')
    """(BooleanColumn) True/False value if the dominant colour was identified as dominant in the PR analysis; None if no colours were identified (orderable)  """
    
    image_angle = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.image_angle }}""", accessor='manualanalysis.image_angle', orderable=True, verbose_name='Image angle')
    """(TemplateColumn) the andle of the object in the image (orderable)"""
    
    iai_extract_correct = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.iai_extract_correct', orderable=True, verbose_name='IAI extract correct')
    """(BooleanColumn) True/False if the correct object was extracted in the Imageai analysis; None if no object was extracted (orderable)  """
    
    pr_extract_correct = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.pr_extract_correct', orderable=True, verbose_name='PR extract correct')
    """(BooleanColumn) True/False if the correct object was extracted in the PR analysis; None if no object was extracted (orderable)"""
    
    plate_extract_correct = tables.columns.BooleanColumn(null=True, accessor='manualanalysis.plate_extract_correct', orderable=True, verbose_name='Plate extract correct')
    """(BooleanColumn) True/False if the correct plate was extracted in the PR analysis; None if no plate was extracted (orderable)"""
    
    actions = tables.columns.TemplateColumn(template_name='psa/partials/actions_column.html', orderable=False, verbose_name='Actions')
    """(TemplateColumn) the actions that can be performed with the entry's data"""
    
    
    def render_arrival_time(self, record):
        """Renders the arrival time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.arrival_time.strftime('%I:%M:%S %p')
        
    
    def render_departure_time(self, record):
        """Renders the departure time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.departure_time.strftime('%I:%M:%S %p')

    def render_status(self, record):
        """Checks the entry's status to determine what class the table data element (\<td\>...\</td\>) should have. 
        """        
        
        if record.status == 'complete':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'view_manual_analysis']
            self.attrs={"td":{ "class": "table-success"}}
        elif record.status == 'needs manual analysis':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'make_manual_analysis']
            self.attrs={"td":{ "class": "table-warning bg-warning"}}
        return record.status


    class Meta:
        """Adds metadata about the table. 
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('successful_feed_in', 'status', 'arrival_time', 'departure_time', 'duration', 'iai_detection_class', 'pr_detection_class', 'prediction_class',
                'license_plate_number', 'dominant_colour', 'dominant_colour_detected_by_iai', 'dominant_colour_identified_as_dominant_by_iai',
                'dominant_colour_detected_by_pr', 'dominant_colour_identified_as_dominant_by_pr', 'image_angle', 'iai_extract_correct', 'pr_extract_correct', 'plate_extract_correct', 'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-arrival_time')
        """(tuple) the default order of the data (descending arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""


class ManualAnalysisTable(DayManualAnalysisTable):
    """Builds the table with entry data needed by the manual/home.html template. Subclass of :class:`DayManualAnalysisTable`.  
    """
    
    day = tables.columns.LinkColumn('manual-day-detail', kwargs={'pk': A('day.id')}, orderable=True, verbose_name='Day')
    """(LinkColumn) the day associated with the entry (clickable link to the 'manual-day-detail page) (orderable)"""

    tried_feed_in = tables.columns.BooleanColumn(null=True, accessor='day.tried_feed_in', orderable=True, verbose_name='Tried Feed In')
    """(BooleanColumn) a nullable boolean column (orderable)"""


    class Meta:
        """Adds metadata about the table. 
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('day', 'tried_feed_in', 'successful_feed_in', 'status', 'arrival_time', 'departure_time', 'duration', 'iai_detection_class', 'pr_detection_class', 'prediction_class',
                'license_plate_number', 'dominant_colour', 'dominant_colour_detected_by_iai', 'dominant_colour_identified_as_dominant_by_iai',
                'dominant_colour_detected_by_pr', 'dominant_colour_identified_as_dominant_by_pr', 'image_angle', 'iai_extract_correct', 'pr_extract_correct', 'plate_extract_correct', 'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-day', '-arrival_time')
        """(tuple) the default order of the data (descending dates and arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""



class DayAnalysisTable(tables.Table):
    """Builds the table with entry data needed by the analysis/day_detail.html template. Subclass of :class:`django_tables2.tables.Table`. 
    """

    successful_feed_in = tables.columns.BooleanColumn(null=True, accessor='automatedanalysis.successful_feed_in', orderable=True, verbose_name='Successful Feed In')
    """(BooleanColumn) a nullable boolean column"""

    status = tables.columns.TemplateColumn(template_code=u"""{{ record.status }}""", orderable=True, verbose_name='Status')
    """(TemplateColumn) the entry's status (orderable)"""

    arrival_time = tables.columns.TemplateColumn(template_code=u"""{{ record.arrival_time }}""", orderable=True, verbose_name='Arrival Time')
    """(TemplateColumn) the entry's arrival time (orderable)"""

    departure_time = tables.columns.TemplateColumn(template_code=u"""{{ record.departure_time }}""", orderable=True, verbose_name='Departure Time')
    """(TemplateColumn) the entry's departure time (orderable)"""

    automated_duration = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.duration }}""", accessor='automatedanalysis.duration', orderable=True, verbose_name='Automated Duration (H:M:S)')
    """(TemplateColumn) the duration from the automated analysis (orderable)"""

    automated_iai_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.iaivehicledetection.type }}""", accessor='automatedanalysis.iaivehicledetection.type', orderable=True, verbose_name='Automated Iai Detection Class')
    """(TemplateColumn) the IAI Detection class from the automated analysis (orderable)"""

    automated_iai_prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.iaivehicleprediction.type }}""", accessor='automatedanalysis.iaivehicleprediction.type', orderable=True, verbose_name='Automated Iai Prediction Class')
    """(TemplateColumn) the Prediction class from the Imageai automated analysis (orderable)"""

    automated_pr_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.prvehicledetection.type }}""", accessor='automatedanalysis.prvehicledetection.type', orderable=True, verbose_name='Automated Pr Detection Class')
    """(TemplateColumn) the PR Detection class from the automated analysis (orderable)"""

    automated_pr_prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.prvehicleprediction.type }}""", accessor='automatedanalysis.prvehicleprediction.type', orderable=True, verbose_name='Automated Pr Prediction Class')
    """(TemplateColumn) the Prediction class from the PR automated analysis (orderable)"""

    automated_plate = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.licenseplate.number }}""", accessor='automatedanalysis.licenseplate.number', orderable=True, verbose_name='Automated Plate')
    """(TemplateColumn) the plate number from the automated analysis (orderable)"""

    manual_duration = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.duration }}""", accessor='manualanalysis.duration', orderable=True, verbose_name='Manual Duration (H:M:S)')
    """(TemplateColumn) the duration from the manual analysis (orderable)"""

    manual_iai_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.iai_detection_class }}""", accessor='manualanalysis.iai_detection_class', orderable=True, verbose_name='Manual Iai Detection Class')
    """(TemplateColumn) the IAI Detection class from the manual analysis (orderable)"""

    manual_pr_detection_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.pr_detection_class }}""", accessor='manualanalysis.pr_detection_class', orderable=True, verbose_name='Manual Pr Detection Class')
    """(TemplateColumn) the PR Detection class from the manual analysis (orderable)"""

    manual_prediction_class = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.prediction_class }}""", accessor='manualanalysis.prediction_class', orderable=True, verbose_name='Manual Prediction Class')
    """(TemplateColumn) the Prediction class from the manual analysis (orderable)"""

    manual_license_plate_number = tables.columns.TemplateColumn(template_code=u"""{{ record.manualanalysis.license_plate_number }}""", accessor='manualanalysis.license_plate_number', orderable=True, verbose_name='Manual Plate')
    """(TemplateColumn) the plate number from the manual analysis (orderable)"""

    actions = tables.columns.TemplateColumn(template_name='psa/partials/actions_column.html', orderable=False, verbose_name='Actions')
    """(TemplateColumn) the actions that can be performed with the entry's data"""

    
    def render_arrival_time(self, record):
        """Renders the arrival time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.arrival_time.strftime('%I:%M:%S %p')
    
    def render_departure_time(self, record):
        """Renders the departure time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM)  
        """
        return record.departure_time.strftime('%I:%M:%S %p')

    def render_status(self, record):
        """Checks the entry's status to determine what class the table data element (\<td\>...\</td\>) should have. 
        """
        
        if record.status == 'complete':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'view_manual_analysis']
            self.attrs={"td":{ "class": "table-success"}}
        elif record.status == 'needs manual analysis':
            record.actions = ['view_analysis_summary', 'view_auto_analysis', 'make_manual_analysis']
            self.attrs={"td":{ "class": "table-warning bg-warning"}}
        return record.status

    
    def order_automated_iai_prediction_class(self, queryset, is_descending):
        """Handles the reordering of the Automated IAI Prediction Class column
        """
        
        queryset = queryset.order_by(("-" if is_descending else "") + "automatedanalysis__iaivehicleprediction__type__name")
        return (queryset, True)

    def order_automated_pr_prediction_class(self, queryset, is_descending):
        """Handles the reordering of the Automated PR Prediction Class column
        """
        
        queryset = queryset.order_by(("-" if is_descending else "") + "automatedanalysis__prvehicleprediction__type__name")
        return (queryset, True)

    def order_manual_prediction_class(self, queryset, is_descending):
        """Handles the reordering of the Manual Prediction Class column
        """

        queryset = queryset.order_by(("-" if is_descending else "") + "manualanalysis__prediction_class__name")
        return (queryset, True)

    class Meta:
        """Adds metadata about the table.
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('successful_feed_in', 'status', 'arrival_time', 'departure_time', 
                'automated_duration', 'automated_iai_detection_class', 'automated_iai_prediction_class', 
                'automated_pr_detection_class', 'automated_pr_prediction_class', 'automated_plate',
                'manual_duration', 'manual_iai_detection_class', 'manual_pr_detection_class', 'manual_prediction_class','manual_license_plate_number',
                'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-arrival_time')
        """(tuple) the default order of the data (descending arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""


class AnalysisTable(DayAnalysisTable):
    """Builds the table with entry data needed by the analysis/home.html template. Subclass of :class:`DayAnalysisTable`.  
    """

    day = tables.columns.LinkColumn('analysis-day-detail', kwargs={'pk': A('day.id')}, orderable=True, verbose_name='Day')
    """(LinkColumn) the day associated with the entry (clickable link to the 'analysis-day-detail page) (orderable)"""

    tried_feed_in = tables.columns.BooleanColumn(null=True, accessor='day.tried_feed_in', orderable=True, verbose_name='Tried Feed In')
    """(BooleanColumn) a nullable boolean column (orderable)"""

    
    class Meta:
        """Adds metadata about the table.
        """
        
        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('day', 'tried_feed_in', 'successful_feed_in', 'status', 'arrival_time', 'departure_time', 
                'automated_duration', 'automated_iai_detection_class', 'automated_iai_prediction_class', 
                'automated_pr_detection_class', 'automated_pr_prediction_class', 'automated_plate',
                'manual_duration', 'manual_iai_detection_class', 'manual_pr_detection_class', 'manual_prediction_class','manual_license_plate_number',
                'actions')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-day', '-arrival_time')
        """(tuple) the default order of the data (descending dates and arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""


class EntryTimesTable(tables.Table):
    """Builds the table with entry timing data needed by the psa/partials/entry_timing_div.html template. Subclass of :class:`django_tables2.tables.Table`.      
    """

    arrival_time = tables.columns.TemplateColumn(template_code=u"""{{ record.arrival_time }}""", orderable=True, verbose_name='Arrival Time')
    """(TemplateColumn) the entry's arrival time (orderable)"""

    get_duration_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.get_duration_time }}""", accessor='automatedanalysis.timing.get_duration_time', orderable=True, verbose_name='Get duration time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.get_duration` function (orderable)"""

    imageai_detection_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.imageai_detection_time }}""", accessor='automatedanalysis.timing.imageai_detection_time', orderable=True, verbose_name='IAI vehicle detection time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.imageai_detect` function (orderable)"""

    imageai_prediction_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.imageai_prediction_time }}""", accessor='automatedanalysis.timing.imageai_prediction_time', orderable=True, verbose_name='IAI vehicle prediction time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the Imageai extracted image (orderable)  """

    imageai_colour_detection_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.imageai_colour_detection_time }}""", accessor='automatedanalysis.timing.imageai_colour_detection_time', orderable=True, verbose_name='IAI colour detection time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.detect_colours` function  when called with the Imageai extracted image (orderable)  """

    total_imageai_analysis_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.total_imageai_analysis_time }}""", accessor='automatedanalysis.timing.total_imageai_analysis_time', orderable=True, verbose_name='Total IAI analysis time')
    """(TemplateColumn) time taken for Imageai analysis (orderable)"""

    pr_detection_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.pr_detection_time }}""", accessor='automatedanalysis.timing.pr_detection_time', orderable=True, verbose_name='PR vehicle detection time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.plate_recognizer_detect` function (orderable)"""

    pr_prediction_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.pr_prediction_time }}""", accessor='automatedanalysis.timing.pr_prediction_time', orderable=True, verbose_name='PR vehicle prediction time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.imageai_predict` function  when called with the  plate recognizer extracted image (orderable)  """

    pr_colour_detection_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.pr_colour_detection_time }}""", accessor='automatedanalysis.timing.pr_colour_detection_time', orderable=True, verbose_name='PR colour detection time')
    """(TemplateColumn) time taken to run the :func:`spot_analysis.detect_colours` function  when called with the  plate recognizer extracted image (orderable)  """

    total_pr_analysis_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.total_pr_analysis_time }}""", accessor='automatedanalysis.timing.total_pr_analysis_time', orderable=True, verbose_name='Total PR analysis time')
    """(TemplateColumn) time taken for Plate Recognizer analysis (orderable)"""

    total_image_analysis_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.total_image_analysis_time }}""", accessor='automatedanalysis.timing.total_image_analysis_time', orderable=True, verbose_name='Total Image analysis time')
    """(TemplateColumn) total time to analyze all the image (orderable)"""

    entry_total_time = tables.columns.TemplateColumn(template_code=u"""{{ record.automatedanalysis.timing.entry_total_time }}""", accessor='automatedanalysis.timing.entry_total_time', orderable=True, verbose_name='Total Entry time')
    """(TemplateColumn) total time to analyze the entry (orderable)"""

    
    def render_arrival_time(self, record):
        """Renders the arrival time in the following format = '%I:%M:%S %p' = 
        Hour (12-hour clock) as a zero-padded decimal number : Minute as a zero-padded decimal number : Second as a zero-padded decimal number : either AM or PM). 
        It also uses the entry's status to determine what class the table data element (\<td\>...\</td\>) should have.
        """
        if record.status == 'complete':
            self.attrs={"td":{ "class": "table-success"}}
        elif record.status == 'needs manual analysis':
            self.attrs={"td":{ "class": "table-warning bg-warning"}}
        return record.arrival_time.strftime('%I:%M:%S %p')

    class Meta:
        """Adds metadata about the table.   
        """

        attrs = {
                    'class': 'table table-condensed table-vertical-center ', 'id': 'day_automated_analysis_table',
                    'th': {"class": 'text-center'},
                }
        """(dict) table attributes such as class, id and th class"""

        row_attrs = {"class": 'text-center'}
        """(dict) row attributes"""

        fields = ('arrival_time','get_duration_time',
                'imageai_detection_time', 'imageai_prediction_time', 'imageai_colour_detection_time', 'total_imageai_analysis_time',
                'pr_detection_time', 'pr_prediction_time', 'pr_colour_detection_time', 'total_pr_analysis_time',
                'total_image_analysis_time', 'entry_total_time')
        """(tuple) the fields in the table"""

        sequence = fields
        """(tuple) the sequence of the fields in the tables"""

        order_by = ('-arrival_time')
        """(tuple) the default order of the data (descending arrival times)"""

        per_page = 10
        """(int) the number of entries per page (10)"""

