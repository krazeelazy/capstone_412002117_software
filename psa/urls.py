from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='psa-home'),
    path('add_day/<path:sub_dir>/', views.add_day, name='psa-add-day'),
    path('add_days/', views.add_days, name='psa-add-days'),
    path('open_file/<path:filepath>/', views.open_file, name='psa-open-file'),
    path('populate_tables/', views.populate_tables, name='psa-populate-tables'),
    path('auto_analyze_day/', views.auto_analyze_day, name='psa-auto-analyze-day'),
    path('auto_analyze_days/', views.auto_analyze_days, name='psa-auto-analyze-days'),
]