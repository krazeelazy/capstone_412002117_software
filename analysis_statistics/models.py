"""Has all the queries and functions to get the data the view needs to pass to the template.
"""

from django.db import models

from analysis.models import Entry
from psa.models import IaiDetectionClass, PredictionClass, PrDetectionClass, Colour, ImageAngle
from automated.models import (
    IaiVehicleDetection, 
    IaiVehiclePrediction, 
    PrVehicleDetection, 
    PrVehiclePrediction
    )

import random as rng

from datetime import timedelta

class GetStatsContext():
    """Gets the context data the view needs
    """
    def get_stats_context(context_days):
        """Gets the title of the page and performs queries and calculations to get the data needed for the tables and graphs 

        :param context_days: the days that are having their data displayed  
        :type context_days: list  
        :return: a dictionary containing the title and all the needed data  
        :rtype: dict  
        """

        response = {}
        
        if context_days.count() == 0:
            response['title'] = 'Statistics'
        else:
            if context_days.count() == 1:
                response['title'] = f'{context_days[0]} Statistics'
            elif context_days.count() > 1:
                response['title'] = f'{context_days.first()} - {context_days.last()} Statistics'

            date_labels = []
            day_objs = []
            number_of_entries_data = {}

            rng.seed(234567891)

            iai_individual_vehicle_detection_auto_names = []
            iai_individual_vehicle_detection_auto_counts = {}              
            iai_individual_vehicle_detection_manual_names = []
            iai_individual_vehicle_detection_manual_counts = {}
            iai_vehicle_detection_chart_colours={}
            iai_vehicle_detection_accuracies={}

            iai_individual_vehicle_prediction_auto_names = []
            iai_individual_vehicle_prediction_auto_counts = {}              
            iai_individual_vehicle_prediction_manual_names = []
            iai_individual_vehicle_prediction_manual_counts = {}
            iai_vehicle_prediction_chart_colours={}
            iai_vehicle_prediction_accuracies={}


            pr_individual_vehicle_detection_auto_names = []
            pr_individual_vehicle_detection_auto_counts = {}              
            pr_individual_vehicle_detection_manual_names = []
            pr_individual_vehicle_detection_manual_counts = {}
            pr_vehicle_detection_chart_colours={}
            pr_vehicle_detection_accuracies={}

            pr_individual_vehicle_prediction_auto_names = []
            pr_individual_vehicle_prediction_auto_counts = {}              
            pr_individual_vehicle_prediction_manual_names = []
            pr_individual_vehicle_prediction_manual_counts = {}
            pr_vehicle_prediction_chart_colours={}
            pr_vehicle_prediction_accuracies={}


            durations_auto = {}
            durations_manual = {}
            durations_auto_stats={}
            durations_manual_stats={}


            iai_individual_colour_detection_auto_names = []
            iai_individual_colour_detection_auto_counts = {}      
            iai_individual_colour_detection_manual_names = []
            iai_individual_colour_detection_manual_counts = {}
            iai_colour_detection_chart_colours={}
            iai_colour_detection_accuracies={}
            iai_identified_dom_colour_as_nondom_count={}
            iai_identified_dom_colour_as_nondom_correct_count={}
            iai_identified_dom_colour_as_nondom_percentage={}

            pr_individual_colour_detection_auto_names = []
            pr_individual_colour_detection_auto_counts = {}              
            pr_individual_colour_detection_manual_names = []
            pr_individual_colour_detection_manual_counts = {}
            pr_colour_detection_chart_colours={}
            pr_colour_detection_accuracies={}
            pr_identified_dom_colour_as_nondom_count={}
            pr_identified_dom_colour_as_nondom_correct_count={}
            pr_identified_dom_colour_as_nondom_percentage={}


            correct_durations_counts = {}    
            correct_iai_vehicle_detection_counts = {}    
            correct_iai_vehicle_prediction_counts = {}
            correct_iai_colour_detection_counts = {}
            correct_pr_vehicle_detection_counts = {}
            correct_pr_vehicle_prediction_counts = {}
            correct_pr_colour_detection_counts = {}
            correct_license_plate_identification_counts = {}


            overall_correct_iai_vehicle_detection_count = 0    
            overall_correct_iai_vehicle_prediction_count = 0
            overall_correct_iai_colour_detection_count = 0
            overall_correct_pr_vehicle_detection_count = 0
            overall_correct_pr_vehicle_prediction_count = 0
            overall_correct_pr_colour_detection_count = 0

            overall_iai_identified_dom_colour_as_nondom_count = 0
            overall_iai_identified_dom_colour_as_nondom_correct_count = 0
            overall_pr_identified_dom_colour_as_nondom_count = 0
            overall_pr_identified_dom_colour_as_nondom_correct_count = 0


            tried_feed_in_counts = {}
            successful_feed_in_counts = {}
            successful_feed_in_percentages = {}
            failed_feed_in_counts = {}
            failed_feed_in_due_to_unextracted_iai_counts = {}
            failed_feed_in_due_to_unextracted_pr_counts = {}
            failed_feed_in_due_to_unextracted_iai_percentages = {}
            failed_feed_in_due_to_unextracted_pr_percentages = {}

            total_tried_feed_in_counts = {}
            total_successful_feed_in_counts = {}
            total_failed_feed_in_counts = {}
            total_failed_feed_in_due_to_unextracted_iai_counts = {}
            total_failed_feed_in_due_to_unextracted_pr_counts = {}
            daily_successful_feed_in_percentages = {}
            daily_failed_feed_in_due_to_unextracted_iai_percentages = {}
            daily_failed_feed_in_due_to_unextracted_pr_percentages = {}
            
            overall_tried_feed_in_count = 0
            overall_successful_feed_in_count = 0
            overall_failed_feed_in_count = 0
            overall_failed_feed_in_due_to_unextracted_iai_count = 0
            overall_failed_feed_in_due_to_unextracted_pr_count = 0


            image_angle_names = []
            image_angle_chart_colours = {}
            image_iai_extract_counts = {}
            image_pr_extract_counts = {}
            image_plate_extract_counts = {}
            correct_image_iai_extract_counts = {}
            correct_image_pr_extract_counts = {}
            correct_image_plate_extract_counts = {}
            complete_image_iai_extract_counts = {}
            complete_image_pr_extract_counts = {}
            complete_image_plate_extract_counts = {}
            total_image_iai_extract_counts = {}
            total_image_pr_extract_counts = {}
            total_image_plate_extract_counts = {}
            total_correct_image_iai_extract_counts = {}
            total_correct_image_pr_extract_counts = {}
            total_correct_image_plate_extract_counts = {}
            total_complete_image_iai_extract_counts = {}
            total_complete_image_pr_extract_counts = {}
            total_complete_image_plate_extract_counts = {}
            image_iai_extract_accuracies = {}
            image_pr_extract_accuracies = {}
            image_plate_extract_accuracies = {}
            complete_image_iai_extract_percentage = {}
            complete_image_pr_extract_percentage = {}
            complete_image_plate_extract_percentage = {}
            daily_complete_image_iai_extract_percentage = {}
            daily_complete_image_pr_extract_percentage = {}
            daily_complete_image_plate_extract_percentage = {}


            day_correct_counts = {}
            day_accuracies = {}
            day_accuracies_chart_colours = {}
            day_accuracies_keys = ['duration','iai_vehicle_detection', 'iai_vehicle_prediction', 'iai_colour_detection', 
                                'pr_vehicle_detection', 'pr_vehicle_prediction', 'pr_colour_detection',
                                'license_plate_identification', 'image_iai_extract', 'image_pr_extract', 'image_plate_extract'
                                ]

            day_accuracies_keys_exclusions = ['duration', 'license_plate_identification', 'image_iai_extract', 'image_pr_extract', 'image_plate_extract']
            
            td_classes_daily = {}
            td_classes_daily_totals = {}

            for key in day_accuracies_keys:
                day_accuracies[key] = {}
                td_classes_daily[key] = {}
                td_classes_daily_totals[key] = {}
                day_accuracies_chart_colours[key] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'
                if key not in day_accuracies_keys_exclusions:
                    day_correct_counts[key] = {}

            additional_td_keys_daily = ['complete_iai', 'complete_pr', 'complete_plate', 'successful_feed_in']
            for key in additional_td_keys_daily:
                td_classes_daily[key] = {}
                td_classes_daily_totals[key] = {}

            additional_td_keys_daily_totals = ['iai_identified_dom_colour_as_nondom', 'pr_identified_dom_colour_as_nondom']
            for key in additional_td_keys_daily_totals:
                td_classes_daily_totals[key] = {}

            timings = {}
            timings_mins = {}
            timings_maxes = {}
            timings_totals = {}
            timings_avgs = {}
            timings_keys = ['get_duration_time','imageai_detection_time', 'imageai_prediction_time', 'imageai_colour_detection_time', 
                                'total_imageai_analysis_time', 'pr_detection_time', 'pr_prediction_time', 'pr_colour_detection_time',
                                'total_pr_analysis_time', 'total_image_analysis_time', 'entry_total_time'
                                ]
            for key in timings_keys:
                timings[key] = {}
                timings_mins[key] = {}
                timings_maxes[key] = {}
                timings_totals[key] = {}
                timings_avgs[key] = {}
                

            overall_accuracies_mins_dates = {}
            overall_accuracies_mins = {}
            overall_accuracies_maxes_dates = {}
            overall_accuracies_maxes = {}
            overall_accuracies = {}
            
            
            overall_timings_mins_dates = {}
            overall_timings_mins = {}
            overall_timings_maxes_dates = {}
            overall_timings_maxes = {}
            overall_timings_totals = {}
            overall_timings_avgs_day = {}
            overall_timings_avgs_entry = {}


            td_classes_overall = {}


            entries = Entry.objects.filter(day__in=context_days, status='complete')


            iai_vehicle_detection_names_auto = IaiVehicleDetection.objects.filter(automated_analysis__entry__in=entries).distinct().order_by('type__name')
            for iai_auto in iai_vehicle_detection_names_auto:
                if iai_auto.type.name not in iai_individual_vehicle_detection_auto_names:
                    iai_individual_vehicle_detection_auto_names.append(iai_auto.type.name)
                    iai_individual_vehicle_detection_auto_counts[iai_auto.type.name] = {}
                    iai_vehicle_detection_chart_colours[iai_auto.type.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'


            iai_vehicle_detection_names_manual = IaiDetectionClass.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name') 
            for iai_manual in iai_vehicle_detection_names_manual:
                if iai_manual.name not in iai_individual_vehicle_detection_manual_names:
                    iai_individual_vehicle_detection_manual_names.append(iai_manual.name)
                    iai_individual_vehicle_detection_manual_counts[iai_manual.name] = {}
                    correct_iai_vehicle_detection_counts[iai_manual.name] = {}
                    iai_vehicle_detection_accuracies[iai_manual.name] = {}
                    td_classes_daily['iai_vehicle_detection'][iai_manual.name] = {}
                    if iai_manual.name not in iai_vehicle_detection_chart_colours.keys():
                        iai_vehicle_detection_chart_colours[iai_manual.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'

            iai_vehicle_prediction_names_auto = IaiVehiclePrediction.objects.filter(automated_analysis__entry__in=entries).distinct().order_by('type__name')
            for iai_auto in iai_vehicle_prediction_names_auto:
                if iai_auto.type.name not in iai_individual_vehicle_prediction_auto_names:
                    iai_individual_vehicle_prediction_auto_names.append(iai_auto.type.name)
                    iai_individual_vehicle_prediction_auto_counts[iai_auto.type.name] = {}
                    iai_vehicle_prediction_chart_colours[iai_auto.type.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'

            iai_vehicle_prediction_names_manual = PredictionClass.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name')
            for iai_manual in iai_vehicle_prediction_names_manual:
                if iai_manual.name not in iai_individual_vehicle_prediction_manual_names:
                    iai_individual_vehicle_prediction_manual_names.append(iai_manual.name)
                    iai_individual_vehicle_prediction_manual_counts[iai_manual.name] = {}
                    correct_iai_vehicle_prediction_counts[iai_manual.name] = {}
                    iai_vehicle_prediction_accuracies[iai_manual.name] = {}
                    td_classes_daily['iai_vehicle_prediction'][iai_manual.name] = {}
                    if iai_manual.name not in iai_vehicle_prediction_chart_colours.keys():
                        iai_vehicle_prediction_chart_colours[iai_manual.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'



            pr_vehicle_detection_names_auto = PrVehicleDetection.objects.filter(automated_analysis__entry__in=entries).distinct().order_by('type__name')
            for pr_auto in pr_vehicle_detection_names_auto:
                if pr_auto.type.name not in pr_individual_vehicle_detection_auto_names:
                    pr_individual_vehicle_detection_auto_names.append(pr_auto.type.name)
                    pr_individual_vehicle_detection_auto_counts[pr_auto.type.name] = {}
                    pr_vehicle_detection_chart_colours[pr_auto.type.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'

            pr_vehicle_detection_names_manual = PrDetectionClass.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name')
            for pr_manual in pr_vehicle_detection_names_manual:
                if pr_manual.name not in pr_individual_vehicle_detection_manual_names:
                    pr_individual_vehicle_detection_manual_names.append(pr_manual.name)
                    pr_individual_vehicle_detection_manual_counts[pr_manual.name] = {}
                    correct_pr_vehicle_detection_counts[pr_manual.name] = {}
                    pr_vehicle_detection_accuracies[pr_manual.name] = {}
                    td_classes_daily['pr_vehicle_detection'][pr_manual.name] = {}
                    if pr_manual.name not in pr_vehicle_detection_chart_colours.keys():
                        pr_vehicle_detection_chart_colours[pr_manual.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'

            pr_vehicle_prediction_names_auto = PrVehiclePrediction.objects.filter(automated_analysis__entry__in=entries).distinct().order_by('type__name')
            for pr_auto in pr_vehicle_prediction_names_auto:
                if pr_auto.type.name not in pr_individual_vehicle_prediction_auto_names:
                    pr_individual_vehicle_prediction_auto_names.append(pr_auto.type.name)
                    pr_individual_vehicle_prediction_auto_counts[pr_auto.type.name] = {}
                    pr_vehicle_prediction_chart_colours[pr_auto.type.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'

            pr_vehicle_prediction_names_manual = PredictionClass.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name')
            for pr_manual in pr_vehicle_prediction_names_manual:
                if pr_manual.name not in pr_individual_vehicle_prediction_manual_names:
                    pr_individual_vehicle_prediction_manual_names.append(pr_manual.name)
                    pr_individual_vehicle_prediction_manual_counts[pr_manual.name] = {}
                    correct_pr_vehicle_prediction_counts[pr_manual.name] = {}
                    pr_vehicle_prediction_accuracies[pr_manual.name] = {}
                    td_classes_daily['pr_vehicle_prediction'][pr_manual.name] = {}
                    if pr_manual.name not in pr_vehicle_prediction_chart_colours.keys():
                        pr_vehicle_prediction_chart_colours[pr_manual.name] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'



            auto_dom_colours = [] 
            for entry in entries: 
                auto_dom_colours.append( entry.automatedanalysis.iaicolourdetections.order_by('-percentage').first() )
            for iai_auto in auto_dom_colours:
                if iai_auto.colour.name not in iai_individual_colour_detection_auto_names:
                    iai_individual_colour_detection_auto_names.append(iai_auto.colour.name)
                    iai_individual_colour_detection_auto_counts[iai_auto.colour.name] = {}
                    iai_colour_detection_chart_colours[iai_auto.colour.name] = f'rgb({iai_auto.colour.r}, {iai_auto.colour.g}, {iai_auto.colour.b})'
            iai_individual_colour_detection_auto_names.sort()

            iai_colour_detection_names_manual = Colour.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name') 
            for iai_manual in iai_colour_detection_names_manual:
                if iai_manual.name not in iai_individual_colour_detection_manual_names:
                    iai_individual_colour_detection_manual_names.append(iai_manual.name)
                    iai_individual_colour_detection_manual_counts[iai_manual.name] = {}
                    correct_iai_colour_detection_counts[iai_manual.name] = {}
                    iai_colour_detection_accuracies[iai_manual.name] = {}
                    td_classes_daily['iai_colour_detection'][iai_manual.name] = {}
                    if iai_manual.name not in iai_colour_detection_chart_colours.keys():
                        iai_colour_detection_chart_colours[iai_manual.name] = f'rgb({iai_manual.r}, {iai_manual.g}, {iai_manual.b})'
            iai_individual_colour_detection_manual_names.sort()



            auto_dom_colours = [] 
            for entry in entries: 
                auto_dom_colours.append( entry.automatedanalysis.prcolourdetections.order_by('-percentage').first() )
            for pr_auto in auto_dom_colours:
                if pr_auto.colour.name not in pr_individual_colour_detection_auto_names:
                    pr_individual_colour_detection_auto_names.append(pr_auto.colour.name)
                    pr_individual_colour_detection_auto_counts[pr_auto.colour.name] = {}
                    pr_colour_detection_chart_colours[pr_auto.colour.name] = f'rgb({pr_auto.colour.r}, {pr_auto.colour.g}, {pr_auto.colour.b})'
            pr_individual_colour_detection_auto_names.sort()

            pr_colour_detection_names_manual = Colour.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name') 
            for pr_manual in pr_colour_detection_names_manual:
                if pr_manual.name not in pr_individual_colour_detection_manual_names:
                    pr_individual_colour_detection_manual_names.append(pr_manual.name)
                    pr_individual_colour_detection_manual_counts[pr_manual.name] = {}
                    correct_pr_colour_detection_counts[pr_manual.name] = {}
                    pr_colour_detection_accuracies[pr_manual.name] = {}
                    td_classes_daily['pr_colour_detection'][pr_manual.name] = {}
                    if pr_manual.name not in pr_colour_detection_chart_colours.keys():
                        pr_colour_detection_chart_colours[pr_manual.name] = f'rgb({pr_manual.r}, {pr_manual.g}, {pr_manual.b})'
            pr_individual_colour_detection_manual_names.sort()



            image_angles = ImageAngle.objects.filter(manualanalysis__entry__in=entries).distinct().order_by('name')
            for angle in image_angles:
                if angle.name not in image_angle_names:
                    image_angle_names.append(angle.name)
                    image_iai_extract_counts[angle.name] = {}
                    image_pr_extract_counts[angle.name] = {}
                    image_plate_extract_counts[angle.name] = {}
                    correct_image_iai_extract_counts[angle.name] = {}
                    correct_image_pr_extract_counts[angle.name] = {}
                    correct_image_plate_extract_counts[angle.name] = {}
                    complete_image_iai_extract_counts[angle.name] = {}
                    complete_image_pr_extract_counts[angle.name] = {}
                    complete_image_plate_extract_counts[angle.name] = {}
                    image_iai_extract_accuracies[angle.name] = {}
                    image_pr_extract_accuracies[angle.name] = {}
                    image_plate_extract_accuracies[angle.name] = {}
                    td_classes_daily['image_iai_extract'][angle.name] = {}
                    td_classes_daily['image_pr_extract'][angle.name] = {}
                    td_classes_daily['image_plate_extract'][angle.name] = {}
                    td_classes_daily['complete_iai'][angle.name] = {}
                    td_classes_daily['complete_pr'][angle.name] = {}
                    td_classes_daily['complete_plate'][angle.name] = {}
                    complete_image_iai_extract_percentage[angle.name] = {}
                    complete_image_pr_extract_percentage[angle.name] = {}
                    complete_image_plate_extract_percentage[angle.name] = {}
                    image_angle_chart_colours[angle.name] = {}
                    tried_feed_in_counts[angle.name] = {}
                    successful_feed_in_counts[angle.name] = {}
                    failed_feed_in_counts[angle.name] = {}
                    failed_feed_in_due_to_unextracted_iai_counts[angle.name] = {}
                    failed_feed_in_due_to_unextracted_pr_counts[angle.name] = {}
                    td_classes_daily['successful_feed_in'][angle.name] = {}
                    successful_feed_in_percentages[angle.name] = {}
                    failed_feed_in_due_to_unextracted_iai_percentages[angle.name] = {}
                    failed_feed_in_due_to_unextracted_pr_percentages[angle.name] = {}



            for day in context_days:
                date = day.date.strftime('%b %d, %Y')
                date_labels.append(date)
                day_objs.append(day)
                num_entries = day.entries.exclude(manualanalysis=None).count()
                number_of_entries_data[date] = num_entries

                day_entries = Entry.objects.exclude(manualanalysis=None).filter(day=day)

                for iai_auto in iai_individual_vehicle_detection_auto_names: 
                    iai_individual_vehicle_detection_auto_counts[iai_auto][date] = 0                

                for iai_manual in iai_individual_vehicle_detection_manual_names: 
                    iai_individual_vehicle_detection_manual_counts[iai_manual][date] = 0
                    correct_iai_vehicle_detection_counts[iai_manual][date] = 0 

                for iai_auto in iai_individual_vehicle_prediction_auto_names: 
                    iai_individual_vehicle_prediction_auto_counts[iai_auto][date] = 0

                for iai_manual in iai_individual_vehicle_prediction_manual_names: 
                    iai_individual_vehicle_prediction_manual_counts[iai_manual][date] = 0
                    correct_iai_vehicle_prediction_counts[iai_manual][date] = 0

                for iai_auto in iai_individual_colour_detection_auto_names: 
                    iai_individual_colour_detection_auto_counts[iai_auto][date] = 0

                for iai_manual in iai_individual_colour_detection_manual_names: 
                    iai_individual_colour_detection_manual_counts[iai_manual][date] = 0
                    correct_iai_colour_detection_counts[iai_manual][date] = 0


                for pr_auto in pr_individual_vehicle_detection_auto_names: 
                    pr_individual_vehicle_detection_auto_counts[pr_auto][date] = 0

                for pr_manual in pr_individual_vehicle_detection_manual_names: 
                    pr_individual_vehicle_detection_manual_counts[pr_manual][date] = 0
                    correct_pr_vehicle_detection_counts[pr_manual][date] = 0

                for pr_auto in pr_individual_vehicle_prediction_auto_names: 
                    pr_individual_vehicle_prediction_auto_counts[pr_auto][date] = 0

                for pr_manual in pr_individual_vehicle_prediction_manual_names: 
                    pr_individual_vehicle_prediction_manual_counts[pr_manual][date] = 0
                    correct_pr_vehicle_prediction_counts[pr_manual][date] = 0

                for pr_auto in pr_individual_colour_detection_auto_names: 
                    pr_individual_colour_detection_auto_counts[pr_auto][date] = 0

                for pr_manual in pr_individual_colour_detection_manual_names: 
                    pr_individual_colour_detection_manual_counts[pr_manual][date] = 0
                    correct_pr_colour_detection_counts[pr_manual][date] = 0


                day_durations_auto = []
                day_durations_manual = []
                for entry in day_entries:
                    auto_dur = entry.automatedanalysis.duration
                    manual_dur = entry.manualanalysis.duration
                    day_durations_auto.append(auto_dur)
                    day_durations_manual.append(manual_dur)

                durations_auto[day] = day_durations_auto
                durations_manual[day] = day_durations_manual

                correct_dur_count = 0
                for key in range(0,num_entries):
                    if day_durations_auto[key] == day_durations_manual[key]:
                        correct_dur_count += 1
                    
                day_accuracies['duration'][date] = round(( correct_dur_count / num_entries ) *100, 2)
                correct_durations_counts[date] = correct_dur_count
                td_classes_daily_totals['duration'][date] = GetStatsContext.get_td_class(correct_dur_count, num_entries)

                day_durations_auto_total = sum(day_durations_auto, timedelta())
                day_durations_manual_total = sum(day_durations_manual, timedelta())

                durations_auto_stats[day] = {
                    'min' : min(day_durations_auto),
                    'max' : max(day_durations_auto),
                    'total' : day_durations_auto_total,
                    'avg' : day_durations_auto_total/len(day_durations_auto)
                }

                durations_manual_stats[day] = {
                    'min' : min(day_durations_manual),
                    'max' : max(day_durations_manual),
                    'total' : day_durations_manual_total,
                    'avg' : day_durations_manual_total/len(day_durations_manual)
                }


                #DAY TIMINGS
                day_get_duration_times = []
                day_imageai_detection_times = []
                day_imageai_prediction_times = []
                day_imageai_colour_detection_times = []
                day_total_imageai_analysis_times = []
                day_pr_detection_times = []
                day_pr_prediction_times = []
                day_pr_colour_detection_times = []
                day_total_pr_analysis_times = []
                day_total_image_analysis_times = []
                day_entry_total_times = []
                for entry in day_entries:
                    day_get_duration_times.append(entry.automatedanalysis.timing.get_duration_time)
                    day_imageai_detection_times.append(entry.automatedanalysis.timing.imageai_detection_time)
                    day_imageai_prediction_times.append(entry.automatedanalysis.timing.imageai_prediction_time)
                    day_imageai_colour_detection_times.append(entry.automatedanalysis.timing.imageai_colour_detection_time)
                    day_total_imageai_analysis_times.append(entry.automatedanalysis.timing.total_imageai_analysis_time)
                    day_pr_detection_times.append(entry.automatedanalysis.timing.pr_detection_time)
                    day_pr_prediction_times.append(entry.automatedanalysis.timing.pr_prediction_time)
                    day_pr_colour_detection_times.append(entry.automatedanalysis.timing.pr_colour_detection_time)
                    day_total_pr_analysis_times.append(entry.automatedanalysis.timing.total_pr_analysis_time)
                    day_total_image_analysis_times.append(entry.automatedanalysis.timing.total_image_analysis_time)
                    day_entry_total_times.append(entry.automatedanalysis.timing.entry_total_time)

                timings['get_duration_time'][date] = day_get_duration_times
                timings['imageai_detection_time'][date] = day_imageai_detection_times
                timings['imageai_prediction_time'][date] = day_imageai_prediction_times
                timings['imageai_colour_detection_time'][date] = day_imageai_colour_detection_times
                timings['total_imageai_analysis_time'][date] = day_total_imageai_analysis_times
                timings['pr_detection_time'][date] = day_pr_detection_times
                timings['pr_prediction_time'][date] = day_pr_prediction_times
                timings['pr_colour_detection_time'][date] = day_pr_colour_detection_times
                timings['total_pr_analysis_time'][date] = day_total_pr_analysis_times
                timings['total_image_analysis_time'][date] = day_total_image_analysis_times
                timings['entry_total_time'][date] = day_entry_total_times

                for key in timings_keys:
                    timings_mins[key][date] = min(timings[key][date])
                    timings_maxes[key][date] = max(timings[key][date])
                    timings_totals[key][date] = sum(timings[key][date], timedelta())
                    timings_avgs[key][date] = timings_totals[key][date] / num_entries


                #DAY ACCURACIES 
                day_iai_vehicle_detection_auto = []
                day_iai_vehicle_detection_manual = []
                day_iai_vehicle_prediction_auto = []
                day_iai_vehicle_prediction_manual = []
                day_iai_colour_detection_auto = []
                day_iai_colour_detection_manual = []
                day_dominant_colour_detected_by_iai = []
                day_dominant_colour_identified_as_dominant_by_iai = []
                day_iai_correct_extractions = []
                day_pr_vehicle_detection_auto = []
                day_pr_vehicle_detection_manual = []
                day_pr_vehicle_prediction_auto = []
                day_pr_vehicle_prediction_manual = []
                day_pr_colour_detection_auto = []
                day_pr_colour_detection_manual = []
                day_dominant_colour_detected_by_pr = []
                day_dominant_colour_identified_as_dominant_by_pr = []
                day_pr_correct_extractions = []
                day_license_plate_identification_auto = []
                day_license_plate_identification_manual = []


                for entry in day_entries:
                    day_iai_vehicle_detection_auto.append(entry.automatedanalysis.iaivehicledetection)
                    day_iai_vehicle_detection_manual.append(entry.manualanalysis.iai_detection_class)
                    day_iai_vehicle_prediction_auto.append(entry.automatedanalysis.iaivehicleprediction)
                    day_iai_vehicle_prediction_manual.append(entry.manualanalysis.prediction_class)
                    day_iai_colour_detection_auto.append(entry.automatedanalysis.iaicolourdetections.order_by('-percentage').first())
                    day_iai_colour_detection_manual.append(entry.manualanalysis.dominant_colour)
                    day_dominant_colour_detected_by_iai.append(entry.manualanalysis.dominant_colour_detected_by_iai)
                    day_dominant_colour_identified_as_dominant_by_iai.append(entry.manualanalysis.dominant_colour_identified_as_dominant_by_iai)
                    day_iai_correct_extractions.append(entry.manualanalysis.iai_extract_correct)
                    day_pr_vehicle_detection_auto.append(entry.automatedanalysis.prvehicledetection)
                    day_pr_vehicle_detection_manual.append(entry.manualanalysis.pr_detection_class)
                    day_pr_vehicle_prediction_auto.append(entry.automatedanalysis.prvehicleprediction)
                    day_pr_vehicle_prediction_manual.append(entry.manualanalysis.prediction_class)
                    day_pr_colour_detection_auto.append(entry.automatedanalysis.prcolourdetections.order_by('-percentage').first())
                    day_pr_colour_detection_manual.append(entry.manualanalysis.dominant_colour)
                    day_dominant_colour_detected_by_pr.append(entry.manualanalysis.dominant_colour_detected_by_pr)
                    day_dominant_colour_identified_as_dominant_by_pr.append(entry.manualanalysis.dominant_colour_identified_as_dominant_by_pr)
                    day_pr_correct_extractions.append(entry.manualanalysis.pr_extract_correct)
                    day_license_plate_identification_auto.append(entry.automatedanalysis.licenseplate)
                    day_license_plate_identification_manual.append(entry.manualanalysis.license_plate_number)

                correct_license_plate_identification_counts[date] = 0
                iai_identified_dom_colour_as_nondom_count[date] = 0
                iai_identified_dom_colour_as_nondom_correct_count[date] = 0
                pr_identified_dom_colour_as_nondom_count[date] = 0
                pr_identified_dom_colour_as_nondom_correct_count[date] = 0

                day_correct_counts['iai_vehicle_detection'][date] = 0
                day_correct_counts['iai_vehicle_prediction'][date] = 0
                day_correct_counts['iai_colour_detection'][date] = 0
                day_correct_counts['pr_vehicle_detection'][date] = 0
                day_correct_counts['pr_vehicle_prediction'][date] = 0
                day_correct_counts['pr_colour_detection'][date] = 0

                for key in range(0,num_entries):

                    iai_individual_vehicle_detection_auto_counts[day_iai_vehicle_detection_auto[key].type.name][date] += 1
                    iai_individual_vehicle_detection_manual_counts[day_iai_vehicle_detection_manual[key].name][date] += 1
                    if day_iai_vehicle_detection_auto[key].type.name == day_iai_vehicle_detection_manual[key].name:
                        correct_iai_vehicle_detection_counts[day_iai_vehicle_detection_manual[key].name][date] += 1
                        day_correct_counts['iai_vehicle_detection'][date] += 1            

                    iai_individual_vehicle_prediction_auto_counts[day_iai_vehicle_prediction_auto[key].type.name][date] += 1
                    iai_individual_vehicle_prediction_manual_counts[day_iai_vehicle_prediction_manual[key].name][date] += 1
                    if day_iai_vehicle_prediction_auto[key].type.name == day_iai_vehicle_prediction_manual[key].name:
                        correct_iai_vehicle_prediction_counts[day_iai_vehicle_prediction_manual[key].name][date] += 1
                        day_correct_counts['iai_vehicle_prediction'][date] += 1            

                    iai_individual_colour_detection_auto_counts[day_iai_colour_detection_auto[key].colour.name][date] += 1
                    iai_individual_colour_detection_manual_counts[day_iai_colour_detection_manual[key].name][date] += 1
                    if day_iai_colour_detection_auto[key].colour.name == day_iai_colour_detection_manual[key].name and day_iai_correct_extractions[key] == True:
                        correct_iai_colour_detection_counts[day_iai_colour_detection_manual[key].name][date] += 1
                        day_correct_counts['iai_colour_detection'][date] += 1
                        
                    if day_dominant_colour_detected_by_iai[key] == True and day_dominant_colour_identified_as_dominant_by_iai[key] == False:
                        iai_identified_dom_colour_as_nondom_count[date] += 1
                        overall_iai_identified_dom_colour_as_nondom_count += 1
                        if day_iai_correct_extractions[key] == True:
                            iai_identified_dom_colour_as_nondom_correct_count[date] += 1
                            overall_iai_identified_dom_colour_as_nondom_correct_count += 1

                    pr_individual_vehicle_detection_auto_counts[day_pr_vehicle_detection_auto[key].type.name][date] += 1
                    pr_individual_vehicle_detection_manual_counts[day_pr_vehicle_detection_manual[key].name][date] += 1
                    if day_pr_vehicle_detection_auto[key].type.name == day_pr_vehicle_detection_manual[key].name:
                        correct_pr_vehicle_detection_counts[day_pr_vehicle_detection_manual[key].name][date] += 1
                        day_correct_counts['pr_vehicle_detection'][date] += 1            

                    pr_individual_vehicle_prediction_auto_counts[day_pr_vehicle_prediction_auto[key].type.name][date] += 1
                    pr_individual_vehicle_prediction_manual_counts[day_pr_vehicle_prediction_manual[key].name][date] += 1
                    if day_pr_vehicle_prediction_auto[key].type.name == day_pr_vehicle_prediction_manual[key].name:
                        correct_pr_vehicle_prediction_counts[day_pr_vehicle_prediction_manual[key].name][date] += 1
                        day_correct_counts['pr_vehicle_prediction'][date] += 1

                    pr_individual_colour_detection_auto_counts[day_pr_colour_detection_auto[key].colour.name][date] += 1
                    pr_individual_colour_detection_manual_counts[day_pr_colour_detection_manual[key].name][date] += 1
                    if day_pr_colour_detection_auto[key].colour.name == day_pr_colour_detection_manual[key].name and day_pr_correct_extractions[key] == True:
                        correct_pr_colour_detection_counts[day_pr_colour_detection_manual[key].name][date] += 1
                        day_correct_counts['pr_colour_detection'][date] += 1

                    if day_dominant_colour_detected_by_pr[key] == True and day_dominant_colour_identified_as_dominant_by_pr[key] == False:
                        pr_identified_dom_colour_as_nondom_count[date] += 1
                        overall_pr_identified_dom_colour_as_nondom_count += 1
                        if day_pr_correct_extractions[key] == True:
                            pr_identified_dom_colour_as_nondom_correct_count[date] += 1
                            overall_pr_identified_dom_colour_as_nondom_correct_count += 1

                    if day_license_plate_identification_auto[key].number == day_license_plate_identification_manual[key]:
                        correct_license_plate_identification_counts[date] += 1


                for angle in image_angle_names:
                    image_angle_chart_colours[angle]['iai'] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'
                    image_angle_chart_colours[angle]['pr'] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'
                    image_angle_chart_colours[angle]['plate'] = f'rgb({rng.randint(0,256)}, {rng.randint(0,256)}, {rng.randint(0,256)})'
                    image_iai_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle).exclude(manualanalysis=None).exclude(manualanalysis__iai_extract_correct=None).count()
                    image_pr_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle).exclude(manualanalysis=None).exclude(manualanalysis__pr_extract_correct=None).count()
                    image_plate_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle).exclude(manualanalysis=None).exclude(manualanalysis__plate_extract_correct=None).count()
                    correct_image_iai_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__iai_extract_correct=True).exclude(manualanalysis=None).count()
                    correct_image_pr_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__pr_extract_correct=True).exclude(manualanalysis=None).count()
                    correct_image_plate_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__plate_extract_correct=True).exclude(manualanalysis=None).count()
                    complete_image_iai_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__iai_extract_entire=True).exclude(manualanalysis=None).count()
                    complete_image_pr_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__pr_extract_entire=True).exclude(manualanalysis=None).count()
                    complete_image_plate_extract_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle,manualanalysis__plate_extract_entire=True).exclude(manualanalysis=None).count()
                    image_iai_extract_accuracies[angle][date] = round((correct_image_iai_extract_counts[angle][date]/ image_iai_extract_counts[angle][date]) * 100, 2) if image_iai_extract_counts[angle][date] != 0 else 0.0
                    image_pr_extract_accuracies[angle][date] = round((correct_image_pr_extract_counts[angle][date]/ image_pr_extract_counts[angle][date]) * 100, 2) if image_pr_extract_counts[angle][date] != 0 else 0.0
                    image_plate_extract_accuracies[angle][date] = round((correct_image_plate_extract_counts[angle][date]/ image_plate_extract_counts[angle][date]) * 100, 2) if image_plate_extract_counts[angle][date] != 0 else 0.0
                    td_classes_daily['image_iai_extract'][angle][date] = GetStatsContext.get_td_class(correct_image_iai_extract_counts[angle][date], image_iai_extract_counts[angle][date])
                    td_classes_daily['image_pr_extract'][angle][date] = GetStatsContext.get_td_class(correct_image_pr_extract_counts[angle][date], image_pr_extract_counts[angle][date])
                    td_classes_daily['image_plate_extract'][angle][date] = GetStatsContext.get_td_class(correct_image_plate_extract_counts[angle][date], image_plate_extract_counts[angle][date])
                    complete_image_iai_extract_percentage[angle][date] = round((complete_image_iai_extract_counts[angle][date]/ correct_image_iai_extract_counts[angle][date]) * 100, 2) if correct_image_iai_extract_counts[angle][date] != 0 else 0.0
                    complete_image_pr_extract_percentage[angle][date] = round((complete_image_pr_extract_counts[angle][date]/ correct_image_pr_extract_counts[angle][date]) * 100, 2) if correct_image_pr_extract_counts[angle][date] != 0 else 0.0
                    complete_image_plate_extract_percentage[angle][date] = round((complete_image_plate_extract_counts[angle][date]/ correct_image_plate_extract_counts[angle][date]) * 100, 2) if correct_image_plate_extract_counts[angle][date] != 0 else 0.0
                    td_classes_daily['complete_iai'][angle][date] = GetStatsContext.get_td_class(complete_image_iai_extract_counts[angle][date], correct_image_iai_extract_counts[angle][date])
                    td_classes_daily['complete_pr'][angle][date] = GetStatsContext.get_td_class(complete_image_pr_extract_counts[angle][date], correct_image_pr_extract_counts[angle][date])
                    td_classes_daily['complete_plate'][angle][date] = GetStatsContext.get_td_class(complete_image_plate_extract_counts[angle][date], correct_image_plate_extract_counts[angle][date])
                    
                    tried_feed_in_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle, day__tried_feed_in=True).exclude(manualanalysis=None).count()
                    successful_feed_in_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle, day__tried_feed_in=True, automatedanalysis__successful_feed_in=True).exclude(manualanalysis=None).count()
                    failed_feed_in_counts[angle][date] = tried_feed_in_counts[angle][date] - successful_feed_in_counts[angle][date]
                    failed_feed_in_due_to_unextracted_iai_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle, day__tried_feed_in=True, automatedanalysis__successful_feed_in=None).exclude(manualanalysis=None).count()
                    failed_feed_in_due_to_unextracted_pr_counts[angle][date] = Entry.objects.filter(day=day, manualanalysis__image_angle__name=angle, day__tried_feed_in=True, automatedanalysis__successful_feed_in=False).exclude(manualanalysis=None).count()
                    successful_feed_in_percentages[angle][date] = round((successful_feed_in_counts[angle][date]/ tried_feed_in_counts[angle][date]) * 100, 2) if tried_feed_in_counts[angle][date] != 0 else 0.0
                    failed_feed_in_due_to_unextracted_iai_percentages[angle][date] = round((failed_feed_in_due_to_unextracted_iai_counts[angle][date]/ failed_feed_in_counts[angle][date]) * 100, 2) if failed_feed_in_counts[angle][date] != 0 else 0.0
                    failed_feed_in_due_to_unextracted_pr_percentages[angle][date] = round((failed_feed_in_due_to_unextracted_pr_counts[angle][date]/ failed_feed_in_counts[angle][date]) * 100, 2) if failed_feed_in_counts[angle][date] != 0 else 0.0
                    td_classes_daily['successful_feed_in'][angle][date] = GetStatsContext.get_td_class(successful_feed_in_counts[angle][date], tried_feed_in_counts[angle][date])
                

                total_image_iai_extract_counts[date] = Entry.objects.filter(day=day).exclude(manualanalysis=None).exclude(manualanalysis__iai_extract_correct=None).count() 
                total_image_pr_extract_counts[date] = Entry.objects.filter(day=day).exclude(manualanalysis=None).exclude(manualanalysis__pr_extract_correct=None).count() 
                total_image_plate_extract_counts[date] = Entry.objects.filter(day=day).exclude(manualanalysis=None).exclude(manualanalysis__plate_extract_correct=None).count() 
                total_correct_image_iai_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__iai_extract_correct=True).exclude(manualanalysis=None).count()
                total_correct_image_pr_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__pr_extract_correct=True).exclude(manualanalysis=None).count()
                total_correct_image_plate_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__plate_extract_correct=True).exclude(manualanalysis=None).count()
                total_complete_image_iai_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__iai_extract_entire=True).exclude(manualanalysis=None).count()
                total_complete_image_pr_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__pr_extract_entire=True).exclude(manualanalysis=None).count()
                total_complete_image_plate_extract_counts[date] = Entry.objects.filter(day=day, manualanalysis__plate_extract_entire=True).exclude(manualanalysis=None).count()

                total_tried_feed_in_counts[date] = Entry.objects.filter(day=day, day__tried_feed_in=True).exclude(manualanalysis=None).count()
                total_successful_feed_in_counts[date] = Entry.objects.filter(day=day, day__tried_feed_in=True, automatedanalysis__successful_feed_in=True).exclude(manualanalysis=None).count()
                total_failed_feed_in_counts[date] = total_tried_feed_in_counts[date] - total_successful_feed_in_counts[date]
                total_failed_feed_in_due_to_unextracted_iai_counts[date] = Entry.objects.filter(day=day, day__tried_feed_in=True, automatedanalysis__successful_feed_in=None).exclude(manualanalysis=None).count()
                total_failed_feed_in_due_to_unextracted_pr_counts[date] = Entry.objects.filter(day=day, day__tried_feed_in=True, automatedanalysis__successful_feed_in=False).exclude(manualanalysis=None).count()
                daily_successful_feed_in_percentages[date] = round((total_successful_feed_in_counts[date] / total_tried_feed_in_counts[date]) * 100, 2) if total_tried_feed_in_counts[date] != 0 else 0.0
                daily_failed_feed_in_due_to_unextracted_iai_percentages[date] = round((total_failed_feed_in_due_to_unextracted_iai_counts[date] / total_failed_feed_in_counts[date]) * 100, 2) if total_failed_feed_in_counts[date] != 0 else 0.0
                daily_failed_feed_in_due_to_unextracted_pr_percentages[date] = round((total_failed_feed_in_due_to_unextracted_pr_counts[date] / total_failed_feed_in_counts[date]) * 100, 2) if total_failed_feed_in_counts[date] != 0 else 0.0


                iai_identified_dom_colour_as_nondom_percentage[date] = round((iai_identified_dom_colour_as_nondom_correct_count[date]/ iai_identified_dom_colour_as_nondom_count[date]) * 100, 2) if iai_identified_dom_colour_as_nondom_count[date] != 0 else 0.0
                pr_identified_dom_colour_as_nondom_percentage[date] = round((pr_identified_dom_colour_as_nondom_correct_count[date]/ pr_identified_dom_colour_as_nondom_count[date]) * 100, 2) if pr_identified_dom_colour_as_nondom_count[date] != 0 else 0.0
                
                daily_complete_image_iai_extract_percentage[date] = round((total_complete_image_iai_extract_counts[date] / total_correct_image_iai_extract_counts[date]) * 100, 2) if total_correct_image_iai_extract_counts[date] != 0 else 0.0
                daily_complete_image_pr_extract_percentage[date] = round((total_complete_image_pr_extract_counts[date] / total_correct_image_pr_extract_counts[date]) * 100, 2) if total_correct_image_pr_extract_counts[date] != 0 else 0.0
                daily_complete_image_plate_extract_percentage[date] = round((total_complete_image_plate_extract_counts[date] / total_correct_image_plate_extract_counts[date]) * 100, 2) if total_correct_image_plate_extract_counts[date] != 0 else 0.0


                
                day_accuracies['iai_vehicle_detection'][date] = round((day_correct_counts['iai_vehicle_detection'][date]/ num_entries) * 100, 2)
                day_accuracies['iai_vehicle_prediction'][date] = round((day_correct_counts['iai_vehicle_prediction'][date]/ num_entries) * 100, 2)
                day_accuracies['iai_colour_detection'][date] = round((day_correct_counts['iai_colour_detection'][date]/ num_entries) * 100, 2)
                day_accuracies['pr_vehicle_detection'][date] = round((day_correct_counts['pr_vehicle_detection'][date]/ num_entries) * 100, 2)
                day_accuracies['pr_vehicle_prediction'][date] = round((day_correct_counts['pr_vehicle_prediction'][date]/ num_entries) * 100, 2)
                day_accuracies['pr_colour_detection'][date] = round((day_correct_counts['pr_colour_detection'][date]/ num_entries) * 100, 2)
                day_accuracies['license_plate_identification'][date] = round((correct_license_plate_identification_counts[date]/ num_entries) * 100, 2)
                day_accuracies['image_iai_extract'][date] = round((total_correct_image_iai_extract_counts[date]/ total_image_iai_extract_counts[date]) * 100, 2) if total_image_iai_extract_counts[date] != 0 else 0.0
                day_accuracies['image_pr_extract'][date] = round((total_correct_image_pr_extract_counts[date]/ total_image_pr_extract_counts[date]) * 100, 2) if total_image_pr_extract_counts[date] != 0 else 0.0
                day_accuracies['image_plate_extract'][date] = round((total_correct_image_plate_extract_counts[date]/ total_image_plate_extract_counts[date]) * 100, 2) if total_image_plate_extract_counts[date] != 0 else 0.0
                                
            
                td_classes_daily_totals['iai_vehicle_detection'][date] = GetStatsContext.get_td_class(day_correct_counts['iai_vehicle_detection'][date], num_entries)
                td_classes_daily_totals['iai_vehicle_prediction'][date] = GetStatsContext.get_td_class(day_correct_counts['iai_vehicle_prediction'][date], num_entries)
                td_classes_daily_totals['iai_colour_detection'][date] = GetStatsContext.get_td_class(day_correct_counts['iai_colour_detection'][date], num_entries)
                td_classes_daily_totals['pr_vehicle_detection'][date] = GetStatsContext.get_td_class(day_correct_counts['pr_vehicle_detection'][date], num_entries)
                td_classes_daily_totals['pr_vehicle_prediction'][date] = GetStatsContext.get_td_class(day_correct_counts['pr_vehicle_prediction'][date], num_entries)
                td_classes_daily_totals['pr_colour_detection'][date] = GetStatsContext.get_td_class(day_correct_counts['pr_colour_detection'][date], num_entries)
                td_classes_daily_totals['license_plate_identification'][date] = GetStatsContext.get_td_class(correct_license_plate_identification_counts[date], num_entries)
                td_classes_daily_totals['image_iai_extract'][date] = GetStatsContext.get_td_class(total_correct_image_iai_extract_counts[date], total_image_iai_extract_counts[date])
                td_classes_daily_totals['image_pr_extract'][date] = GetStatsContext.get_td_class(total_correct_image_pr_extract_counts[date], total_image_pr_extract_counts[date])
                td_classes_daily_totals['image_plate_extract'][date] = GetStatsContext.get_td_class(total_correct_image_plate_extract_counts[date], total_image_plate_extract_counts[date])
                td_classes_daily_totals['iai_identified_dom_colour_as_nondom'][date] = GetStatsContext.get_td_class(iai_identified_dom_colour_as_nondom_correct_count[date], iai_identified_dom_colour_as_nondom_count[date])
                td_classes_daily_totals['pr_identified_dom_colour_as_nondom'][date] = GetStatsContext.get_td_class(pr_identified_dom_colour_as_nondom_correct_count[date], pr_identified_dom_colour_as_nondom_count[date])
                td_classes_daily_totals['complete_iai'][date] = GetStatsContext.get_td_class(total_complete_image_iai_extract_counts[date], total_correct_image_iai_extract_counts[date])
                td_classes_daily_totals['complete_pr'][date] = GetStatsContext.get_td_class(total_complete_image_pr_extract_counts[date], total_correct_image_pr_extract_counts[date])
                td_classes_daily_totals['complete_plate'][date] = GetStatsContext.get_td_class(total_complete_image_plate_extract_counts[date], total_correct_image_plate_extract_counts[date])
                td_classes_daily_totals['successful_feed_in'][date] = GetStatsContext.get_td_class(total_successful_feed_in_counts[date], total_tried_feed_in_counts[date])

            #ACCURACIES
            for key in iai_individual_vehicle_detection_manual_counts.keys():    
                if key not in iai_individual_vehicle_detection_auto_counts.keys():
                    for date in iai_individual_vehicle_detection_manual_counts[key].keys():
                        iai_vehicle_detection_accuracies[key][date] = 0
                        td_classes_daily['iai_vehicle_detection'][key][date] = '' if iai_individual_vehicle_detection_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in iai_individual_vehicle_detection_manual_counts[key].keys():
                        td_classes_daily['iai_vehicle_detection'][key][date] = GetStatsContext.get_td_class_daily(correct_iai_vehicle_detection_counts[key][date],iai_individual_vehicle_detection_manual_counts[key][date],iai_individual_vehicle_detection_auto_counts[key][date])
                        if (date not in iai_individual_vehicle_detection_auto_counts[key].keys() or iai_individual_vehicle_detection_auto_counts[key][date] == 0 or 
                        iai_individual_vehicle_detection_manual_counts[key][date] == 0 or correct_iai_vehicle_detection_counts[key][date] == 0):
                            iai_vehicle_detection_accuracies[key][date] = 0
                        else:
                            iai_vehicle_detection_accuracies[key][date] = round(
                                (correct_iai_vehicle_detection_counts[key][date]/iai_individual_vehicle_detection_auto_counts[key][date]
                                ) *100, 2)

            for key in iai_individual_vehicle_prediction_manual_counts.keys():
                if key not in iai_individual_vehicle_prediction_auto_counts.keys():
                    for date in iai_individual_vehicle_prediction_manual_counts[key].keys():
                        iai_vehicle_prediction_accuracies[key][date] = 0
                        td_classes_daily['iai_vehicle_prediction'][key][date] = '' if iai_individual_vehicle_prediction_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in iai_individual_vehicle_prediction_manual_counts[key].keys():
                        td_classes_daily['iai_vehicle_prediction'][key][date] = GetStatsContext.get_td_class_daily(correct_iai_vehicle_prediction_counts[key][date],iai_individual_vehicle_prediction_manual_counts[key][date],iai_individual_vehicle_prediction_auto_counts[key][date])
                        if (date not in iai_individual_vehicle_prediction_auto_counts[key].keys() or iai_individual_vehicle_prediction_auto_counts[key][date] == 0 or 
                        iai_individual_vehicle_prediction_manual_counts[key][date] == 0 or correct_iai_vehicle_prediction_counts[key][date] == 0):
                            iai_vehicle_prediction_accuracies[key][date] = 0
                        else:
                            iai_vehicle_prediction_accuracies[key][date] = round(
                                (correct_iai_vehicle_prediction_counts[key][date]/iai_individual_vehicle_prediction_auto_counts[key][date]
                                ) *100, 2)

            for key in iai_individual_colour_detection_manual_counts.keys():
                if key not in iai_individual_colour_detection_auto_counts.keys():
                    for date in iai_individual_colour_detection_manual_counts[key].keys():
                        iai_colour_detection_accuracies[key][date] = 0
                        td_classes_daily['iai_colour_detection'][key][date] = '' if iai_individual_colour_detection_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in iai_individual_colour_detection_manual_counts[key].keys():
                        td_classes_daily['iai_colour_detection'][key][date] = GetStatsContext.get_td_class_daily(correct_iai_colour_detection_counts[key][date],iai_individual_colour_detection_manual_counts[key][date],iai_individual_colour_detection_auto_counts[key][date])
                        if (date not in iai_individual_colour_detection_auto_counts[key].keys() or iai_individual_colour_detection_auto_counts[key][date] == 0 or 
                        iai_individual_colour_detection_manual_counts[key][date] == 0 or correct_iai_colour_detection_counts[key][date] == 0):
                            iai_colour_detection_accuracies[key][date] = 0
                        else:
                            iai_colour_detection_accuracies[key][date] = round(
                                (correct_iai_colour_detection_counts[key][date]/iai_individual_colour_detection_auto_counts[key][date]
                                ) *100, 2)


            for key in pr_individual_vehicle_detection_manual_counts.keys():
                if key not in pr_individual_vehicle_detection_auto_counts.keys():
                    for date in pr_individual_vehicle_detection_manual_counts[key].keys():
                        pr_vehicle_detection_accuracies[key][date] = 0
                        td_classes_daily['pr_vehicle_detection'][key][date] = '' if pr_individual_vehicle_detection_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in pr_individual_vehicle_detection_manual_counts[key].keys():
                        td_classes_daily['pr_vehicle_detection'][key][date] = GetStatsContext.get_td_class_daily(correct_pr_vehicle_detection_counts[key][date],pr_individual_vehicle_detection_manual_counts[key][date],pr_individual_vehicle_detection_auto_counts[key][date])
                        if (date not in pr_individual_vehicle_detection_auto_counts[key].keys() or pr_individual_vehicle_detection_auto_counts[key][date] == 0 or 
                        pr_individual_vehicle_detection_manual_counts[key][date] == 0 or correct_pr_vehicle_detection_counts[key][date] == 0):
                            pr_vehicle_detection_accuracies[key][date] = 0
                        else:
                            pr_vehicle_detection_accuracies[key][date] = round(
                                (correct_pr_vehicle_detection_counts[key][date]/pr_individual_vehicle_detection_auto_counts[key][date]
                                ) *100, 2)

            for key in pr_individual_vehicle_prediction_manual_counts.keys():
                if key not in pr_individual_vehicle_prediction_auto_counts.keys():
                    for date in pr_individual_vehicle_prediction_manual_counts[key].keys():
                        pr_vehicle_prediction_accuracies[key][date] = 0
                        td_classes_daily['pr_vehicle_prediction'][key][date] = '' if pr_individual_vehicle_prediction_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in pr_individual_vehicle_prediction_manual_counts[key].keys():
                        td_classes_daily['pr_vehicle_prediction'][key][date] = GetStatsContext.get_td_class_daily(correct_pr_vehicle_prediction_counts[key][date],pr_individual_vehicle_prediction_manual_counts[key][date],pr_individual_vehicle_prediction_auto_counts[key][date])
                        if (date not in pr_individual_vehicle_prediction_auto_counts[key].keys() or pr_individual_vehicle_prediction_auto_counts[key][date] == 0 or 
                        pr_individual_vehicle_prediction_manual_counts[key][date] == 0 or correct_pr_vehicle_prediction_counts[key][date] == 0):
                            pr_vehicle_prediction_accuracies[key][date] = 0
                        else:
                            pr_vehicle_prediction_accuracies[key][date] = round(
                                (correct_pr_vehicle_prediction_counts[key][date]/pr_individual_vehicle_prediction_auto_counts[key][date]
                                ) *100, 2)

            for key in pr_individual_colour_detection_manual_counts.keys():
                if key not in pr_individual_colour_detection_auto_counts.keys():
                    for date in pr_individual_colour_detection_manual_counts[key].keys():
                        pr_colour_detection_accuracies[key][date] = 0
                        td_classes_daily['pr_colour_detection'][key][date] = '' if pr_individual_colour_detection_manual_counts[key][date] == 0 else 'table-danger'
                else:
                    for date in pr_individual_colour_detection_manual_counts[key].keys():
                        td_classes_daily['pr_colour_detection'][key][date] = GetStatsContext.get_td_class_daily(correct_pr_colour_detection_counts[key][date],pr_individual_colour_detection_manual_counts[key][date],pr_individual_colour_detection_auto_counts[key][date])
                        if (date not in pr_individual_colour_detection_auto_counts[key].keys() or pr_individual_colour_detection_auto_counts[key][date] == 0 or 
                        pr_individual_colour_detection_manual_counts[key][date] == 0 or correct_pr_colour_detection_counts[key][date] == 0):
                            pr_colour_detection_accuracies[key][date] = 0
                        else:
                            pr_colour_detection_accuracies[key][date] = round(
                                (correct_pr_colour_detection_counts[key][date]/pr_individual_colour_detection_auto_counts[key][date]
                                ) *100, 2)
            


            #OVERALL
            num_days = context_days.count()
            tot_entries = entries.count()

            #OVERALL COORECT COUNTS AND ACCURACIES 
            for key in day_accuracies_keys:
                overall_accuracies_mins[key] = min(day_accuracies[key].values())
                overall_accuracies_mins_dates[key] = [date for date in day_accuracies[key] if day_accuracies[key][date] == overall_accuracies_mins[key]]
                overall_accuracies_maxes[key] = max(day_accuracies[key].values())
                overall_accuracies_maxes_dates[key] = [date for date in day_accuracies[key] if day_accuracies[key][date] == overall_accuracies_maxes[key]]
            
            overall_correct_durations_count = sum(correct_durations_counts.values())
            overall_accuracies['duration'] = round( ( overall_correct_durations_count / tot_entries ) * 100, 2)

            overall_correct_iai_vehicle_detection_count = sum(day_correct_counts['iai_vehicle_detection'].values())
            overall_accuracies['iai_vehicle_detection'] = round( ( overall_correct_iai_vehicle_detection_count / tot_entries ) * 100, 2)

            overall_correct_iai_vehicle_prediction_count = sum(day_correct_counts['iai_vehicle_prediction'].values())
            overall_accuracies['iai_vehicle_prediction'] = round( ( overall_correct_iai_vehicle_prediction_count / tot_entries ) * 100, 2)

            overall_correct_iai_colour_detection_count = sum(day_correct_counts['iai_colour_detection'].values())
            overall_accuracies['iai_colour_detection'] = round( ( overall_correct_iai_colour_detection_count / tot_entries ) * 100, 2)

            overall_correct_pr_vehicle_detection_count = sum(day_correct_counts['pr_vehicle_detection'].values())
            overall_accuracies['pr_vehicle_detection'] = round( ( overall_correct_pr_vehicle_detection_count / tot_entries ) * 100, 2)

            overall_correct_pr_vehicle_prediction_count = sum(day_correct_counts['pr_vehicle_prediction'].values())
            overall_accuracies['pr_vehicle_prediction'] = round( ( overall_correct_pr_vehicle_prediction_count / tot_entries ) * 100, 2)

            overall_correct_pr_colour_detection_count = sum(day_correct_counts['pr_colour_detection'].values())
            overall_accuracies['pr_colour_detection'] = round( ( overall_correct_pr_colour_detection_count / tot_entries ) * 100, 2)

            overall_correct_license_plate_identification_count = sum(correct_license_plate_identification_counts.values())
            overall_accuracies['license_plate_identification'] = round( ( overall_correct_license_plate_identification_count / tot_entries ) * 100, 2)

            overall_correct_image_iai_extract_count = sum(total_correct_image_iai_extract_counts.values())
            overall_image_iai_extract_count = sum(total_image_iai_extract_counts.values())
            overall_accuracies['image_iai_extract']= round((overall_correct_image_iai_extract_count / overall_image_iai_extract_count) * 100, 2) if overall_image_iai_extract_count != 0 else 0.0

            overall_correct_image_pr_extract_count = sum(total_correct_image_pr_extract_counts.values())
            overall_image_pr_extract_count = sum(total_image_pr_extract_counts.values())
            overall_accuracies['image_pr_extract']= round((overall_correct_image_pr_extract_count / overall_image_pr_extract_count) * 100, 2) if overall_image_pr_extract_count != 0 else 0.0

            overall_correct_image_plate_extract_count = sum(total_correct_image_plate_extract_counts.values())
            overall_image_plate_extract_count = sum(total_image_plate_extract_counts.values())
            overall_accuracies['image_plate_extract']= round((overall_correct_image_plate_extract_count / overall_image_plate_extract_count) * 100, 2) if overall_image_plate_extract_count != 0 else 0.0


            #OVERALL TIMING
            for key in timings_keys:
                overall_timings_mins[key] = min(timings_mins[key].values())
                overall_timings_mins_dates[key] = [date for date in timings_mins[key] if timings_mins[key][date] == overall_timings_mins[key]]
                overall_timings_maxes[key] = max(timings_maxes[key].values())
                overall_timings_maxes_dates[key] = [date for date in timings_maxes[key] if timings_maxes[key][date] == overall_timings_maxes[key]]
                overall_timings_totals[key] = sum(timings_totals[key].values(), timedelta())
                overall_timings_avgs_day[key] = overall_timings_totals[key] / num_days
                overall_timings_avgs_entry[key] = overall_timings_totals[key] / tot_entries

            #OVERALL PERCENTAGE OF TIMES DOMINANT COLOUR WAS IDENTIFIED AS NON-DOMINANT
            overall_iai_identified_dom_colour_as_nondom_percentage = round((overall_iai_identified_dom_colour_as_nondom_correct_count/ overall_iai_identified_dom_colour_as_nondom_count) * 100, 2) if overall_iai_identified_dom_colour_as_nondom_count != 0 else 0.0
            overall_pr_identified_dom_colour_as_nondom_percentage = round((overall_pr_identified_dom_colour_as_nondom_correct_count/ overall_pr_identified_dom_colour_as_nondom_count) * 100, 2) if overall_pr_identified_dom_colour_as_nondom_count != 0 else 0.0



            #OVERALL PERCENTAGE OF TIMES ENTIRE OBJECT WAS EXTRACTED
            overall_complete_image_iai_extract_count = sum(total_complete_image_iai_extract_counts.values())
            overall_complete_image_iai_extract_percentage= round((overall_complete_image_iai_extract_count / overall_correct_image_iai_extract_count) * 100, 2) if overall_correct_image_iai_extract_count != 0 else 0.0
            
            overall_complete_image_pr_extract_count = sum(total_complete_image_pr_extract_counts.values())
            overall_complete_image_pr_extract_percentage= round((overall_complete_image_pr_extract_count / overall_correct_image_pr_extract_count) * 100, 2) if overall_correct_image_pr_extract_count != 0 else 0.0
            
            overall_complete_image_plate_extract_count = sum(total_complete_image_plate_extract_counts.values())
            overall_complete_image_plate_extract_percentage= round((overall_complete_image_plate_extract_count / overall_correct_image_plate_extract_count) * 100, 2) if overall_correct_image_plate_extract_count != 0 else 0.0



            #OVERALL FEED IN INFO
            overall_tried_feed_in_count = sum(total_tried_feed_in_counts.values())
            overall_successful_feed_in_count = sum(total_successful_feed_in_counts.values())
            overall_failed_feed_in_count = sum(total_failed_feed_in_counts.values())
            overall_failed_feed_in_due_to_unextracted_iai_count = sum(total_failed_feed_in_due_to_unextracted_iai_counts.values())
            overall_failed_feed_in_due_to_unextracted_pr_count = sum(total_failed_feed_in_due_to_unextracted_pr_counts.values())
            overall_successful_feed_in_percentage = round((overall_successful_feed_in_count / overall_tried_feed_in_count) * 100, 2) if overall_tried_feed_in_count != 0 else 0.0
            overall_failed_feed_in_due_to_unextracted_iai_percentage = round((overall_failed_feed_in_due_to_unextracted_iai_count / overall_failed_feed_in_count) * 100, 2) if overall_failed_feed_in_count != 0 else 0.0
            overall_failed_feed_in_due_to_unextracted_pr_percentage = round((overall_failed_feed_in_due_to_unextracted_pr_count / overall_failed_feed_in_count) * 100, 2) if overall_failed_feed_in_count != 0 else 0.0


            #CLASSES FOR THE TABLE DATA
            td_classes_overall['duration'] = GetStatsContext.get_td_class(overall_correct_durations_count, tot_entries)
            td_classes_overall['iai_vehicle_detection'] = GetStatsContext.get_td_class(overall_correct_iai_vehicle_detection_count, tot_entries)
            td_classes_overall['iai_vehicle_prediction'] = GetStatsContext.get_td_class(overall_correct_iai_vehicle_prediction_count, tot_entries)
            td_classes_overall['iai_colour_detection'] = GetStatsContext.get_td_class(overall_correct_iai_colour_detection_count, tot_entries)
            td_classes_overall['pr_vehicle_detection'] = GetStatsContext.get_td_class(overall_correct_pr_vehicle_detection_count, tot_entries)
            td_classes_overall['pr_vehicle_prediction'] = GetStatsContext.get_td_class(overall_correct_pr_vehicle_prediction_count, tot_entries)
            td_classes_overall['pr_colour_detection'] = GetStatsContext.get_td_class(overall_correct_pr_colour_detection_count, tot_entries)
            td_classes_overall['license_plate_identification'] = GetStatsContext.get_td_class(overall_correct_license_plate_identification_count, tot_entries)
            td_classes_overall['image_iai_extract'] = GetStatsContext.get_td_class(overall_correct_image_iai_extract_count, overall_image_iai_extract_count)
            td_classes_overall['image_pr_extract'] = GetStatsContext.get_td_class(overall_correct_image_pr_extract_count, overall_image_pr_extract_count)
            td_classes_overall['image_plate_extract'] = GetStatsContext.get_td_class(overall_correct_image_plate_extract_count, overall_image_plate_extract_count)
            td_classes_overall['iai_identified_dom_colour_as_nondom'] = GetStatsContext.get_td_class(overall_iai_identified_dom_colour_as_nondom_correct_count, overall_iai_identified_dom_colour_as_nondom_count)
            td_classes_overall['pr_identified_dom_colour_as_nondom'] = GetStatsContext.get_td_class(overall_pr_identified_dom_colour_as_nondom_correct_count, overall_pr_identified_dom_colour_as_nondom_count)
            td_classes_overall['complete_iai'] = GetStatsContext.get_td_class(overall_complete_image_iai_extract_count, overall_correct_image_iai_extract_count)
            td_classes_overall['complete_pr'] = GetStatsContext.get_td_class(overall_complete_image_pr_extract_count, overall_correct_image_pr_extract_count)
            td_classes_overall['complete_plate'] = GetStatsContext.get_td_class(overall_complete_image_plate_extract_count, overall_correct_image_plate_extract_count)
            td_classes_overall['successful_feed_in'] = GetStatsContext.get_td_class(overall_successful_feed_in_count, overall_tried_feed_in_count)



            response['date_labels'] = date_labels
            response['day_objs'] = day_objs
            response['number_of_entries_data'] = number_of_entries_data


            response['durations_auto'] = durations_auto                            
            response['durations_auto_stats'] = durations_auto_stats                            
            response['durations_manual'] = durations_manual                                                
            response['durations_manual_stats'] = durations_manual_stats 
            response['correct_durations_counts'] = correct_durations_counts


            response['iai_individual_vehicle_detection_auto_names'] = iai_individual_vehicle_detection_auto_names
            response['iai_individual_vehicle_detection_auto_counts'] = iai_individual_vehicle_detection_auto_counts
            response['iai_individual_vehicle_detection_manual_names'] = iai_individual_vehicle_detection_manual_names
            response['iai_individual_vehicle_detection_manual_counts'] = iai_individual_vehicle_detection_manual_counts
            response['iai_vehicle_detection_chart_colours'] = iai_vehicle_detection_chart_colours
            response['iai_vehicle_detection_accuracies'] = iai_vehicle_detection_accuracies

            response['iai_individual_vehicle_prediction_auto_names'] = iai_individual_vehicle_prediction_auto_names
            response['iai_individual_vehicle_prediction_auto_counts'] = iai_individual_vehicle_prediction_auto_counts
            response['iai_individual_vehicle_prediction_manual_names'] = iai_individual_vehicle_prediction_manual_names
            response['iai_individual_vehicle_prediction_manual_counts'] = iai_individual_vehicle_prediction_manual_counts
            response['iai_vehicle_prediction_chart_colours'] = iai_vehicle_prediction_chart_colours
            response['iai_vehicle_prediction_accuracies'] = iai_vehicle_prediction_accuracies

            response['iai_individual_colour_detection_auto_names'] = iai_individual_colour_detection_auto_names
            response['iai_individual_colour_detection_auto_counts'] = iai_individual_colour_detection_auto_counts
            response['iai_individual_colour_detection_manual_names'] = iai_individual_colour_detection_manual_names
            response['iai_individual_colour_detection_manual_counts'] = iai_individual_colour_detection_manual_counts
            response['iai_colour_detection_chart_colours'] = iai_colour_detection_chart_colours
            response['iai_colour_detection_accuracies'] = iai_colour_detection_accuracies


            response['pr_individual_vehicle_detection_auto_names'] = pr_individual_vehicle_detection_auto_names
            response['pr_individual_vehicle_detection_auto_counts'] = pr_individual_vehicle_detection_auto_counts
            response['pr_individual_vehicle_detection_manual_names'] = pr_individual_vehicle_detection_manual_names
            response['pr_individual_vehicle_detection_manual_counts'] = pr_individual_vehicle_detection_manual_counts
            response['pr_vehicle_detection_chart_colours'] = pr_vehicle_detection_chart_colours
            response['pr_vehicle_detection_accuracies'] = pr_vehicle_detection_accuracies

            response['pr_individual_vehicle_prediction_auto_names'] = pr_individual_vehicle_prediction_auto_names
            response['pr_individual_vehicle_prediction_auto_counts'] = pr_individual_vehicle_prediction_auto_counts
            response['pr_individual_vehicle_prediction_manual_names'] = pr_individual_vehicle_prediction_manual_names
            response['pr_individual_vehicle_prediction_manual_counts'] = pr_individual_vehicle_prediction_manual_counts
            response['pr_vehicle_prediction_chart_colours'] = pr_vehicle_prediction_chart_colours
            response['pr_vehicle_prediction_accuracies'] = pr_vehicle_prediction_accuracies

            response['pr_individual_colour_detection_auto_names'] = pr_individual_colour_detection_auto_names
            response['pr_individual_colour_detection_auto_counts'] = pr_individual_colour_detection_auto_counts
            response['pr_individual_colour_detection_manual_names'] = pr_individual_colour_detection_manual_names
            response['pr_individual_colour_detection_manual_counts'] = pr_individual_colour_detection_manual_counts
            response['pr_colour_detection_chart_colours'] = pr_colour_detection_chart_colours
            response['pr_colour_detection_accuracies'] = pr_colour_detection_accuracies

            response['correct_iai_vehicle_detection_counts'] = correct_iai_vehicle_detection_counts
            response['correct_iai_vehicle_prediction_counts'] = correct_iai_vehicle_prediction_counts
            response['correct_iai_colour_detection_counts'] = correct_iai_colour_detection_counts
            response['correct_pr_vehicle_detection_counts'] = correct_pr_vehicle_detection_counts
            response['correct_pr_vehicle_prediction_counts'] = correct_pr_vehicle_prediction_counts
            response['correct_pr_colour_detection_counts'] = correct_pr_colour_detection_counts
            response['correct_license_plate_identification_counts'] = correct_license_plate_identification_counts

            response['iai_identified_dom_colour_as_nondom_count'] = iai_identified_dom_colour_as_nondom_count
            response['iai_identified_dom_colour_as_nondom_correct_count'] = iai_identified_dom_colour_as_nondom_correct_count
            response['iai_identified_dom_colour_as_nondom_percentage'] = iai_identified_dom_colour_as_nondom_percentage
            response['pr_identified_dom_colour_as_nondom_count'] = pr_identified_dom_colour_as_nondom_count
            response['pr_identified_dom_colour_as_nondom_correct_count'] = pr_identified_dom_colour_as_nondom_correct_count
            response['pr_identified_dom_colour_as_nondom_percentage'] = pr_identified_dom_colour_as_nondom_percentage

            response['tried_feed_in_counts'] = tried_feed_in_counts
            response['successful_feed_in_counts'] = successful_feed_in_counts
            response['successful_feed_in_percentages'] = successful_feed_in_percentages
            response['failed_feed_in_counts'] = failed_feed_in_counts
            response['failed_feed_in_due_to_unextracted_iai_counts'] = failed_feed_in_due_to_unextracted_iai_counts
            response['failed_feed_in_due_to_unextracted_pr_counts'] = failed_feed_in_due_to_unextracted_pr_counts
            response['failed_feed_in_due_to_unextracted_iai_percentages'] = failed_feed_in_due_to_unextracted_iai_percentages
            response['failed_feed_in_due_to_unextracted_pr_percentages'] = failed_feed_in_due_to_unextracted_pr_percentages
            response['total_tried_feed_in_counts'] = total_tried_feed_in_counts
            response['total_successful_feed_in_counts'] = total_successful_feed_in_counts
            response['total_failed_feed_in_counts'] = total_failed_feed_in_counts
            response['total_failed_feed_in_due_to_unextracted_iai_counts'] = total_failed_feed_in_due_to_unextracted_iai_counts
            response['total_failed_feed_in_due_to_unextracted_pr_counts'] = total_failed_feed_in_due_to_unextracted_pr_counts
            response['daily_successful_feed_in_percentages'] = daily_successful_feed_in_percentages
            response['daily_failed_feed_in_due_to_unextracted_iai_percentages'] =  daily_failed_feed_in_due_to_unextracted_iai_percentages
            response['daily_failed_feed_in_due_to_unextracted_pr_percentages'] = daily_failed_feed_in_due_to_unextracted_pr_percentages

            response['image_angle_names']  = image_angle_names
            response['image_angle_chart_colours']  = image_angle_chart_colours
            response['image_iai_extract_counts']  = image_iai_extract_counts
            response['image_pr_extract_counts']  = image_pr_extract_counts
            response['image_plate_extract_counts']  = image_plate_extract_counts
            response['correct_image_iai_extract_counts']  = correct_image_iai_extract_counts
            response['correct_image_pr_extract_counts']  = correct_image_pr_extract_counts
            response['correct_image_plate_extract_counts']  = correct_image_plate_extract_counts
            response['complete_image_iai_extract_counts'] = complete_image_iai_extract_counts
            response['complete_image_pr_extract_counts'] = complete_image_pr_extract_counts
            response['complete_image_plate_extract_counts'] = complete_image_plate_extract_counts
            response['total_image_iai_extract_counts']  = total_image_iai_extract_counts
            response['total_image_pr_extract_counts']  = total_image_pr_extract_counts
            response['total_image_plate_extract_counts']  = total_image_plate_extract_counts
            response['total_correct_image_iai_extract_counts']  = total_correct_image_iai_extract_counts
            response['total_correct_image_pr_extract_counts']  = total_correct_image_pr_extract_counts
            response['total_correct_image_plate_extract_counts']  = total_correct_image_plate_extract_counts
            response['total_complete_image_iai_extract_counts'] = total_complete_image_iai_extract_counts
            response['total_complete_image_pr_extract_counts'] = total_complete_image_pr_extract_counts
            response['total_complete_image_plate_extract_counts'] = total_complete_image_plate_extract_counts
            response['image_iai_extract_accuracies']  = image_iai_extract_accuracies
            response['image_pr_extract_accuracies']  = image_pr_extract_accuracies
            response['image_plate_extract_accuracies']  = image_plate_extract_accuracies
            response['complete_image_iai_extract_percentage'] = complete_image_iai_extract_percentage
            response['complete_image_pr_extract_percentage'] = complete_image_pr_extract_percentage
            response['complete_image_plate_extract_percentage'] = complete_image_plate_extract_percentage
            response['daily_complete_image_iai_extract_percentage'] = daily_complete_image_iai_extract_percentage
            response['daily_complete_image_pr_extract_percentage'] = daily_complete_image_pr_extract_percentage
            response['daily_complete_image_plate_extract_percentage'] = daily_complete_image_plate_extract_percentage


            response['day_accuracies_keys'] = day_accuracies_keys
            response['day_accuracies_keys_exclusions'] = day_accuracies_keys_exclusions
            response['day_accuracies'] = day_accuracies
            response['day_correct_counts'] = day_correct_counts
            response['day_accuracies_chart_colours'] = day_accuracies_chart_colours

            response['timings'] = timings
            response['timings_mins'] = timings_mins
            response['timings_maxes'] = timings_maxes
            response['timings_totals'] = timings_totals
            response['timings_avgs'] = timings_avgs
            response['timings_keys'] = timings_keys

            response['num_days'] = num_days
            response['tot_entries'] = tot_entries


            response['overall_accuracies_mins_dates'] = overall_accuracies_mins_dates
            response['overall_accuracies_mins'] = overall_accuracies_mins
            response['overall_accuracies_maxes_dates'] = overall_accuracies_maxes_dates
            response['overall_accuracies_maxes'] = overall_accuracies_maxes
            response['overall_accuracies'] = overall_accuracies

            response['overall_correct_durations_count'] = overall_correct_durations_count
            response['overall_correct_iai_vehicle_detection_count'] = overall_correct_iai_vehicle_detection_count   
            response['overall_correct_iai_vehicle_prediction_count'] = overall_correct_iai_vehicle_prediction_count
            response['overall_correct_iai_colour_detection_count'] = overall_correct_iai_colour_detection_count
            response['overall_correct_pr_vehicle_detection_count'] = overall_correct_pr_vehicle_detection_count
            response['overall_correct_pr_vehicle_prediction_count'] = overall_correct_pr_vehicle_prediction_count
            response['overall_correct_pr_colour_detection_count'] = overall_correct_pr_colour_detection_count
            response['overall_correct_license_plate_identification_count'] = overall_correct_license_plate_identification_count

            response['overall_correct_image_iai_extract_count'] = overall_correct_image_iai_extract_count
            response['overall_image_iai_extract_count'] = overall_image_iai_extract_count
            response['overall_correct_image_pr_extract_count'] = overall_correct_image_pr_extract_count
            response['overall_image_pr_extract_count'] = overall_image_pr_extract_count
            response['overall_correct_image_plate_extract_count'] = overall_correct_image_plate_extract_count
            response['overall_image_plate_extract_count'] = overall_image_plate_extract_count



            response['overall_tried_feed_in_count'] = overall_tried_feed_in_count
            response['overall_successful_feed_in_count'] = overall_successful_feed_in_count
            response['overall_failed_feed_in_count'] = overall_failed_feed_in_count
            response['overall_failed_feed_in_due_to_unextracted_iai_count'] = overall_failed_feed_in_due_to_unextracted_iai_count
            response['overall_failed_feed_in_due_to_unextracted_pr_count'] = overall_failed_feed_in_due_to_unextracted_pr_count
            response['overall_successful_feed_in_percentage'] = overall_successful_feed_in_percentage
            response['overall_failed_feed_in_due_to_unextracted_iai_percentage'] = overall_failed_feed_in_due_to_unextracted_iai_percentage
            response['overall_failed_feed_in_due_to_unextracted_pr_percentage'] = overall_failed_feed_in_due_to_unextracted_pr_percentage



            response['overall_timings_mins_dates'] = overall_timings_mins_dates
            response['overall_timings_mins'] = overall_timings_mins
            response['overall_timings_maxes_dates'] = overall_timings_maxes_dates
            response['overall_timings_maxes'] = overall_timings_maxes
            response['overall_timings_totals'] = overall_timings_totals
            response['overall_timings_avgs_day'] = overall_timings_avgs_day
            response['overall_timings_avgs_entry'] = overall_timings_avgs_entry

            response['overall_iai_identified_dom_colour_as_nondom_count'] = overall_iai_identified_dom_colour_as_nondom_count 
            response['overall_iai_identified_dom_colour_as_nondom_correct_count'] = overall_iai_identified_dom_colour_as_nondom_correct_count 
            response['overall_pr_identified_dom_colour_as_nondom_count'] = overall_pr_identified_dom_colour_as_nondom_count 
            response['overall_pr_identified_dom_colour_as_nondom_correct_count'] = overall_pr_identified_dom_colour_as_nondom_correct_count 
            response['overall_iai_identified_dom_colour_as_nondom_percentage'] = overall_iai_identified_dom_colour_as_nondom_percentage
            response['overall_pr_identified_dom_colour_as_nondom_percentage'] = overall_pr_identified_dom_colour_as_nondom_percentage

            response['overall_complete_image_iai_extract_count'] = overall_complete_image_iai_extract_count
            response['overall_complete_image_iai_extract_percentage'] = overall_complete_image_iai_extract_percentage
            response['overall_complete_image_pr_extract_count'] = overall_complete_image_pr_extract_count
            response['overall_complete_image_pr_extract_percentage'] = overall_complete_image_pr_extract_percentage
            response['overall_complete_image_plate_extract_count'] = overall_complete_image_plate_extract_count
            response['overall_complete_image_plate_extract_percentage'] = overall_complete_image_plate_extract_percentage



            response['td_classes_daily'] = td_classes_daily
            response['td_classes_daily_totals'] = td_classes_daily_totals
            response['td_classes_overall'] = td_classes_overall

        return response

    def get_td_class(correct_count, total_count):
        """Used to determine the class of a table data element (\<td\>...\</td\>) by comparing the number that are correct to the total number

        :param correct_count: number that was correct  
        :type correct_count: int  
        :param total_count: total number  
        :type total_count: int  
        :return: the class of the td element  
        :rtype: str  
        """

        td_class = ''

        if  correct_count == total_count and total_count != 0:
            td_class = 'table-success'
        elif correct_count < total_count and total_count != 0 and correct_count != 0:
            td_class = 'table-warning'
        elif correct_count == 0 and total_count != 0:
            td_class = 'table-danger'

        return td_class

    def get_td_class_daily(correct_count, manual_count, total_count):
        """Used to determine the class of a table data element (\<td\>...\</td\>) by comparing the number that are correct, number identified in the manual 
        analysis and the total number

        :param correct_count: number that was correct  
        :type correct_count: int  
        :param manual_count: number that was identfied in the manual analysis  
        :type manual_count: int  
        :param total_count: total number  
        :type total_count: int  
        :return: the class of the td element  
        :rtype: str  
        """

        td_class = ''

        if  correct_count == total_count and total_count != 0:
            td_class = 'table-success'
        elif correct_count < total_count and total_count != 0 and correct_count != 0:
            td_class = 'table-warning'
        elif correct_count == 0 and total_count != 0:
            td_class = 'table-danger'
        elif manual_count != 0 and total_count == 0:
            td_class = 'table-danger'
        return td_class
