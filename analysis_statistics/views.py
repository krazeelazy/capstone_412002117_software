"""Has a class that has functions to gather all the data for the template.
"""

from django.views.generic import ListView

from django_filters.views import FilterView

from analysis.models import Day, Entry

from psa.filters import StatisticsFilter

from .models import GetStatsContext

class AllStatisticsView(FilterView, ListView):
    """Gets the data needed by the analysis_statistics/home.html template. Uses :class:`django_filters.views.FilterView` and 
    :class:`django.views.generic.ListView`.   
    """

    model = Day
    """(Day) the model the view uses to get data from the database"""

    template_name = 'analysis_statistics/home.html'
    """(str) the name of the template the view is passing the data to (analysis_statistics/home.html)"""


    filterset_class = StatisticsFilter
    """(StatisticsFilter) the class of the filterset in :mod:`psa.filters`"""


    def get_queryset(self):
        """Returns the list of items for this view.

        :return: a list of the days that have at least one entry with a status of complete (sorted in ascending order by date)  
        :rtype: list  
        """

        entries = Entry.objects.filter(status='complete')
        return Day.objects.filter(entries__in=entries).distinct().order_by('date')

    def get_context_data(self, **kwargs):  
        """Calls the :meth:`analysis_statistics.models.GetStatsContext.get_stats_context` method to get all the data needed by the template and adds it to 
        the context dictionary

        :return: a dictionary containing all the context data for the template
        :rtype: dict
        """

        context = super(AllStatisticsView, self).get_context_data(**kwargs)

        for (key, value) in GetStatsContext.get_stats_context(context['object_list']).items():
            context[key] = value

        return context
