"""Has the configuration for the analysis_statistics app (sets the name)
"""

from django.apps import AppConfig


class AnalysisStatisticsConfig(AppConfig):
    """Class representing the analysis_statistics application and its configuration. Subclass of :class:`django.apps.AppConfig`.
    """
    
    name = 'analysis_statistics'
    """ (str) The name of the application
    """
