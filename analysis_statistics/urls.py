from django.urls import path
from .views import AllStatisticsView

urlpatterns = [
    path('',AllStatisticsView.as_view(), name='analysis_statistics-home')
]