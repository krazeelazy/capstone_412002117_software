#!/usr/bin/env python3

# @author: Mellissa Marie (412002117)

""" This script is used as part of the software component of my capstone project.
The script does the analysis of the input data. It takes the arrival and departure times captured by the hardware section to determine the duration of 
stay. The image captured is processed to determine things such as vehicle type, colour and license plate number. The time it takes to process the data is 
also noted. All the information produced is stored in a JSON file that is fed into the final part of the solution. 

As the script runs, the function calls and times for the functions to run are logged in the terminal and a log file (one per day). The script is written 
so that even if the project directory is moved, it should function without having to change the values of the constants that are parent directories used throughout 
the script.  

The CLOUD_API_TOKEN constant will have to be changed on line 56.
"""

#---------------IMPORTS------------
from datetime import datetime
import json
import os
import os.path
import time
import imutils
import cv2
import math
from imageai.Detection import ObjectDetection
from imageai.Prediction import ImagePrediction
import requests
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import shutil
import logging
import random as rng

rng.seed(time.time())

#-------------CONSTANTS------------
#: parent dir of entire project (parent dir of this file) (dynamically assigned based on the location of the project)
PARENT_DIR = os.path.dirname(os.path.abspath(__file__))
#: data directory location (contains images and JSON file)
DATA_DIR = os.path.join(PARENT_DIR, 'data', '')
#: new directory where new data will be put to be analyzed
NEW_DIR = os.path.join(DATA_DIR, 'new', '')
#: analyzed directory where the analyzed data and images are stored
ANALYZED_DIR = os.path.join(DATA_DIR, 'analyzed', '')
#: location of helper model and info files
HELPERS_DIR = os.path.join(PARENT_DIR, 'helpers', '')
#: location of the detection model for imageai detection
DETECTION_MODEL_PATH = os.path.join(HELPERS_DIR, 'detection_models','resnet50_coco_best_v2.0.1.h5')
#: location of the prediction model for imageai prediction
PREDICTION_MODEL_PATH = os.path.join(HELPERS_DIR, 'prediction_models','DenseNet-BC-121-32.h5')
#: location of the colour_info.csv file for the colour analysis
COLOUR_INFO_CSV = os.path.join(HELPERS_DIR, 'colour_info.csv')
#: the key used to access the plate recognizer api
CLOUD_API_TOKEN = 'YOUR API KEY HERE'


#-------------LOGGER------------
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

FORMATTER = logging.Formatter('%(message)s')

STREAM_HANDLER = logging.StreamHandler()
STREAM_HANDLER.setFormatter(FORMATTER)

LOGGER.addHandler(STREAM_HANDLER)


#-------------FUNCTIONS-----------

# ------------------------ ANALYSIS FUNCTIONS---------------------------

#--------DURATION--------

def get_duration(arrival_string, departure_string):
	"""Determines how long the vehicle was in the spot and returns the duration

	:param arrival_string: a string containing the arrival time  
	:type arrival_string: str  
	:param departure_string: a string containing the departure time  
	:type departure_string: str  
	:return: a dictionary containing the duration in seconds and as a human readable h:m:s string  
	:rtype: dict  
	"""


	get_duration_start_time = time.time()
	LOGGER.info(f'Start get_duration for {arrival_string}')

	#convert the arrival_string and departure_string to datetime objects
	arrival_time = datetime.strptime(arrival_string, "%Y-%m-%d-%H:%M:%S")
	departure_time = datetime.strptime(departure_string, "%Y-%m-%d-%H:%M:%S")

	duration = departure_time - arrival_time
	duration_in_s = duration.total_seconds() 

	def hours():
		return divmod(duration_in_s, 3600) # Seconds in an hour = 3600

	def minutes(seconds = None):
		return divmod(seconds if seconds != None else duration_in_s, 60) # Seconds in a minute = 60

	def seconds(seconds = None):
		if seconds != None:
			return divmod(seconds, 1)   
		return duration_in_s

	def total_duration():
		h = hours()
		m = minutes(h[1])# Use remainder to calculate next variable
		s = seconds(m[1])

		return f"{int(h[0])} hours, {int(m[0])} minutes and {int(s[0])} seconds"

	get_duration_time = round(time.time() - get_duration_start_time, 3)
	LOGGER.info(f'End get_duration for {arrival_string}')
	LOGGER.info(f"--- get_duration for {arrival_string} took {get_duration_time} seconds ---")

	return {
		'seconds': int(seconds()),
		'human_readable': total_duration()
	}


#-------VEHICLE TYPE AND LICENSE PLATE

def find_center(x1,y1,x2,y2, image_name):
	""" Finds the center of images, objects (bounding box of object), and the center of boxes drawn on analyzed images

	:param x1: the first x coordinate  
	:type x1: int  
	:param y1: the first y coordinate  
	:type y1: int  
	:param x2: the second x coordinate  
	:type x2: int  
	:param y2: the second y coordinate  
	:type y2: int  
	:param image_name: the name of the image (only used for logging)  
	:type image_name: str  
	:return: the center coordinates  
	:rtype: tuple  
	"""

	find_center_start_time = time.time()
	LOGGER.info(f'Start find_center for {image_name}')

	center = (round((x1+x2)/2), round((y1+y2)/2))

	find_center_time = round(time.time() - find_center_start_time, 3)
	LOGGER.info(f'End find_center for {image_name}')
	LOGGER.info(f"--- find_center for {image_name} took {find_center_time} seconds ---")

	return center

def find_distance_from_img_center(img_center,obj_center, image_name):
	"""Determines the distance of the object's center from the image's center

	:param img_center: center coordinates of the image  
	:type img_center: tuple  
	:param obj_center: center coordinates of the object  
	:type obj_center: tuple  
	:param image_name: the name of the image (only used for logging)  
	:type image_name: str  
	:return: the distance between the two centers  
	:rtype: float  
	"""

	find_distance_from_img_center_start_time = time.time()
	LOGGER.info(f'Start find_distance_from_img_center for {image_name}')

	distance = math.sqrt(( (img_center[0]-obj_center[0])**2 )+( (img_center[1]-obj_center[1])**2 ) )
	
	find_distance_from_img_center_time = round(time.time() - find_distance_from_img_center_start_time ,3)
	LOGGER.info(f'End find_distance_from_img_center for {image_name}')
	LOGGER.info(f"--- find_distance_from_img_center for {image_name} took {find_distance_from_img_center_time} seconds ---")

	return distance

#--imageai
def imageai_detect(image_path, extracted_image_sub_dir, analyzed_imageai_images_sub_dir):
	"""Detects the Imageai detection class (bicycle, bus, car, motorcycle, or truck) of the object in the center of the image, extracts the object and 
	makes an analyzed image 

	:param image_path: the path to the image being processed  
	:type image_path: str  
	:param extracted_image_sub_dir: path to the images/extracted/imageai sub directory  
	:type extracted_image_sub_dir: str  
	:param analyzed_imageai_images_sub_dir: path to the images/analyzed/imageai subdirectory  
	:type analyzed_imageai_images_sub_dir: str  
	:return: None or a dictionary containing the imageai detection class, imageai detection probability, imageai coordinates, extracted imageai image path and analyzed imageai image path  
	:rtype: None or dict  
	"""

	imageai_detect_start_time = time.time()
	LOGGER.info(f'Start imageai_detect for {image_path}')

	image =cv2.imread(image_path)

	#DETECTION
	detector = ObjectDetection()
	detector.setModelTypeAsRetinaNet()
	detector.setModelPath(DETECTION_MODEL_PATH)
	detector.loadModel()
	custom = detector.CustomObjects(bicycle=True, car=True, motorcycle=True, bus=True, truck=True) #only detect these objects

	returned_image, detections = detector.detectCustomObjectsFromImage(custom_objects=custom, input_image=image,input_type="array",output_type="array")

	if detections == []:#if no objects were detected
		imageai_detect_time = round(time.time() - imageai_detect_start_time, 3)
		LOGGER.info(f'End imageai_detect for {image_path}. NO VEHICLES DETECTED')
		LOGGER.info(f"--- imageai_detect for {image_path} took {imageai_detect_time} seconds ---")
		
		return None

	#determine image dimensions
	image_height = image.shape[0]
	image_width = image.shape[1]

	#find center point
	image_center = find_center(image_width,image_height,0,0,image_path)

	min_dist = image_height if image_height >= image_width else image_width
	closest_obj = None

	for (each_object, i) in zip(detections, range(1,len(detections)+1)):
		center = find_center(each_object["box_points"][0],each_object["box_points"][1],
							each_object["box_points"][2],each_object["box_points"][3], f'{image_path} object {i}/{len(detections)}')
		dist = find_distance_from_img_center(image_center, center, f'{image_path} object {i}/{len(detections)}')
		if dist < min_dist:
			min_dist = dist
			closest_obj = each_object

	detection_class = closest_obj["name"]
	detection_probability = round(closest_obj["percentage_probability"],2)
	
	x_min = closest_obj["box_points"][0] if closest_obj["box_points"][0] >= 0 else 0
	y_min = closest_obj["box_points"][1] if closest_obj["box_points"][1] >= 0 else 0
	x_max = closest_obj["box_points"][2] if closest_obj["box_points"][2] >= 0 else 0
	y_max = closest_obj["box_points"][3] if closest_obj["box_points"][3] >= 0 else 0
	extracted_imageai_image_path = make_extracted_image(image_path, extracted_image_sub_dir, x_min, y_min, x_max, y_max)

	#save the image with all the detected objects
	orig_image_name = os.path.split(image_path)[1]
	extension =os.path.splitext(orig_image_name)[1]    
	analyzed_imageai_image_name = orig_image_name.replace(f'{extension}',f'_analyzed_imageai{extension}')
	analyzed_imageai_image_path = os.path.join(analyzed_imageai_images_sub_dir, analyzed_imageai_image_name)
	cv2.imwrite(analyzed_imageai_image_path,returned_image)

	imageai_detect_time = round(time.time() - imageai_detect_start_time, 3)
	LOGGER.info(f'End imageai_detect for {image_path}')
	LOGGER.info(f"--- imageai_detect for {image_path} took {imageai_detect_time} seconds ---")

	return{
		'imageai_detection_class': detection_class,
		'imageai_detection_probability':detection_probability,
		'imageai_coordinates':{
			'x_min':x_min,
			'y_min': y_min,
			'x_max':x_max,
			'y_max':y_max   
		},
		'extracted_imageai_image_path':extracted_imageai_image_path,
		'analyzed_imageai_image_path': analyzed_imageai_image_path
	}


def imageai_predict(extracted_image_path):
	"""Gets a more specific prediction of what the object in the image is (used in both Imageai and Plate Recognizer analyses)

	:param extracted_image_path: the path to an extracted image  
	:type extracted_image_path: str  
	:return: a dictionary containing the prediction class and prediction probability  
	:rtype: dict  
	"""

	imageai_predict_start_time = time.time()
	LOGGER.info(f'Start imageai_predict for {extracted_image_path}')

	#PREDICTION
	prediction = ImagePrediction()
	prediction.setModelTypeAsDenseNet()
	prediction.setModelPath(PREDICTION_MODEL_PATH)
	prediction.loadModel()

	prediction_class, probability = prediction.predictImage(image_input=extracted_image_path, input_type="file", result_count=1 )
	
	imageai_predict_time = round(time.time() - imageai_predict_start_time, 3)
	LOGGER.info(f'End imageai_predict for {extracted_image_path}')
	LOGGER.info(f"--- imageai_predict for {extracted_image_path} took {imageai_predict_time} seconds ---")

	return{
		'prediction_class': prediction_class[0],
		'prediction_probability': round(probability[0],2)
	}

#-- plate recognizer
def plate_recognizer_detect(image_path, extracted_vehicle_image_sub_dir, extracted_plate_image_sub_dir, analyzed_pr_images_sub_dir, analyzed_pr_base_image=None, x_compensate=0, y_compensate=0):
	"""Detects the Plate Recognizer detection class (Ambulance, Bus, Car, Limousine, Motorcycle, Taxi, Truck, Van, Unknown) of the object in the 
	center of the image, extracts the object and makes an analyzed image

	:param image_path: the path to the image being processed  
	:type image_path: str  
	:param extracted_vehicle_image_sub_dir: path to the images/extracted/pr sub directory  
	:type extracted_vehicle_image_sub_dir: str  
	:param extracted_plate_image_sub_dir: path to the images/extracted/plate sub directory  
	:type extracted_plate_image_sub_dir: str  
	:param analyzed_pr_images_sub_dir: path to the images/analyzed/pr subdirectory  
	:type analyzed_pr_images_sub_dir: str  
	:param analyzed_pr_base_image: path to the original image (used if the extracted Imageai image is fed in using image_path), defaults to None  
	:type analyzed_pr_base_image: str, optional  
	:param x_compensate: how much the x coordinate has to be shifted if the extracted Imageai image was fed in using image_path, defaults to 0  
	:type x_compensate: int, optional  
	:param y_compensate: how much the y coordinate has to be shifted if the extracted Imageai image was fed in using image_path, defaults to 0  
	:type y_compensate: int, optional  
	:return: None or a dictionary containing the pr(plate recognizer) detection class, pr detection probability, pr vehicle coordinates, extracted pr 
	         image path, plate number, plate probability, plate coordinates extracted plate image path and analyzed pr image path  
	:rtype: None or dict  
	"""

	plate_recognizer_detect_start_time = time.time()
	LOGGER.info(f'Start plate_recognizer_detect for {image_path}')

	image =cv2.imread(image_path)

	with open(image_path, 'rb') as fp:
		response = requests.post(
			'https://api.platerecognizer.com/v1/plate-reader/',
			files=dict(upload=fp),
			headers={'Authorization': f'Token {CLOUD_API_TOKEN}'})
		
	resp=response.json()
	results = resp['results']

	if results == []: #if no objects were detected
		plate_recognizer_detect_time = round(time.time() - plate_recognizer_detect_start_time, 3)
		LOGGER.info(f'End plate_recognizer_detect for {image_path}. NO VEHICLES DETECTED')
		LOGGER.info(f"--- plate_recognizer_detect for {image_path} took {plate_recognizer_detect_time} seconds ---")

		return None

	#determine image dimensions
	image_height = image.shape[0]
	image_width = image.shape[1]

	#find center point
	image_center = find_center(image_width,image_height,0,0,image_path)

	min_dist = image_height if image_height >= image_width else image_width
	closest_obj = None

	image_info = []

	for (result, i) in zip(results, range(1,len(results)+1)):
		image_info.append({
			'vehicle_coordinates': result['vehicle']['box'],
			'type': result['vehicle']['type'],
			'detection_probability': round(result['vehicle']['score']*100,2),
			'plate_coordinates': result['box'],
			'plate_number' : str.upper(result['plate']),
			'plate_probability': round(result['score']*100,2)
		})
		center = find_center(result['vehicle']['box']['xmin'],result['vehicle']['box']['ymin'],result['vehicle']['box']['xmax'],
							result['vehicle']['box']['ymax'], f'{image_path} object {i}/{len(results)}')
		dist = find_distance_from_img_center(image_center, center, f'{image_path} object {i}/{len(results)}')
		if dist < min_dist:
			min_dist = dist
			closest_obj = result

	detection_class = closest_obj['vehicle']['type']
	detection_probability = round(closest_obj['vehicle']['score']*100,2)
	vehicle_box  = closest_obj['vehicle']['box']
	vehicle_box['xmin'] = vehicle_box['xmin'] if vehicle_box['xmin'] >= 0 else 0
	vehicle_box['ymin'] = vehicle_box['ymin'] if vehicle_box['ymin'] >= 0 else 0
	vehicle_box['xmax'] = vehicle_box['xmax'] if vehicle_box['xmax'] >= 0 else 0
	vehicle_box['ymax'] = vehicle_box['ymax'] if vehicle_box['ymax'] >= 0 else 0
	extracted_pr_image_path = make_extracted_image(image_path,extracted_vehicle_image_sub_dir,
													vehicle_box['xmin'],vehicle_box['ymin'],vehicle_box['xmax'],vehicle_box['ymax'])

	plate_number = str.upper(closest_obj['plate'])
	plate_probability = round(closest_obj['score']*100,2)
	plate_box = closest_obj['box']
	extracted_plate_image_path = make_extracted_image(image_path,extracted_plate_image_sub_dir,
														plate_box['xmin'],plate_box['ymin'],plate_box['xmax'],plate_box['ymax'])


	if analyzed_pr_base_image != None: #if the feed in method is being used
		for info in image_info: #the analyzed image will use the original image so the coordinated of the objects detected using extracted_iai will have to be compensated
			info['vehicle_coordinates']['xmin'] += x_compensate
			info['vehicle_coordinates']['ymin'] += y_compensate
			info['vehicle_coordinates']['xmax'] += x_compensate
			info['vehicle_coordinates']['ymax'] += y_compensate
			info['plate_coordinates']['xmin'] += x_compensate
			info['plate_coordinates']['ymin'] += y_compensate
			info['plate_coordinates']['xmax'] += x_compensate
			info['plate_coordinates']['ymax'] += y_compensate

		analyzed_pr_image_path = make_analyzed_pr_image(analyzed_pr_base_image, analyzed_pr_images_sub_dir, image_info)
	
	elif analyzed_pr_base_image == None:
		analyzed_pr_image_path = make_analyzed_pr_image(image_path, analyzed_pr_images_sub_dir, image_info)

	plate_recognizer_detect_end_time = round(time.time() - plate_recognizer_detect_start_time, 3)
	LOGGER.info(f'End plate_recognizer_detect for {image_path}')
	LOGGER.info(f"--- plate_recognizer_detect for {image_path} took {plate_recognizer_detect_end_time} seconds ---")

	return{
		'pr_detection_class': detection_class,
		'pr_detection_probability': detection_probability,
		'pr_vehicle_coordinates':  vehicle_box,
		'extracted_pr_image_path': extracted_pr_image_path,
		'plate_number': plate_number,
		'plate_probability' : plate_probability,
		'plate_coordinates':plate_box,
		'extracted_plate_image_path':extracted_plate_image_path,
		'analyzed_pr_image_path': analyzed_pr_image_path
	}

#--------------------- COLOUR ANALYSIS

def centroid_histogram(clt):
	"""Get the percentages of all the clusters

	:param clt: KMeans clusters  
	:type clt: KMeans  
	:return: the percentages of all the clusters (normalized so it adds up to 100%)  
	:rtype: tuple 
	"""
	centroid_histogram_start_time = time.time()
	LOGGER.info('Start centroid_histogram')

	# grab the number of different clusters and create a histogram
	# based on the number of pixels assigned to each cluster
	numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
	(hist, _) = np.histogram(clt.labels_, bins = numLabels)
	
	# normalize the histogram, such that it sums to one
	hist = hist.astype("float")
	hist /= hist.sum()
	
	centroid_histogram_time = round(time.time() - centroid_histogram_start_time, 3)
	LOGGER.info('End centroid_histogram')
	LOGGER.info(f"--- centroid_histogram took {centroid_histogram_time} seconds ---")

	# return the histogram
	return hist

def get_colour_name(R,G,B):
	"""Determines the name of a colour (RGB values are compared to the ones in the helpers/colour_info.csv file; the name of the closest colour is 
	assigned to the colour being tested)

	:param R: The red value of the colour  
	:type R: int
	:param G: The green value of the colour  
	:type G: int
	:param B: The blue value of the colour  
	:type B: int
	:return: The assigned colour name
	:rtype: str
	"""

	get_colour_name_start_time = time.time()
	LOGGER.info(f'Start get_colour_name for {R},{G},{B}')

	index=["colour_name","hex","R","G","B"]
	csv = pd.read_csv(COLOUR_INFO_CSV, names=index, header=0)

	minimum = 10000
	for i in range(len(csv)):
		d = abs(R- int(csv.loc[i,"R"])) + abs(G- int(csv.loc[i,"G"]))+ abs(B- int(csv.loc[i,"B"]))
		if(d<=minimum):
			minimum = d
			cname = csv.loc[i,"colour_name"]

	get_colour_name_time = round(time.time() - get_colour_name_start_time, 3)
	LOGGER.info(f'End get_colour_name for {R},{G},{B}')
	LOGGER.info(f"--- get_colour_name for for {R},{G},{B} took {get_colour_name_time} seconds ---")

	return cname

def get_all_colour_info(hist, centroids):
	"""Get all the info about the colours (containing no duplicates) in the image

	:param hist: the percentages of all the clusters (normalized so it adds up to 100%)  
	:type hist: tuple  
	:param centroids: cluster centers  
	:type centroids: ndarray  
	:return: a dictionary of dictionaries, each dictionary contains the name, red value, green value, blue value, hex value and percentage of the image 
	         the colour takes up for one detected colour.  
	:rtype: dict  
	"""

	get_all_colour_info_start_time = time.time()
	LOGGER.info('Start get_all_colour_info')

	names = []
	for i in range(0,len(centroids)):
		centroids[i][0] = round(centroids[i][0])
		centroids[i][1] = round(centroids[i][1])
		centroids[i][2] = round(centroids[i][2])
		names.append(get_colour_name(centroids[i][0],centroids[i][1],centroids[i][2]))

	new_hist = []
	names_no_dup = []
	new_colour = []
	new_hex = []

	#remove any duplicates
	for i in range(0,len(names)):
		if len(names_no_dup) > 0:
			if names[i] in names_no_dup:
				for j in range(0,len(names_no_dup)):
					if names[i] == names_no_dup[j] and new_hist[j] != 100:
						new_hist[j] += hist[i]*100
			else:
				names_no_dup.append(names[i])
				new_hist.append(hist[i]*100)
				new_colour.append(centroids[i])
				new_hex.append(f'#{int(centroids[i][0]):02x}{int(centroids[i][1]):02x}{int(centroids[i][2]):02x}')
		else:
			names_no_dup.append(names[0])
			new_hist.append(hist[0]*100)
			new_colour.append(centroids[0])
			new_hex.append(f'#{int(centroids[0][0]):02x}{int(centroids[0][1]):02x}{int(centroids[0][2]):02x}')

	#a dictionary for all the colours and their info
	colours = {}

	# loop over the percentage, colour, hex value and name of each cluster 
	for (percent, colour, name, new_hex, i) in zip(new_hist, new_colour, names_no_dup, new_hex, range(1,len(names_no_dup)+1)):
		colours[f'colour{i}']={
			'name':name,
			'r':colour[0],
			'g':colour[1],
			'b':colour[2],
			'hex':new_hex,
			'percent':percent
		}
		
	get_all_colour_info_time = round(time.time() - get_all_colour_info_start_time, 3)
	LOGGER.info('End get_all_colour_info')
	LOGGER.info(f"--- get_all_colour_info took {get_all_colour_info_time} seconds ---")

	# return the colours and their info
	return colours

def detect_colours(extracted_image_path):
	"""Prepares the image for processing, makes the clusters, calls :func:`centroid_histogram` to build a histogram of clusters and 
	:func:`get_all_colour_info` to get all the colour info

	:param extracted_image_path: the path to the extracted image being processed  
	:type extracted_image_path: str  
	:return: a dictionary of dictionaries, each dictionary contains the name, red value, green value, blue value, hex value and percentage of the image 
	         the colour takes up for one detected colour.  
	:rtype: dict  
	"""

	detect_colours_start_time = time.time()
	LOGGER.info(f'Start detect_colours for {extracted_image_path}')

	# load the image and convert it from BGR to RGB 
	image = cv2.imread(extracted_image_path)
	image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	# reshape the image to be a list of pixels
	image = image.reshape((image.shape[0] * image.shape[1], 3))

	# cluster the pixel intensities into 10 clusters
	# so we get the 10 most dominant colours
	clt = KMeans(n_clusters = 10)
	clt.fit(image)

	# build a histogram of clusters
	hist = centroid_histogram(clt)

	#get the info for the top 10 colours
	colours = get_all_colour_info(hist, clt.cluster_centers_)

	detect_colours_time = round(time.time() - detect_colours_start_time, 3)
	LOGGER.info(f'End detect_colours for {extracted_image_path}')
	LOGGER.info(f"--- detect_colours for {extracted_image_path} took {detect_colours_time} seconds ---")

	return colours



# ------------------------ FILE FUNCTIONS---------------------------
def get_new_sub_dirs():
	"""Gets a list of all the sub directories in the data/new directory

	:return: a sorted list of the subdirectories of the data/new directory  
	:rtype: list  
	"""

	get_new_sub_dirs_start_time = time.time()
	LOGGER.info('Start get_new_sub_dirs')  
	
	sub_dir_path_list = [f.path for f in os.scandir(NEW_DIR) if f.is_dir()]
	sub_dir_path_list.sort()

	get_new_sub_dirs_time = round(time.time() - get_new_sub_dirs_start_time, 3)
	LOGGER.info('End get_new_sub_dirs')
	LOGGER.info(f"--- get_new_sub_dirs took {get_new_sub_dirs_time} seconds ---")
	
	return sub_dir_path_list

def make_log_file(date):
	"""Creates the day's subdirectory in the data/analyzed directory with a log file in it

	:param date: the day's date  
	:type date: str  
	:return: the path to the log file (data/analyzed/{date}/{date}.log)  
	:rtype: str  
	"""

	make_log_file_start_time = time.time()
	LOGGER.info('Start make_log_file')

	sub_dir_path = os.path.join(ANALYZED_DIR, date, '')
	log_file_path = os.path.join(sub_dir_path, f'{date}.log')

	os.makedirs(sub_dir_path) #create the directory
	with open(log_file_path,'w+') as f:#create the log file
		pass
	
	make_log_file_time = round(time.time() - make_log_file_start_time, 3)
	LOGGER.info('End make_log_file')
	LOGGER.info(f"--- make_log_file took {make_log_file_time} seconds ---")

	return log_file_path

def make_analyzed_sub_dir(date):
	"""Makes the image subdirectories (images, images/original, images/analyzed/comparison, images/analyzed/imageai, images/analyzed/pr, images/extracted, 
	images/extracted/imageai, images/extracted/pr, images/extracted/plates) in the data/analyzed/{date} directory.

	:param date: the day's date  
	:type date: str  
	:return: a dictionary containing the path to the date’s subdirectory in data/analyzed, and all the image subdirectories within it  
	:rtype: dict  
	"""

	make_analyzed_sub_dir_start_time = time.time()
	LOGGER.info('Start make_analyzed_sub_dir')

	sub_dir_path = os.path.join(ANALYZED_DIR, date, '')
	images_path = os.path.join(sub_dir_path, 'images','')
	images_original_path = os.path.join(images_path, 'original','')
	images_analyzed_comparison_path = os.path.join(images_path, 'analyzed','comparison', '')
	images_analyzed_imageai_path = os.path.join(images_path, 'analyzed','imageai', '')
	images_analyzed_pr_path = os.path.join(images_path, 'analyzed','pr', '')
	images_extracted_imageai_path = os.path.join(images_path, 'extracted','imageai', '')
	images_extracted_pr_path = os.path.join(images_path, 'extracted','pr', '')
	images_extracted_plates_path = os.path.join(images_path, 'extracted','plates', '')

	os.makedirs(images_original_path) #create the images subdirectory and images/original sub-directory
	os.makedirs(images_analyzed_comparison_path) #create the images/analyzed/comparison sub-directory
	os.makedirs(images_analyzed_imageai_path) #create the images/analyzed/imageai sub-directory
	os.makedirs(images_analyzed_pr_path) #create the images/analyzed/pr sub-directory
	os.makedirs(images_extracted_imageai_path) #create the images/extracted/imageai sub-directory
	os.makedirs(images_extracted_pr_path) #create the images/extracted/pr (plate recognizer) sub-directory
	os.makedirs(images_extracted_plates_path) #create the images/extracted/plates sub-directory
	
	make_analyzed_sub_dir_time = round(time.time() - make_analyzed_sub_dir_start_time, 3)
	LOGGER.info('End make_analyzed_sub_dir')
	LOGGER.info(f"--- make_analyzed_sub_dir took {make_analyzed_sub_dir_time} seconds ---")

	return {
		'sub_dir': sub_dir_path,
		'images': images_path,
		'original_images': images_original_path,
		'analyzed_comparison_images': images_analyzed_comparison_path,
		'analyzed_imageai_images': images_analyzed_imageai_path,
		'analyzed_pr_images': images_analyzed_pr_path,
		'imageai_images': images_extracted_imageai_path,
		'pr_images': images_extracted_pr_path,
		'plates_images': images_extracted_plates_path
	}

def get_data(filePath):
	"""Gets the data from the input JSON file located in data/new/{date} 

	:param filePath: path to the input JSON file (data/new/{date}/{date}.json)  
	:type filePath: str  
	:return: a tuple of dictionaries each containing the data (arrival time, departure time and image name) for a single entry  
	:rtype: tuple  
	"""

	get_data_start_time = time.time()
	LOGGER.info('Start get_data')

	with open(filePath) as json_file:
		data = json.load(json_file)

	get_data_time = round(time.time() - get_data_start_time, 3)
	LOGGER.info('End get_data')
	LOGGER.info(f"--- get_data took {get_data_time} seconds ---")

	return data

def write_json(data, filename): 
	"""Creates a JSON file and writes the data in the file

	:param data: the analyzed data for the day  
	:type data: dict  
	:param filename: the desired path to the JSON file  (data/analyzed/{date}/{date}.json)  
	:type filename: str  
	"""

	write_json_start_time = time.time()
	LOGGER.info('Start write_json')

	with open(filename,'w+') as f: #open the file in write mode and create it if it doesn't already exist
		json.dump(data, f, indent=4) #store the JSON data in the file

	write_json_time = round(time.time() - write_json_start_time, 3)
	LOGGER.info('End write_json')
	LOGGER.info(f"--- write_json took {write_json_time} seconds ---")

	return

def make_analyzed_json(analyzed_sub_dir, date, analyzed_data):
	"""Makes the string containing the desired path to the output JSON file (data/analyzed/{date}/{date}.json) and calls :func:`write_json` to create 
	the file and write the data to it.

	:param analyzed_sub_dir: path to the day's analyzed subdir (data/analyzed/{date})  
	:type analyzed_sub_dir: str  
	:param date: the day's date  
	:type date: str  
	:param analyzed_data: the analyzed data for the day  
	:type analyzed_data: dict  
	"""

	make_analyzed_json_start_time = time.time()
	LOGGER.info('Start make_analyzed_json')

	json_file_name = os.path.join(analyzed_sub_dir, f'{date}.json')

	write_json(analyzed_data,json_file_name)

	make_analyzed_json_time = round(time.time() - make_analyzed_json_start_time, 3)
	LOGGER.info('End make_analyzed_json')
	LOGGER.info(f"--- make_analyzed_json took {make_analyzed_json_time} seconds ---")

	return

def remove_old_sub_dir(sub_dir):
	"""Deletes the day’s subdir in the data/new directory along with all its content (the JSON file and images subdir with all the original input images)

	:param sub_dir: the path to the day's input subdir (data/new/{date})  
	:type sub_dir: str  
	"""

	remove_old_sub_dir_start_time = time.time()
	LOGGER.info('Start remove_old_sub_dir')

	shutil.rmtree(sub_dir, ignore_errors=True)

	remove_old_sub_dir_time = round(time.time() - remove_old_sub_dir_start_time, 3)
	LOGGER.info('End remove_old_sub_dir')
	LOGGER.info(f"--- remove_old_sub_dir took {remove_old_sub_dir_time} seconds ---")
	return

def make_relative_path(orig_path, sub_dir):
	"""Removes the parent dir from the path so the system still works even if the parent dir is moved

	:param orig_path: the absolute path to a file  
	:type orig_path: str  
	:param sub_dir: the path to the file's parent directory  
	:type sub_dir: str  
	:return: the relative path to the file  
	:rtype: str  
	"""

	make_relative_path_start_time = time.time()
	LOGGER.info(f'Start make_relative_path for {orig_path}')

	if orig_path == None:
		path = None
	else:
		path =  orig_path.replace(sub_dir, '')

	make_relative_path_time = round(time.time() - make_relative_path_start_time, 3)
	LOGGER.info('End make_relative_path')
	LOGGER.info(f"--- make_relative_path for {orig_path} took {make_relative_path_time} seconds ---")
	return path

# ------------------------ IMAGE FUNCTIONS---------------------------
def move_orig_image(image_name, orig_path, analyzed_orig_image_sub_dir, rotate=False):
	"""Moves the image from the original input location in the data/new/{date}/images subdir to the data/analyzed/{date}/images/original subdir. The image is rotated 
	by 90 deg. if rotate is set to True.

	:param image_name: the name of the image ({arrival_time}.jpg)  
	:type image_name: str  
	:param orig_path: the original path to the image (data/new/{date}/images/{arrival_time}.jpg)  
	:type orig_path: str  
	:param analyzed_orig_image_sub_dir: the day's original images subdir (data/analyzed/{date}/images/original)  
	:type analyzed_orig_image_sub_dir: str  
	:param rotate: whether or not the image has to be rotated, defaults to False  
	:type rotate: bool, optional  
	:return:  the new path to the original (input) image  
	:rtype: str  
	"""

	move_orig_image_start_time = time.time()
	LOGGER.info(f'Start move_orig_image for {image_name}')
	
	#incase the script is run on a windows machine that can't use ':' in filenames and ':' was auto replaced with '_'
	if os.path.isfile(orig_path):
		image_name = image_name
		orig_path = orig_path  
	else:
		image_name = image_name.replace(":","_")
		orig_path = orig_path.replace(":","_")

	new_path = os.path.join(analyzed_orig_image_sub_dir, image_name)
	new_path = new_path.replace(":","_") #replace : with _ so that the produced images are usable on windows
	os.rename(orig_path, new_path) #move the image
	
	if rotate == True:
		rotate_image(new_path) #rotate the image

	move_orig_image_time = round(time.time() - move_orig_image_start_time, 3)
	LOGGER.info(f'End move_orig_image for {image_name}')
	LOGGER.info(f"--- move_orig_image for {image_name} took {move_orig_image_time} seconds ---")

	return new_path

def rotate_image(image_path):
	"""Rotates the landscape image captured by the hardware device to portrait orientation

	:param image_path: the path to the image being rotated  
	:type image_path: str  
	"""

	rotate_image_start_time = time.time()
	LOGGER.info(f'Start rotate_image for {image_path}')
	
	image = cv2.imread(image_path)
	rotated = imutils.rotate_bound(image, 90) #rotate the image
	cv2.imwrite(image_path,rotated) #save the rotated image

	rotate_image_time = round(time.time() - rotate_image_start_time, 3)
	LOGGER.info(f'End rotate_image for {image_path}')
	LOGGER.info(f"--- rotate_image for {image_path} took {rotate_image_time} seconds ---")

	return

def make_extracted_image(orig_path, extracted_image_sub_dir , x_min,y_min,x_max,y_max):
	"""Makes an image that only contains the detected object

	:param orig_path: the path to the original image  
	:type orig_path: str  
	:param extracted_image_sub_dir: the path to the extracted image sub dir where the produced image will be stored  
	:type extracted_image_sub_dir: str  
	:param x_min: the minimum x coordinate  
	:type x_min: int  
	:param y_min: the minimum y coordinate  
	:type y_min: int  
	:param x_max: the maximum x coordinate  
	:type x_max: int  
	:param y_max: the maximum y coordinate  
	:type y_max: int  
	:return: path to the extracted image  
	:rtype: str  
	"""

	make_extracted_image_start_time = time.time()
	LOGGER.info(f'Start make_extracted_image for {orig_path}')

	img =cv2.imread(orig_path)
	copy =img.copy()

	orig_image_name = os.path.split(orig_path)[1]
	extension = os.path.splitext(orig_image_name)[1]    
	
	extracted_image_sub_dir_split = os.path.split(extracted_image_sub_dir)
	if(extracted_image_sub_dir_split[1] == ''):
		detection_type = os.path.split(extracted_image_sub_dir_split[0])[1]
	else:
		detection_type = extracted_image_sub_dir_split[1]

	extracted_image_name = orig_image_name.replace(extension,f'_{detection_type}{extension}')

	extracted_image_path = os.path.join(extracted_image_sub_dir, extracted_image_name)

	extracted_image = copy[y_min:y_max, x_min:x_max] #crop the image
	cv2.imwrite(extracted_image_path,extracted_image) #save the image

	make_extracted_image_time = round(time.time() - make_extracted_image_start_time, 3)
	LOGGER.info(f'End make_extracted_image for {orig_path}')
	LOGGER.info(f"--- make_extracted_image for {orig_path} took {make_extracted_image_time} seconds ---")

	return extracted_image_path

def make_analyzed_pr_image(orig_path, analyzed_pr_images_sub_dir, image_info):
	"""Makes an analyzed image for the Plate Recognizer analysis that has bounding boxes drawn around all detected objects, along with their detected 
	classes and detection probability; bounding boxes around detected plates, plate numbers and probabilities

	:param orig_path: the path to the original image  
	:type orig_path: str  
	:param analyzed_pr_images_sub_dir: the path to the analyzed pr images subdir (data/analyzed/{date}/images/extracted/pr)   
	:type analyzed_pr_images_sub_dir: str  
	:param image_info: the image info produced by plate_recognizer_detect  
	:type image_info: list  
	:return: the path to the produced image (data/analyzed/{date}/images/analyzed/pr/{arrival_time}.jpg)  
	:rtype: str  
	"""

	make_analyzed_pr_image_start_time = time.time()
	LOGGER.info(f'Start make_analyzed_pr_image for {orig_path}')

	img =cv2.imread(orig_path)
	copy =img.copy()

	for info in image_info:
		colour = (rng.randint(0,256), rng.randint(0,256), rng.randint(0,256))
		rectangle_bgr = (255-colour[0], 255-colour[1], 255-colour[2])
		
		#vehicle
		cv2.rectangle(copy,(info['vehicle_coordinates']['xmin'], info['vehicle_coordinates']['ymin']),
							(info['vehicle_coordinates']['xmax'], info['vehicle_coordinates']['ymax']),
							colour,2) 

		(mX, mY) = (info['vehicle_coordinates']['xmin'], info['vehicle_coordinates']['ymin'])

		text = f"{info['type']} {info['detection_probability']}%"
		(text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_TRIPLEX, fontScale=2, thickness=2)[0]

		cv2.rectangle(copy,(int(mX), int(mY+12)),(int(mX+text_width+12), int(mY -text_height-22)),
							rectangle_bgr,cv2.FILLED)
		cv2.putText(copy, text, (int(mX), int(mY - 10)), cv2.FONT_HERSHEY_TRIPLEX, 2, colour, 2)

		#plate
		cv2.rectangle(copy,(info['plate_coordinates']['xmin'], info['plate_coordinates']['ymin']),
							(info['plate_coordinates']['xmax'], info['plate_coordinates']['ymax']),
							colour,2) 

		(mX, mY) = (info['plate_coordinates']['xmin'], info['plate_coordinates']['ymin'])

		text = f"{info['plate_number']} {info['plate_probability']}%"
		(text_width, text_height) = cv2.getTextSize(text, cv2.FONT_HERSHEY_TRIPLEX, fontScale=2, thickness=2)[0]

		cv2.rectangle(copy,(int(mX), int(mY+12)),(int(mX+text_width+12), int(mY -text_height-22)),
							rectangle_bgr,cv2.FILLED)
		cv2.putText(copy, text, (int(mX), int(mY - 10)), cv2.FONT_HERSHEY_TRIPLEX, 2, colour, 2)

	#save the image
	orig_image_name = os.path.split(orig_path)[1]
	extension =os.path.splitext(orig_image_name)[1]    
	analyzed_pr_image_name = orig_image_name.replace(f'{extension}',f'_analyzed_pr{extension}')
	analyzed_pr_image_path = os.path.join(analyzed_pr_images_sub_dir, analyzed_pr_image_name)
	cv2.imwrite(analyzed_pr_image_path,copy)

	make_analyzed_pr_image_time = round(time.time() - make_analyzed_pr_image_start_time, 3)
	LOGGER.info(f'End make_analyzed_pr_image for {orig_path}')
	LOGGER.info(f"--- make_analyzed_pr_image for {orig_path} took {make_analyzed_pr_image_time} seconds ---")
	
	return analyzed_pr_image_path

def make_analyzed_comparison_image(orig_path, analyzed_comparison_images_sub_dir, imageai_coordinates, pr_coordinates, plate_coordinates):
	"""Makes an image showing the vehicle bounding boxes produced by the 2 different detections and the license plate box

	:param orig_path: the path to the original image  
	:type orig_path: str  
	:param analyzed_comparison_images_sub_dir: path to the analyzed comparison subdir (data/analyzed/{date}/images/analyzed/comparison)  
	:type analyzed_comparison_images_sub_dir: str  
	:param imageai_coordinates: coordinates of the bounding box for the object detected by Imageai  
	:type imageai_coordinates: dict  
	:param pr_coordinates: coordinates of the bounding box for the object detected by Plate Recognizer  
	:type pr_coordinates: dict  
	:param plate_coordinates: coordinates of the bounding box for the plate  
	:type plate_coordinates: dict  
	:return: None if all coordinates are empty or the path to an image that shows the bounding boxes representing the coordinates 
	:rtype: None or str  
	"""

	make_analyzed_comparison_image_start_time = time.time()
	LOGGER.info(f'Start make_analyzed_comparison_image for {orig_path}')

	if imageai_coordinates == None and pr_coordinates == None and plate_coordinates == None:
		make_analyzed_comparison_image_time = round(time.time() - make_analyzed_comparison_image_start_time, 3)
		LOGGER.info(f'End make_analyzed_comparison_image for {orig_path}. NO IMAGE MADE')
		LOGGER.info(f"--- make_analyzed_comparison_image for {orig_path} took {make_analyzed_comparison_image_time} seconds ---")

		return None

	img =cv2.imread(orig_path)
	copy =img.copy()


	#imageai box (blue)
	if imageai_coordinates != None:
		cv2.rectangle(copy,(imageai_coordinates['x_min'], imageai_coordinates['y_min']),(imageai_coordinates['x_max'], imageai_coordinates['y_max']),(255,0,0),2) #blue box
		(mX, mY) = find_center(imageai_coordinates['x_min'], imageai_coordinates['y_max'], imageai_coordinates['x_max'], imageai_coordinates['y_max'],'imageai box') #center of bottom of box
		(text_width, text_height) = cv2.getTextSize("imageai", cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, thickness=2)[0]
		cv2.rectangle(copy,(int(mX), int(mY-12)),(int(mX+text_width+12), int(mY + text_height+12)),(255,255,255),cv2.FILLED) #rectangle with white background
		cv2.putText(copy, "imageai", (int(mX), int(mY + 10)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0), 2) #blue label 

	#plate recognizer vehicle box (red)
	if pr_coordinates != None:
		cv2.rectangle(copy,(pr_coordinates['xmin'], pr_coordinates['ymin']),(pr_coordinates['xmax'], pr_coordinates['ymax']),(0,0,255),2) #red box
		(mX, mY) = find_center(pr_coordinates['xmin'], pr_coordinates['ymin'], pr_coordinates['xmax'], pr_coordinates['ymin'],'pr box') #center of top of box
		(text_width, text_height) = cv2.getTextSize("pr", cv2.FONT_HERSHEY_SIMPLEX, fontScale=1, thickness=2)[0]
		cv2.rectangle(copy,(int(mX), int(mY+12)),(int(mX+text_width+12), int(mY -text_height-12)),(255,255,255),cv2.FILLED)
		cv2.putText(copy, "pr", (int(mX), int(mY - 10)), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2)

	#plate recognizer plate box (green)
	if plate_coordinates != None:
		cv2.rectangle(copy,(plate_coordinates['xmin'], plate_coordinates['ymin']),(plate_coordinates['xmax'], plate_coordinates['ymax']),(0,255,0),2) #green box

	#save the image
	orig_image_name = os.path.split(orig_path)[1]
	extension =os.path.splitext(orig_image_name)[1]    
	analyzed_comparison_image_name = orig_image_name.replace(f'{extension}',f'_analyzed_comparison{extension}')
	analyzed_comparison_image_path = os.path.join(analyzed_comparison_images_sub_dir, analyzed_comparison_image_name)
	cv2.imwrite(analyzed_comparison_image_path,copy)

	make_analyzed_comparison_image_time = round(time.time() - make_analyzed_comparison_image_start_time, 3)
	LOGGER.info(f'End make_analyzed_comparison_image for {orig_path}')
	LOGGER.info(f"--- make_analyzed_comparison_image for {orig_path} took {make_analyzed_comparison_image_time} seconds ---")

	return analyzed_comparison_image_path



#------------------------------FULL ANALYSIS--------------------------------------------------------------------------

#------------------ANALYZE ONE DAY----------------------
def analyze_day(date, rotate=False, feed_in=False):
	"""Analyzes all the entries for a day. If using hardware results need to pass in rotate=True so the images are rotated

	:param date: the day's date  
	:type date: str  
	:param rotate: whether or not the images have to be rotated, defaults to False  
	:type rotate: bool, optional  
	:param feed_in: whether or not the images extracted during the Imageai analysis have to be fed into :func:`plate_recognizer_detect`, defaults to False  
	:type feed_in: bool, optional  
	:return: the day's date  
	:rtype: str  
	"""

	day_start_time = time.time() #time to process entire day
	total_get_duration_time = 0 #total time to run get_duration for all entries for this day
	total_imageai_detection_time = 0 #total time to run imageai_detection for all entries for this day
	total_imageai_prediction_time = 0 #total time to do prediction for the imageai extracted images for all entries for this day
	total_imageai_colour_detection_time = 0 #total time to do colour detection for the imageai extracted images for all entries for this day
	total_imageai_analysis_time = 0 #total time to do the imageai analysis for all entries for this day
	total_pr_detection_time = 0 #total time to run pr_detection for all entries for this day
	total_pr_prediction_time = 0 #total time to do prediction for the pr extracted images for all entries for this day
	total_pr_colour_detection_time = 0 #total time to do colour detection for the pr extracted images for all entries for this day
	total_pr_analysis_time = 0 #total time to do the pr analysis for all entries for this day
	total_image_analysis_time = 0 #total time to analyze the images for all entries for this day
	total_entry_time = 0 #calculated time ot process all entriesfor this day


	#deal with logger
	log_file = make_log_file(date)
	file_handler = logging.FileHandler(log_file)
	file_handler.setFormatter(FORMATTER)
	LOGGER.addHandler(file_handler)

	LOGGER.info('***********************************************************************************************')
	LOGGER.info(f'Start analyze_day for {date} (rotate={rotate} and feed_in={feed_in})')

		
	analyzed_sub_dir_info = make_analyzed_sub_dir(date)

	sub_dir = os.path.join(NEW_DIR, date)
	orig_json_path = os.path.join(sub_dir, f'{date}.json')
	unanalyzed = get_data(orig_json_path)

	analyzed_sub_dir = analyzed_sub_dir_info['sub_dir']

	analyzed = []

	for (entry,i) in zip(unanalyzed, range(1,len(unanalyzed)+1)):
		entry_start_time = time.time()
		progress = f'{i}/{len(unanalyzed)}'
		percent = round(( i / len(unanalyzed) ) * 100, 2)
		LOGGER.info(f"Start analysis of {entry['arrival_time']} ({progress})")

		#DURATION ANALYSIS
		entry_get_duration_start_time = time.time()

		arrival_string = entry['arrival_time']
		departure_string = entry['departure_time']
		duration = get_duration(arrival_string,departure_string)
		duration_in_seconds = duration['seconds']
		human_readable_duration = duration['human_readable']

		entry_get_duration_time = round((time.time() - entry_get_duration_start_time),3) #total time to get the amount of time the vehicle was parked for this entry


		#ANALYZE THE IMAGE
		entry_total_image_analysis_start_time = time.time()

		#Move Original Image
		image_name = entry['image']
		image_path = os.path.join(sub_dir, 'images', image_name)
		analyzed_orig_image_sub_dir = analyzed_sub_dir_info['original_images']
		original_image = move_orig_image(image_name,image_path,analyzed_orig_image_sub_dir, rotate) #if using hardware results need to pass in rotate=True so the images are rotated

		#Imageai Analysis
		entry_total_imageai_analysis_start_time = time.time()

		imageai_images = analyzed_sub_dir_info['imageai_images']
		analyzed_imageai_images = analyzed_sub_dir_info['analyzed_imageai_images']

		entry_imageai_detection_start_time = time.time()
		imageai_analysis = imageai_detect(original_image,imageai_images, analyzed_imageai_images)
		entry_imageai_detection_time = round((time.time() - entry_imageai_detection_start_time),3) #time to detect one entry's object using imageai

		if imageai_analysis == None: #if no vehicles were detected
			extracted_imageai_image=None
			analyzed_imageai_image=None
			imageai_coordinates=None				
			imageai_detection_class=None
			imageai_detection_probability=None
			imageai_prediction_class=None
			imageai_prediction_probability=None
			entry_imageai_prediction_time=0.000
			entry_imageai_colour_detection_time=0.000
			imageai_colours=None
		else:
			extracted_imageai_image = imageai_analysis['extracted_imageai_image_path']
			analyzed_imageai_image = imageai_analysis['analyzed_imageai_image_path']
			imageai_coordinates = imageai_analysis['imageai_coordinates']
			imageai_detection_class = imageai_analysis['imageai_detection_class']
			imageai_detection_probability = imageai_analysis['imageai_detection_probability']
			
			#perform prediction using the extracted image
			entry_imageai_prediction_start_time = time.time()
			imageai_prediction = imageai_predict(extracted_imageai_image)
			imageai_prediction_class = imageai_prediction['prediction_class']
			imageai_prediction_probability = imageai_prediction['prediction_probability']
			entry_imageai_prediction_time = round((time.time() - entry_imageai_prediction_start_time),3) #time to predict one entry's object using imageai extracted image

			#perform colour detection using the extracted image
			entry_imageai_colour_detection_start_time = time.time()
			imageai_colours = detect_colours(extracted_imageai_image)
			entry_imageai_colour_detection_time = round((time.time() - entry_imageai_colour_detection_start_time),3) #time to detect one entry's object's colour using imageai extracted image

		entry_total_imageai_analysis_time = round((time.time() - entry_total_imageai_analysis_start_time),3) #total time to process one entry's image using imageai for one day

		#Plate Recognizer Analysis
		entry_total_pr_analysis_start_time = time.time()

		pr_images = analyzed_sub_dir_info['pr_images']
		plates_images = analyzed_sub_dir_info['plates_images']
		analyzed_pr_images = analyzed_sub_dir_info['analyzed_pr_images']
		

		entry_pr_detection_start_time = time.time()

		if feed_in == True and extracted_imageai_image != None:
			x_compensate = imageai_coordinates['x_min']
			y_compensate = imageai_coordinates['y_min']
			pr_analysis = plate_recognizer_detect(extracted_imageai_image, pr_images, plates_images, analyzed_pr_images, original_image, x_compensate, y_compensate) 
		else:
			pr_analysis = plate_recognizer_detect(original_image, pr_images, plates_images, analyzed_pr_images) 


		entry_pr_detection_time = round((time.time() - entry_pr_detection_start_time),3) #time to detect one entry's object using plate recognizer

		
		if pr_analysis == None: #if no vehicles were detected
			extracted_pr_image = None
			extracted_plate_image=None
			analyzed_pr_image=None
			pr_vehicle_coordinates=None
			plate_coordinates=None
			pr_detection_class=None
			pr_detection_probability=None
			plate_number=None
			plate_probability=None
			pr_prediction_class=None
			pr_prediction_probability=None
			entry_pr_prediction_time=0.000
			entry_pr_colour_detection_time=0.000
			pr_colours=None
		else:
			extracted_pr_image = pr_analysis['extracted_pr_image_path']
			extracted_plate_image = pr_analysis['extracted_plate_image_path']
			analyzed_pr_image = pr_analysis['analyzed_pr_image_path']	
			pr_vehicle_coordinates = pr_analysis['pr_vehicle_coordinates']
			plate_coordinates = pr_analysis['plate_coordinates']		
			pr_detection_class = pr_analysis['pr_detection_class']
			pr_detection_probability = pr_analysis['pr_detection_probability']
			plate_number = pr_analysis['plate_number']
			plate_probability = pr_analysis['plate_probability']

			#perform prediction using the extracted image
			entry_pr_prediction_start_time = time.time()
			pr_prediction = imageai_predict(extracted_pr_image)
			pr_prediction_class = pr_prediction['prediction_class'] 
			pr_prediction_probability = pr_prediction['prediction_probability']
			entry_pr_prediction_time = round((time.time() - entry_pr_prediction_start_time),3) #time to predict one entry's object using pr extracted image

			#perform colour detection using the extracted image
			entry_pr_colour_detection_start_time = time.time()
			pr_colours = detect_colours(extracted_pr_image)
			entry_pr_colour_detection_time = round((time.time() - entry_pr_colour_detection_start_time),3) #time to detect one entry's object's colour using pr extracted image

		entry_total_pr_analysis_time = round((time.time() - entry_total_pr_analysis_start_time),3) #total time to process one entry's image using plate recognizer for one day


		#make analyzed comparison image
		analyzed_comparison_images_sub_dir = f"{analyzed_sub_dir_info['analyzed_comparison_images']}"
		analyzed_comparison_image = make_analyzed_comparison_image(original_image,analyzed_comparison_images_sub_dir,imageai_coordinates,pr_vehicle_coordinates, plate_coordinates)

		entry_total_image_analysis_time = round((time.time() - entry_total_image_analysis_start_time),3) #total time to process one entry's image for one day
		entry_total_time = round((time.time() - entry_start_time),3) #total time to process one entry for one day
		LOGGER.info(f"End analysis of {entry['arrival_time']}  ({progress}) ({percent}%)")
		LOGGER.info(f"--- analysis of {entry['arrival_time']} took {entry_total_time} seconds ---")
		
		#Data for JSON File
		new_data = {
					"analysis":{
								"arrival_time":arrival_string,
								"departure_time":departure_string,
								"duration": {
												'in_seconds': duration_in_seconds,
												'human_readable': human_readable_duration
											},
								"images":{
											'original' : make_relative_path(original_image, analyzed_sub_dir),
											'extracted_imageai' : make_relative_path(extracted_imageai_image, analyzed_sub_dir),
											'extracted_pr' : make_relative_path(extracted_pr_image, analyzed_sub_dir),
											'extracted_plate' : make_relative_path(extracted_plate_image, analyzed_sub_dir),
											'analyzed_comparison' : make_relative_path(analyzed_comparison_image, analyzed_sub_dir),
											'analyzed_imageai' : make_relative_path(analyzed_imageai_image, analyzed_sub_dir),
											'analyzed_pr' : make_relative_path(analyzed_pr_image, analyzed_sub_dir)
								},
								"successful_feed_in" : None if extracted_imageai_image == None or feed_in == False else True if extracted_pr_image and 'imageai' in extracted_pr_image else False if extracted_pr_image == None or 'imageai' not in extracted_pr_image else None,
								"imageai_analysis": {
									"detection":{
										'class':imageai_detection_class,
										'probability':imageai_detection_probability
									},
									"prediction":{
										'class':imageai_prediction_class,
										'probability':imageai_prediction_probability
									},
									"colours":imageai_colours
								},
								"pr_analysis": {
									"detection":{
										'class':pr_detection_class,
										'probability':pr_detection_probability
									},
									"prediction":{
										'class':pr_prediction_class,
										'probability':pr_prediction_probability
									},
									"colours":pr_colours,
									"license_plate":{
										'plate_number': plate_number,
										'plate_probability' : plate_probability
									}
								}
					},
					"timing":{
								"get_duration_time" : round(entry_get_duration_time, 3),
								"imageai_detection_time" : round(entry_imageai_detection_time, 3),
								"imageai_prediction_time" : round(entry_imageai_prediction_time, 3),
								"imageai_colour_detection_time" : round(entry_imageai_colour_detection_time, 3),
								"total_imageai_analysis_time" : round(entry_total_imageai_analysis_time, 3),
								"pr_detection_time" : round(entry_pr_detection_time, 3),
								"pr_prediction_time" : round(entry_pr_prediction_time, 3),
								"pr_colour_detection_time" : round(entry_pr_colour_detection_time, 3),
								"total_pr_analysis_time" : round(entry_total_pr_analysis_time, 3),
								"total_image_analysis_time" : round(entry_total_image_analysis_time, 3),
								"entry_total_time" : round(entry_total_time, 3)
					}
				}

		analyzed.append(new_data)
		LOGGER.info(f"************ Analysis of {entry['arrival_time']} complete!  ({progress}) ({percent}%) ************")
		LOGGER.info('***********************************************************************************************')
		LOGGER.info('') #skip a line between entries

		total_get_duration_time += entry_get_duration_time
		total_imageai_detection_time += entry_imageai_detection_time
		total_imageai_prediction_time += entry_imageai_prediction_time
		total_imageai_colour_detection_time += entry_imageai_colour_detection_time
		total_imageai_analysis_time += entry_total_imageai_analysis_time
		total_pr_detection_time += entry_pr_detection_time
		total_pr_prediction_time = entry_pr_prediction_time
		total_pr_colour_detection_time += entry_pr_colour_detection_time
		total_pr_analysis_time += entry_total_pr_analysis_time
		total_image_analysis_time += entry_total_image_analysis_time
		total_entry_time += entry_total_time
	
	total_day_time = round((time.time() - day_start_time),3) #total time to process all entries for the entire day
	LOGGER.info(f'End analysis of {date}')
	LOGGER.info(f"--- analysis of {date} (rotate={rotate} and feed_in={feed_in}) took {total_day_time} seconds ---")
	LOGGER.info('')
	
	data = {
			"tried_feed_in" : feed_in,
			"analysis" : analyzed,
			"total_times" : {
							"total_get_duration_time":round(total_get_duration_time, 3),
							"total_imageai_detection_time":round(total_imageai_detection_time, 3),
							"total_imageai_prediction_time":round(total_imageai_prediction_time, 3),
							"total_imageai_colour_detection_time":round(total_imageai_colour_detection_time, 3),
							"total_imageai_analysis_time":round(total_imageai_analysis_time, 3),
							"total_pr_detection_time":round(total_pr_detection_time, 3),
							"total_pr_prediction_time":round(total_pr_prediction_time, 3),
							"total_pr_colour_detection_time":round(total_pr_colour_detection_time, 3),
							"total_pr_analysis_time":round(total_pr_analysis_time, 3),
							"total_image_analysis_time":round(total_image_analysis_time, 3),
							"total_entry_time":round(total_entry_time, 3),
							"total_day_time" : round(total_day_time, 3)
			},
			"log_file" : make_relative_path(log_file, analyzed_sub_dir)
	}

	make_analyzed_json(analyzed_sub_dir, date, data)

	remove_old_sub_dir(sub_dir) #remove the day's sub dir after all the data has been analyzed
	LOGGER.removeHandler(file_handler)

	return date


#------------------MAIN FUNCTION-------------------------
#analyzez all the days in the 'new' sub dir
def main(rotate=False, feed_in=False):
	"""Analyzes all the dates with subdirs in the data/new directory one after the other by calling :func:`analyze_day`. If using hardware results need to pass in rotate=True so the 
	images are rotated.

	:param rotate: whether or not the images have to be rotated, defaults to False  
	:type rotate: bool, optional  
	:param feed_in: whether or not the images extracted during the Imageai analysis have to be fed into :func:`plate_recognizer_detect`, defaults to False  
	:type feed_in: bool, optional  
	:return: a list of the dates that were analyzed
	:rtype: list
	"""

	LOGGER.info('Starting')
	main_start_time = time.time()

	new_sub_dirs = get_new_sub_dirs() #check for new sub directories in the 'new' directory
	
	dates = []

	for (sub_dir, i) in zip(new_sub_dirs, range(1,len(new_sub_dirs)+1)): #for each subdirectory
		
		date = os.path.split(sub_dir)[1]
		
		progress = f'{i}/{len(new_sub_dirs)}'
		percent = round(( i / len(new_sub_dirs) ) * 100, 2)
		LOGGER.info(f"Start analysis of {date} from main() (rotate={rotate} and feed_in={feed_in}) ({progress})")
		
		dates.append(analyze_day(date=date, rotate=rotate, feed_in=feed_in))
		
		LOGGER.info(f'------ End analysis of {date} from main() (rotate={rotate} and feed_in={feed_in}) ({progress}) ({percent}%) ------')
		LOGGER.info('')

	main_time = round(time.time() - main_start_time, 3)
	LOGGER.info(f"--- main took {main_time} seconds ---")
	
	return dates


#-------------MAIN PROGRAM--------
if __name__ == '__main__':
	main()
