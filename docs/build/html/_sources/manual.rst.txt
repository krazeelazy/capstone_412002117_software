manual package
==============

manual.apps module
------------------

.. automodule:: manual.apps
   :members:
   :undoc-members:
   :show-inheritance:

manual.models module
--------------------

.. automodule:: manual.models
   :members:
   :undoc-members:
   :show-inheritance:
   
manual.views module
-------------------

.. automodule:: manual.views
   :members:
   :undoc-members:
   :show-inheritance:
