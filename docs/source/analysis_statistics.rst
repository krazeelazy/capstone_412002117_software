analysis\_statistics package
============================

analysis\_statistics.apps module
--------------------------------

.. automodule:: analysis_statistics.apps
   :members:
   :undoc-members:
   :show-inheritance:

analysis\_statistics.models module
----------------------------------

.. automodule:: analysis_statistics.models
   :members:
   :undoc-members:
   :show-inheritance:

analysis\_statistics.views module
---------------------------------

.. automodule:: analysis_statistics.views
   :members:
   :undoc-members:
   :show-inheritance:
   