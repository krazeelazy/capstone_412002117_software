.. _Admin URLS:

Admin URLS
==========

.. table:: Admin URLS table
    :class: longtable
    
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | Route                                                           | View                                                                      | Name                                              |
    +=================================================================+===========================================================================+===================================================+
    | /admin/                                                         | django.contrib.admin.sites.index                                          | admin:index                                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/<app_label>/                                             | django.contrib.admin.sites.app_index                                      | admin:app_list                                    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/                                            | django.contrib.admin.options.changelist_view                              | admin:analysis_day_changelist                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/<path:object_id>/                           | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/<path:object_id>/change/                    | django.contrib.admin.options.change_view                                  | admin:analysis_day_change                         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/<path:object_id>/delete/                    | django.contrib.admin.options.delete_view                                  | admin:analysis_day_delete                         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/<path:object_id>/history/                   | django.contrib.admin.options.history_view                                 | admin:analysis_day_history                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/add/                                        | django.contrib.admin.options.add_view                                     | admin:analysis_day_add                            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/day/autocomplete/                               | django.contrib.admin.options.autocomplete_view                            | admin:analysis_day_autocomplete                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/                                      | django.contrib.admin.options.changelist_view                              | admin:analysis_daytiming_changelist               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/<path:object_id>/                     | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/<path:object_id>/change/              | django.contrib.admin.options.change_view                                  | admin:analysis_daytiming_change                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/<path:object_id>/delete/              | django.contrib.admin.options.delete_view                                  | admin:analysis_daytiming_delete                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/<path:object_id>/history/             | django.contrib.admin.options.history_view                                 | admin:analysis_daytiming_history                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/add/                                  | django.contrib.admin.options.add_view                                     | admin:analysis_daytiming_add                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/daytiming/autocomplete/                         | django.contrib.admin.options.autocomplete_view                            | admin:analysis_daytiming_autocomplete             |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/                                          | django.contrib.admin.options.changelist_view                              | admin:analysis_entry_changelist                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/<path:object_id>/                         | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/<path:object_id>/change/                  | django.contrib.admin.options.change_view                                  | admin:analysis_entry_change                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/<path:object_id>/delete/                  | django.contrib.admin.options.delete_view                                  | admin:analysis_entry_delete                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/<path:object_id>/history/                 | django.contrib.admin.options.history_view                                 | admin:analysis_entry_history                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/add/                                      | django.contrib.admin.options.add_view                                     | admin:analysis_entry_add                          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/analysis/entry/autocomplete/                             | django.contrib.admin.options.autocomplete_view                            | admin:analysis_entry_autocomplete                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/                                              | django.contrib.admin.options.changelist_view                              | admin:auth_group_changelist                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/<path:object_id>/                             | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/<path:object_id>/change/                      | django.contrib.admin.options.change_view                                  | admin:auth_group_change                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/<path:object_id>/delete/                      | django.contrib.admin.options.delete_view                                  | admin:auth_group_delete                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/<path:object_id>/history/                     | django.contrib.admin.options.history_view                                 | admin:auth_group_history                          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/add/                                          | django.contrib.admin.options.add_view                                     | admin:auth_group_add                              |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/group/autocomplete/                                 | django.contrib.admin.options.autocomplete_view                            | admin:auth_group_autocomplete                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/                                               | django.contrib.admin.options.changelist_view                              | admin:auth_user_changelist                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/<id>/password/                                 | django.contrib.auth.admin.user_change_password                            | admin:auth_user_password_change                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/<path:object_id>/                              | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/<path:object_id>/change/                       | django.contrib.admin.options.change_view                                  | admin:auth_user_change                            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/<path:object_id>/delete/                       | django.contrib.admin.options.delete_view                                  | admin:auth_user_delete                            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/<path:object_id>/history/                      | django.contrib.admin.options.history_view                                 | admin:auth_user_history                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/add/                                           | django.contrib.auth.admin.add_view                                        | admin:auth_user_add                               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/auth/user/autocomplete/                                  | django.contrib.admin.options.autocomplete_view                            | admin:auth_user_autocomplete                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/                             | django.contrib.admin.options.changelist_view                              | admin:automated_automatedanalysis_changelist      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/<path:object_id>/            | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/<path:object_id>/change/     | django.contrib.admin.options.change_view                                  | admin:automated_automatedanalysis_change          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/<path:object_id>/delete/     | django.contrib.admin.options.delete_view                                  | admin:automated_automatedanalysis_delete          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/<path:object_id>/history/    | django.contrib.admin.options.history_view                                 | admin:automated_automatedanalysis_history         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/add/                         | django.contrib.admin.options.add_view                                     | admin:automated_automatedanalysis_add             |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/automatedanalysis/autocomplete/                | django.contrib.admin.options.autocomplete_view                            | admin:automated_automatedanalysis_autocomplete    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/                            | django.contrib.admin.options.changelist_view                              | admin:automated_iaicolourdetection_changelist     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/<path:object_id>/           | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/<path:object_id>/change/    | django.contrib.admin.options.change_view                                  | admin:automated_iaicolourdetection_change         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/<path:object_id>/delete/    | django.contrib.admin.options.delete_view                                  | admin:automated_iaicolourdetection_delete         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/<path:object_id>/history/   | django.contrib.admin.options.history_view                                 | admin:automated_iaicolourdetection_history        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/add/                        | django.contrib.admin.options.add_view                                     | admin:automated_iaicolourdetection_add            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaicolourdetection/autocomplete/               | django.contrib.admin.options.autocomplete_view                            | admin:automated_iaicolourdetection_autocomplete   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/                           | django.contrib.admin.options.changelist_view                              | admin:automated_iaivehicledetection_changelist    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/<path:object_id>/          | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/<path:object_id>/change/   | django.contrib.admin.options.change_view                                  | admin:automated_iaivehicledetection_change        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/<path:object_id>/delete/   | django.contrib.admin.options.delete_view                                  | admin:automated_iaivehicledetection_delete        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/<path:object_id>/history/  | django.contrib.admin.options.history_view                                 | admin:automated_iaivehicledetection_history       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/add/                       | django.contrib.admin.options.add_view                                     | admin:automated_iaivehicledetection_add           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicledetection/autocomplete/              | django.contrib.admin.options.autocomplete_view                            | admin:automated_iaivehicledetection_autocomplete  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/                          | django.contrib.admin.options.changelist_view                              | admin:automated_iaivehicleprediction_changelist   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/<path:object_id>/         | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/<path:object_id>/change/  | django.contrib.admin.options.change_view                                  | admin:automated_iaivehicleprediction_change       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/<path:object_id>/delete/  | django.contrib.admin.options.delete_view                                  | admin:automated_iaivehicleprediction_delete       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/<path:object_id>/history/ | django.contrib.admin.options.history_view                                 | admin:automated_iaivehicleprediction_history      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/add/                      | django.contrib.admin.options.add_view                                     | admin:automated_iaivehicleprediction_add          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/iaivehicleprediction/autocomplete/             | django.contrib.admin.options.autocomplete_view                            | admin:automated_iaivehicleprediction_autocomplete |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/                                         | django.contrib.admin.options.changelist_view                              | admin:automated_image_changelist                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/<path:object_id>/                        | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/<path:object_id>/change/                 | django.contrib.admin.options.change_view                                  | admin:automated_image_change                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/<path:object_id>/delete/                 | django.contrib.admin.options.delete_view                                  | admin:automated_image_delete                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/<path:object_id>/history/                | django.contrib.admin.options.history_view                                 | admin:automated_image_history                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/add/                                     | django.contrib.admin.options.add_view                                     | admin:automated_image_add                         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/image/autocomplete/                            | django.contrib.admin.options.autocomplete_view                            | admin:automated_image_autocomplete                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/                                  | django.contrib.admin.options.changelist_view                              | admin:automated_licenseplate_changelist           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/<path:object_id>/                 | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/<path:object_id>/change/          | django.contrib.admin.options.change_view                                  | admin:automated_licenseplate_change               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/<path:object_id>/delete/          | django.contrib.admin.options.delete_view                                  | admin:automated_licenseplate_delete               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/<path:object_id>/history/         | django.contrib.admin.options.history_view                                 | admin:automated_licenseplate_history              |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/add/                              | django.contrib.admin.options.add_view                                     | admin:automated_licenseplate_add                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/licenseplate/autocomplete/                     | django.contrib.admin.options.autocomplete_view                            | admin:automated_licenseplate_autocomplete         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/                             | django.contrib.admin.options.changelist_view                              | admin:automated_prcolourdetection_changelist      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/<path:object_id>/            | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/<path:object_id>/change/     | django.contrib.admin.options.change_view                                  | admin:automated_prcolourdetection_change          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/<path:object_id>/delete/     | django.contrib.admin.options.delete_view                                  | admin:automated_prcolourdetection_delete          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/<path:object_id>/history/    | django.contrib.admin.options.history_view                                 | admin:automated_prcolourdetection_history         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/add/                         | django.contrib.admin.options.add_view                                     | admin:automated_prcolourdetection_add             |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prcolourdetection/autocomplete/                | django.contrib.admin.options.autocomplete_view                            | admin:automated_prcolourdetection_autocomplete    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/                            | django.contrib.admin.options.changelist_view                              | admin:automated_prvehicledetection_changelist     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/<path:object_id>/           | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/<path:object_id>/change/    | django.contrib.admin.options.change_view                                  | admin:automated_prvehicledetection_change         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/<path:object_id>/delete/    | django.contrib.admin.options.delete_view                                  | admin:automated_prvehicledetection_delete         |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/<path:object_id>/history/   | django.contrib.admin.options.history_view                                 | admin:automated_prvehicledetection_history        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/add/                        | django.contrib.admin.options.add_view                                     | admin:automated_prvehicledetection_add            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicledetection/autocomplete/               | django.contrib.admin.options.autocomplete_view                            | admin:automated_prvehicledetection_autocomplete   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/                           | django.contrib.admin.options.changelist_view                              | admin:automated_prvehicleprediction_changelist    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/<path:object_id>/          | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/<path:object_id>/change/   | django.contrib.admin.options.change_view                                  | admin:automated_prvehicleprediction_change        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/<path:object_id>/delete/   | django.contrib.admin.options.delete_view                                  | admin:automated_prvehicleprediction_delete        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/<path:object_id>/history/  | django.contrib.admin.options.history_view                                 | admin:automated_prvehicleprediction_history       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/add/                       | django.contrib.admin.options.add_view                                     | admin:automated_prvehicleprediction_add           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/prvehicleprediction/autocomplete/              | django.contrib.admin.options.autocomplete_view                            | admin:automated_prvehicleprediction_autocomplete  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/                                        | django.contrib.admin.options.changelist_view                              | admin:automated_timing_changelist                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/<path:object_id>/                       | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/<path:object_id>/change/                | django.contrib.admin.options.change_view                                  | admin:automated_timing_change                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/<path:object_id>/delete/                | django.contrib.admin.options.delete_view                                  | admin:automated_timing_delete                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/<path:object_id>/history/               | django.contrib.admin.options.history_view                                 | admin:automated_timing_history                    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/add/                                    | django.contrib.admin.options.add_view                                     | admin:automated_timing_add                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/automated/timing/autocomplete/                           | django.contrib.admin.options.autocomplete_view                            | admin:automated_timing_autocomplete               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/jsi18n/                                                  | django.contrib.admin.sites.i18n_javascript                                | admin:jsi18n                                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/login/                                                   | django.contrib.admin.sites.login                                          | admin:login                                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/logout/                                                  | django.contrib.admin.sites.logout                                         | admin:logout                                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/                                   | django.contrib.admin.options.changelist_view                              | admin:manual_manualanalysis_changelist            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/<path:object_id>/                  | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/<path:object_id>/change/           | django.contrib.admin.options.change_view                                  | admin:manual_manualanalysis_change                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/<path:object_id>/delete/           | django.contrib.admin.options.delete_view                                  | admin:manual_manualanalysis_delete                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/<path:object_id>/history/          | django.contrib.admin.options.history_view                                 | admin:manual_manualanalysis_history               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/add/                               | django.contrib.admin.options.add_view                                     | admin:manual_manualanalysis_add                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/manual/manualanalysis/autocomplete/                      | django.contrib.admin.options.autocomplete_view                            | admin:manual_manualanalysis_autocomplete          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/password_change/                                         | django.contrib.admin.sites.password_change                                | admin:password_change                             |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/password_change/done/                                    | django.contrib.admin.sites.password_change_done                           | admin:password_change_done                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/                                              | django.contrib.admin.options.changelist_view                              | admin:psa_colour_changelist                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/<path:object_id>/                             | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/<path:object_id>/change/                      | django.contrib.admin.options.change_view                                  | admin:psa_colour_change                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/<path:object_id>/delete/                      | django.contrib.admin.options.delete_view                                  | admin:psa_colour_delete                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/<path:object_id>/history/                     | django.contrib.admin.options.history_view                                 | admin:psa_colour_history                          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/add/                                          | django.contrib.admin.options.add_view                                     | admin:psa_colour_add                              |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/colour/autocomplete/                                 | django.contrib.admin.options.autocomplete_view                            | admin:psa_colour_autocomplete                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/                                   | django.contrib.admin.options.changelist_view                              | admin:psa_iaidetectionclass_changelist            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/<path:object_id>/                  | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/<path:object_id>/change/           | django.contrib.admin.options.change_view                                  | admin:psa_iaidetectionclass_change                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/<path:object_id>/delete/           | django.contrib.admin.options.delete_view                                  | admin:psa_iaidetectionclass_delete                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/<path:object_id>/history/          | django.contrib.admin.options.history_view                                 | admin:psa_iaidetectionclass_history               |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/add/                               | django.contrib.admin.options.add_view                                     | admin:psa_iaidetectionclass_add                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/iaidetectionclass/autocomplete/                      | django.contrib.admin.options.autocomplete_view                            | admin:psa_iaidetectionclass_autocomplete          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/                                          | django.contrib.admin.options.changelist_view                              | admin:psa_imageangle_changelist                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/<path:object_id>/                         | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/<path:object_id>/change/                  | django.contrib.admin.options.change_view                                  | admin:psa_imageangle_change                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/<path:object_id>/delete/                  | django.contrib.admin.options.delete_view                                  | admin:psa_imageangle_delete                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/<path:object_id>/history/                 | django.contrib.admin.options.history_view                                 | admin:psa_imageangle_history                      |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/add/                                      | django.contrib.admin.options.add_view                                     | admin:psa_imageangle_add                          |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imageangle/autocomplete/                             | django.contrib.admin.options.autocomplete_view                            | admin:psa_imageangle_autocomplete                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/                                           | django.contrib.admin.options.changelist_view                              | admin:psa_imagetype_changelist                    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/<path:object_id>/                          | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/<path:object_id>/change/                   | django.contrib.admin.options.change_view                                  | admin:psa_imagetype_change                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/<path:object_id>/delete/                   | django.contrib.admin.options.delete_view                                  | admin:psa_imagetype_delete                        |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/<path:object_id>/history/                  | django.contrib.admin.options.history_view                                 | admin:psa_imagetype_history                       |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/add/                                       | django.contrib.admin.options.add_view                                     | admin:psa_imagetype_add                           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/imagetype/autocomplete/                              | django.contrib.admin.options.autocomplete_view                            | admin:psa_imagetype_autocomplete                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/                                    | django.contrib.admin.options.changelist_view                              | admin:psa_prdetectionclass_changelist             |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/<path:object_id>/                   | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/<path:object_id>/change/            | django.contrib.admin.options.change_view                                  | admin:psa_prdetectionclass_change                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/<path:object_id>/delete/            | django.contrib.admin.options.delete_view                                  | admin:psa_prdetectionclass_delete                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/<path:object_id>/history/           | django.contrib.admin.options.history_view                                 | admin:psa_prdetectionclass_history                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/add/                                | django.contrib.admin.options.add_view                                     | admin:psa_prdetectionclass_add                    |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/prdetectionclass/autocomplete/                       | django.contrib.admin.options.autocomplete_view                            | admin:psa_prdetectionclass_autocomplete           |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/                                     | django.contrib.admin.options.changelist_view                              | admin:psa_predictionclass_changelist              |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/<path:object_id>/                    | django.views.generic.base.RedirectView                                    |                                                   |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/<path:object_id>/change/             | django.contrib.admin.options.change_view                                  | admin:psa_predictionclass_change                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/<path:object_id>/delete/             | django.contrib.admin.options.delete_view                                  | admin:psa_predictionclass_delete                  |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/<path:object_id>/history/            | django.contrib.admin.options.history_view                                 | admin:psa_predictionclass_history                 |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/add/                                 | django.contrib.admin.options.add_view                                     | admin:psa_predictionclass_add                     |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/psa/predictionclass/autocomplete/                        | django.contrib.admin.options.autocomplete_view                            | admin:psa_predictionclass_autocomplete            |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+
    | /admin/r/<int:content_type_id>/<path:object_id>/                | django.contrib.contenttypes.views.shortcut                                | admin:view_on_site                                |
    +-----------------------------------------------------------------+---------------------------------------------------------------------------+---------------------------------------------------+

