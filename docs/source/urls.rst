URLs
====

.. toctree::
    :maxdepth: 2
    :caption: Sections:

    Project<urls_project>
    Admin<urls_admin>
    All<urls_all>