Presentation Features
=====================

This is done by the first part of the Django web application. It is split into a few sections:

1) `PSA`_ 
2) `Automated`_
3) `Manual`_
4) `Analysis`_
5) `Admin`_



PSA
---

.. _PSA Routes:

Routes
~~~~~~

1) / or /psa/ to access home page
2) /auto_analyze_day/ or /psa/auto_analyze_day/ performs the automated analysis of one day
3) /auto_analyze_days/ or /psa/auto_analyze_days/ performs the automated analysis of all days in the data/new directory
4) /add_day/<path:sub_dir>/ or /psa/add_day/<path:sub_dir>/ adds the automated analyses for a day to the database 
5) /add_days/<path:sub_dir>/ or /psa/add_days/<path:sub_dir>/ adds the automated analyses for all days in the data/analyzed directory to the database 
6) /open_file/<path:filepath>/ or /psa/open_file/<path:filepath>/ opens a file (used to open day’s JSON and log files)
7) /populate_tables/ or /psa/populate_tables/ populates the psa tables in the database (one at a time or all unpopulated at once)

This is the "backbone" of the web app. It has the logic for the home page, it connects the web app to the spot_analysis.py script, its models contain the 
generic information that's referenced by the other parts of the app (it has tables that contain data for all colours, image types, image angles, Imageai 
detection classes, plate recognizer detection classes, and prediction classes).

.. _PSA Navbar:

Navbar
~~~~~~

The main navbar has 4 sections:

1) The "brand" section that has the word PSA and links back to the home page

2) The PSA section that has a dropdown menu (seen below):
    
    a) Home links to the home page

    b) Populate Tables links to the Populate PSA Tables page

3) The Analysis section that has a dropdown menu (seen below)

    a) All links to the home page for the analysis app (has a table summarizing the automated and manual analyses)

    b) Automated links to the home page of the automated app (has a table summarizing the automated analyses for all days)

    c) Manual links to the home page of the manual app (has a table summarizing the manual analyses for all days)

4) the Statistics section which links to the statistics page

.. figure:: _images/features_presentation/psa/nav_bar/1_psa.png
    :alt: Navbar - PSA dropdown

    Navbar - PSA dropdown

.. figure:: _images/features_presentation/psa/nav_bar/2_analysis.png
    :alt: Navbar - Analysis dropdown

    Navbar - Analysis dropdown

Home page
~~~~~~~~~

The home page looks different based on the state of the file structure and database. The one constant is the action bar at the top that allows you to run group operations:

.. figure:: _images/features_presentation/psa/home_page/0_action_bar.jpg
    :alt: Action Bar

    Action Bar

Home page states:

.. figure:: _images/features_presentation/psa/home_page/1_hp_before_new.png
    :alt: No data in data/new and database is empty

    No data in data/new and database is empty

.. figure:: _images/features_presentation/psa/home_page/2_hp_default.png
    :alt: After first 9 days added to data/new

    After first 9 days added to data/new

The number of days (in data/new + data/analyzed + database) is shown. If there are more than 10 days, then the table is paginated (10 per page). By default, the days are shown 
in descending order so the most recent day is shown at the top. The day and status columns are sortable.There is a status filter to the left of the action bar. The status 
filters are:

1) all (default value): shows all days no matter the status 
2) need automated analysis: shows all days that have the "needs automated analyses" status (they are in data/new and need to be analyzed by spot_analysis.py)
3) not in database: shows all days that have been analyzed by spot_analysis.py and are in data/analyzed, but need to be added to the database
4) need manual analysis: shows all days that are in the database and have automated analyses, but don't have manual analyses
5) complete: shows all days that have the status "complete". All their entries have both manual and automated analyses.

These statuses are shown in the status column of the table, but the row colours also make it easy to identify the status of a particular day. The colour code is:

1) need automated analysis = black
2) not in database = red
3) need manual analysis = yellow 
4) complete = green

As the data is worked on and the statuses of the days change the table is changed to reflect these changes. These changes also include the action bar.

.. figure:: _images/features_presentation/psa/home_page/3_hp_day_action_bar_need_auto_a.jpg
    :alt: Action bar for day that needs automated analysis

    Action bar for day that needs automated analysis

.. figure:: _images/features_presentation/psa/home_page/4a_hp_after_18_auto_a.png
    :alt: Home page after auto analysis of first day

    Home page after auto analysis of first day

.. figure:: _images/features_presentation/psa/home_page/4b_auto_a_rotated_message.png
    :alt: Successful message for day that had images rotated during analysis

    Successful message for day that had images rotated during analysis

.. figure:: _images/features_presentation/psa/home_page/4c_auto_a_grouped_message.png
    :alt: Successful message for grouped days

    Successful message for grouped days

.. figure:: _images/features_presentation/psa/home_page/4d_auto_a_fed_in_message.png
    :alt: Successful message for fed in day

    Successful message for fed in day

.. figure:: _images/features_presentation/psa/home_page/4e_auto_a_rotated_fed_in_message.png
    :alt: Successful message for rotated and fed in day

    Successful message for rotated and fed in day

.. figure:: _images/features_presentation/psa/home_page/4f_auto_a_fed_in_group_message.png
    :alt: Successful message for grouped fed in days

    Successful message for grouped fed in days

.. figure:: _images/features_presentation/psa/home_page/5_hp_day_action_bar_add_to_db.jpg
    :alt: Action bar for day that needs to be added to the database

    Action bar for day that needs to be added to the database

.. figure:: _images/features_presentation/psa/home_page/6_hp_after_18_add_to_db.png
    :alt: Home page after add first day to database

    Home page after add first day to database

.. figure:: _images/features_presentation/psa/home_page/7_hp_day_action_bar_need_manual_a.jpg
    :alt: Action bar for day that needs manual analysis

    Action bar for day that needs manual analysis

.. figure:: _images/features_presentation/psa/home_page/8_hp_after_18_complete.png
    :alt: Home page after all automated and manual analyses done for first day

    Home page after all automated and manual analyses done for first day

.. figure:: _images/features_presentation/psa/home_page/9_hp_day_action_bar_complete.jpg
    :alt: Action bar for day that is complete

    Action bar for day that is complete

.. figure:: _images/features_presentation/psa/home_page/10_hp_after_30_complete.png
    :alt: Home page after completing the 30 :sup:`th` 

    Home page after completing the 30 :sup:`th` 

.. figure:: _images/features_presentation/psa/home_page/11_hp_after_add_feed_in_data.png
    :alt: Home page after adding feed in data to data/new

    Home page after adding feed in data to data/new

.. figure:: _images/features_presentation/psa/home_page/12_hp_while_working_on_feed_in_data.png
    :alt: Home page while working on feed in data

    Home page while working on feed in data

.. figure:: _images/features_presentation/psa/home_page/13_hp_after_all_days_complete.png
    :alt: Home page after all days complete

    Home page after all days complete


Populate PSA tables
~~~~~~~~~~~~~~~~~~~

This page is used when starting with an empty database. It uses the files in the json_for_psa_models directory to populate the PSA tables:

1) colours.json: has the names, hex, red, green and blue values for the colours (including an 'unknown' colour incase the colour of the object wasn't detected)

2) image_angles.json: the names of the possible angles of the object in the image

3) image_types.json: the names of the different image types (same as the names of the images subdirectories)

4) imageai_detection_classes.json: the names of the possible Imageai detection classes of the objects (including an 'unknown' class incase the object's Imageai detection class wasn't identified)

5) pr_detection_classes.json: the names of the possible pr detection classes of the objects

6) prediction_classes.json: the names of the possible prediction classes of the objects (including an 'unknown' class incase the object's prediction class wasn't identified) 

.. figure:: _images/features_presentation/psa/pop_psa_table/1a_before.png
    :alt: Before any tables are populated

    Before any tables are populated

.. figure:: _images/features_presentation/psa/pop_psa_table/1b_populate_psa_tables_actions.jpg
    :alt: Populate PSA tables actions

    Populate PSA tables actions
    
.. figure:: _images/features_presentation/psa/pop_psa_table/2_after_colour.png
    :alt: After first table is populated

    After first table is populated
    
.. figure:: _images/features_presentation/psa/pop_psa_table/3_before_last_2.png
    :alt: Before populating the last two (2) tables

    Before populating the last two (2) tables
    
.. figure:: _images/features_presentation/psa/pop_psa_table/4_after_last_2.png
    :alt: After populating the last two (2) tables

    After populating the last two (2) tables



Automated
---------

This section shows the results of the automated analyses.

.. _Automated Routes:

Routes
~~~~~~

1) /analysis/automated/ summarizes the automated analysis results for all days (is filterable)
2) /analysis/automated/day/<int:pk>/ summarizes the automated analysis results for one day (is filterable)  (has links to open JSON and log files) and shows the entry and day timing
3) /analysis/automated/entry/<int:pk>/ shows the automated analysis results for one entry (including images)


.. _Automated Home:

Home
~~~~

Like the project home page (psa home), this page changes as the data is processed. Its rows use the same colour system. However, only two (2) statuses will appear here: 
1) needs manual analysis (yellow) and 2) complete (green).


.. figure:: _images/features_presentation/automated/home/1a_before.png
    :alt: Before any data is added to the database

    Before any data is added to the database
    
.. figure:: _images/features_presentation/automated/home/1b_top.jpg
    :alt: Action bar and filter (no days added) (in development)

    Action bar and filters (no days added) (in development)
    
.. figure:: _images/features_presentation/automated/home/6b_complete_top.png
    :alt: Action bar and filter (all days added) (final version)

    Action bar and filters (all days added) (final version)
    
.. figure:: _images/features_presentation/automated/home/1c_action_bar.jpg
    :alt: After first day is added

    After first day is added
    
.. figure:: _images/features_presentation/automated/home/1d_filters.png
    :alt: Filters

    Filters
    
The filters for the automated analysis home page are:

1) Day: date range (inclusive)
2) Tried Feed In: True/False value indicated if an attempt was made to feed the Imageai extracted image in to the plate recognizer analysis
3) Successful Feed In: True/False value if the feed in was successful
4) Status: needs manual analysis or complete
5) Arrival Time: 24hr format time range (inclusive)
6) Departure Time: 24hr format time range (inclusive)
7) Duration greater than or equal to : hour:minute:second format
8) Duration (equal to): hour:minute:second format
9) Duration less than or equal to : hour:minute:second format
10) Iai Detection Class: a list of Imageai detection classes that have been used in the automated analyses (filtered down from all possible detection classes) 
11) Iai Prediction Class: a list of prediction classes that have been used in the automated analyses Imageai analyses (filtered down from all possible prediction classes) 
12) Pr Detection Class: a list of plate recognizer detection classes that have been used in the automated analyses (filtered down from all possible detection classes) 
13) Pr Prediction Class: a list of prediction classes that have been used in the automated analyses plate recognizer analyses (filtered down from all possible prediction classes) 
14) Plate number contains: a partial or full plate number

.. note:: All select inputs can be typed in to speed up searches. All select inputs only support one (1) selection at a time

When filters are applied, the number of entries shown at the top and bottom are updated so you know how many results you have.

.. figure:: _images/features_presentation/automated/home/2_after_add_first_day.png
    :alt: After first day is added

    After first day is added
    
.. figure:: _images/features_presentation/automated/home/3a_after_complete_one_entry_for_first_day.png
    :alt: After complete one entry for first day

    After complete one entry for first day
    
.. figure:: _images/features_presentation/automated/home/3b_actions_need_manual_a.jpg
    :alt: Actions for an entry that needs a manual analysis

    Actions for an entry that needs a manual analysis
    
.. figure:: _images/features_presentation/automated/home/3c_actions_complete.jpg
    :alt: Actions for an entry that is complete

    Actions for an entry that is complete
    
.. figure:: _images/features_presentation/automated/home/4_after_first_day_completed.png
    :alt: After first day is completed

    After first day is completed
    
.. figure:: _images/features_presentation/automated/home/5_after_add_first_feed_in_day.png
    :alt: After first feed in day is added

    After first feed in day is added
    
.. figure:: _images/features_presentation/automated/home/6_after_all_complete.png
    :alt: After all days are complete

    After all days are complete

Clicking on a day in the day column takes you to the `Automated Day Summary`_ for that day.


.. _Automated Day Summary:

Day Summary
~~~~~~~~~~~

.. figure:: _images/features_presentation/automated/day/1a_full_page.png
    :alt: Automated Day summary (full)

    Automated Day summary (full)

.. figure:: _images/features_presentation/automated/day/1b_full_page_cropped.png
    :alt: Automated Day summary (cropped)

    Automated Day summary (cropped)


.. figure:: _images/features_presentation/automated/day/1c_full_page_no_feed_in.png
    :alt: Automated Day summary (didn't try feed in)

    Automated Day summary (didn't try feed in)

The top of this page looks similar to the top of the automated home page. 

.. figure:: _images/features_presentation/automated/day/2a_title_bar.png
    :alt: Title bar

    Title bar
    
.. figure:: _images/features_presentation/automated/day/2b_title_bar_no_feed_in.png
    :alt: Title bar (didn't try feed in)

    Title bar (didn't try feed in)

.. figure:: _images/features_presentation/automated/day/2c_action_bar.jpg
    :alt: Grouped Action bar

    Grouped Action bar

.. figure:: _images/features_presentation/automated/day/3_filters.png
    :alt: Filters

    Filters

The filters are the same as the automated home page, except for the fact that day and tried feed in are excluded.

The rest of the page is broken into three (3) tabs:


.. _Automated Day Summary Entry Info Tab:

Entry Info tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/automated/day/1a_full_page.png
    :alt: Entry Info tab

    Entry Info tab

The row colours are the same as the automated home page. The columns are also the same except for the fact that day and tried feed in are excluded. 
The entry actions are also the same.


.. _Automated Day Summary Entry Timing Tab:

Entry Timing tab
^^^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/automated/day/4_entry_timing.png
    :alt: Entry Timing tab

    Entry Timing tab


Each entry is identified by its arrival time.

The time columns are:

1) Get duration time: how long it took to run the :func:`spot_analysis.get_duration` function.

2) IAI vehicle detection time: how long the :func:`spot_analysis.imageai_detect` function took

3) IAI vehicle prediction time: how long the :func:`spot_analysis.imageai_predict` function took when called with the Imageai extracted image

4) IAI colour detection time: how long the :func:`spot_analysis.detect_colours` function took when called with the Imageai extracted image

5) Total IAI analysis time: total time for Imageai analysis of the image

6) PR vehicle detection time: how long the :func:`spot_analysis.plate_recognizer_detect` function took

7) PR vehicle prediction time: how long the :func:`spot_analysis.imageai_predict` function took when called with the plate recognizer extracted image

8) PR colour detection time: how long the :func:`spot_analysis.detect_colours` function took when called with the plate recognizer extracted image

9) Total PR analysis time: total time for Plate recognizer analysis of the image

10) Total Image analysis time: total time to analyze the image

11) Total Entry time: total time to process the entry


.. _Automated Day Summary Day Timing Tab:

Day Timing tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/automated/day/5_day_timing.png
    :alt: Day timing tab

    Day timing tab


The first eleven (11) times are just the sums of the entry values. The last time is the total time to process the entire day (analyze entries, setup and move files, etc.)


.. _Automated Entry Details:

Entry Details
~~~~~~~~~~~~~

.. figure:: _images/features_presentation/automated/entry/1a_orig_image_needs_manual_a.png
    :alt: Automated Entry Details page (needs manual analysis)

    Automated Entry Details page (needs manual analysis)

.. figure:: _images/features_presentation/automated/entry/1b_orig_image_complete.png
    :alt: Automated Entry Details page (complete)

    Automated Entry Details page (complete)


.. figure:: _images/features_presentation/automated/entry/0a_title_bar_needs_manual_a.png
    :alt: Title bar (needs manual analysis)

    Title bar (needs manual analysis)


.. figure:: _images/features_presentation/automated/entry/0b_title_bar_complete.png
    :alt: Title bar (complete)

    Title bar (complete)


.. figure:: _images/features_presentation/automated/entry/0c_action_buttons_needs_manual_a.jpg
    :alt: Action Buttons (needs manual analysis)

    Action Buttons (needs manual analysis)


.. figure:: _images/features_presentation/automated/entry/0d_action_buttons_complete.jpg
    :alt: Action Buttons (complete)

    Action Buttons (complete)



The page is divided into two sections:

1) `Images <#automated-entry-images>`_
2) `Information <#automated-entry-information>`_

.. _Automated Entry Images:

Entry Images
^^^^^^^^^^^^

This is shown on the left and is further broken into tabs (each containing one image):

Original tab
""""""""""""

.. figure:: _images/features_presentation/automated/entry/1b_orig_image_complete.png
    :alt: Original Image (entire page)

    Original Image (entire page)

.. figure:: _images/features_presentation/automated/entry/1c_original_cropped.png
    :alt: Original Image (cropped)

    Original Image (cropped)


Extracted Imageai tab
"""""""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/2a_extracted_iai_not_fed_in.png
    :alt: Extracted Imageai Image (not fed in) (entire page)

    Extracted Imageai Image (not fed in) (entire page)

.. figure:: _images/features_presentation/automated/entry/2b_extracted_iai_not_fed_in_cropped.png
    :alt: Extracted Imageai Image (not fed in) (cropped)

    Extracted Imageai Image (not fed in) (cropped)

.. figure:: _images/features_presentation/automated/entry/2c_extracted_iai_fed_in.png
    :alt: Extracted Imageai Image (fed in) (entire page)

    Extracted Imageai Image (fed in) (entire page)

.. figure:: _images/features_presentation/automated/entry/2d_extracted_iai_fed_in_cropped.png
    :alt: Extracted Imageai Image (fed in) (cropped)

    Extracted Imageai Image (fed in) (cropped)


Extracted PR tab
""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/3a_extracted_pr_not_fed_in.png
    :alt: Extracted Pr Image (not fed in) (entire page)

    Extracted Pr Image (not fed in) (entire page)

.. figure:: _images/features_presentation/automated/entry/3b_extracted_pr_not_fed_in_cropped.png
    :alt: Extracted Pr Image (not fed in) (cropped)

    Extracted Pr Image (not fed in) (cropped)

.. figure:: _images/features_presentation/automated/entry/3c_extracted_pr_fed_in.png
    :alt: Extracted Pr Image (fed in) (entire page)

    Extracted Pr Image (fed in) (entire page)

.. figure:: _images/features_presentation/automated/entry/ 3d_extracted_pr_fed_in_cropped.png
    :alt: Extracted Pr Image (fed in) (cropped)

    Extracted Pr Image (fed in) (cropped)


Extracted Plate tab
"""""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/4a_extracted_plate.png
    :alt: Extracted Plate Image (entire page)

    Extracted Plate Image (entire page)

.. figure:: _images/features_presentation/automated/entry/4b_extracted_plate_cropped.png
    :alt: Extracted Plate Image (cropped)

    Extracted Plate Image (cropped)



Analyzed Comparison tab
"""""""""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/5a_analyzed_comparison.png
    :alt: Analyzed Comparison Image (entire page)

    Analyzed Comparison Image (entire page)

.. figure:: _images/features_presentation/automated/entry/5b_analyzed_comparison_cropped.png
    :alt: Analyzed Comparison Image (cropped)

    Analyzed Comparison Image (cropped)



Analyzed Imageai tab
""""""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/6a_analyzed_iai.png
    :alt: Analyzed Imageai Image (entire page)

    Analyzed Imageai Image (entire page)

.. figure:: _images/features_presentation/automated/entry/6b_analyzed_iai_cropped.png
    :alt: Analyzed Imageai Image (cropped)

    Analyzed Imageai Image (cropped)


Analyzed Pr tab
"""""""""""""""

.. figure:: _images/features_presentation/automated/entry/7a_analyzed_pr.png
    :alt: Analyzed Pr Image (entire page)

    Analyzed Pr Image (entire page)

.. figure:: _images/features_presentation/automated/entry/7b_analyzed_pr_cropped.png
    :alt: Analyzed Pr Image (cropped)

    Analyzed Pr Image (cropped)


.. _Automated Entry Information:

Entry Information
^^^^^^^^^^^^^^^^^

This is shown on the right and is further broken into tabs:

.. _Automated Entry Basic tab:

Basic tab
"""""""""

.. figure:: _images/features_presentation/automated/entry/1b_orig_image_complete.png
    :alt: Basic tab (entire page)

    Basic tab (entire page)

.. figure:: _images/features_presentation/automated/entry/8_basic_tab_cropped.png
    :alt: Basic tab (cropped)

    Basic tab (cropped)

Has the basic information for the entry:

1) Day: the date
2) Status
3) Arrival time: in 12hr am/pm format
4) Departure time: in 12hr am/pm format
5) Duration: in hours:minutes:seconds format
6) Feed in
    
    a) Tried Feed In: True or False based on whether feed in was attempted
    b) Successful Feed In: True if it was successful, False if it wasn't and None if it wasn't attempted.


.. _Automated Entry IAI tab:

IAI tab
"""""""

.. figure:: _images/features_presentation/automated/entry/9a_iai_tab.png
    :alt: IAI tab (entire page)

    IAI tab (entire page)

.. figure:: _images/features_presentation/automated/entry/9b_iai_tab_cropped.png
    :alt: IAI tab (cropped)

    IAI tab (cropped)

Has the Imageai analysis information for the entry:

1) IAI vehicle detection class: the detected class
2) IAI vehicle detection probability: the detection probability as a percentage
3) IAI vehicle prediction class: the prediction class
4) IAI vehicle prediction probability: the prediction probability as a percentage
5) IAI colour detections: 

    a) An interactive pie chart showing the proportions of the colours detected.
    b) A table containing:

        i) the name of the colour
        ii) the percentage/proportion
        iii) the detected RGB values (r,g,b)
        iv) the detected hex value
        v) a sample of the actual colour that was detected.

.. _Automated Entry PR tab:

PR tab
""""""

.. figure:: _images/features_presentation/automated/entry/10a_pr_tab.png
    :alt: PR tab (entire page)

    PR tab (entire page)

.. figure:: _images/features_presentation/automated/entry/10b_pr_tab_cropped.png
    :alt: PR tab (cropped)

    PR tab (cropped)

The sections are the same as the IAI tab sections, but the information is for the plate recognizer analysis.


.. _Automated Entry License Plate tab:

License Plate tab
"""""""""""""""""

.. figure:: _images/features_presentation/automated/entry/11a_plate_tab.png
    :alt: License Plate tab (entire page)

    License Plate tab (entire page)

.. figure:: _images/features_presentation/automated/entry/11b_plate_tab_cropped.png
    :alt: License Plate tab (cropped)

    License Plate tab (cropped)

Has the license plate information for the entry:

1) License plate number
2) License plate probability (as a percentage)


.. _Automated Entry Timing tab:

Timing tab
""""""""""

.. figure:: _images/features_presentation/automated/entry/12a_timing_tab.png
    :alt: Timing tab (entire page)

    Timing tab (entire page)

.. figure:: _images/features_presentation/automated/entry/12b_timing_tab_cropped.png
    :alt: Timing tab (cropped)

    Timing tab (cropped)

Has the entry's timing information. Same values as the `Automated Day Summary Entry Timing tab`_.



Manual
------

This section shows the results of the manual analyses.

.. _Manual Routes:

Routes
~~~~~~

1) /analysis/manual/  summarizes the manual analysis results for all days (is filterable)
2) /analysis/manual/new/<int:pk>/ used to create a manual analysis for an entry (one side has basic info (day, status, arrival and departure times), iai colours, pr colours and the images; the other has a form used to create the manual analysis)
3) /analysis/manual/entry/<int:pk>/update/ used to update a manual analysis for an entry (one side has basic info (day, status, arrival and departure times), iai colours, pr colours and the images; the other has a form used to update the manual analysis)
4) /analysis/manual/day/<int:pk>/ summarizes the manual analysis results for one day (is filterable)  (has links to open JSON and log files) and shows the entry and day timing
5) /analysis/manual/entry/<int:pk>/ Shows the manual analysis results for one entry


.. _Manual Home:

Home
~~~~

Like the project (psa home) and automated (automated home) home pages, this page changes as the data is processed. It starts out blank and as manual analyses are added, it is populated. Its rows use the same colour system. However, only one (1) status will 
appear here (complete (green)), because once an entry has both an automated and manual analysis, it is considered complete.

.. figure:: _images/features_presentation/manual/home/1a_before.png
    :alt: Before any manual analyses were added

    Before any manual analyses were added
    
.. figure:: _images/features_presentation/manual/home/1b_top_in_development.jpg
    :alt: Action bar and filter (no manual analyses added) (in development)

    Action bar and filters (no manual analyses added) (in development)
    
.. figure:: _images/features_presentation/manual/home/1c_top_final.jpg
    :alt: Action bar and filter (all manual analyses added) (final version)

    Action bar and filters (all manual analyses added) (final version)
    
.. figure:: _images/features_presentation/manual/home/1d_title_bar.png
    :alt: Title bar

    Title bar
    
.. figure:: _images/features_presentation/manual/home/1e_group_actions.jpg
    :alt: Group actions

    Group actions
    
.. figure:: _images/features_presentation/manual/home/1f_filters.png
    :alt: Filters

    Filters

The filters for the manual analysis home page are:

1) Day: date range (inclusive)
2) Tried Feed In: True/False value indicated if an attempt was made to feed the Imageai extracted image in to the plate recognizer analysis
3) Successful Feed In: True/False value if the feed in was successful
4) Arrival Time: 24hr format time range (inclusive)
5) Departure Time: 24hr format time range (inclusive)
6) Duration greater than or equal to : hour:minute:second format
7) Duration (equal to): hour:minute:second format
8) Duration less than or equal to : hour:minute:second format
9) Iai Detection Class: a list of Imageai detection classes that have been used in the manual analyses (filtered down from all possible detection classes) 
10) Pr Detection Class: a list of plate recognizer detection classes that have been used in the manual analyses (filtered down from all possible detection classes) 
11) Manual Prediction Class: a list of prediction classes that have been used in the manual analyses (filtered down from all possible prediction classes) 
12) Plate number contains: a partial or full plate number
13) Dominant Colour: a list of the colours identified as dominant (filtered down from all colours)
14) Dominant Colour detected by IAI: True/False value if the dominant colour was deteted in the Imageai analysis
15) Dominant Colour identified as dominant by IAI: True/False value if the dominant colour was identified as dominant in the Imageai analysis; None if no colours were identified
16) Dominant Colour detected by PR: True/False value if the dominant colour was deteted in the Plate Recognizer analysis
17) Dominant Colour identified as dominant by PR: True/False value if the dominant colour was identified as dominant in the Plate Recognizer analysis; None if no colours were identified
18) Image Angle: the possible angles of the objects in the images
19) IAI extract correct: True/False if the correct object was extracted in the Imageai analysis; None if no object was extracted
20) PR extract correct: True/False if the correct object was extracted in the Plate Recognizer analysis; None if no object was extracted
21) Plate extract correct: True/False if the correct plate was extracted in the Plate Recognizer analysis; None if no plate was extracted

.. note:: All select inputs can be typed in to speed up searches. All select inputs only support one (1) selection at a time

When filters are applied, the number of entries shown at the top and bottom are updated so you know how many results you have.

.. figure:: _images/features_presentation/manual/home/2_after_complete_first_entry_for_first_day.png
    :alt: After complete first entry for first day

    After complete first entry for first day

.. figure:: _images/features_presentation/manual/home/3_after_all_complete.png
    :alt: After all complete

    After all complete
    
.. figure:: _images/features_presentation/manual/home/4_entry_actions.jpg
    :alt: Entry actions

    Entry actions

    Clicking on a day in the day column takes you to the `Manual Day Summary`_ for that day.


.. _Manual Creation:

Creation
~~~~~~~~

Used to create a manual analysis for an entry.

.. figure:: _images/features_presentation/manual/creation/1_entire_page.png
    :alt: Manual analysis creation page

    Manual analysis creation page

It's broken into two parts:

1) `Information <Manual Creation Information>`_
2) `Form <Manual Creation Form>`_


.. _Manual Creation Information:

Information 
^^^^^^^^^^^

.. figure:: _images/features_presentation/manual/creation/2_info_basic.png
    :alt: Information section

    Information section

.. _Manual Creation Information Basic Info tab:

Basic Info tab
""""""""""""""

.. figure:: _images/features_presentation/manual/creation/2_info_basic.png
    :alt: Basic Info tab

    Basic Info tab

Has the basic information for the entry:

1) Day: the date
2) Status
3) Arrival time: in 24hr format
4) Departure time: in 24hr format

.. _Manual Creation Information IAI Colours tab:

IAI Colours tab
"""""""""""""""

.. figure:: _images/features_presentation/manual/creation/3_info_iai_colours.png
    :alt: IAI Colours tab

    IAI Colours tab

This is the same IAI colours section that was used in `Automated Entry IAI tab`_.

.. _Manual Creation Information PR Colours tab:

PR Colours tab
""""""""""""""

.. figure:: _images/features_presentation/manual/creation/4_info_pr_colours.png
    :alt: PR Colours tab

    PR Colours tab

This is the same PR colours section that was used in `Automated Entry PR tab`_.

.. _Manual Creation Information Images tab:

Images tab
""""""""""

.. figure:: _images/features_presentation/manual/creation/5_info_images.png
    :alt: Images tab

    Images tab

These are the same images that were used in `Automated Entry Images`_.


.. _Manual Creation Form:

Form
^^^^

.. figure:: _images/features_presentation/manual/creation/6_form_orig.png
    :alt: Form (original)

    Form (original)

.. figure:: _images/features_presentation/manual/creation/7_form_expanded.png
    :alt: Form (expanded)

    Form (expanded)
    
.. figure:: _images/features_presentation/manual/creation/8_form_completed.png
    :alt: Form (completed)

    Form (completed)
    
.. figure:: _images/features_presentation/manual/creation/9a_form_iai_expand.png
    :alt: Form (IAI detection class expanded)

    Form (IAI detection class expanded)
    
.. figure:: _images/features_presentation/manual/creation/9b_form_iai_expand_typing.png
    :alt: Form (IAI detection class expanded + typed in)

    Form (IAI detection class expanded + typed in)
    
.. figure:: _images/features_presentation/manual/creation/10a_form_colour_expand.png
    :alt: Form (Colour class expanded)

    Form (Colour class expanded)
    
.. figure:: _images/features_presentation/manual/creation/10b_form_colour_expand_typing.png
    :alt: Form (Colour class expanded + typed in)

    Form (Colour class expanded + typed in)

Sections:

1) Duration: in hours:minutes:seconds format 
2) Iai detection class: a list of Imageai detection classes, that the correct class can be selected from
3) Pr detection class: a list of plate recognizer detection classes, that the correct class can be selected from
4) Prediction class: a list of prediction classes, that the correct class can be selected from
5) License plate number: text box to input the license plate
6) Dominant colour: a list of colours in the database that the dominant colour can be selected from
7) Dominant colour detected by iai: checkbox to show if the dominant colour was deteted in the Imageai analysis
8) Dominant colour identified as dominant by iai (only shown if checkbox for 'dominant colour detected by iai' is checked): True/False/None value
9) Dominant colour detected by pr: checkbox to show if the dominant colour was deteted in the Plate recognizer analysis
10) Dominant colour identified as dominant by pr (only shown if checkbox for 'dominant colour detected by pr' is checked): True/False/None value
11) Image angle: the angle of the object in the image
12) Iai extract correct: True/False if the correct object was extracted in the Imageai analysis; None if no object was extracted
13) Iai extract entire (only shown if checkbox for 'Iai extract correct' is checked): True/False if the entire object was extracted in the Imageai analysis; None if no object was extracted
14) Pr extract correct: True/False if the correct object was extracted in the Plate recognizer analysis; None if no object was extracted
15) Pr extract entire (only shown if checkbox for 'Pr extract correct' is checked): True/False if the entire object was extracted in the PLate recognizer analysis; None if no object was extracted
16) Plate extract correct: True/False if the correct plate was extracted in the Plate recognizer analysis; None if no plate was extracted
17) Plate extract entire (only shown if checkbox for 'Plate extract correct' is checked): True/False if the entire plate was extracted in the Plate recognizer analysis; None if no plate was extracted

.. figure:: _images/features_presentation/manual/creation/11_after_creation.png
    :alt: After successful creation (redirected to manual entry details)

    After successful creation (redirected to manual entry details)

.. _Manual Update:

Update
~~~~~~

Used to update a manual analysis for an entry.

.. figure:: _images/features_presentation/manual/update/1_entire_window.png
    :alt: Entire window

    Entire window
    
.. figure:: _images/features_presentation/manual/update/2_entire_page.png
    :alt: Entire page

    Entire page

Like the `Manual Creation`_ page, it's broken into two parts:

1) `Information <Manual Update Information>`_
2) `Form <Manual Update Form>`_


.. _Manual Update Information:

Information 
^^^^^^^^^^^

.. figure:: _images/features_presentation/manual/update/3_information.png
    :alt: Information

    Information

Has the same tabs as `Manual Creation Information`_.


.. _Manual Update Form:

Form
^^^^
    
.. figure:: _images/features_presentation/manual/update/4_form.png
    :alt: Form

    Form
    
Has the same sections as `Manual Creation Form`_. It's the same form just pre-filled with the data for the entry's manual analysis.

.. figure:: _images/features_presentation/manual/update/5_after_update.png
    :alt: After update (redirected to manual entry details)

    After update (redirected to manual entry details)


.. _Manual Day Summary:

Day Summary 
~~~~~~~~~~~

.. figure:: _images/features_presentation/manual/day/1_full_window.png
    :alt: Entire window

    Entire window

.. figure:: _images/features_presentation/manual/day/2_full_page.png
    :alt: Entire page

    Entire page

The top of this page looks similar to the top of the manual home page.

.. figure:: _images/features_presentation/manual/day/3_top.jpg
    :alt: Top of page

    Top of page
    
.. figure:: _images/features_presentation/manual/day/4_title_bar.png
    :alt: Title bar

    Title bar
    
.. figure:: _images/features_presentation/manual/day/5_group_actions.jpg
    :alt: Group actions

    Group actions
    
.. figure:: _images/features_presentation/manual/day/6_filters.png
    :alt: Filters

    Filters

The filters are the same as the manual home page, except for the fact that day and tried feed in are excluded.

The rest of the page is broken into the same three tabs as `Automated Day Summary`_:

.. _Manual Day Summary Entry Info Tab:

Entry Info tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/manual/day/2_full_page.png
    :alt: Entry Info tab

    Entry Info tab

The row colours are the same as the manual home page. The columns are also the same except for the fact that day and tried feed in are excluded. 
The entry actions are also the same.


.. _Manual Day Summary Entry Timing Tab:

Entry Timing tab
^^^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/manual/day/7_entry_timing.png
    :alt: Entry Timing tab

    Entry Timing tab

Identical to the one in `Automated Day Summary Entry Timing Tab`_.

.. _Manual Day Summary Day Timing Tab:

Day Timing tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/manual/day/8_day_timing.png
    :alt: Day timing tab

    Day timing tab

Identical to the one in `Automated Day Summary Day Timing Tab`_.


.. _Manual Entry Details:

Entry Details
~~~~~~~~~~~~~

.. figure:: _images/features_presentation/manual/entry/1_entire_window.png
    :alt: Entire Window

    Entire Window

.. figure:: _images/features_presentation/manual/entry/2_title_bar.png
    :alt: Title bar

    Title bar

.. figure:: _images/features_presentation/manual/entry/3_action_buttons.jpg
    :alt: Action Buttons

    Action Buttons

The page is divided into the same two sections as `Automated Entry Details`_:

1) `Images <#manual-entry-images>`_
2) `Information <#manual-entry-information>`_

.. _Manual Entry Images:

Entry Images
^^^^^^^^^^^^

This is shown on the left and is further broken into tabs (each containing one image) (it's the same 'images' section used in `Automated Entry Images`_):

.. figure:: _images/features_presentation/manual/entry/4_images.png
    :alt: Images

    Images



.. _Manual Entry Information:

Entry Information
^^^^^^^^^^^^^^^^^

This is shown on the right and is further broken into tabs (same tabs as `Automated Entry Information`_):

.. figure:: _images/features_presentation/manual/entry/5_info.png
    :alt: Information

    Information


.. _Manual Entry Basic tab:

Basic tab
"""""""""

.. figure:: _images/features_presentation/manual/entry/6_basic_info_tab.png
    :alt: Basic tab

    Basic tab

Has the basic information for the entry. Has the same information as `Automated Entry Basic tab`_, but the duration is the one from the manual analysis.


.. _Manual Entry IAI tab:

IAI tab
"""""""

.. figure:: _images/features_presentation/manual/entry/7_iai_tab.png
    :alt: IAI tab

    IAI tab

Has the Imageai information for the entry:

1) IAI vehicle detection class: the class from the manual analysis

2) Prediction class: the prediction class

3) IAI colour info:

    a) Dominant colour
    b) Dominint colour detected by IAI
    c) Dominant colour identified by IAI as dominant

4) IAI image info:

    a) Image angle
    b) IAI extracted correct object
    c) IAI extracted entire object

.. _Manual Entry PR tab:

PR tab
""""""

.. figure:: _images/features_presentation/manual/entry/8_pr_tab.png
    :alt: PR tab

    PR tab

The sections are the same as the IAI tab sections, but the information is for the plate recognizer information.


.. _Manual Entry License Plate tab:

License Plate tab
"""""""""""""""""

.. figure:: _images/features_presentation/manual/entry/9_plate_tab.png
    :alt: License Plate tab

    License Plate tab

Has the license plate information for the entry:

1) License plate number

2) Plate image info:

    a) Image angle
    b) Correct plate extracted
    c) Entire plate extracted


.. _Manual Entry Timing tab:

Timing tab
""""""""""

.. figure:: _images/features_presentation/manual/entry/10_timing_tab.png
    :alt: Timing tab

    Timing tab

Has the entry's timing information. Same values as the `Automated Day Summary Entry Timing tab`_.



Analysis
--------

This section combines the results of the automated and manual analyses.

.. _Analysis Routes:

Routes
~~~~~~

1) /analysis/ summarizes the automated and manual analysis results for all days (is filterable)
2) /analysis/day/<int:pk>/ summarizes the automated and manual analysis results for one day (is filterable)  (has links to open JSON and log files) and shows the entry and day timing
3) /analysis/entry/<int:pk>/ shows the automated and manual analysis results for one entry (including images)


.. _Analysis Home:

Home
~~~~

Like all the other home pages, this page changes as the data is processed. Its rows use the same colour system. Only two (2) statuses will appear here: 
1) needs manual analysis (yellow) and 2) complete (green). 


.. figure:: _images/features_presentation/analysis/home/1a_before.png
    :alt: Before any data is added to the database

    Before any data is added to the database
    
.. figure:: _images/features_presentation/analysis/home/1b_top_in_development.jpg
    :alt: Action bar and filter (no days added) (in development)

    Action bar and filters (no days added) (in development)
    
.. figure:: _images/features_presentation/analysis/home/1c_top_final.png
    :alt: Action bar and filter (all days added) (final version)

    Action bar and filters (all days added) (final version)
    
.. figure:: _images/features_presentation/analysis/home/1d_title_bar.png
    :alt: Title Bar

    Title Bar
    
.. figure:: _images/features_presentation/analysis/home/1e_group_actions.jpg
    :alt: Group actions

    Group actions
    
.. figure:: _images/features_presentation/analysis/home/1f_filters.png
    :alt: Filters

    Filters
    
The filters for the analysis home page are:

1) Day: date range (inclusive)
2) Tried Feed In: True/False value indicated if an attempt was made to feed the Imageai extracted image in to the plate recognizer analysis
3) Successful Feed In: True/False value if the feed in was successful
4) Status: needs manual analysis or complete
5) Arrival Time: 24hr format time range (inclusive)
6) Departure Time: 24hr format time range (inclusive)
7) Automated Duration greater than or equal to : hour:minute:second format
8) Automated Duration (equal to): hour:minute:second format
9) Automated Duration less than or equal to : hour:minute:second format
10) Automated Iai Detection Class: a list of Imageai detection classes that have been used in the automated analyses (filtered down from all possible detection classes) 
11) Automated Iai Prediction Class: a list of prediction classes that have been used in the automated analyses Imageai analyses (filtered down from all possible prediction classes) 
12) Automated Pr Detection Class: a list of plate recognizer detection classes that have been used in the automated analyses (filtered down from all possible detection classes) 
13) Automated Pr Prediction Class: a list of prediction classes that have been used in the automated analyses plate recognizer analyses (filtered down from all possible prediction classes) 
14) Automated Plate number contains: a partial or full plate number
15) Manual Duration greater than or equal to : hour:minute:second format
16) Manual Duration (equal to): hour:minute:second format
17) Manual Duration less than or equal to : hour:minute:second format
18) Manual Iai Detection Class: a list of Imageai detection classes that have been used in the manual analyses (filtered down from all possible detection classes) 
19) Manual Pr Detection Class: a list of plate recognizer detection classes that have been used in the manual analyses (filtered down from all possible detection classes) 
20) Manual Prediction Class: a list of prediction classes that have been used in the manual analyses plate recognizer analyses (filtered down from all possible prediction classes) 
21) Manual Plate number contains: a partial or full plate number

.. note:: All select inputs can be typed in to speed up searches. All select inputs only support one (1) selection at a time

When filters are applied, the number of entries shown at the top and bottom are updated so you know how many results you have.

.. figure:: _images/features_presentation/analysis/home/2_after_add_first_day.png
    :alt: After first day is added

    After first day is added
    
.. figure:: _images/features_presentation/analysis/home/3a_after_complete_one_entry_for_first_day.png
    :alt: After complete one entry for first day

    After complete one entry for first day
    
.. figure:: _images/features_presentation/analysis/home/3b_actions_need_manual_a.jpg
    :alt: Actions for an entry that needs a manual analysis

    Actions for an entry that needs a manual analysis
    
.. figure:: _images/features_presentation/analysis/home/3c_actions_complete.jpg
    :alt: Actions for an entry that is complete

    Actions for an entry that is complete
    
.. figure:: _images/features_presentation/analysis/home/4_after_first_day_completed.png
    :alt: After first day is completed

    After first day is completed
    
.. figure:: _images/features_presentation/analysis/home/5_after_add_first_feed_in_day.png
    :alt: After first feed in day is added

    After first feed in day is added
    
.. figure:: _images/features_presentation/analysis/home/6_after_all_complete.png
    :alt: After all days are complete

    After all days are complete

Clicking on a day in the day column takes you to the `Analysis Day Summary`_ for that day.


.. _Analysis Day Summary:

Day Summary
~~~~~~~~~~~

.. figure:: _images/features_presentation/analysis/day/1_full_window.png
    :alt: Entire window

    Entire window

.. figure:: _images/features_presentation/analysis/day/2_full_page.png
    :alt: Entire page

    Entire page

The top of this page looks similar to the top of the `Analysis Home`_ page.

.. figure:: _images/features_presentation/analysis/day/3_top.jpg
    :alt: Top of page

    Top of page
    
.. figure:: _images/features_presentation/analysis/day/4_title_bar.png
    :alt: Title bar

    Title bar
    
.. figure:: _images/features_presentation/analysis/day/5_group_actions.jpg
    :alt: Group actions

    Group actions
    
.. figure:: _images/features_presentation/analysis/day/6_filters.png
    :alt: Filters

    Filters

The filters are the same as the `Analysis Home`_ page, except for the fact that day and tried feed in are excluded.

The rest of the page is broken into the same three tabs as `Automated Day Summary`_:

.. _Analysis Day Summary Entry Info Tab:

Entry Info tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/analysis/day/7_entry_info.png
    :alt: Entry Info tab

    Entry Info tab

The row colours are the same as the `Analysis Home`_ page. The columns are also the same except for the fact that day and tried feed in are excluded. 
The entry actions are also the same.


.. _Analysis Day Summary Entry Timing Tab:

Entry Timing tab
^^^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/analysis/day/8_entry_timing.png
    :alt: Entry Timing tab

    Entry Timing tab

Identical to the one in `Automated Day Summary Entry Timing Tab`_.

.. _Analysis Day Summary Day Timing Tab:

Day Timing tab
^^^^^^^^^^^^^^

.. figure:: _images/features_presentation/analysis/day/9_day_timing.png
    :alt: Day timing tab

    Day timing tab

Identical to the one in `Automated Day Summary Day Timing Tab`_.



.. _Analysis Entry Details:

Entry Details
~~~~~~~~~~~~~

This page looks different based on the status of the entry. If the entry doesn't have a manual analysis (status = needs manual analysis), then only the automated 
data is shown, if the status is complete then both the automated and manual data are shown side by side.

.. figure:: _images/features_presentation/analysis/entry/1a_entire_window_needs_manual_a.png
    :alt: Entire Window (needs manual analysis)

    Entire Window (needs manual analysis)
    
.. figure:: _images/features_presentation/analysis/entry/1b_entire_window_complete.png
    :alt: Entire Window (complete)

    Entire Window (complete)
    
.. figure:: _images/features_presentation/analysis/entry/2a_entire_page_needs_manual_a.png
    :alt: Entire Page (needs manual analysis)

    Entire Page (needs manual analysis)
    
.. figure:: _images/features_presentation/analysis/entry/2b_entire_page_complete.png
    :alt: Entire Page (complete)

    Entire Page (complete)

.. figure:: _images/features_presentation/analysis/entry/3a_title_bar_needs_manual_a.png
    :alt: Title bar (needs manual analysis)

    Title bar (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/3b_title_bar_complete.png
    :alt: Title bar (complete)

    Title bar (complete)

.. figure:: _images/features_presentation/analysis/entry/4a_action_buttons_needs_manual_a.jpg
    :alt: Action Buttons (needs manual analysis)

    Action Buttons (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/4b_action_buttons_complete.jpg
    :alt: Action Buttons (complete)

    Action Buttons (complete)

The page is divided into the same two sections as `Automated Entry Details`_ and `Manual Entry Details`_:

1) `Images <#analysis-entry-images>`_
2) `Information <#analysis-entry-information>`_

.. _Analysis Entry Images:

Entry Images
^^^^^^^^^^^^

This is shown at the top and is further broken into tabs (each containing one image) (it's the same 'images' section used in `Automated Entry Images`_):

.. figure:: _images/features_presentation/analysis/entry/5a_images_needs_manual_a.png
    :alt: Images (needs manual analysis)

    Images (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/5b_images_complete.png
    :alt: Images (complete)

    Images (complete)



.. _Analysis Entry Information:

Entry Information
^^^^^^^^^^^^^^^^^

This is shown at the bottom and is further broken into tabs (same tabs as `Automated Entry Information`_ and `Manual Entry Information`_):

.. figure:: _images/features_presentation/analysis/entry/6a_info_needs_manual_a.png
    :alt: Information (needs manual analysis)

    Information (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/6b_info_complete.png
    :alt: Information (complete)

    Information (complete)


.. _Analysis Entry Basic tab:

Basic tab
"""""""""

.. figure:: _images/features_presentation/analysis/entry/7a_basic_info_needs_manual_a.png
    :alt: Basic tab (needs manual analysis) 

    Basic tab (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/7b_basic_info_tab_complete.png
    :alt: Basic tab (complete)

    Basic tab (complete)

Has the basic information for the entry. Has the same information as `Automated Entry Basic tab`_, but the durations for both the automated and manual analyses 
are shown if they are present, else, just the automated is shown.


.. _Analysis Entry IAI tab:

IAI tab
"""""""

.. figure:: _images/features_presentation/analysis/entry/8a_iai_tab_needs_manual_a.png
    :alt: IAI tab (needs manual analysis)

    IAI tab (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/8b_iai_tab_complete.png
    :alt: IAI tab (complete)

    IAI tab (complete)

If the status is 'needs manual analysis', then just the information from the `Automated Entry IAI tab`_ is shown here, else the 
information from both `Automated Entry IAI tab`_ and `Manual Entry IAI tab`_ is shown.

.. _Analysis Entry PR tab:

PR tab
""""""

.. figure:: _images/features_presentation/analysis/entry/9a_pr_tab_needs_manual_a.png
    :alt: PR tab (needs manual analysis)

    PR tab (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/9b_pr_tab_complete.png
    :alt: PR tab (complete)

    PR tab (complete)

The sections are the same as the IAI tab sections, but the information is for the plate recognizer information.


.. _Analysis Entry License Plate tab:

License Plate tab
"""""""""""""""""

.. figure:: _images/features_presentation/analysis/entry/10a_plate_tab_needs_manual_a.png
    :alt: License Plate tab (needs manual analysis)

    License Plate tab (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/10b_plate_tab_complete.png
    :alt: License Plate tab (complete)

    License Plate tab (complete)

If the status is 'needs manual analysis', then just the information from the `Automated Entry License Plate tab`_ is shown here, else the 
information from both `Automated Entry License Plate tab`_ and `Manual Entry License Plate tab`_ is shown.


.. _Analysis Entry Timing tab:

Timing tab
""""""""""

.. figure:: _images/features_presentation/analysis/entry/11a_timing_tab_needs_manual_a.png
    :alt: Timing tab (needs manual analysis)

    Timing tab (needs manual analysis)

.. figure:: _images/features_presentation/analysis/entry/11b_timing_tab_complete.png
    :alt: Timing tab (complete)

    Timing tab (complete)

Has the entry's timing information. Same values as the `Automated Day Summary Entry Timing tab`_.



Admin
-----

This section was auto genereated by registering the models in the admin.py file in each app. It allows you to go in and edit or add values 
in the database.

.. _Admin Routes:

Routes
~~~~~~

The routes can be found in :ref:`Admin URLS`.


.. _Admin Home:

Home
~~~~

.. figure:: _images/features_presentation/admin/1_full_window.png
    :alt: Full window

    Full window

.. figure:: _images/features_presentation/admin/2_full_page.png
    :alt: Full page

    Full page