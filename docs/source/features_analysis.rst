Analysis Features
=================

The automated analysis is handled by the :mod:`spot_analysis.py` module. As the script runs, the function calls and times for the functions to 
run are logged in the terminal and a log file (one per day). The script is written so that even if the project directory is moved, it should 
function without having to change the values of the constants that store the absolute paths to: 

1) parent directory of the script (capstone_412002117_software)
2) data directory (capstone_412002117_software/data)
3) new directory (capstone_412002117_software/data/new) (has the input data to be processed)
4) analyzed directory (capstone_412002117_software/data/analyzed) (has the output data that has been processed)
5) helpers directory (capstone_412002117_software/helpers)
6) detection model path (capstone_412002117_software/helpers/detection_models/resnet50_coco_best_v2.0.1.h5) (the path to the detection model used 
   by Imageai detection)
7) prediction model path (capstone_412002117_software/helpers/prediction_models/DenseNet-BC-121-32.h5) (the path to the prediction model used 
   by the predictor)
8) path to colour info csv file (capstone_412002117_software/helpers/colour_info.csv) (contains the names, hex and red, green and blue values of 
   139 colour from `rapidtables <https://www.rapidtables.com/web/color/RGB_Color.html>`_)

Analysis Functions
------------------

Duration
~~~~~~~~

The :func:`spot_analysis.get_duration` function takes in the arrival and departure times as strings, converts them to datetime objects and calculates the 
difference, which is stored in a variable called duration. The duration in converted to seconds and stored in a variable called duration 
in seconds and this variable is used to create a human readable string containing the hours, minutes and seconds of the duration. The 
duration in seconds and the human readable string are returned.

Vehicle Type and License Plate
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For the object detection analysis, two (2) different detectors are used:  

1) Imageai (local)
2) Plate recognizer (uses an api accessed over the internet)

The extracted images produced by the detections are fed into the predictor by Imageai.

Find center
^^^^^^^^^^^

This is a general purpose function (:func:`spot_analysis.find_center`) that is used to find the center of images, objects (bounding box of object), and the center of boxes 
drawn on analyzed images. It uses the simple math equation:

.. math::

    center = ( \frac{x_1 + x_2}{2}, \frac{y_1 + y_2}{2} )

to determine the (x, y) coordinates of the center (x_1: image width, y_1: image height, x_2 and y_2 are 0).

Find distance from image center
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This uses the find_distance_from_img_center funntion to determine how far away an objects center is from the image's center. These distances are 
compared for all objects in an image to determine which one is closest to the center. The one closest to the center is is used as the vehicle that 
is parked in the spot. It takes in the image's center coordinates, object's center coordinates and the image name (for log purposes). It uses the 
following formula to determine the distance which is then returned:

.. math::

    distance = \sqrt{ ( image center_x - object center_x )^2 + ( image center_y - object center_y )^2 }

Imageai Detect
^^^^^^^^^^^^^^

This uses the :func:`spot_analysis.imageai_detect` function which take in the path to the original image, path to the extracted images, the path to the Imageai subdir in 
the analyzed images subdir. The steps are as follows:  

An Imageai detector object (uses a RetinaNet model) that only detects bicycles, buses, cars, motorcycles, and trucks is created. The detector is used on the original image. 

.. figure:: _images/features_analysis/imageai_detect/original.jpg
    :alt: Original image

    Original image

By default, Imageai's detector returns an image with all the detected objects surrounded by bounding boxes and labeled with the detected object types 
and the detection probability as shown below:

.. figure:: _images/features_analysis/imageai_detect/returned_image.jpg
    :alt: Returned image

    Returned image

The data for the returned image is stored in a variable called returned_image. The data about the detections (detection classes, detection probabilities, 
and bounding box coordinates) are stored in a variable called detections.  

If no objects were detected, the function stops here and just returns None.

Next, if objects were detected, the image dimensions (height and width) are determined. These dimensions are passed to find_center to determine the central 
point of the image:

.. figure:: _images/features_analysis/imageai_detect/image_center.jpg
    :alt: Image Center (blue dot)

    Image Center (blue dot)

The center of each object is determined by passing the bounding box coordinates (x\ :sub:`min`\, y\ :sub:`min`\, x\ :sub:`max`\, and y\ :sub:`max`\) of each 
object to find_center. The distance of each object from the center is determined using find_distance_from_img_center:

.. figure:: _images/features_analysis/imageai_detect/distance_from_center0.jpg
    :alt: Object 1 distance from center

    Object 1 distance from center


.. figure:: _images/features_analysis/imageai_detect/distance_from_center1.jpg
    :alt: Object 2 distance from center

    Object 2 distance from center


.. figure:: _images/features_analysis/imageai_detect/distance_from_center_all.jpg
    :alt: All objects' distances from center

    All objects' distances from center


The object that's closest to the center is used as the vehicle that's parked in the spot. Its data (detection classes, detection probabilities, and bounding box coordinates) 
is stored in a variable called closest_obj and the coordinates are used to create an extracted image:

.. figure:: _images/features_analysis/imageai_detect/extracted_closest_object.jpg
    :alt: Extracted closest object

    Extracted closest object

The image is created using make_extracted_image and its path is stored in extracted_imageai_image_path. The returned image is stored in the Imageai subdir inside the analyzed subdir.  

Finally, a dictionary containing the Imageai detection class, Imageai detection probability, Imageai coordinates, extracted Imageai image path and analyzed Imageai image path are returned.


Plate Recognizer Detect
^^^^^^^^^^^^^^^^^^^^^^^

This is done using the :func:`spot_analysis.plate_recognizer_detect` function. The function starts by passing the image that was passed to it to the plate recognizer api (https://api.platerecognizer.com/v1/plate-reader/) 
in a post request. It stores the results in the response it gets back in a variable called results.

If no objects were detected, then the function stops here and just returns None.

Next, if objects were detected, the function continues just like `Imageai Detect`_ to determine the object that's closest to the center. The only differences are that:

1) plate recognizer classifies objects as: Ambulance, Bus, Car, Limousine, Motorcycle, Taxi, Truck, Van, Unknown
2) plate recognizer gives us the plate number, plate coordinates and plate probability, along with some other data that wasn't used


This function, like :func:`spot_analysis.imageai_detect`, produces an extracted image and an analyzed image. 
It also produces an extracted image of the plate. The analyzed image is made using the :func:`spot_analysis.make_analyzed_pr_image` function. 

.. figure:: _images/features_analysis/plate_recognizer_detect/analyzed_pr.jpg
    :alt: Analyzed Plate Recognizer image

    Analyzed Plate Recognizer image

Finally, a dictionary containing the pr(plate recognizer) detection class, pr detection probability, pr vehicle coordinates, extracted pr image path, plate number, plate probability, plate coordinates 
extracted plate image path and analyzed pr image path is returned.


Imageai Predict
^^^^^^^^^^^^^^^

This is done using the :func:`spot_analysis.imageai_predict` function which is used for both the Imageai and plate recognizer analyses. It takes in the path to an extracted image.

First an Imageai predictor object (uses a DenseNet model) is created. The predictor is used on the original image. A dictionary containing the prediction class and prediction probability is returned.

.. note:: For a list of all the prediction classes go to:

    1) json_for_psa_models/prediction_classes.json  

    or  
    
    2) the prediction classes section of the admin area (http://localhost:8000/admin/psa/predictionclass/)
    

Colour Analysis
~~~~~~~~~~~~~~~

This done using the :func:`spot_analysis.detect_colours` function. The function takes in the path to an extracted image and returns a dictionary of dictionaries, each dictionary contains the name, red value, green value, 
blue value, hex value and percentage of the image the colour takes up for one detected colour.

The image is loaded and converted from BGR format to RGB. Then it is reshaped to be a list of pixels. The pixel intensities are clustered into 10 clusters, so we can get (up to) the 10 most dominant 
colours (uses Kmeans). The cluster info is passed to the :func:`spot_analysis.centroid_histogram` function which gets the percentages of all the clusters (normalized so it adds up to 100%). The histogram and cluster data 
are passed to the :func:`spot_analysis.get_all_colour_info` function which first uses the :func:`spot_analysis.get_colour_name` function determine the name of each colour (RGB values are compared to the ones in the helpers/colour_info.csv file; 
the name of the closest colour is assigned to the colour being tested). Then get_all_colour_info removes duplicate colour names and finds the overall percentages of the colours. Finally, it build the 
dictionary of dictionaries needed by :func:`spot_analysis.detect_colours` and passes it back to :func:`spot_analysis.detect_colours`, which then passes it back to the calling (:func:`spot_analysis.analyze_day`) function.


File Functions
--------------

Get new subdirectories
~~~~~~~~~~~~~~~~~~~~~~

This uses the :func:`spot_analysis.get_new_sub_dirs` function, which checks the data/new directory and produces a sorted list of the subdirectories (each named after a date e.g. '2020-03-30').


Make log file
~~~~~~~~~~~~~

This uses the :func:`spot_analysis.make_log_file` function which takes in the date and makes a corresponding subdirectory in the data/analyzed directory with a log file in it. The path to the log file 
(data/analyzed/{date}/{date}.log) is returned.


Make analyzed subdirectories
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uses the :func:`spot_analysis.make_analyzed_sub_dir` function which takes in the date and creates image subdirectories in the data/analyzed/{date} directory. The subdirectories are: 

| images
| images/original
| images/analyzed/comparison
| images/analyzed/imageai
| images/analyzed/pr
| images/extracted
| images/extracted/imageai
| images/extracted/pr
| images/extracted/plates  

It returns a dictionary containing the path to the date's subdirectory in data/analyzed, and all the image subdirectories within it.


Get data from the JSON file
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uses the :func:`spot_analysis.get_data` function which takes in the path to the input JSON file located in data/new/{date} (filepath example: data/new/2020-03-30/2020-03-30.json). It opens the file, 
reads the data, stores it in a variable called data, which it returns.

The input data has the arrival and departure times along with the image names for the day's entries. The data is in the following format:

.. code-block:: json

    [
        {
            "arrival_time": "2020-03-18-13:14:38",
            "departure_time": "2020-03-18-13:16:32",
            "image": "2020-03-18-13:14:38.jpg"
        },
        {
            "arrival_time": "2020-03-18-13:18:41",
            "departure_time": "2020-03-18-13:24:52",
            "image": "2020-03-18-13:18:41.jpg"
        },
        {
            "arrival_time": "2020-03-18-13:26:11",
            "departure_time": "2020-03-18-13:37:01",
            "image": "2020-03-18-13:26:11.jpg"
        }
    ]


Create new JSON file
~~~~~~~~~~~~~~~~~~~~

As the day's entries are processed, the data is stored in variables. The variables are combined into a dictionary like:

.. code-block:: python

    new_data = {
        "analysis":{
            "arrival_time":arrival_string,
            "departure_time":departure_string,
            "duration": {
                'in_seconds': duration_in_seconds,
                'human_readable': human_readable_duration
            },
            "images":{
                'original' : make_relative_path(original_image, analyzed_sub_dir),
                'extracted_imageai' : make_relative_path(extracted_imageai_image, analyzed_sub_dir),
                'extracted_pr' : make_relative_path(extracted_pr_image, analyzed_sub_dir),
                'extracted_plate' : make_relative_path(extracted_plate_image, analyzed_sub_dir),
                'analyzed_comparison' : make_relative_path(analyzed_comparison_image, analyzed_sub_dir),
                'analyzed_imageai' : make_relative_path(analyzed_imageai_image, analyzed_sub_dir),
                'analyzed_pr' : make_relative_path(analyzed_pr_image, analyzed_sub_dir)
            },
            "successful_feed_in" : None if extracted_imageai_image == None or feed_in == False 
                                   else True if extracted_pr_image and 'imageai' in extracted_pr_image 
                                   else False if extracted_pr_image == None or 'imageai' not in extracted_pr_image 
                                   else None,
            "imageai_analysis": {
                "detection":{
                    'class':imageai_detection_class,
                    'probability':imageai_detection_probability
                },
                "prediction":{
                    'class':imageai_prediction_class,
                    'probability':imageai_prediction_probability
                },
                "colours":imageai_colours
            },
            "pr_analysis": {
                "detection":{
                    'class':pr_detection_class,
                    'probability':pr_detection_probability
                },
                "prediction":{
                    'class':pr_prediction_class,
                    'probability':pr_prediction_probability
                },
                "colours":pr_colours,
                "license_plate":{
                    'plate_number': plate_number,
                    'plate_probability' : plate_probability
                }
            }
        },
        "timing":{
            "get_duration_time" : round(entry_get_duration_time, 3),
            "imageai_detection_time" : round(entry_imageai_detection_time, 3),
            "imageai_prediction_time" : round(entry_imageai_prediction_time, 3),
            "imageai_colour_detection_time" : round(entry_imageai_colour_detection_time, 3),
            "total_imageai_analysis_time" : round(entry_total_imageai_analysis_time, 3),
            "pr_detection_time" : round(entry_pr_detection_time, 3),
            "pr_prediction_time" : round(entry_pr_prediction_time, 3),
            "pr_colour_detection_time" : round(entry_pr_colour_detection_time, 3),
            "total_pr_analysis_time" : round(entry_total_pr_analysis_time, 3),
            "total_image_analysis_time" : round(entry_total_image_analysis_time, 3),
            "entry_total_time" : round(entry_total_time, 3)
        }
    }

The :func:`spot_analysis.make_relative_path` function takes in an absolute path to a file and the path to the parent directory and produces a relative path by removing the parent dir path from the file's 
path. This is done so that the system still works even if the parent dir is moved.

The data for the entries is combined in a list called analyzed which is stored in a directory with some other data to produce the data variable that has all the data for the day:

.. code-block:: python
    
    data = {
        "tried_feed_in" : feed_in,
        "analysis" : analyzed,
        "total_times" : {
            "total_get_duration_time":round(total_get_duration_time, 3),
            "total_imageai_detection_time":round(total_imageai_detection_time, 3),
            "total_imageai_prediction_time":round(total_imageai_prediction_time, 3),
            "total_imageai_colour_detection_time":round(total_imageai_colour_detection_time, 3),
            "total_imageai_analysis_time":round(total_imageai_analysis_time, 3),
            "total_pr_detection_time":round(total_pr_detection_time, 3),
            "total_pr_prediction_time":round(total_pr_prediction_time, 3),
            "total_pr_colour_detection_time":round(total_pr_colour_detection_time, 3),
            "total_pr_analysis_time":round(total_pr_analysis_time, 3),
            "total_image_analysis_time":round(total_image_analysis_time, 3),
            "total_entry_time":round(total_entry_time, 3),
            "total_day_time" : round(total_day_time, 3)
        },
        "log_file" : make_relative_path(log_file, analyzed_sub_dir)
	}

The :func:`spot_analysis.make_analyzed_json` function is called with the path to the analyzed subdir, the date and the analyzed data (shown above). It creates a string with the desired path to 
the output JSON file (data/analyzed/{date}/{date}.json). It passes the analyzed data and the JSON file name to the :func:`spot_analysis.write_json` function. The :func:`spot_analysis.write_json` function creates the JSON file 
and writes the analyzed data in the file.


Remove old input subdir
~~~~~~~~~~~~~~~~~~~~~~~

After all entries for the day are processed, and all data hs been produces the :func:`spot_analysis.remove_old_sub_dir` function is called with the path to the day's subdir in the data/new directory. 
This function deletes the day's subdir in the data/new directory along with all its content (the JSON file and images subdir with all the original input images).



Image Functions
---------------

Move original image to analyzed subdirectory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This uses the :func:`spot_analysis.move_orig_image` function which takes in image name, original absolute path, the path to the day's original images sub directory in the data/analyzed directory (data/analyzed/
{date}/images/original) (where the image will be moved to), and a True/False value for whether or not the image need to be rotated. 

The hardware component names images with a timestamp (h:m:s) (2020-03-18-13:14:38.jpg), but the ':' is not a valid character in filenames on Windows OS. If a file with ':' is moved to a Windows 
OS the ':' is automatically replaced with '_'. So to ensure the filenames and functions are valid on all OSes, the first thing the function does is check to see if the filename with ':' exists, if 
it doesn;t then it checks to see if a filename with '_' exists. Next it makes a string with the new desired path for the image and replaces any ':' with '_'. Then it moves the image to its new location. 
Finally, if the rotate parameter was True, then it passes the new path to the :func:`spot_analysis.rotate_image` function.

The :func:`spot_analysis.rotate_image` function rotates the image 90 |deg| (needed for images produced by the hardware).

.. |deg|    unicode:: U+000B0 .. DEGREE SIGN
   :ltrim:

The :func:`spot_analysis.move_orig_image` function returns the new path to the original image.


Make extracted image
~~~~~~~~~~~~~~~~~~~~

Uses the :func:`spot_analysis.make_extracted_image` function which takes in the path to the original image, the path to the extracted images subdir (data/analyzed/{date}/images/extracted), along with the coordinates of the 
detected object (x_min,y_min,x_max, and y_max). It returns the path to the extracted image taht's produced.

First it opens the image and creates a copy of it (so the original image isn't altered in any way), Then it detemines the extraction type (Imageai, pr or plate) and uses this to produce strings with the 
correct extracted image name and extracted image path. The extracted image is created by cropping the image using the object's coordinates. Finallay the image is saved. 


Make analyzed plate recognizer image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uses the :func:`spot_analysis.make_analyzed_pr_image` function which takes in the path to the original image, the path to the analyzed pr images subdir (data/analyzed/{date}/images/extracted/pr), and the image info produced by 
plate_recognizer_detect. It returns the path to an analyzed image that has bounding boxes drawn around all detected objects, along with their detected classes and detection probability; bounding boxes around detected plates, 
plate numbers and probabilities. 

First it opens the image and creates a copy of it (so the original image isn't altered in any way), Then it detemines the extraction type (Imageai, pr or plate). Then it loops through the image info and generates 
a random colour (using the random number generator to get random red, green and blue values) that's used for the bounding box and text and the inverse colour is used for the background of the rectangles placed 
behind the text (new combos for each object). Then it adds the bounding boxes and all text for the objects to the copy of the image. The image is saves in data/analyzed/{date}/images/analyzed/pr/{arrival_time}.jpg


Make analyzed comparison image
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Uses :func:`spot_analysis.make_analyzed_comparison_image` which takes in the path to the original image, the path to the analyzed comparison subdir (data/analyzed/{date}/images/analyzed/comparison), the vehicle coordinated detected by imageai_detect, 
the vehicle coordinated detected by plate_recognizer_detect, and the plate coordinated detected by plate_recognizer_detect. It returns an image that shows the bounding boxes representing the coordinates.

If all the coordinates are empty, then the function stops and just returns None.

If all the coordinates aren't empty, then ca poy of the image is made.

If Imageai coordinates are passed in, then the coordinates are used to draw a blue bounding box on the copy of the image, which is labeled "imageai".

If plate recognizer vehicle coordinates are passed in, then the coordinates are used to draw a red bounding box on the copy of the image, which is labeled "pr".

If plate recognizer plate coordinates are passed in, then the coordinates are used to draw a green bounding box on the copy of the image.

The new image is saved to data/analyzed/{date}/images/analyzed/comparison/{arrival_time}.jpg



Full Analysis
-------------

Analyze one day
~~~~~~~~~~~~~~~

Uses the :func:`spot_analysis.analyze_day` function which takes in the date (as a string), the True/False rotate parameter and the True/False feed_in parameter and returns the date after the day is fully processed and all files are produces.

It starts by passing the date to make_log_file to create the day's subdirectory in the analyzed directory (data/analyzed/{date}), along with the log file. It then passes the date to make_analyzed_sub_dir to make the image 
subdirectories inside the day's subdirectory. It gets the unanalyzed data from the input JSON file (data/new/{date}/{date}.json) using get_data. The unanalyzed data is actually a list of dictionaries, each containing arrival and 
departure times and image names for an entry. 

The next step is to loop through the entry data one by one and analyze each entry. 

The first step is duration analysis. The arrival and departure times are passed to get_duration to determine the duration in seconds and a human readable format.

Then it analyzes the image:

1) The image is moved, using move_orig_image, to the day's analyzed subdir (date/analyzed/{date}/images/original) and rotated if the rotate parameter was True.

2) Imageai analysis is performed:

    a) Imageai detection analysis is performed by imageai_detect. 

    b) If no vehicles were detected, then all the Imageai data for that entry is set to null values (extracted_imageai_image, analyzed_imageai_image, imageai_coordinates, 
       imageai_detection_class, imageai_detection_probability, imageai_prediction_class, imageai_prediction_probability and imageai_colours are set to None; while entry_imageai_prediction_time and entry_imageai_colour_detection_time 
       are set to 0)  

    c) If vehicles are detected, then:
        
        i) the detection data is stored in variables (extracted_imageai_image, analyzed_imageai_image, imageai_coordinates, imageai_detection_class, imageai_detection_probability) 

        ii) prediction is performed by passing the path to the extracted image to the :func:`spot_analysis.imageai_predict` function. The returned prediction class, prediction probability and time the prediction took are stored in variables.

        iii) colour detection is performed by passing the path to the extracted image to the :func:`spot_analysis.detect_colours` function. The results are stored in a variable.
    
3) Plate recognizer analysis is performed (very similar to Imageai analysis):

    a) First it determines if the plate recognizer analysis will use the extracted image produced by the Imageai analysis as its "original" image (/ determines if the Imageai extracted image should be "fed in" to the plate recognizer analysis). 
        
        i) If the feed_in parameter was True and an extracted image was produced by the Imageai analysis, then a variable called x_compensate is set up using the x :sub:`min` value of the Imageai coordinate, and a second variable called y_compensate 
           is set up using the y :sub:`min` value. These two (2) variables along with the path to the Imageai extracted image (data/analyzed/{date}/images/extracted/imageai/{arrival_time}.jpg), the path to the extracted pr images subdir 
           (data/analyzed/{date}/images/extracted/pr), the path to the extracted plate images (data/analyzed/{date}/images/extracted/plates), the path to the analyzed pr images (data/analyzed/{date}/images/analyzed/pr) and the original imageare passed 
           to the :func:`spot_analysis.plate_recognizer_detect` function. The detection analysis is performed on the extracted Imageai image. The x_compensate and y_compensate values are used when drawing the bounding boxes for the vehicle and plate on the original image.

        ii) If the feed_in parameter is False, then the paths to the original image, extracted pr images, extracted plate images and analyzed pr images are passed to the :func:`spot_analysis.plate_recognizer_detect` function.
    
    b) If no vehicles were detected, then all the pr data for that entry is set to null values (extracted_pr_image, extracted_plate_image, analyzed_pr_image, pr_vehicle_coordinates, plate_coordinates, pr_detection_class, pr_detection_probability, plate_number, 
       plate_probability, pr_prediction_class, pr_prediction_probability and pr_colours are set to None; while entry_pr_prediction_time and entry_pr_colour_detection_time are set to 0) 
    
    c) If vehicles are detected, then:
        
        i) the detection data is stored in variables (extracted_pr_image, extracted_plate_image, analyzed_pr_image, pr_vehicle_coordinates, plate_coordinates, pr_detection_class, pr_detection_probability, plate_number and plate_probability) 

        ii) prediction is performed by passing the path to the extracted image to the :func:`spot_analysis.imageai_predict` function. The returned prediction class, prediction probability and time the prediction took are stored in variables.

        iii) colour detection is performed by passing the path to the extracted image to the :func:`spot_analysis.detect_colours` function. The results are stored in a variable.
    
4) The analyzed comparison image is made by passing the path to the original image, path to the analyzed comparison images subdir (data/analyzed/{date}/images/analyzed/comparison), Imageai coordinates, pr vehicle coordinates and plate coordinates to the 
   :func:`spot_analysis.make_analyzed_comparison_image` function

All the data is stored in a dictionary called new_data (structure shown in `Create new JSON file`_'s first code block).

Once all the entries for the day are processed the data is stored in a list called analyzed which is stored in a dictionary called data (structure shown in `Create new JSON file`_'s second code block). The data dictionary is passed to the :func:`spot_analysis.make_analyzed_json` function so the output JSON file can be produced 
(data/analyzed/{date}/{date}.json). Then the :func:`spot_analysis.remove_old_sub_dir` function is called to remove the day's subdir in the data/new directory.



Main Function
~~~~~~~~~~~~~

The main function is called :func:`spot_analysis.main` and takes in the True/False rotate and feed_in parameters. It can be used to analyzed all the dates with subdirs in the data/new directory one after the other. It returns a list of the dates that were analyzed.

First it uses the :func:`spot_analysis.get_new_sub_dirs` function to check the data/new directory for new subdirectories. Then for each subdirectory, it determines the date and passes the date to the :func:`spot_analysis.analyze_day` function along with the rotate and feed_in parameters. 
The :func:`spot_analysis.analyze_day` function returns the date once it is done being analyzed, this date is appended to the dates list.  

This function is called if you run the script from the command line or if you use any of the analyze all functions in the web interface (presentation section).



