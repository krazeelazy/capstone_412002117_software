ERDs
====

.. figure:: _images/erds/erd_full.png
    :alt: Full ERD

    Full ERD

    .. raw:: html

        <a href="_images/erd_full.png" target="_blank">open in new tab</a>


.. figure:: _images/erds/erd.png
    :alt: ERD

    ERD

    .. raw:: html

        <a href="_images/erd.png" target="_blank">open in new tab</a>


.. figure:: _images/erds/analysis.png
    :alt: Analysis ERD

    Analysis ERD

    .. raw:: html

        <a href="_images/analysis.png" target="_blank">open in new tab</a>
        

.. figure:: _images/erds/automated.png
    :alt: Automated ERD

    Automated ERD

    .. raw:: html

        <a href="_images/automated.png" target="_blank">open in new tab</a>
        

.. figure:: _images/erds/manual.png
    :alt: Manual ERD

    Manual ERD

    .. raw:: html

        <a href="_images/manual.png" target="_blank">open in new tab</a>
        

.. figure:: _images/erds/psa.png
    :alt: PSA ERD

    PSA ERD

    .. raw:: html

        <a href="_images/psa.png" target="_blank">open in new tab</a>
        