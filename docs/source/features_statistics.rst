Statistics Features
===================

This page shows all the statistics. Only entries with manual analyses are used in the statistics. The days can be filtered into a range or specific days 
can be selected. There are multiple sections showing tables and interactive graphs.

**Interactive graphs:**

.. figure:: _images/features_statistics/1_all/daily/graphs/22a_pr_vehicle_auto_predictions_by_day.png
    :alt: Original Graph

    Original Graph

.. figure:: _images/features_statistics/1_all/daily/graphs/22b_pr_vehicle_auto_predictions_by_day_interactive_hover.png
    :alt: Graph overlay on hover

    Graph overlay on hover

.. figure:: _images/features_statistics/1_all/daily/graphs/22c_pr_vehicle_auto_predictions_by_day_interactive_labels.png
    :alt: Graph data manipulation using labels

    Graph data manipulation using labels


This page also changes as the data is processed.

.. figure:: _images/features_statistics/0_progress/1_before.png
    :alt: Before any manual analyses

    Before any manual analyses

.. figure:: _images/features_statistics/0_progress/2_after_first_entry_for_first_day.png
    :alt: After complete first entry for first day 

    After complete first entry for first day 

.. figure:: _images/features_statistics/0_progress/3_after_first_day_complete.png
    :alt: After complete first day

    After complete first day

.. figure:: _images/features_statistics/0_progress/4_after_second_day_complete.png
    :alt: After complete second day

    After complete second day

.. figure:: _images/features_statistics/0_progress/5_after_all_complete.png
    :alt: After complete all days

    After complete all days


The filters that can be applied are:

1) An inclusive date range
2) Specific dates selected from a list of all dates that have atleast one entry with  a manual analysis (multiple can be selected at once)

.. figure:: _images/features_statistics/filters.png
    :alt: Filters

    Filters

.. figure:: _images/features_statistics/filters_specific_expanded.png
    :alt: Filters (expanded multiselect)

    Filters (expanded multiselect)

The top window looks different based on the filters being applied:

.. figure:: _images/features_statistics/2_orig/1_top_of_window.png
    :alt: Top of window (not fed in)

    Top of window (not fed in)

.. figure:: _images/features_statistics/3_fed_in/1_top_of_window.png
    :alt: Top of window (fed in)

    Top of window (fed in)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/1_top_of_window.png
    :alt: Top of window (all days) (without unrotated)

    Top of window (all days) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/1_top_of_window.png
    :alt: Top of window (not fed in) (without unrotated)

    Top of window (not fed in) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/1_top_of_window.png
    :alt: Top of window (fed in) (without unrotated)

    Top of window (fed in) (without unrotated)



The graph labels and contents also change based on the applied filters (seen in `Daily PR Vehicle Predictions`_).



Route
~~~~~

/analysis_statistics/  shows all the statistics.

Page structure
~~~~~~~~~~~~~~

The page is broken down into tabs. There are two (2) main tabs:

1) `Daily`_
2) `Overall`_

.. note:: The colour scheme for the tables that show correct counts and accuracies in the tabs is:
           
           1) white = no data
           2) green = total success
           3) yellow = mixed (some successes, some failures)
           4) red = total failure


Daily
~~~~~

This shows granular statistics for each day. It has eight (8) sub tabs:

1) `Basic <#daily-basic>`_
2) `IAI <#daily-iai>`_
3) `PR <#daily-pr>`_
4) `Plate <#daily-plate>`_
5) `Image Extractions <#daily-image-extractions>`_
6) `Correct Counts <#daily-correct-counts>`_
7) `Accuracies <#daily-accuracies>`_
8) `Timings <#daily-timings>`_

.. figure:: _images/features_statistics/tabs/1_daily_basic.png
    :alt: Daily sub tabs

    Daily sub tabs


.. _Daily Basic:

Basic
^^^^^

This shows the basic information for the days. It is broken into two sub tabs:

1) `# of Entries <#daily-basic-number-of-entries>`_
2) `Durations <#daily-basic-durations>`_

.. figure:: _images/features_statistics/1_all/daily/full/0_basic.png
    :alt: Basic tab (all days) (page)

    Basic tab (all days) (page)

.. figure:: _images/features_statistics/tabs/1_daily_basic.png
    :alt: Daily Basic sub tabs

    Daily Basic sub tabs

.. _Daily Basic Number of Entries:

# of Entries
""""""""""""

This shows the number of entries that have manual analyses for each day.

.. figure:: _images/features_statistics/1_all/daily/full/1_basic_number_of_entries.png
    :alt: # of Entries (all days) (tab)

    # of Entries (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/1_number_of_entries.png
    :alt: # of Entries (all days) (card)

    # of Entries (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/1_number_of_entries.png
    :alt: # of Entries (all days) (graph)

    # of Entries (all days) (graph)
    
.. figure:: _images/features_statistics/1_all/daily/tables/1_number_of_entries.png
    :alt: # of Entries (all days) (table)

    # of Entries (all days) (table)

.. _Daily Basic Durations:

Durations
"""""""""

This shows the durations data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/2_basic_durations.png
    :alt: Durations (all days) (tab)

    Durations (all days) (tab)

Its cards are:

1) `Auto Durations by day <#daily-basic-durations-auto-durations-by-day>`_
2) `Manual Durations by day <#daily-basic-durations-manual-durations-by-day>`_
3) `Correct Durations by day <#daily-basic-durations-correct-durations-by-day>`_
4) `Duration Accuracy by day <#daily-basic-durations-duration-accuracy-by-day>`_

.. _Daily Basic Durations Auto Durations by day:

Auto Durations by day
*********************

This shows the durations automatically calculated using the arrival and departure times, along with the min, max, total, and 
average durations for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/2_auto_durations_by_day.png
    :alt: Auto Durations by day (all days) (card)

    Auto Durations by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/2_auto_durations_by_day.png
    :alt: Auto Durations by day (all days) (card)

    Auto Durations by day (all days) (card)


.. _Daily Basic Durations Manual Durations by day:

Manual Durations by day
***********************

This shows the durations manually calculated using the arrival and departure times, along with the min, max, total, and 
average durations for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/3_manual_durations_by_day.png
    :alt: Manual Durations by day (all days) (card)

    Manual Durations by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/3_manual_durations_by_day.png
    :alt: Manual Durations by day (all days) (card)

    Manual Durations by day (all days) (card)


.. _Daily Basic Durations Correct Durations by day:

Correct Durations by day
************************

This shows the number of correctly automatically calculated durations for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/4_correct_durations_by_day.png
    :alt: Correct Durations by day (all days) (card)

    Correct Durations by day (all days) (card)


.. figure:: _images/features_statistics/1_all/daily/graphs/2_correct_durations_by_day.png
    :alt: Correct Durations by day (all days) (graph)

    Correct Durations by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/4_correct_durations_by_day.png
    :alt: Correct Durations by day (all days) (table)

    Correct Durations by day (all days) (table)



.. _Daily Basic Durations Duration Accuracy by day:

Duration Accuracy by day
************************

This shows the accuracy of the automatically calculated durations for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/5_duration_accuracy_by_day.png
    :alt: Duration Accuracy by day (all days) (card)

    Duration Accuracy by day (all days) (card)
    

.. figure:: _images/features_statistics/1_all/daily/graphs/3_duration_accuracy_by_day.png
    :alt: Duration Accuracy by day (all days) (graph)

    Duration Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/5_duration_accuracy_by_day.png
    :alt: Duration Accuracy by day (all days) (table)

    Duration Accuracy by day (all days) (table)




.. _Daily IAI:

IAI
^^^

This shows the Imageai information for the days. It is broken into 3 sub tabs:

1) `Vehicle Detections <#daily-iai-vehicle-detections>`_
2) `Vehicle Predictions <#daily-iai-vehicle-predictions>`_
3) `Dominant Colour Detections <#daily-iai-dominant-colour-detections>`_

.. figure:: _images/features_statistics/tabs/2_daily_iai.png
    :alt: Daily IAI sub tabs

    Daily IAI sub tabs

.. _Daily IAI Vehicle Detections:

Vehicle Detections
""""""""""""""""""

This shows the Imageai Vehicle Detection data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/3_iai_vehicle_detections.png
    :alt: IAI Vehicle Detections (all days) (tab)

    IAI Vehicle Detections (all days) (tab)

Its cards are:

1) `IAI Vehicle Auto Detections by day <#daily-iai-iai-vehicle-auto-detections-by-day>`_
2) `IAI Vehicle Manual Detections by day <#daily-iai-iai-vehicle-manual-detections-by-day>`_
3) `IAI Correct Vehicle Detections by day <#daily-iai-iai-correct-vehicle-detections-by-day>`_
4) `IAI Vehicle Detection Accuracy by day <#daily-iai-iai-vehicle-detection-accuracy-by-day>`_


.. _Daily IAI IAI Vehicle Auto Detections by day:

IAI Vehicle Auto Detections by day
***********************************

This shows the counts for the automatically detected Imageai vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/6_iai_vehicle_auto_detections_by_day.png
    :alt: IAI Vehicle Auto Detections by day (all days) (card)

    IAI Vehicle Auto Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/4_iai_vehicle_auto_detections_by_day.png
    :alt: IAI Vehicle Auto Detections by day (all days) (graph)

    IAI Vehicle Auto Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/6_iai_vehicle_auto_detections_by_day.png
    :alt: IAI Vehicle Auto Detections by day (all days) (table)

    IAI Vehicle Auto Detections by day (all days) (table)


.. _Daily IAI IAI Vehicle Manual Detections by day:

IAI Vehicle Manual Detections by day
************************************

This shows the counts for the manually identified Imageai vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/7_iai_vehicle_manual_detections_by_day.png
    :alt: IAI Vehicle Manual Detections by day (all days) (card)

    IAI Vehicle Manual Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/5_iai_vehicle_manual_detections_by_day.png
    :alt: IAI Vehicle Manual Detections by day (all days) (graph)

    IAI Vehicle Manual Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/7_iai_vehicle_manual_detections_by_day.png
    :alt: IAI Vehicle Manual Detections by day (all days) (table)

    IAI Vehicle Manual Detections by day (all days) (table)


.. _Daily IAI IAI Correct Vehicle Detections by day:

IAI Correct Vehicle Detections by day
*************************************

This shows the number of correctly automatically detected Imageai vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/8_iai_correct_vehicle_detections_by_day.png
    :alt: IAI Correct Vehicle Detections by day (all days) (card)

    IAI Correct Vehicle Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/6_iai_correct_vehicle_detections_by_day.png
    :alt: IAI Correct Vehicle Detections by day (all days) (graph)

    IAI Correct Vehicle Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/8_iai_correct_vehicle_detections_by_day.png
    :alt: IAI Correct Vehicle Detections by day (all days) (table)

    IAI Correct Vehicle Detections by day (all days) (table)


.. _Daily IAI IAI Vehicle Detection Accuracy by day:

IAI Vehicle Detection Accuracy by day
*************************************

This shows the accuracy of the automatically detected Imageai vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/9_iai_vehicle_detection_accuracy_by_day.png
    :alt: IAI Vehicle Detection Accuracy by day (all days) (card)

    IAI Vehicle Detection Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/7_iai_vehicle_detection_accuracy_by_day.png
    :alt: IAI Vehicle Detection Accuracy by day (all days) (graph)

    IAI Vehicle Detection Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/9_iai_vehicle_detection_accuracy_by_day.png
    :alt: IAI Vehicle Detection Accuracy by day (all days) (table)

    IAI Vehicle Detection Accuracy by day (all days) (table)



.. _Daily IAI Vehicle Predictions:

Vehicle Predictions
"""""""""""""""""""

This shows the Imageai Vehicle Prediction data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/4_iai_vehicle_predictions.png
    :alt: IAI Vehicle Predictions (all days) (tab)

    IAI Vehicle Predictions (all days) (tab)

Its cards are:

1) `IAI Vehicle Auto Predictions by day <#daily-iai-iai-vehicle-auto-predictions-by-day>`_
2) `IAI Vehicle Manual Predictions by day <#daily-iai-iai-vehicle-manual-predictions-by-day>`_
3) `IAI Correct Vehicle Predictions by day <#daily-iai-iai-correct-vehicle-predictions-by-day>`_
4) `IAI Vehicle Prediction Accuracy by day <#daily-iai-iai-vehicle-prediction-accuracy-by-day>`_


.. _Daily IAI IAI Vehicle Auto Predictions by day:

IAI Vehicle Auto Predictions by day
***********************************

This shows the counts for the classes that were automatically predicted by Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/10_iai_vehicle_auto_predictions_by_day.png
    :alt: IAI Vehicle Auto Predictions by day (all days) (card)

    IAI Vehicle Auto Predictions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/8_iai_vehicle_auto_predictions_by_day.png
    :alt: IAI Vehicle Auto Predictions by day (all days) (graph)

    IAI Vehicle Auto Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/10_iai_vehicle_auto_predictions_by_day.png
    :alt: IAI Vehicle Auto Predictions by day (all days) (table)

    IAI Vehicle Auto Predictions by day (all days) (table)


.. _Daily IAI IAI Vehicle Manual Predictions by day:

IAI Vehicle Manual Predictions by day
*************************************

This shows the counts for the prediction classes that were manually identified for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/11_iai_vehicle_manual_predictions_by_day.png
    :alt: IAI Vehicle Manual Predictions by day (all days) (card)

    IAI Vehicle Manual Predictions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/9_iai_vehicle_manual_predictions_by_day.png
    :alt: IAI Vehicle Manual Predictions by day (all days) (graph)

    IAI Vehicle Manual Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/11_iai_vehicle_manual_predictions_by_day.png
    :alt: IAI Vehicle Manual Predictions by day (all days) (table)

    IAI Vehicle Manual Predictions by day (all days) (table)


.. _Daily IAI IAI Correct Vehicle Predictions by day:

IAI Correct Vehicle Predictions by day
**************************************

This shows the number of correctly predicted vehicle classes for the automatic Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/12_iai_correct_vehicle_predictions_by_day.png
    :alt: IAI Correct Vehicle Predictions by day (all days) (card)

    IAI Correct Vehicle Predictions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/10_iai_correct_vehicle_predictions_by_day.png
    :alt: IAI Correct Vehicle Predictions by day (all days) (graph)

    IAI Correct Vehicle Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/12_iai_correct_vehicle_predictions_by_day.png
    :alt: IAI Correct Vehicle Predictions by day (all days) (table)

    IAI Correct Vehicle Predictions by day (all days) (table)


.. _Daily IAI IAI Vehicle Prediction Accuracy by day:

IAI Vehicle Prediction Accuracy by day
**************************************

This shows the accuracy of the predicted vehicle classes for the automatic Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/13_iai_vehicle_prediction_accuracy_by_day.png
    :alt: IAI Vehicle Prediction Accuracy by day (all days) (card)

    IAI Vehicle Prediction Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/11_iai_vehicle_prediction_accuracy_by_day.png
    :alt: IAI Vehicle Prediction Accuracy by day (all days) (graph)

    IAI Vehicle Prediction Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/13_iai_vehicle_prediction_accuracy_by_day.png
    :alt: IAI Vehicle Prediction Accuracy by day (all days) (table)

    IAI Vehicle Prediction Accuracy by day (all days) (table)



.. _Daily IAI Dominant Colour Detections:

Dominant Colour Detections
"""""""""""""""""""""""""""

This shows the Imageai Dominant Colour Detection data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/5_iai_dominant_colour_detections.png
    :alt: IAI Dominant Colour Detections (all days) (tab)

    IAI Dominant Colour Detections (all days) (tab)

Its cards are:

1) `IAI Dominant Colour Auto Detections by day`_
2) `IAI Dominant Colour Manual Detections by day`_
3) `IAI Correct Dominant Colour Detections by day`_
4) `IAI Dominant Colour Detection Accuracy by day`_
5) `How often IAI correctly identified dominant colour as non-dominant by day`_
6) `Percentage of time IAI correctly identified dominant colour as non-dominant by day`_



.. _IAI Dominant Colour Auto Detections by day:

IAI Dominant Colour Auto Detections by day
******************************************

This shows the counts for the automatically detected dominant colours found during Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/14_iai_dominant_colour_auto_detections_by_day.png
    :alt: IAI Dominant Colour Auto Detections by day (all days) (card)

    IAI Dominant Colour Auto Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/12_iai_dominant_colour_auto_detections_by_day.png
    :alt: IAI Dominant Colour Auto Detections by day (all days) (graph)

    IAI Dominant Colour Auto Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/14_iai_dominant_colour_auto_detections_by_day.png
    :alt: IAI Dominant Colour Auto Detections by day (all days) (table)

    IAI Dominant Colour Auto Detections by day (all days) (table)


.. _IAI Dominant Colour Manual Detections by day:

IAI Dominant Colour Manual Detections by day
********************************************

This shows the counts for the manually identified dominant colours for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/15_iai_dominant_colour_manual_detections_by_day.png
    :alt: IAI Dominant Colour Manual Detections by day (all days) (card)

    IAI Dominant Colour Manual Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/13_iai_dominant_colour_manual_detections_by_day.png
    :alt: IAI Dominant Colour Manual Detections by day (all days) (graph)

    IAI Dominant Colour Manual Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/15_iai_dominant_colour_manual_detections_by_day.png
    :alt: IAI Dominant Colour Manual Detections by day (all days) (table)

    IAI Dominant Colour Manual Detections by day (all days) (table)


.. _IAI Correct Dominant Colour Detections by day:

IAI Correct Dominant Colour Detections by day
*********************************************

This shows the number of correctly detected dominant colours for the automated Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/16_iai_correct_dominant_colour_detections_by_day.png
    :alt: IAI Correct Dominant Colour Detections by day (all days) (card)

    IAI Correct Dominant Colour Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/14_iai_correct_dominant_colour_detections_by_day.png
    :alt: IAI Correct Dominant Colour Detections by day (all days) (graph)

    IAI Correct Dominant Colour Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/16_iai_correct_dominant_colour_detections_by_day.png
    :alt: IAI Correct Dominant Colour Detections by day (all days) (table)

    IAI Correct Dominant Colour Detections by day (all days) (table)


.. _IAI Dominant Colour Detection Accuracy by day:

IAI Dominant Colour Detection Accuracy by day
*********************************************

This shows the accuracy of the detected dominant colours for the automated Imageai analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/17_iai_dominant_colour_detection_accuracy_by_day.png
    :alt: IAI Dominant Colour Detection Accuracy by day (all days) (card)

    IAI Dominant Colour Detection Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/15_iai_dominant_colour_detection_accuracy_by_day.png
    :alt: IAI Dominant Colour Detection Accuracy by day (all days) (graph)

    IAI Dominant Colour Detection Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/17_iai_dominant_colour_detection_accuracy_by_day.png
    :alt: IAI Dominant Colour Detection Accuracy by day (all days) (table)

    IAI Dominant Colour Detection Accuracy by day (all days) (table)


.. _How often IAI correctly identified dominant colour as non-dominant by day:

How often IAI correctly identified dominant colour as non-dominant by day
*************************************************************************

This shows how often the dominant colour is detected by the automated Imageai analysis, but is not identified as the dominant colour.

.. figure:: _images/features_statistics/1_all/daily/cards/18_how_often_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often IAI correctly identified dominant colour as non-dominant by day (all days) (card)

    How often IAI correctly identified dominant colour as non-dominant by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/16_how_often_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often IAI correctly identified dominant colour as non-dominant by day (all days) (graph)

    How often IAI correctly identified dominant colour as non-dominant by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/18_how_often_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often IAI correctly identified dominant colour as non-dominant by day (all days) (table)

    How often IAI correctly identified dominant colour as non-dominant by day (all days) (table)


.. _Percentage of time IAI correctly identified dominant colour as non-dominant by day:

Percentage of time IAI correctly identified dominant colour as non-dominant by day
**********************************************************************************

This shows the percentage of time that the dominant colour is detected by the automated Imageai analysis using the correct image, but is not identified as the dominant colour.

.. figure:: _images/features_statistics/1_all/daily/cards/19_percentage_of_time_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (card)

    Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/17_percentage_of_time_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (graph)

    Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/19_percentage_of_time_iai_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (table)

    Percentage of time IAI correctly identified dominant colour as non-dominant by day (all days) (table)




.. _Daily PR:

PR
^^

This shows the Plate Recognizer (PR) information for the days. It is broken into 3 sub tabs:

1) `Vehicle Detections <#daily-pr-vehicle-detections>`_
2) `Vehicle Predictions <#daily-pr-vehicle-predictions>`_
3) `Dominant Colour Detections <#daily-pr-dominant-colour-detections>`_

.. figure:: _images/features_statistics/tabs/3_daily_pr.png
    :alt: Daily PR sub tabs

    Daily PR sub tabs


.. _Daily PR Vehicle Detections:

Vehicle Detections
""""""""""""""""""

This shows the PR Vehicle Detection data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/6_pr_vehicle_detections.png
    :alt: PR Vehicle Detections (all days) (tab)

    PR Vehicle Detections (all days) (tab)

Its cards are:

1) `PR Vehicle Auto Detections by day <#daily-pr-pr-vehicle-auto-detections-by-day>`_
2) `PR Vehicle Manual Detections by day <#daily-pr-pr-vehicle-manual-detections-by-day>`_
3) `PR Correct Vehicle Detections by day <#daily-pr-pr-correct-vehicle-detections-by-day>`_
4) `PR Vehicle Detection Accuracy by day <#daily-pr-pr-vehicle-detection-accuracy-by-day>`_


.. _Daily PR PR Vehicle Auto Detections by day:

PR Vehicle Auto Detections by day
***********************************

This shows the counts for the automatically detected PR vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/20_pr_vehicle_auto_detections_by_day.png
    :alt: PR Vehicle Auto Detections by day (all days) (card)

    PR Vehicle Auto Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/18_pr_vehicle_auto_detections_by_day.png
    :alt: PR Vehicle Auto Detections by day (all days) (graph)

    PR Vehicle Auto Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/20_pr_vehicle_auto_detections_by_day.png
    :alt: PR Vehicle Auto Detections by day (all days) (table)

    PR Vehicle Auto Detections by day (all days) (table)


.. _Daily PR PR Vehicle Manual Detections by day:

PR Vehicle Manual Detections by day
************************************

This shows the counts for the manually identified PR vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/21_pr_vehicle_manual_detections_by_day.png
    :alt: PR Vehicle Manual Detections by day (all days) (card)

    PR Vehicle Manual Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/19_pr_vehicle_manual_detections_by_day.png
    :alt: PR Vehicle Manual Detections by day (all days) (graph)

    PR Vehicle Manual Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/21_pr_vehicle_manual_detections_by_day.png
    :alt: PR Vehicle Manual Detections by day (all days) (table)

    PR Vehicle Manual Detections by day (all days) (table)


.. _Daily PR PR Correct Vehicle Detections by day:

PR Correct Vehicle Detections by day
*************************************

This shows the number of correctly automatically detected PR vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/22_pr_correct_vehicle_detections_by_day.png
    :alt: PR Correct Vehicle Detections by day (all days) (card)

    PR Correct Vehicle Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/20_pr_correct_vehicle_detections_by_day.png
    :alt: PR Correct Vehicle Detections by day (all days) (graph)

    PR Correct Vehicle Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/22_pr_correct_vehicle_detections_by_day.png
    :alt: PR Correct Vehicle Detections by day (all days) (table)

    PR Correct Vehicle Detections by day (all days) (table)


.. _Daily PR PR Vehicle Detection Accuracy by day:

PR Vehicle Detection Accuracy by day
*************************************

This shows the accuracy of the automatically detected PR vehicle classes for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/23_pr_vehicle_detection_accuracy_by_day.png
    :alt: PR Vehicle Detection Accuracy by day (all days) (card)

    PR Vehicle Detection Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/21_pr_vehicle_detection_accuracy_by_day.png
    :alt: PR Vehicle Detection Accuracy by day (all days) (graph)

    PR Vehicle Detection Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/23_pr_vehicle_detection_accuracy_by_day.png
    :alt: PR Vehicle Detection Accuracy by day (all days) (table)

    PR Vehicle Detection Accuracy by day (all days) (table)



.. _Daily PR Vehicle Predictions:

Vehicle Predictions
"""""""""""""""""""

This shows the PR Vehicle Prediction data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/7_pr_vehicle_predictions.png
    :alt: PR Vehicle Predictions (all days) (tab)

    PR Vehicle Predictions (all days) (tab)

Its cards are:

1) `PR Vehicle Auto Predictions by day <#daily-pr-pr-vehicle-auto-predictions-by-day>`_
2) `PR Vehicle Manual Predictions by day <#daily-pr-pr-vehicle-manual-predictions-by-day>`_
3) `PR Correct Vehicle Predictions by day <#daily-pr-pr-correct-vehicle-predictions-by-day>`_
4) `PR Vehicle Prediction Accuracy by day <#daily-pr-pr-vehicle-prediction-accuracy-by-day>`_


.. _Daily PR PR Vehicle Auto Predictions by day:

PR Vehicle Auto Predictions by day
***********************************

This shows the counts for the classes that were automatically predicted by PR analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/24_pr_vehicle_auto_predictions_by_day.png
    :alt: PR Vehicle Auto Predictions by day (all days) (card)

    PR Vehicle Auto Predictions by day (all days) (card)

.. figure:: _images/features_statistics/2_orig/daily/1_pr_vehicle_auto_predictions_by_day_card.png
    :alt: PR Vehicle Auto Predictions by day (not fed in) (card)

    PR Vehicle Auto Predictions by day (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/daily/1_pr_vehicle_auto_predictions_by_day_card.png
    :alt: PR Vehicle Auto Predictions by day (fed in) (card)

    PR Vehicle Auto Predictions by day (fed in) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/22a_pr_vehicle_auto_predictions_by_day.png
    :alt: PR Vehicle Auto Predictions by day (all days) (graph)

    PR Vehicle Auto Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/2_orig/daily/2_pr_vehicle_auto_predictions_by_day_graph.png
    :alt: PR Vehicle Auto Predictions by day (not fed in) (graph)

    PR Vehicle Auto Predictions by day (not fed in) (graph)

.. figure:: _images/features_statistics/3_fed_in/daily/2_pr_vehicle_auto_predictions_by_day_graph.png
    :alt: PR Vehicle Auto Predictions by day (fed in) (graph)

    PR Vehicle Auto Predictions by day feed ins) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/24_pr_vehicle_auto_predictions_by_day.png
    :alt: PR Vehicle Auto Predictions by day (all days) (table)

    PR Vehicle Auto Predictions by day (all days) (table)

.. figure:: _images/features_statistics/2_orig/daily/3_pr_vehicle_auto_predictions_by_day_table.png
    :alt: PR Vehicle Auto Predictions by day (not fed in) (table)

    PR Vehicle Auto Predictions by day (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/daily/3_pr_vehicle_auto_predictions_by_day_table.png
    :alt: PR Vehicle Auto Predictions by day (fed in) (table)

    PR Vehicle Auto Predictions by day (fed in) (table)


.. _Daily PR PR Vehicle Manual Predictions by day:

PR Vehicle Manual Predictions by day
*************************************

This shows the counts for the prediction classes that were manually identified for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/25_pr_vehicle_manual_predictions_by_day.png
    :alt: PR Vehicle Manual Predictions by day (all days) (card)

    PR Vehicle Manual Predictions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/23_pr_vehicle_manual_predictions_by_day.png
    :alt: PR Vehicle Manual Predictions by day (all days) (graph)

    PR Vehicle Manual Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/25_pr_vehicle_manual_predictions_by_day.png
    :alt: PR Vehicle Manual Predictions by day (all days) (table)

    PR Vehicle Manual Predictions by day (all days) (table)


.. _Daily PR PR Correct Vehicle Predictions by day:

PR Correct Vehicle Predictions by day
**************************************

This shows the number of correctly predicted vehicle classes for the automatic PR analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/26_pr_correct_vehicle_predictions_by_day.png
    :alt: PR Correct Vehicle Predictions by day (all days) (card)

    PR Correct Vehicle Predictions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/24_pr_correct_vehicle_predictions_by_day.png
    :alt: PR Correct Vehicle Predictions by day (all days) (graph)

    PR Correct Vehicle Predictions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/26_pr_correct_vehicle_predictions_by_day.png
    :alt: PR Correct Vehicle Predictions by day (all days) (table)

    PR Correct Vehicle Predictions by day (all days) (table)


.. _Daily PR PR Vehicle Prediction Accuracy by day:

PR Vehicle Prediction Accuracy by day
**************************************

This shows the accuracy of the predicted vehicle classes for the automatic PR analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/27_pr_vehicle_prediction_accuracy_by_day.png
    :alt: PR Vehicle Prediction Accuracy by day (all days) (card)

    PR Vehicle Prediction Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/25_pr_vehicle_prediction_accuracy_by_day.png
    :alt: PR Vehicle Prediction Accuracy by day (all days) (graph)

    PR Vehicle Prediction Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/27_pr_vehicle_prediction_accuracy_by_day.png
    :alt: PR Vehicle Prediction Accuracy by day (all days) (table)

    PR Vehicle Prediction Accuracy by day (all days) (table)



.. _Daily PR Dominant Colour Detections:

Dominant Colour Detections
"""""""""""""""""""""""""""

This shows the PR Dominant Colour Detection data for each day.

.. figure:: _images/features_statistics/1_all/daily/full/8_pr_dominant_colour_detections.png
    :alt: PR Dominant Colour Detections (all days) (tab)

    PR Dominant Colour Detections (all days) (tab)

Its cards are:

1) `PR Dominant Colour Auto Detections by day`_
2) `PR Dominant Colour Manual Detections by day`_
3) `PR Correct Dominant Colour Detections by day`_
4) `PR Dominant Colour Detection Accuracy by day`_
5) `How often PR correctly identified dominant colour as non-dominant by day`_
6) `Percentage of time PR correctly identified dominant colour as non-dominant by day`_



.. _PR Dominant Colour Auto Detections by day:

PR Dominant Colour Auto Detections by day
******************************************

This shows the counts for the automatically detected dominant colours found during PR analysis for each day.


.. figure:: _images/features_statistics/1_all/daily/cards/28_pr_dominant_colour_auto_detections_by_day.png
    :alt: PR Dominant Colour Auto Detections by day (all days) (card)

    PR Dominant Colour Auto Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/26_pr_dominant_colour_auto_detections_by_day.png
    :alt: PR Dominant Colour Auto Detections by day (all days) (graph)

    PR Dominant Colour Auto Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/28_pr_dominant_colour_auto_detections_by_day.png
    :alt: PR Dominant Colour Auto Detections by day (all days) (table)

    PR Dominant Colour Auto Detections by day (all days) (table)


.. _PR Dominant Colour Manual Detections by day:

PR Dominant Colour Manual Detections by day
********************************************

This shows the counts for the manually identified dominant colours for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/29_pr_dominant_colour_manual_detections_by_day.png
    :alt: PR Dominant Colour Manual Detections by day (all days) (card)

    PR Dominant Colour Manual Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/27_pr_dominant_colour_manual_detections_by_day.png
    :alt: PR Dominant Colour Manual Detections by day (all days) (graph)

    PR Dominant Colour Manual Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/29_pr_dominant_colour_manual_detections_by_day.png
    :alt: PR Dominant Colour Manual Detections by day (all days) (table)

    PR Dominant Colour Manual Detections by day (all days) (table)


.. _PR Correct Dominant Colour Detections by day:

PR Correct Dominant Colour Detections by day
*********************************************

This shows the number of correctly detected dominant colours for the automated PR analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/30_pr_correct_dominant_colour_detections_by_day.png
    :alt: PR Correct Dominant Colour Detections by day (all days) (card)

    PR Correct Dominant Colour Detections by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/28_pr_correct_dominant_colour_detections_by_day.png
    :alt: PR Correct Dominant Colour Detections by day (all days) (graph)

    PR Correct Dominant Colour Detections by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/30_pr_correct_dominant_colour_detections_by_day.png
    :alt: PR Correct Dominant Colour Detections by day (all days) (table)

    PR Correct Dominant Colour Detections by day (all days) (table)


.. _PR Dominant Colour Detection Accuracy by day:

PR Dominant Colour Detection Accuracy by day
*********************************************

This shows the accuracy of the detected dominant colours for the automated PR analysis for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/31_pr_dominant_colour_detection_accuracy_by_day.png
    :alt: PR Dominant Colour Detection Accuracy by day (all days) (card)

    PR Dominant Colour Detection Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/29_pr_dominant_colour_detection_accuracy_by_day.png
    :alt: PR Dominant Colour Detection Accuracy by day (all days) (graph)

    PR Dominant Colour Detection Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/31_pr_dominant_colour_detection_accuracy_by_day.png
    :alt: PR Dominant Colour Detection Accuracy by day (all days) (table)

    PR Dominant Colour Detection Accuracy by day (all days) (table)


.. _How often PR correctly identified dominant colour as non-dominant by day:

How often PR correctly identified dominant colour as non-dominant by day
*************************************************************************

This shows how often the dominant colour is detected by the automated PR analysis, but is not identified as the dominant colour.

.. figure:: _images/features_statistics/1_all/daily/cards/32_how_often_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often PR correctly identified dominant colour as non-dominant by day (all days) (card)

    How often PR correctly identified dominant colour as non-dominant by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/30_how_often_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often PR correctly identified dominant colour as non-dominant by day (all days) (graph)

    How often PR correctly identified dominant colour as non-dominant by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/32_how_often_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: How often PR correctly identified dominant colour as non-dominant by day (all days) (table)

    How often PR correctly identified dominant colour as non-dominant by day (all days) (table)


.. _Percentage of time PR correctly identified dominant colour as non-dominant by day:

Percentage of time PR correctly identified dominant colour as non-dominant by day
**********************************************************************************

This shows the percentage of time that the dominant colour is detected by the automated PR analysis using the correct image, but is not identified as the dominant colour.

.. figure:: _images/features_statistics/1_all/daily/cards/33_percentage_of_time_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (card)

    Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/31_percentage_of_time_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (graph)

    Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/33_percentage_of_time_pr_correctly_ident_dom_as_non_dom_by_day.png
    :alt: Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (table)

    Percentage of time PR correctly identified dominant colour as non-dominant by day (all days) (table)




.. _Daily Plate:

Plate
^^^^^

This shows the plate information for the days. 

.. figure:: _images/features_statistics/1_all/daily/full/9_plate.png
    :alt: Plate (all days) (tab)

    Plate (all days) (tab)

.. figure:: _images/features_statistics/tabs/4_daily_plate.png
    :alt: Daily Plate (tab only)

    Daily Plate (tab only)

Its cards are:

1) `Correct License Plate Identifications by day <#daily-plate-correct-license-plate-identifications-by-day>`_
2) `License Plate Identification Accuracy by day <#daily-plate-licens-plate-identification-accuracy-by-day>`_


.. _Daily Plate Correct License Plate Identifications by day:

Correct License Plate Identifications by day
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This shows the count of correctly identified plate numbers for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/34_correct_license_plate_identifications_by_day.png
    :alt: Correct License Plate Identifications by day (all days) (card)

    Correct License Plate Identifications by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/32_correct_license_plate_identifications_by_day.png
    :alt: Correct License Plate Identifications by day (all days) (graph)

    Correct License Plate Identifications by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/34_correct_license_plate_identifications_by_day.png
    :alt: Correct License Plate Identifications by day (all days) (table)

    Correct License Plate Identifications by day (all days) (table)


.. _Daily Plate License Plate Identification Accuracy by day:

License Plate Identification Accuracy by day
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This shows the accuracy of the plate number identifications for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/35_license_plate_identification_accuracy_by_day.png
    :alt: License Plate Identification Accuracy by day (all days) (card)

    License Plate Identification Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/33_license_plate_identification_accuracy_by_day.png
    :alt: License Plate Identification Accuracy by day (all days) (graph)

    License Plate Identification Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/35_license_plate_identification_accuracy_by_day.png
    :alt: License Plate Identification Accuracy by day (all days) (table)

    License Plate Identification Accuracy by day (all days) (table)




.. _Daily Image Extractions:

Image Extractions
^^^^^^^^^^^^^^^^^

This shows the image extraction information for the days. It is broken into 3 sub tabs:

1) `Correct Extractions <#daily-image-extractions-complete-extractions>`_
2) `Complete Extractions <#daily-image-extractions-correct-extractions>`_
3) `Feed Ins <#daily-image-extractions-feed-ins>`_

.. figure:: _images/features_statistics/tabs/5_daily_image_extractions.png
    :alt: Daily Image Extractions sub tabs

    Daily Image Extractions sub tabs

.. _Daily Image Extractions Correct Extractions:

Correct Extractions
"""""""""""""""""""

This shows the correct image extraction data for the days.

.. figure:: _images/features_statistics/1_all/daily/full/10_image_extractions_correct_extractions.png
    :alt: Daily Correct Extractions (all days) (tab)

    Daily Correct Extractions (all days) (tab)

Its cards are:

1) `Correct Image Extractions by day`_
2) `Image Extraction Accuracy by day`_
3) `Total Correct Image Extractions by day`_
4) `Overall Image Extraction Accuracy by day`_


.. _Correct Image Extractions by day:

Correct Image Extractions by day
********************************

This shows the correct image extraction counts for the days broken down by image type (iai vehicle, pr vehicle and plate) and image 
angle.

.. figure:: _images/features_statistics/1_all/daily/cards/36_correct_image_extractions_by_day.png
    :alt: Correct Image Extractions by day (all days) (card)

    Correct Image Extractions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/34_correct_image_extractions_by_day.png
    :alt: Correct Image Extractions by day (all days) (graph)

    Correct Image Extractions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/36_correct_image_extractions_by_day.png
    :alt: Correct Image Extractions by day (all days) (table)

    Correct Image Extractions by day (all days) (table)


.. _Image Extraction Accuracy by day:

Image Extraction Accuracy by day
********************************

This shows the image extraction accuracy for the days broken down by image type (iai vehicle, pr vehicle and plate) and image 
angle.

.. figure:: _images/features_statistics/1_all/daily/cards/37_image_extraction_accuracy_by_day.png
    :alt: Image Extraction Accuracy by day (all days) (card)

    Image Extraction Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/35_image_extraction_accuracy_by_day.png
    :alt: Image Extraction Accuracy by day (all days) (graph)

    Image Extraction Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/37_image_extraction_accuracy_by_day.png
    :alt: Image Extraction Accuracy by day (all days) (table)

    Image Extraction Accuracy by day (all days) (table)


.. _Total Correct Image Extractions by day:

Total Correct Image Extractions by day
**************************************

This shows the correct image extraction counts for the days broken down by image type (iai vehicle, pr vehicle and plate).

.. figure:: _images/features_statistics/1_all/daily/cards/38_total_correct_image_extractions_by_day.png
    :alt: Total Correct Image Extractions by day (all days) (card)

    Total Correct Image Extractions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/36_total_correct_image_extractions_by_day.png
    :alt: Total Correct Image Extractions by day (all days) (graph)

    Total Correct Image Extractions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/38_total_correct_image_extractions_by_day.png
    :alt: Total Correct Image Extractions by day (all days) (table)

    Total Correct Image Extractions by day (all days) (table)


.. _Overall Image Extraction Accuracy by day:

Overall Image Extraction Accuracy by day
****************************************

This shows the image extraction accuracy for the days broken down by image type (iai vehicle, pr vehicle and plate).

.. figure:: _images/features_statistics/1_all/daily/cards/39_overall_image_extraction_accuracy_by_day.png
    :alt: Overall Image Extraction Accuracy by day (all days) (card)

    Overall Image Extraction Accuracy by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/37_overall_image_extraction_accuracy_by_day.png
    :alt: Overall Image Extraction Accuracy by day (all days) (graph)

    Overall Image Extraction Accuracy by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/39_overall_image_extraction_accuracy_by_day.png
    :alt: Overall Image Extraction Accuracy by day (all days) (table)

    Overall Image Extraction Accuracy by day (all days) (table)




.. _Daily Image Extractions Complete Extractions:

Complete Extractions
""""""""""""""""""""

This shows the complete object extraction data for the days.

.. figure:: _images/features_statistics/1_all/daily/full/11_image_extractions_complete_extractions.png
    :alt: Daily Complete Extractions (all days) (tab)

    Daily Complete Extractions (all days) (tab)

Its cards are:

1) `Complete Object Extractions by day`_
2) `Complete Object Extraction Percentage by day`_
3) `Total Complete Object Extractions by day`_
4) `Overall Complete Object Extraction Percentage by day`_


.. _Complete Image Extractions by day:

Complete Object Extractions by day
**********************************

This shows the complete object extraction counts for the days broken down by image type (iai vehicle, pr vehicle and plate) and image 
angle. (How often the entire object was extracted)

.. figure:: _images/features_statistics/1_all/daily/cards/40_complete_object_extractions_by_day.png
    :alt: Complete Object Extractions by day (all days) (card)

    Complete Object Extractions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/38_complete_object_extractions_by_day.png
    :alt: Complete Object Extractions by day (all days) (graph)

    Complete Object Extractions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/40_complete_object_extractions_by_day.png
    :alt: Complete Object Extractions by day (all days) (table)

    Complete Object Extractions by day (all days) (table)


.. _Complete Object Extraction Percentage by day:

Complete Object Extraction Percentage by day
********************************************

This shows the percentage of time that the entire object was extracted for the days broken down by image type (iai vehicle, pr vehicle and plate) and image 
angle.

.. figure:: _images/features_statistics/1_all/daily/cards/41_complete_object_extraction_percentage_by_day.png
    :alt: Complete Object Extraction Percentage by day (all days) (card)

    Complete Object Extraction Percentage by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/39_complete_object_extraction_percentage_by_day.png
    :alt: Complete Object Extraction Percentage by day (all days) (graph)

    Complete Object Extraction Percentage by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/41_complete_object_extraction_percentage_by_day.png
    :alt: Complete Object Extraction Percentage by day (all days) (table)

    Complete Object Extraction Percentage by day (all days) (table)


.. _Total Complete Object Extractions by day:

Total Complete Object Extractions by day
****************************************

This shows the complete object extraction counts for the days broken down by image type.

.. figure:: _images/features_statistics/1_all/daily/cards/42_total_complete_object_extractions_by_day.png
    :alt: Total Complete Object Extractions by day (all days) (card)

    Total Complete Object Extractions by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/40_total_complete_object_extractions_by_day.png
    :alt: Total Complete Object Extractions by day (all days) (graph)

    Total Complete Object Extractions by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/42_total_complete_object_extractions_by_day.png
    :alt: Total Complete Object Extractions by day (all days) (table)

    Total Complete Object Extractions by day (all days) (table)


.. _Overall Complete Object Extraction Percentage by day:

Overall Complete Object Extraction Percentage by day
****************************************************

This shows the percentage of time that the entire object was extracted for the days broken down by image type.

.. figure:: _images/features_statistics/1_all/daily/cards/43_overall_complete_object_extraction_percentage_by_day.png
    :alt: Overall Complete Object Extraction Percentage by day (all days) (card)

    Overall Complete Object Extraction Percentage by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/41_overall_complete_object_extraction_percentage_by_day.png
    :alt: Overall Complete Object Extraction Percentage by day (all days) (graph)

    Overall Complete Object Extraction Percentage by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/43_overall_complete_object_extraction_percentage_by_day.png
    :alt: Overall Complete Object Extraction Percentage by day (all days) (table)

    Overall Complete Object Extraction Percentage by day (all days) (table)



.. _Daily Image Extractions Feed Ins:

Feed Ins
""""""""

This shows the feed in data for the days.

.. figure:: _images/features_statistics/1_all/daily/full/12_image_extractions_feed_ins.png
    :alt: Daily Feed Ins (all days) (tab)

    Daily Feed Ins (all days) (tab)

Its cards are:

1) `Successful Feed Ins by day`_
2) `Percentage Successful Feed Ins by day`_
3) `Total Successful Feed Ins by day`_
4) `Overall Percentage Successful Feed Ins by day`_
5) `Failed Feed Ins by day`_
6) `% Failed Feed Ins by day <#percentage-failed-feed-ins-by-day>`_
7) `Total Failed Feed Ins by day`_
8) `Overall % Failed Feed Ins by day <#overall-percentage-failed-feed-ins-by-day>`_


.. _Successful Feed Ins by day:

Successful Feed Ins by day
**************************

This shows the successful feed in counts for each day broken down by image angle.

.. figure:: _images/features_statistics/1_all/daily/cards/44_successful_feed_ins_by_day.png
    :alt: Successful Feed Ins by day (all days) (card)

    Successful Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/42_successful_feed_ins_by_day.png
    :alt: Successful Feed Ins by day (all days) (graph)

    Successful Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/44_successful_feed_ins_by_day.png
    :alt: Successful Feed Ins by day (all days) (table)

    Successful Feed Ins by day (all days) (table)


.. _Percentage Successful Feed Ins by day:

Percentage Successful Feed Ins by day
*************************************

This shows the percentage of successful feed ins for each day broken down by image angle.

.. figure:: _images/features_statistics/1_all/daily/cards/45_percentage_successful_feed_ins_by_day.png
    :alt: Percentage Successful Feed Ins by day (all days) (card)

    Percentage Successful Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/43_percentage_successful_feed_ins_by_day.png
    :alt: Percentage Successful Feed Ins by day (all days) (graph)

    Percentage Successful Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/45_percentage_successful_feed_ins_by_day.png
    :alt: Percentage Successful Feed Ins by day (all days) (table)

    Percentage Successful Feed Ins by day (all days) (table)


.. _Total Successful Feed Ins by day:

Total Successful Feed Ins by day
********************************

This shows the successful feed in counts for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/46_total_successful_feed_ins_by_day.png
    :alt: Total Successful Feed Ins by day (all days) (card)

    Total Successful Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/44_total_successful_feed_ins_by_day.png
    :alt: Total Successful Feed Ins by day (all days) (graph)

    Total Successful Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/46_total_successful_feed_ins_by_day.png
    :alt: Total Successful Feed Ins by day (all days) (table)

    Total Successful Feed Ins by day (all days) (table)


.. _Overall Percentage Successful Feed Ins by day:

Overall Percentage Successful Feed Ins by day
*********************************************

This shows the percentage of successful feed ins for each day.

.. figure:: _images/features_statistics/1_all/daily/cards/47_overall_percentage_successful_feed_ins_by_day.png
    :alt: Overall Percentage Successful Feed Ins by day (all days) (card)

    Overall Percentage Successful Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/45_overall_percentage_successful_feed_ins_by_day.png
    :alt: Overall Percentage Successful Feed Ins by day (all days) (graph)

    Overall Percentage Successful Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/47_overall_percentage_successful_feed_ins_by_day.png
    :alt: Overall Percentage Successful Feed Ins by day (all days) (table)

    Overall Percentage Successful Feed Ins by day (all days) (table)


.. _Failed Feed Ins by day:

Failed Feed Ins by day
**********************

This shows the failed feed in counts for each day broken down by reason for failure (unextracted IAI image or unextracted PR image) and  image angle.

.. figure:: _images/features_statistics/1_all/daily/cards/48_failed_feed_ins_by_day.png
    :alt: Failed Feed Ins by day (all days) (card)

    Failed Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/46_failed_feed_ins_by_day.png
    :alt: Failed Feed Ins by day (all days) (graph)

    Failed Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/48_failed_feed_ins_by_day.png
    :alt: Failed Feed Ins by day (all days) (table)

    Failed Feed Ins by day (all days) (table)


.. _Percentage Failed Feed Ins by day:

% Failed Feed Ins by day
************************

This shows the percentage of failed feed ins for each day broken down by reason for failure and image angle (percentage of failures attributed to each reason).

.. figure:: _images/features_statistics/1_all/daily/cards/49_%_failed_feed_ins_by_day.png
    :alt: % Failed Feed Ins by day (all days) (card)

    % Failed Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/47_%_failed_feed_ins_by_day.png
    :alt: % Failed Feed Ins by day (all days) (graph)

    % Failed Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/49_%_failed_feed_ins_by_day.png
    :alt: % Failed Feed Ins by day (all days) (table)

    % Failed Feed Ins by day (all days) (table)


.. _Total Failed Feed Ins by day:

Total Failed Feed Ins by day
****************************

This shows the failed feed in counts for each day broken down by reason for failure.

.. figure:: _images/features_statistics/1_all/daily/cards/50_total_failed_feed_ins_by_day.png
    :alt: Total Failed Feed Ins by day (all days) (card)

    Total Failed Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/48_total_failed_feed_ins_by_day.png
    :alt: Total Failed Feed Ins by day (all days) (graph)

    Total Failed Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/50_total_failed_feed_ins_by_day.png
    :alt: Total Failed Feed Ins by day (all days) (table)

    Total Failed Feed Ins by day (all days) (table)


.. _Overall Percentage Failed Feed Ins by day:

Overall % Failed Feed Ins by day
********************************

This shows the percentage of failed feed ins for each day broken down by reason for failure (percentage of failures attributed to each reason).

.. figure:: _images/features_statistics/1_all/daily/cards/51_overall_%_failed_feed_ins_by_day.png
    :alt: Overall % Failed Feed Ins by day (all days) (card)

    Overall % Failed Feed Ins by day (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/49_overall_%_failed_feed_ins_by_day.png
    :alt: Overall % Failed Feed Ins by day (all days) (graph)

    Overall % Failed Feed Ins by day (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/51_overall_%_failed_feed_ins_by_day.png
    :alt: Overall % Failed Feed Ins by day (all days) (table)

    Overall % Failed Feed Ins by day (all days) (table)




.. _Daily Correct Counts:

Correct Counts
^^^^^^^^^^^^^^

This shows the correct counts for each day for:

1) Duration
2) Iai Vehicle Detection
3) Iai Vehicle Prediction
4) Iai Dominant Colour Detection (if the dominant colour was detected but identified as non-dominant, this and the total detections are also shown)
5) Pr Vehicle Detection
6) Pr Vehicle Prediction
7) Pr Dominant Colour Detection  (if the dominant colour was detected but identified as non-dominant, this and the total detections are also shown)
8) License Plate Identification
9) Image Iai Extract (the number of unextracted images and incomplete object extractions are also shown)
10) Image Pr Extract (the number of unextracted images and incomplete object extractions are also shown)
11) Image Plate Extract (the number of unextracted images and incomplete plate extractions are also shown)

.. figure:: _images/features_statistics/1_all/daily/full/13_correct_counts.png
    :alt: Daily Correct Counts (all days) (tab)

    Daily Correct Counts (all days) (tab)

.. figure:: _images/features_statistics/tabs/6_daily_correct_counts.png
    :alt: Daily Correct Counts (tab only)

    Daily Correct Counts (tab only)

.. figure:: _images/features_statistics/1_all/daily/cards/52_day_correct_counts.png
    :alt: Daily Correct Counts (all days) (card)

    Daily Correct Counts (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/50_day_correct_counts.png
    :alt: Daily Correct Counts (all days) (graph)

    Daily Correct Counts (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/52_day_correct_counts.png
    :alt: Daily Correct Counts (all days) (table)

    Daily Correct Counts (all days) (table)
    



.. _Daily Accuracies:

Accuracies
^^^^^^^^^^

This shows the accuracies for each day for:

1) Duration
2) Iai Vehicle Detection
3) Iai Vehicle Prediction
4) Iai Dominant Colour Detection (if the dominant colour was detected but identified as non-dominant, this, number of times it was identified as dominant 
   and the total detections are also shown)
5) Pr Vehicle Detection
6) Pr Vehicle Prediction
7) Pr Dominant Colour Detection  (if the dominant colour was detected but identified as non-dominant, this, number of times it was identified as dominant 
   and the total detections are also shown)
8) License Plate Identification
9) Image Iai Extract (the number of unextracted images, incorrect extractions and incomplete object extractions are also shown)
10) Image Pr Extract (the number of unextracted images, incorrect extractions and incomplete object extractions are also shown)
11) Image Plate Extract (the number of unextracted images, incorrect extractions and incomplete plate extractions are also shown)

.. figure:: _images/features_statistics/1_all/daily/full/14_accuracies.png
    :alt: Daily Accuracies (all days) (tab)

    Daily Accuracies (all days) (tab)

.. figure:: _images/features_statistics/tabs/7_daily_accuracies.png
    :alt: Daily Accuracies (tab only)

    Daily Accuracies (tab only)

.. figure:: _images/features_statistics/1_all/daily/cards/53_day_accuracies.png
    :alt: Daily Accuracies (all days) (card)

    Daily Accuracies (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/graphs/51_day_accuracies.png
    :alt: Daily Accuracies (all days) (graph)

    Daily Accuracies (all days) (graph)

.. figure:: _images/features_statistics/1_all/daily/tables/53_day_accuracies.png
    :alt: Daily Accuracies (all days) (table)

    DailyAccuracies (all days) (table)




.. _Daily Timings:

Timings
^^^^^^^

This shows the timing information for the days. It is broken into 11 sub tabs:

1) `Get Duration Time <#daily-timings-get-duration-time>`_
2) `Imageai Detection Time <#daily-timings-imageai-detection-time>`_
3) `Imageai Prediction Time <#daily-timings-imageai-prediction-time>`_
4) `Imageai Colour Detection Time <#daily-timings-imageai-colour-detection-time>`_
5) `Total Imageai Analysis Time <#daily-timings-total-imageai-analysis-time>`_
6) `Pr Detection Time <#daily-timings-pr-detection-time>`_
7) `Pr Prediction Time <#daily-timings-pr-prediction-time>`_
8) `Pr Colour Detection Time <#daily-timings-pr-colour-detection-time>`_
9) `Total Pr Analysis Time <#daily-timings-total-pr-analysis-time>`_
10) `Total Image Analysis Time <#daily-timings-total-image-analysis-time>`_
11) `Entry Total Time <#daily-timings-entry-total-time>`_

.. figure:: _images/features_statistics/tabs/8_daily_timings.png
    :alt: Daily Timings (tab only)

    Daily Timings (tab only)

Each section shows the times for the days' entries, along with the min, max, total and average times.

.. _Daily Timings Get Duration Time:

Get Duration Time
"""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/15_timings_get_duration_time.png
    :alt: Get Duration Time (all days) (tab)

    Get Duration Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/54_entry_get_duration_time_by_day.png
    :alt: Get Duration Time (all days) (card)

    Get Duration Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/54_entry_get_duration_time_by_day.png
    :alt: Get Duration Time (all days) (table)

    Get Duration Time (all days) (table)



.. _Daily Timings Imageai Detection Time:

Imageai Detection Time
""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/16_timings_imageai_detection_time.png
    :alt: Imageai Detection Time (all days) (tab)

    Imageai Detection Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/55_entry_imageai_detection_time_by_day.png
    :alt: Imageai Detection Time (all days) (card)

    Imageai Detection Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/55_entry_imageai_detection_time_by_day.png
    :alt: Imageai Detection Time (all days) (table)

    Imageai Detection Time (all days) (table)



.. _Daily Timings Imageai Prediction Time:

Imageai Prediction Time
"""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/17_timings_imageai_prediction_time.png
    :alt: Imageai Prediction Time (all days) (tab)

    Imageai Prediction Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/56_entry_imageai_prediction_time_by_day.png
    :alt: Imageai Prediction Time (all days) (card)

    Imageai Prediction Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/56_entry_imageai_prediction_time_by_day.png
    :alt: Imageai Prediction Time (all days) (table)

    Imageai Prediction Time (all days) (table)



.. _Daily Timings Imageai Colour Detection Time:

Imageai Colour Detection Time
"""""""""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/18_timings_imageai_colour_detection_time.png
    :alt: Imageai Colour Detection Time (all days) (tab)

    Imageai Colour Detection Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/57_entry_imageai_colour_detection_time_by_day.png
    :alt: Imageai Colour Detection Time (all days) (card)

    Imageai Colour Detection Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/57_entry_imageai_colour_detection_time_by_day.png
    :alt: Imageai Colour Detection Time (all days) (table)

    Imageai Colour Detection Time (all days) (table)



.. _Daily Timings Total Imageai Analysis Time:

Total Imageai Analysis Time
"""""""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/19_timings_total_imageai_analysis_time.png
    :alt: Total Imageai Analysis Time (all days) (tab)

    Total Imageai Analysis Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/58_entry_total_imageai_analysis_time_by_day.png
    :alt: Total Imageai Analysis Time (all days) (card)

    Total Imageai Analysis Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/58_entry_total_imageai_analysis_time_by_day.png
    :alt: Total Imageai Analysis Time (all days) (table)

    Total Imageai Analysis Time (all days) (table)



.. _Daily Timings Pr Detection Time:

Pr Detection Time
"""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/20_timings_pr_detection_time.png
    :alt: Pr Detection Time (all days) (tab)

    Pr Detection Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/59_entry_pr_detection_time_by_day.png
    :alt: Pr Detection Time (all days) (card)

    Pr Detection Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/59_entry_pr_detection_time_by_day.png
    :alt: Pr Detection Time (all days) (table)

    Pr Detection Time (all days) (table)
    


.. _Daily Timings Pr Prediction Time:

Pr Prediction Time
""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/21_timings_pr_prediction_time.png
    :alt: Pr Prediction Time (all days) (tab)

    Pr Prediction Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/60_entry_pr_prediction_time_by_day.png
    :alt: Pr Prediction Time (all days) (card)

    Pr Prediction Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/60_entry_pr_prediction_time_by_day.png
    :alt: Pr Prediction Time (all days) (table)

    Pr Prediction Time (all days) (table)



.. _Daily Timings Pr Colour Detection Time:

Pr Colour Detection Time
""""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/22_timings_pr_colour_detection_time.png
    :alt: Pr Prediction Time (all days) (tab)

    Pr Prediction Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/61_entry_pr_colour_detection_time_by_day.png
    :alt: Pr Colour Detection Time (all days) (card)

    Pr Colour Detection Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/61_entry_pr_colour_detection_time_by_day.png
    :alt: Pr Colour Detection Time (all days) (table)

    Pr Colour Detection Time (all days) (table)



.. _Daily Timings Total Pr Analysis Time:

Total Pr Analysis Time
""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/23_timings_total_pr_analysis_time.png
    :alt: Total Pr Analysis Time (all days) (tab)

    Total Pr Analysis Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/62_entry_total_pr_analysis_time_by_day.png
    :alt: Total Pr Analysis Time (all days) (card)

    Total Pr Analysis Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/62_entry_total_pr_analysis_time_by_day.png
    :alt: Total Pr Analysis Time (all days) (table)

    Total Pr Analysis Time (all days) (table)



.. _Daily Timings Total Image Analysis Time:

Total Image Analysis Time
"""""""""""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/24_timings_total_image_analysis_time.png
    :alt: Total Image Analysis Time (all days) (tab)

    Total Image Analysis Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/63_entry_total_image_analysis_time_by_day.png
    :alt: Total Image Analysis Time (all days) (card)

    Total Image Analysis Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/63_entry_total_image_analysis_time_by_day.png
    :alt: Total Image Analysis Time (all days) (table)

    Total Image Analysis Time (all days) (table)



.. _Daily Timings Entry Total Time:

Entry Total Time
""""""""""""""""

.. figure:: _images/features_statistics/1_all/daily/full/25_timings_entry_total_time.png
    :alt: Entry Total Time (all days) (tab)

    Entry Total Time (all days) (tab)

.. figure:: _images/features_statistics/1_all/daily/cards/64_entry_total_time_by_day.png
    :alt: Entry Total Time (all days) (card)

    Entry Total Time (all days) (card)

.. figure:: _images/features_statistics/1_all/daily/tables/64_entry_total_time_by_day.png
    :alt: Entry Total Time (all days) (table)

    Entry Total Time (all days) (table)




Overall
~~~~~~~
This shows the combined statistics for all the days. It has five (5) sub tabs:

1) `Correct Counts <#overall-correct-counts>`_
2) `Accuracies <#overall-accuracies>`_
3) `Dom. Colour Identification <#overall-dom.-colour-identification>`_
4) `Feed Ins <#overall-feed-ins>`_
5) `Timings <#overall-timings>`_

.. figure:: _images/features_statistics/tabs/9_overall.png
    :alt: Overall sub tabs

    Overall sub tabs


.. _Overall Correct Counts:

Correct Counts
^^^^^^^^^^^^^^

This shows the correct counts for:

1) Duration
2) Iai Vehicle Detection
3) Iai Vehicle Prediction
4) Iai Dominant Colour Detection (if the dominant colour was detected but identified as non-dominant, this and the total detections are also shown)
5) Pr Vehicle Detection
6) Pr Vehicle Prediction
7) Pr Dominant Colour Detection  (if the dominant colour was detected but identified as non-dominant, this and the total detections are also shown)
8) License Plate Identification
9) Image Iai Extract (the number of unextracted images, incomplete object extractions and attempted feeds are also shown)
10) Image Pr Extract (the number of unextracted images, incomplete object extractions, successful feed ins and a breakdown of the failed feed ins are also shown)
11) Image Plate Extract (the number of unextracted images and incomplete plate extractions are also shown)

.. figure:: _images/features_statistics/1_all/overall/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (tab)

    Overall Correct Counts (all days) (tab)

.. figure:: _images/features_statistics/2_orig/overall/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (tab)

    Overall Correct Counts (not fed in) (tab)

.. figure:: _images/features_statistics/3_fed_in/overall/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (tab)

    Overall Correct Counts (fed in) (tab)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (tab) (without unrotated)

    Overall Correct Counts (all days) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (tab) (without unrotated)

    Overall Correct Counts (not fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/full/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (tab) (without unrotated)

    Overall Correct Counts (fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (card)

    Overall Correct Counts (all days) (card)

.. figure:: _images/features_statistics/2_orig/overall/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (card)

    Overall Correct Counts (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/overall/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (card)

    Overall Correct Counts (fed in) (card)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (card) (without unrotated)

    Overall Correct Counts (all days) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (card) (without unrotated)

    Overall Correct Counts (not fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/cards/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (card) (without unrotated)

    Overall Correct Counts (fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (graph)

    Overall Correct Counts (all days) (graph)

.. figure:: _images/features_statistics/2_orig/overall/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (graph)

    Overall Correct Counts (not fed in) (graph)

.. figure:: _images/features_statistics/3_fed_in/overall/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (graph)

    Overall Correct Counts (fed in) (graph)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (graph) (without unrotated)

    Overall Correct Counts (all days) (graph) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (graph) (without unrotated)

    Overall Correct Counts (not fed in) (graph) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/graphs/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (graph) (without unrotated)

    Overall Correct Counts (fed in) (graph) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (table)

    Overall Correct Counts (all days) (table)

.. figure:: _images/features_statistics/2_orig/overall/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (table)

    Overall Correct Counts (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/overall/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (table)

    Overall Correct Counts (fed in) (table)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (all days) (table) (without unrotated)

    Overall Correct Counts (all days) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (not fed in) (table) (without unrotated)

    Overall Correct Counts (not fed in) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/tables/1_overall_correct_counts.png
    :alt: Overall Correct Counts (fed in) (table) (without unrotated)

    Overall Correct Counts (fed in) (table) (without unrotated)



.. _Overall Accuracies:

Accuracies
^^^^^^^^^^

This shows the min (along with the day(s) it occurred on), max (along with the day(s) it occurred on) and overall (average) accuracies for:

1) Duration
2) Iai Vehicle Detection
3) Iai Vehicle Prediction
4) Iai Dominant Colour Detection (if the dominant colour was detected but identified as non-dominant, this, number of times it was identified as dominant 
   and the total detections are also shown)
5) Pr Vehicle Detection
6) Pr Vehicle Prediction
7) Pr Dominant Colour Detection  (if the dominant colour was detected but identified as non-dominant, this, number of times it was identified as dominant 
   and the total detections are also shown)
8) License Plate Identification
9) Image Iai Extract (the number of unextracted images, incorrect extractions and incomplete object extractions are also shown)
10) Image Pr Extract (the number of unextracted images, incorrect extractions and incomplete object extractions are also shown)
11) Image Plate Extract (the number of unextracted images, incorrect extractions and incomplete plate extractions are also shown)

.. figure:: _images/features_statistics/1_all/overall/full/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (tab)

    Overall Accuracies (all days) (tab)

.. figure:: _images/features_statistics/2_orig/overall/full/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (tab)

    Overall Accuracies (not fed in) (tab)

.. figure:: _images/features_statistics/3_fed_in/overall/full/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (tab)

    Overall Accuracies (fed in) (tab)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/full/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (tab) (without unrotated)

    Overall Accuracies (all days) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/full/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (tab) (without unrotated)

    Overall Accuracies (not fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/full/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (tab) (without unrotated)

    Overall Accuracies (fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (card)

    Overall Accuracies (all days) (card)

.. figure:: _images/features_statistics/2_orig/overall/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (card)

    Overall Accuracies (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/overall/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (card)

    Overall Accuracies (fed in) (card)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (card) (without unrotated)

    Overall Accuracies (all days) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (card) (without unrotated)

    Overall Accuracies (not fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/cards/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (card) (without unrotated)

    Overall Accuracies (fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (graph)

    Overall Accuracies (all days) (graph)

.. figure:: _images/features_statistics/2_orig/overall/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (graph)

    Overall Accuracies (not fed in) (graph)

.. figure:: _images/features_statistics/3_fed_in/overall/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (graph)

    Overall Accuracies (fed in) (graph)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (graph) (without unrotated)

    Overall Accuracies (all days) (graph) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (graph) (without unrotated)

    Overall Accuracies (not fed in) (graph) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/graphs/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (graph) (without unrotated)

    Overall Accuracies (fed in) (graph) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (table)

    Overall Accuracies (all days) (table)

.. figure:: _images/features_statistics/2_orig/overall/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (table)

    Overall Accuracies (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/overall/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (table)

    Overall Accuracies (fed in) (table)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (all days) (table) (without unrotated)

    Overall Accuracies (all days) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (not fed in) (table) (without unrotated)

    Overall Accuracies (not fed in) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/tables/2_overall_accuracies.png
    :alt: Overall Accuracies (fed in) (table) (without unrotated)

    Overall Accuracies (fed in) (table) (without unrotated)



.. _Overall Dom. Colour Identification:

Dom. Colour Identification
^^^^^^^^^^^^^^^^^^^^^^^^^^

This shows how often the dominant colour was correctly detected, but was not identified as the dominant colour.

.. figure:: _images/features_statistics/1_all/overall/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (tab)

    Overall Dom. Colour Identification (all days) (tab)

.. figure:: _images/features_statistics/2_orig/overall/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (tab)

    Overall Dom. Colour Identification (not fed in) (tab)

.. figure:: _images/features_statistics/3_fed_in/overall/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (tab)

    Overall Dom. Colour Identification (fed in) (tab)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (tab) (without unrotated)

    Overall Dom. Colour Identification (all days) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (tab) (without unrotated)

    Overall Dom. Colour Identification (not fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/full/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (tab) (without unrotated)

    Overall Dom. Colour Identification (fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (card)

    Overall Dom. Colour Identification (all days) (card)

.. figure:: _images/features_statistics/2_orig/overall/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (card)

    Overall Dom. Colour Identification (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/overall/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (card)

    Overall Dom. Colour Identification (fed in) (card)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (card) (without unrotated)

    Overall Dom. Colour Identification (all days) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (card) (without unrotated)

    Overall Dom. Colour Identification (not fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/cards/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (card) (without unrotated)

    Overall Dom. Colour Identification (fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (table)

    Overall Dom. Colour Identification (all days) (table)

.. figure:: _images/features_statistics/2_orig/overall/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (table)

    Overall Dom. Colour Identification (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/overall/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (table)

    Overall Dom. Colour Identification (fed in) (table)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (all days) (table) (without unrotated)

    Overall Dom. Colour Identification (all days) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (not fed in) (table) (without unrotated)

    Overall Dom. Colour Identification (not fed in) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/tables/3_overall_dom_colour_identification.png
    :alt: Overall Dom. Colour Identification (fed in) (table) (without unrotated)

    Overall Dom. Colour Identification (fed in) (table) (without unrotated)



.. _Overall Feed Ins:

Feed Ins
^^^^^^^^

This shows the overall feed in data including:

1) # tried feed ins

2) # successful feed ins

3) % successful feed ins

4) # failed feed ins which is broken into:

    a) # failures due to unextracted IAI
    b) # failures due to unextracted PR
    c) tot. # failures

5) % failed feed ins which is broken into:

    a) % failures due to unextracted IAI
    b) % failures due to unextracted PR
    c) tot. % failures

.. figure:: _images/features_statistics/1_all/overall/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (tab)

    Overall Feed Ins (all days) (tab)

.. figure:: _images/features_statistics/2_orig/overall/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (tab)

    Overall Feed Ins (not fed in) (tab)

.. figure:: _images/features_statistics/3_fed_in/overall/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (tab)

    Overall Feed Ins (fed in) (tab)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (tab) (without unrotated)

    Overall Feed Ins (all days) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (tab) (without unrotated)

    Overall Feed Ins (not fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/full/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (tab) (without unrotated)

    Overall Feed Ins (fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (card)

    Overall Feed Ins (all days) (card)

.. figure:: _images/features_statistics/2_orig/overall/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (card)

    Overall Feed Ins (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/overall/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (card)

    Overall Feed Ins (fed in) (card)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (card) (without unrotated)

    Overall Feed Ins (all days) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (card) (without unrotated)

    Overall Feed Ins (not fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/cards/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (card) (without unrotated)

    Overall Feed Ins (fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (table)

    Overall Feed Ins (all days) (table)

.. figure:: _images/features_statistics/2_orig/overall/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (table)

    Overall Feed Ins (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/overall/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (table)

    Overall Feed Ins (fed in) (table)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (all days) (table) (without unrotated)

    Overall Feed Ins (all days) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (not fed in) (table) (without unrotated)

    Overall Feed Ins (not fed in) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/tables/4_overall_feed_ins.png
    :alt: Overall Feed Ins (fed in) (table) (without unrotated)

    Overall Feed Ins (fed in) (table) (without unrotated)



.. _Overall Timings:

Timings
^^^^^^^

This shows the min (along with the day(s) it occurred on), max (along with the day(s) it occurred on), total, average per day and 
average per entry timing for:

1) Get Duration Time 
2) Imageai Detection Time 
3) Imageai Prediction Time 
4) Imageai Colour Detection Time
5) Total Imageai Analysis Time
6) Pr Detection Time 
7) Pr Prediction Time 
8) Pr Colour Detection Time
9) Total Pr Analysis Time
10) Total Image Analysis Time
11) Entry Total Time 

.. figure:: _images/features_statistics/1_all/overall/full/5_overall_timings.png
    :alt: Overall Timings (all days) (tab)

    Overall Timings (all days) (tab)

.. figure:: _images/features_statistics/2_orig/overall/full/5_overall_timings.png
    :alt: Overall Timings (not fed in) (tab)

    Overall Timings (not fed in) (tab)

.. figure:: _images/features_statistics/3_fed_in/overall/full/5_overall_timings.png
    :alt: Overall Timings (fed in) (tab)

    Overall Timings (fed in) (tab)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/full/5_overall_timings.png
    :alt: Overall Timings (all days) (tab) (without unrotated)

    Overall Timings (all days) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/full/5_overall_timings.png
    :alt: Overall Timings (not fed in) (tab) (without unrotated)

    Overall Timings (not fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/full/5_overall_timings.png
    :alt: Overall Timings (fed in) (tab) (without unrotated)

    Overall Timings (fed in) (tab) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/cards/5_overall_timings.png
    :alt: Overall Timings (all days) (card)

    Overall Timings (all days) (card)

.. figure:: _images/features_statistics/2_orig/overall/cards/5_overall_timings.png
    :alt: Overall Timings (not fed in) (card)

    Overall Timings (not fed in) (card)

.. figure:: _images/features_statistics/3_fed_in/overall/cards/5_overall_timings.png
    :alt: Overall Timings (fed in) (card)

    Overall Timings (fed in) (card)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/cards/5_overall_timings.png
    :alt: Overall Timings (all days) (card) (without unrotated)

    Overall Timings (all days) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/cards/5_overall_timings.png
    :alt: Overall Timings (not fed in) (card) (without unrotated)

    Overall Timings (not fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/cards/5_overall_timings.png
    :alt: Overall Timings (fed in) (card) (without unrotated)

    Overall Timings (fed in) (card) (without unrotated)

.. figure:: _images/features_statistics/1_all/overall/tables/5_overall_timings.png
    :alt: Overall Timings (all days) (table)

    Overall Timings (all days) (table)

.. figure:: _images/features_statistics/2_orig/overall/tables/5_overall_timings.png
    :alt: Overall Timings (not fed in) (table)

    Overall Timings (not fed in) (table)

.. figure:: _images/features_statistics/3_fed_in/overall/tables/5_overall_timings.png
    :alt: Overall Timings (fed in) (table)

    Overall Timings (fed in) (table)

.. figure:: _images/features_statistics/4_without_unrotated/1_all/tables/5_overall_timings.png
    :alt: Overall Timings (all days) (table) (without unrotated)

    Overall Timings (all days) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/2_orig/tables/5_overall_timings.png
    :alt: Overall Timings (not fed in) (table) (without unrotated)

    Overall Timings (not fed in) (table) (without unrotated)

.. figure:: _images/features_statistics/4_without_unrotated/3_fed_in/tables/5_overall_timings.png
    :alt: Overall Timings (fed in) (table) (without unrotated)

    Overall Timings (fed in) (table) (without unrotated)