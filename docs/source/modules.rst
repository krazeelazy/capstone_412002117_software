capstone_412002117_software project
===================================

.. toctree::
   :maxdepth: 4

   analysis
   analysis_statistics
   automated
   capstone_412002117_software
   manage
   manual
   psa
   spot_analysis
