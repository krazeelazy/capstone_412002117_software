automated package
=================

automated.apps module
---------------------

.. automodule:: automated.apps
   :members:
   :undoc-members:
   :show-inheritance:

automated.models module
-----------------------

.. automodule:: automated.models
   :members:
   :undoc-members:
   :show-inheritance:
   
automated.views module
----------------------

.. automodule:: automated.views
   :members:
   :undoc-members:
   :show-inheritance:
