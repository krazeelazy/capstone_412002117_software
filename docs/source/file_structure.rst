File Structure
==============


Project Structure
~~~~~~~~~~~~~~~~~~~

.. figure:: _images/file_structure/1a_all1.png
    :alt: Directory Structure

    Directory Structure

.. figure:: _images/file_structure/1b_all2.png
    :alt: Directory Structure (more detail)

    Directory Structure (more detail)

The project is broken into:

1) `Analysis Directory`_
2) `Analysis Statistics Directory`_
3) `Automated Directory`_
4) `Capstone 412002117 Software Directory`_
5) `Data Directory`_
6) `Docs Directory`_
7) `Helpers Directory`_
8) `JSON for PSA models  Directory`_
9) `Manual Directory`_
10) `Media Directory`_
11) `PSA Directory`_
12) `Other Files <#project-other-files>`_


Analysis Directory
~~~~~~~~~~~~~~~~~~

Has the files for the analysis app.

.. figure:: _images/file_structure/2_analysis.png
    :alt: Analysis Directory Structure

    Analysis Directory Structure

Its sections are:

1) `migrations  <#analysis-migrations>`_
2) `templates/analysis`_
3) `other files  <#analysis-other-files>`_


.. _Analysis migrations:

migrations
^^^^^^^^^^

Has the migrations that are used to make changes to the models (adding a field, deleting a model, etc.) in the database schema. 

templates/analysis
^^^^^^^^^^^^^^^^^^

This contains the templates for the analysis pages that contain the static parts of the desired HTML output as well as some special syntax describing how 
dynamic content is inserted.

**day_detail.html**: This is the template for the /analysis/day/<int:pk>/ route (`Analysis Day Summary <features_presentation.html#analysis-day-summary>`_).


**entry_detail.html**: This is the template for the /analysis/entry/<int:pk>/ route (`Analysis Entry Details <features_presentation.html#analysis-entry-details>`_).


**home.html**: This is the template for the /analysis/ route (`Analysis Home <features_presentation.html#analysis-home>`_).



.. _Analysis other files:

other files
^^^^^^^^^^^

**admin.py**: This is where models are registered so they can be accessed from the admin section of the site.

**apps.py**: This has the configuration for the analysis app (sets the name and imports the signals from the psa app).

**models.py**:

Has the models for the analysis app:

1) Day
2) Entry
3) DayTiming

**urls.py**: Has the urls for routing within the analysis app.

**views.py**: Has classes that have functions to gather all the data for the templates.



Analysis Statistics Directory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Has the files for the analysis_statistics app.

.. figure:: _images/file_structure/3_analysis_statistics.png
    :alt: Analysis Statistics Directory Structure

    Analysis Statistics Directory Structure

.. note:: This app has no models so there are no migrations.

The sections are:

1) `templates/analysis_statistics`_
2) `other files  <#analysis_statistics-other-files>`_



templates/analysis_statistics
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

This contains the template for the analysis_statistics page.

**home.html**: This is the template for the /analysis_statistics/ route (`Statistics Home <Statistics Features>`_).


.. _analysis_statistics other files:

other files
^^^^^^^^^^^

**apps.py**: This has the configuration for the analysis_statistics app (sets the name).

**models.py**: Has all the queries and functions to get the data the view needs to pass to the template.

**urls.py**: Has the url for routing within the analysis_statistics app.

**views.py**: Has a class that has functions to gather all the data for the template.



Automated Directory
~~~~~~~~~~~~~~~~~~~

Has the files for the automated app.

.. figure:: _images/file_structure/4_automated.png
    :alt: Automated Directory Structure

    Automated Directory Structure

Its sections are:

1) `migrations  <#automated-migrations>`_
2) `templates/automated`_
3) `other files  <#automated-other-files>`_


.. _Automated migrations:

migrations
^^^^^^^^^^

Has the migrations that are used to make changes to the models in the database schema. 

templates/automated
^^^^^^^^^^^^^^^^^^^

This contains the templates for the automated pages.

**day_detail.html**: This is the template for the /analysis/automated/day/<int:pk>/ route (`Automated Day Summary <features_presentation.html#automated-day-summary>`_).


**entry_detail.html**: This is the template for the /analysis/automated/entry/<int:pk>/ route (`Automated Entry Details <features_presentation.html#automated-entry-details>`_).


**home.html**: This is the template for the /analysis/automated/ route (`Automated Home <features_presentation.html#automated-home>`_).


.. _Automated other files:

other files
^^^^^^^^^^^

**admin.py**: This is where models are registered so they can be accessed from the admin section of the site.

**apps.py**: This has the configuration for the automated app (sets the name and imports the signals from the psa app).

**models.py**:

Has the models for the automated app:

1) AutomatedAnalysis
2) Image
3) IaiVehicleDetection
4) IaiVehiclePrediction
5) IaiColourDetection
6) PrVehicleDetection
7) PrVehiclePrediction
8) PrColourDetection
9) LicensePlate
10) Timing

**urls.py**: Has the urls for routing within the automated app.

**views.py**: Has classes that have functions to gather all the data for the templates.



Capstone 412002117 Software Directory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Has the main files for the project.

.. figure:: _images/file_structure/5_capstone_412002117_software.png
    :alt: Capstone 412002117 Software Directory Structure

    Capstone 412002117 Software Directory Structure

**asgi.py**: It exposes the ASGI callable as a module-level variable named ``application`` (auto generated when project was created).

**settings.py**: Has the settings for the project.

**urls.py**: Has the URL configuration for the project.

**wsgi.py**: It exposes the WSGI callable as a module-level variable named ``application`` (auto generated when project was created).



Data Directory
~~~~~~~~~~~~~~

Has the files used during analysis.

.. figure:: _images/file_structure/6_data.png
    :alt: Data Directory Structure

    Data Directory Structure

Its sections are:

1) `analyzed <#data-analyzed>`_
2) `new <#data-new>`_

.. _data analyzed:

analyzed
^^^^^^^^

This is where analyzed data is stored until is added to the database. 
It holds the output of spot_analysis.py.

.. _data new:

new
^^^

This is where new input data is put so it can be analyzed by spot_analysis.py.




Docs Directory
~~~~~~~~~~~~~~

This is where the documentation for the project is stored.

.. figure:: _images/file_structure/7_docs.png
    :alt: Docs Directory Structure

    Docs Directory Structure


Helpers Directory
~~~~~~~~~~~~~~~~~

Has the helper files for spot_analysis.py.

.. figure:: _images/file_structure/8_helpers.png
    :alt: Helpers Directory Structure

    Helpers Directory Structure

Its sections are:

1) `detection models`_
2) `prediction models`_
3) `colour_info.csv`_


detection models
^^^^^^^^^^^^^^^^

Has the resnet50_coco_best_v2.0.1.h5 file which has the detection model used during the Imageai analysis.

prediction models
^^^^^^^^^^^^^^^^^

Has the DenseNet-BC-121-32.h5 file which has the prediction model used during predictions.

colour_info.csv
^^^^^^^^^^^^^^^

Is a comma separated file containingthe names, hex, red, green and blue values for 139 colours.




JSON for PSA models  Directory
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Has the files used to populate the PSA tables when starting with a blank database.

.. figure:: _images/file_structure/9_json_for_psa_models.png
    :alt: JSON for PSA models Directory Structure

    JSON for PSA models Directory Structure


**colours.json**: has the names, hex, red, green and blue values for the colours (including an 'unknown' colour incase the colour of the object wasn't detected)

**image_angles.json**: the names of the possible angles of the object in the image

**image_types.json**: the names of the different image types (same as the names of the images subdirectories)

**imageai_detection_classes.json**: the names of the possible Imageai detection classes of the objects (including an 'unknown' class incase the object's Imageai detection class wasn't identified)

**pr_detection_classes.json**: the names of the possible pr detection classes of the objects

**prediction_classes.json**: the names of the possible prediction classes of the objects (including an 'unknown' class incase the object's prediction class wasn't identified) 



Manual Directory
~~~~~~~~~~~~~~~~

Has the files for the manual app:

.. figure:: _images/file_structure/10_manual.png
    :alt: Manual Directory Structure

    Manual Directory Structure

Its sections are:

1) `migrations  <#manual-migrations>`_
2) `templates/manual`_
3) `other files  <#manual-other-files>`_



.. _Manual migrations:

migrations
^^^^^^^^^^

Has the migrations that are used to make changes to the models in the database schema. 

templates/manual
^^^^^^^^^^^^^^^^

This contains the templates for the manual pages. It's broken into:

1) `manualanalysis_form_partials subdirectory`_
2) `other templates <#manual-other-templates>`_


manualanalysis_form_partials subdirectory
"""""""""""""""""""""""""""""""""""""""""

This contains:

**creation_script.html**: has a script that formats the manual analysis form when it's being used to create a new manual analysis.

**update_script.html**: has a script that formats the manual analysis form when it's being used to update an existing manual analysis.


.. _manual other templates:

other templates
"""""""""""""""

**day_detail.html**: This is the template for the /analysis/manual/day/<int:pk>/ route (`Manual Day Summary <features_presentation.html#manual-day-summary>`_).


**entry_detail.html**: This is the template for the /analysis/manual/entry/<int:pk>/ route (`Manual Entry Details <features_presentation.html#manual-entry-details>`_).


**home.html**: This is the template for the /analysis/manual/ route (`Manual Home <features_presentation.html#manual-home>`_).

**manualanalysis_form.html**: This is the template for the /analysis/manual/new/<int:pk>/ (`Manual Creation <features_presentation.html#manual-creation>`_) and /analysis/manual/entry/<int:pk>/update/ 
(`Manual Update <features_presentation.html#manual-update>`_) routes.



.. _Manual other files:

other files
^^^^^^^^^^^

**admin.py**: This is where the model is registered so it can be accessed from the admin section of the site.

**apps.py**: This has the configuration for the manual app (sets the name).

**models.py**: Has the model for the manual app (ManualAnalysis) and a class that has a function to get the data the view needs to pass to the form in the manualanalysis_form.html template.

**urls.py**: Has the urls for routing within the manual app.

**views.py**: Has classes that have functions to gather all the data for the templates and to handle incoming data from the creation and update forms.



Media Directory
~~~~~~~~~~~~~~~

Stores the media for the web application.

.. figure:: _images/file_structure/11_media.png
    :alt: Media Directory Structure

    Media Directory Structure

Its sections are:

1) `files`_
2) `images`_
3) `default.jpg`_


files
^^^^^

Has the text files. It's broken into:

1) `json`_
2) `log`_

json
""""

Has the JSON files for all the days in the database.

log
"""

Has the log files for all the days in the database.



images
^^^^^^

Has all the images for the entries. Is broken into:

1) `analyzed <#images-analyzed>`_
2) `extracted <#images-extracted>`_
3) `original <#images-original>`_

.. _images analyzed:

analyzed
""""""""

This has the analyzed images for the entries. It's broken into:

1) `comparison <#images-analyzed-comparison>`_
2) `imageai <#images-analyzed-imageai>`_
3) `pr <#images-analyzed-pr>`_


.. _images analyzed comparison:

comparison
**********

Contains all analyzed comparison images for the entries.

.. _images analyzed imageai:

imageai
*******

Contains all analyzed Imageai images for the entries.

.. _images analyzed pr:

pr
**

Contains all analyzed pr images for the entries.

.. _images extracted:

extracted
"""""""""

This has the extracted images for the entries. It's broken into:

1) `Imageai <#images-extracted-Imageai>`_
2) `plate <#images-extracted-plate>`_
3) `pr <#images-extracted-pr>`_


.. _images extracted Imageai:

Imageai
*******

Contains all extracted Imageai images for the entries.

.. _images extracted plate:

plate
*****

Contains all extracted plate images for the entries.

.. _images extracted pr:

pr
**

Contains all extracted pr images for the entries.

original
""""""""

Contains all the original images for the entries.


default.jpg
^^^^^^^^^^^

Is the default image shown if an entry doesn't have a particular image type.




PSA Directory
~~~~~~~~~~~~~

Has the files for the psa app, along with some files used by the other apps:

.. figure:: _images/file_structure/12_psa.png
    :alt: PSA Directory Structure

    PSA Directory Structure


Its sections are:

1) `migrations  <#psa-migrations>`_
2) `static/psa`_
3) `templates/psa`_
4) `templatetags`_
5) `other files  <#psa-other-files>`_



.. _psa migrations:

migrations
^^^^^^^^^^

Has the migrations that are used to make changes to the models in the database schema. 

static/psa
^^^^^^^^^^

This contains static files used in the web app:

**favicon.ico**: The website icon, shown next to the page's title in its tab in the browser.

**main.css**: A custom style sheet for the web app.


templates/psa
^^^^^^^^^^^^^

This contains the templates for the psa pages and some partials used by other apps. It's broken into:

1) `layouts`_
2) `partials`_
3) `other templates <#psa-other-templates>`_


layouts
"""""""

**base.html**: This template provides the layout for all pages.

partials
""""""""

These templates are used in several apps.

**actions_column.html**: handles the action column in all tables throughout the web app.

**day_timing_section.html**: handles the day timing section in the day_detail.html templates in the analysis, automated and manual apps.

**entry_basic.html**: handles the basic info in the entry_detail.html templates in the analysis, automated and manual apps.

**entry_timing_div.html**: handles the entry timing div in the entry_detail.html templates in the analysis, automated and manual apps.

**iai_auto_div.html**: handles the Imageai automated analysis div in the entry_detail.html templates in the  analysis and automated apps.

**iai_colour_div.html**: handles the Imageai colour div in the manualanalysis_form.html template in the manual app and the iai_auto_div.html template.

**iai_colour_script.html**: has a script that makes the chart for iai_colour_div.html.

**iai_manual_div.html**: handles the Imageai manual analysis div in the entry_detail.html templates in the  analysis and manual apps.

**image_section.html**: handles the image tabs used throughout the web app.

**plate_auto_div.html**: handles the plate automated analysis div in the entry_detail.html templates in the  analysis and automated apps.

**plate_manual_div.html**: handles the plate manual analysis div in the entry_detail.html templates in the  analysis and manual apps.

**pr_auto_div.html**: handles the Plate Recognizer automated analysis div in the entry_detail.html templates in the  analysis and automated apps.

**pr_colour_div.html**: handles the PR colour div in the manualanalysis_form.html template in the manual app and the pr_auto_div.html template.

**pr_colour_script.html**: has a script that makes the chart for pr_colour_div.html.

**pr_manual_div.html**: handles the PR manual analysis div in the entry_detail.html templates in the  analysis and manual apps.


.. _psa other templates:

other templates
"""""""""""""""

These templates are used only by the psa app.

**home.html**: This is the template for the / and /psa/ routes (`Home page <features_presentation.html#home-page>`_). The home page for the entire web app.

**populate_psa_tables.html**: This is the template for the  /populate_tables/ and /psa/populate_tables/ routes (`Populate PSA Tables <features_presentation.html#populate-psa-tables>`_).


templatetags
^^^^^^^^^^^^

**index.py**: has functions used in the templates to generate and display information dynamically.

.. _psa other files:

other files
^^^^^^^^^^^

**admin.py**: This is where the models are registered so they can be accessed from the admin section of the site.

**apps.py**: This has the configuration for the psa app (sets the name).

**filters.py**: Handles the display and processing of all filters throughout the web app.

**forms.py**: Handles custom forms in the web app.

**models.py**: Has:
1) The models for the psa app:

    a) Colour
    b) ImageType
    c) ImageAngle
    d) IaiDetectionClass
    e) PrDetectionClass
    f) PredictionClass

2) classes that have functions to get the data the views need to pass to the templates, and to process the input data:

    a) CheckDates
    b) CheckPsaTables
    c) MakeDay
    d) PopulatePsaTables

**signals.py**: Has functions that  helps allow decoupled applications get notified when actions occur elsewhere in the framework. 
Signals allow certain senders to notify a set of receivers that some action has taken place.

**tables.py**: Hsa functions that make most of the tables for the web app.

**urls.py**: Has the urls for routing within the psa app.

**views.py**: Has classes that have functions to gather all the data for the templates and to handle incoming data. Makes calls to the spot_analysis.py script 
to do the analyses.



.. _Project Other Files:

Other Files
~~~~~~~~~~~

.. figure:: _images/file_structure/13_others.png
    :alt: Other files

    Other files

**db.sqlite3**: The database file for the web app.

**manage.py**: Django's command-line utility for administrative tasks (auto generated when project was created).

**README.md**: A markdown version of the readme file.

**README.rst**: A reStructuredText version of the readme file.

**requirements.rst**: A reStructuredText version of the requirements file.

**requirements.txt**: A pip requirements file.

**spot_analysis.py**: The script that handles the automated analysis of the input data.


